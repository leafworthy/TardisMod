package net.tardis.mod.entity;

import java.util.List;

import javax.annotation.Nullable;

import com.mojang.math.Vector3d;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.Packet;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.entity.IEntityAdditionalSpawnData;
import net.minecraftforge.network.NetworkHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.Control;
import net.tardis.mod.control.ControlPositionData;
import net.tardis.mod.control.ControlPositionDataRegistry;
import net.tardis.mod.control.ControlRegistry;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.helpers.TRegistryHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ControlEntitySizeMessage;

public class ControlEntity extends Entity implements IEntityAdditionalSpawnData {

    private Control control;
    private Vec3 size;
    private ResourceLocation consoleLocation;

    private AABB controlAABB;

    public ControlEntity(Level level, ControlPositionData data, ConsoleTile tile) {
        super(EntityRegistry.CONTROL.get(), level);
        this.control = data.getControlType().create();
        this.size = data.getSize();
        this.consoleLocation = ForgeRegistries.BLOCK_ENTITY_TYPES.getKey(tile.getType());

    }

    public ControlEntity(EntityType<?> type, Level level){
        super(type, level);
    }

    @Nullable
    public ControlPositionData getData(ResourceLocation loc){
        List<ControlPositionData> datas = ControlPositionDataRegistry.POSITION_DATAS.get(loc);
        if(datas != null){
            for(ControlPositionData data : datas){
                if(data.getControlType() == this.control.getType()){
                    return data;
                }
            }
        }
        return null;
    }

    public void setControl(ControlType type, @Nullable ResourceLocation consoleLoc){
        this.control = type.create();
        if(consoleLoc != null){
            ControlPositionData data = this.getData(consoleLoc);
            if(data != null) {
                this.size = data.getSize();
                this.reapplyPosition(); //DO NOT delete, or else the entity's hitbox becomes stuck at 0,0
                if(!this.level.isClientSide)
                    Network.sendToTracking(this, new ControlEntitySizeMessage(this.getId(), this.size));
            }
        }

    }

    public void setSize(Vec3 size){
        this.size = size;
        this.reapplyPosition();
    }

    @Override
    protected void defineSynchedData() {}

    @Override
    protected void readAdditionalSaveData(CompoundTag tag) {
        ControlType type = ControlRegistry.REGISTRY.get().getValue(new ResourceLocation(tag.getString("control_type")));
        this.consoleLocation = tag.contains("console_key") ? new ResourceLocation(tag.getString("console_key")) : null;
        this.setControl(type, this.consoleLocation);
    }

    @Override
    protected void addAdditionalSaveData(CompoundTag tag) {
        tag.putString("control_type", TRegistryHelper.getKey(this.control.getType()).toString());
        if(this.consoleLocation != null)
            tag.putString("console_key", this.consoleLocation.toString());

    }

    public AABB makeAABBFromControlData(Vec3 size){
        return new AABB(0, 0, 0, size.x, size.y, size.z);
    }

    public AABB getControlAABB(){
        if(this.controlAABB == null){
            if(this.size != null)
                return this.controlAABB = makeAABBFromControlData(this.size);
            return this.getBoundingBox();

        }
        return this.controlAABB;
    }

    @Override
    protected AABB makeBoundingBox() {
        return this.getControlAABB() != null ? this.getControlAABB().move(this.position()) : super.makeBoundingBox();
    }

    @Override
    public Packet<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    public boolean isPickable() {
        return true;
    }

    @Override
    public boolean canBeCollidedWith() {
        return true;
    }

    @Override
    public InteractionResult interact(Player player, InteractionHand hand) {

        if(this.control != null){
            LazyOptional<ITardisLevel> tardisHolder = this.level.getCapability(Capabilities.TARDIS);

            if(tardisHolder.isPresent()){
                ITardisLevel tardis = tardisHolder.orElseThrow(NullPointerException::new);
                ControlData<?> data = tardis.getControlDataOrCreate(this.control.type);

                if(tardis.getCurrentFlightEvent() != null && tardis.getCurrentFlightEvent().onControlUse(data)){
                    data.playAnimation((int)tardis.getLevel().getGameTime());
                    return InteractionResult.sidedSuccess(player.getLevel().isClientSide);
                }

                InteractionResult result =  this.control.onUse(player, hand, tardis);
                if(result != InteractionResult.PASS)
                    data.playAnimation((int)tardis.getLevel().getGameTime());
                return result;
            }
        }

        return InteractionResult.PASS;
    }

    public void writeSpawnData(FriendlyByteBuf buf) {

        buf.writeRegistryId(ControlRegistry.REGISTRY.get(), this.control.getType());

        boolean hasSize = this.size != null;
        buf.writeBoolean(hasSize);

        if(hasSize){
            buf.writeDouble(this.size.x);
            buf.writeDouble(this.size.y);
            buf.writeDouble(this.size.z);
        }

    }

    @Override
    public void readSpawnData(FriendlyByteBuf buf) {

        this.control = buf.readRegistryIdSafe(ControlType.class).create();

        boolean hasSize = buf.readBoolean();

        if(hasSize){
            this.size = new Vec3(buf.readDouble(), buf.readDouble(), buf.readDouble());
            this.reapplyPosition();
        }
    }

    @Override
    public void setDeltaMovement(Vec3 pMotion) {}


}
