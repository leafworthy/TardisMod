package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public class ControlEntitySizeMessage {

    public final Vec3 size;
    public final int entityID;

    public ControlEntitySizeMessage(int entityID, Vec3 size){
        this.entityID = entityID;
        this.size = size;

    }

    public static void encode(ControlEntitySizeMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.entityID);
        buf.writeDouble(mes.size.x);
        buf.writeDouble(mes.size.y);
        buf.writeDouble(mes.size.z);
    }

    public static ControlEntitySizeMessage decode(FriendlyByteBuf buf){
        return new ControlEntitySizeMessage(buf.readInt(), new Vec3(buf.readDouble(), buf.readDouble(), buf.readDouble()));
    }

    public static void handle(ControlEntitySizeMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleControlSizeMessage(mes);
        });
        context.get().setPacketHandled(true);
    }

}
