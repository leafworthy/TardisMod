package net.tardis.mod.client.renderers;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.entity.ControlEntity;

public class RendererControl extends EntityRenderer<ControlEntity> {

    public RendererControl(EntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public ResourceLocation getTextureLocation(ControlEntity controlEntity) {
        return null;
    }
}
