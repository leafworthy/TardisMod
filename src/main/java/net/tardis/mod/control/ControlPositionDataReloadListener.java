package net.tardis.mod.control;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;
import net.tardis.mod.Tardis;

public class ControlPositionDataReloadListener extends SimplePreparableReloadListener<Map<ResourceLocation, List<ControlPositionData>>>{
    
	protected static final String folderName = Tardis.MODID + "/controls";
	public static final ControlPositionDataReloadListener INSTANCE = new ControlPositionDataReloadListener();
	
	@Override
	protected Map<ResourceLocation, List<ControlPositionData>> prepare(ResourceManager pResourceManager,
			ProfilerFiller pProfiler) {
		ControlPositionDataReloadListener.load(pResourceManager);
		Map<ResourceLocation, List<ControlPositionData>> map = ControlPositionDataRegistry.POSITION_DATAS;
		return map;
	}

	@Override
	protected void apply(Map<ResourceLocation, List<ControlPositionData>> pObject, ResourceManager pResourceManager,
			ProfilerFiller pProfiler) {
	    
	}
	
	public static void load(ResourceManager manager){
        ControlPositionDataRegistry.POSITION_DATAS.clear();
        Map<ResourceLocation, Resource> map = manager.listResources(folderName + "/", file -> file.getPath().endsWith(".json"));

        for(Map.Entry<ResourceLocation, Resource> entry : map.entrySet()){
            System.out.println("Loading console control layout file %s".formatted(entry.getKey()));

            try{
                JsonObject root = JsonParser.parseReader(new InputStreamReader(entry.getValue().open())).getAsJsonObject();

                ResourceLocation consoleLocation = new ResourceLocation(root.get("console").getAsString());
                System.out.println("Loading control data for console: " + consoleLocation.toString());
                List<ControlPositionData> consoleControlPositionList = new ArrayList<>();
                for(Map.Entry<String, JsonElement> data : root.entrySet()){

                    if(data.getValue().isJsonObject()){
                        ControlType type = ControlRegistry.REGISTRY.get().getValue(new ResourceLocation(data.getKey()));
                        if(type != null){
                            ControlPositionData positionData = new ControlPositionData(type);
                            positionData.deserialize(data.getValue());
                            consoleControlPositionList.add(positionData);

                        }
                    }

                }
                ControlPositionDataRegistry.POSITION_DATAS.put(consoleLocation, consoleControlPositionList);
                System.out.println("Finished loading control data for console: " + consoleLocation.toString());
            }
            catch(IOException e){}

        }
    }

}
