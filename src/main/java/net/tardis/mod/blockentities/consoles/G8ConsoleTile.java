package net.tardis.mod.blockentities.consoles;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.TileRegistry;

public class G8ConsoleTile extends ConsoleTile{


    public G8ConsoleTile(BlockPos pos, BlockState state) {
        super(TileRegistry.G8_CONSOLE.get(), pos, state);
    }
}
