package net.tardis.mod.world;

import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.helpers.Helper;

public class TardisARSPiece {

    public static int LOCKED_PIECE_CHUNK_SIZE = 3;

    // Resource location defined in constructor.
    private ResourceLocation resourceLocation;

    /**
     * 3x3 chunk structure used to place infinite corridor generation.
     * @param resourceLocation ResourceLocation of the structure file
     */
    public TardisARSPiece(ResourceLocation resourceLocation) {
        this.resourceLocation = resourceLocation;
    }

    /**
     * Fetch the resource location of the piece
     * @return resource location of the piece
     */
    public ResourceLocation getResourceLocation() {return resourceLocation;}

}
