package net.tardis.mod.datagen;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.mojang.serialization.JsonOps;
import net.minecraft.core.BlockPos;
import net.minecraft.core.RegistryAccess;
import net.minecraft.data.DataGenerator;
import net.minecraft.resources.RegistryOps;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.common.data.JsonCodecProvider;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import net.tardis.mod.JsonRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSRoom;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.sound.SoundSchemes;
import org.checkerframework.checker.units.qual.K;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGen {

    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    @SubscribeEvent
    public static void registerDataGen(GatherDataEvent event){
        event.getGenerator().addProvider(true, new TardisLangProvider(event.getGenerator()));
        event.getGenerator().addProvider(true, new TardisAdvancementProvider(event.getGenerator()));
        event.getGenerator().addProvider(true, new TardisAnimationProvider(event.getGenerator()));
        event.getGenerator().addProvider(true, new TardisBlockStateProvider(event.getGenerator(), event.getExistingFileHelper()));
        event.getGenerator().addProvider(true, new DraftingTableRecipeProvider(event.getGenerator()));

        registerDataPackRegistry(event.getGenerator(), event.getExistingFileHelper(), JsonRegistries.SOUND_SCHEMES_REGISTRY.get(), map -> {
            map.put(Helper.createRL("default"), new SoundSchemes(SoundEvents.EXPERIENCE_BOTTLE_THROW, 200, SoundEvents.BEE_LOOP, 40, SoundEvents.ANVIL_LAND, 200));
        });

        registerDataPackRegistry(event.getGenerator(), event.getExistingFileHelper(), JsonRegistries.ARS_ROOM_REGISTRY.get(), map -> {
            map.put(Helper.createRL("default_console"), new ARSRoom(Helper.createRL("console_room/console_room_dev_m"), BlockPos.ZERO));
        });
    }

    public static <V> void registerDataPackRegistry(DataGenerator generator, ExistingFileHelper helper, IForgeRegistry<V> registry, MapBuilder<ResourceLocation, V> mapBuilder){

        HashMap<ResourceLocation, V> map = new HashMap<>();
        mapBuilder.fill(map);

        generator.addProvider(true,
                JsonCodecProvider.forDatapackRegistry(generator, helper, Tardis.MODID, RegistryOps.create(JsonOps.INSTANCE, RegistryAccess.builtinCopy()), registry.getRegistryKey(), map));
    }

    public interface MapBuilder<K, V>{
        void fill(HashMap<K, V> map);
    }

}
