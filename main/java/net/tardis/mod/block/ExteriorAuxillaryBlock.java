package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class ExteriorAuxillaryBlock extends Block {

    public ExteriorAuxillaryBlock() {
        super(Properties.of(Material.AIR));
    }

    @Override
    public boolean hasDynamicShape() {
        return true;
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {

        BlockState other = pLevel.getBlockState(pPos.below());
        if(other.getBlock() instanceof ExteriorBlock ext){
            return ext.getShape(other, pLevel, pPos.below(), pContext).move(0, -1, 0);
        }

        return super.getShape(pState, pLevel, pPos, pContext);
    }
}
