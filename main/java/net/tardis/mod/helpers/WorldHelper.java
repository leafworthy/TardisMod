package net.tardis.mod.helpers;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.Vec3;

public class WorldHelper {

    public static Direction getHorizontalFacing(BlockState state){
        if(state.hasProperty(BlockStateProperties.HORIZONTAL_FACING))
            return state.getValue(BlockStateProperties.HORIZONTAL_FACING);

        if(state.hasProperty(BlockStateProperties.FACING)){
            Direction dir = state.getValue(BlockStateProperties.FACING);
            return dir.getAxis().isHorizontal() ? dir : Direction.NORTH;
        }

        return Direction.NORTH;
    }

    public static Vec3 centerOfBlockPos(BlockPos pos){
        return centerOfBlockPos(pos, true);
    }

    public static Vec3 centerOfBlockPos(BlockPos pos, boolean centerY){
        return new Vec3(pos.getX() + 0.5, pos.getY() + (centerY ? 0.5 : 0), pos.getZ() + 0.5);
    }

    public static BlockPos vecToBlockPos(Vec3 pos) {
        return new BlockPos((int)Math.floor(pos.x), (int)Math.floor(pos.y), (int)Math.floor(pos.z));
    }

    public static float getDegreeFromRotation(Direction dir) {
        switch(dir){
            default: return 0;
            case EAST: return 90;
            case SOUTH: return 180;
            case WEST: return 270;
        }
    }
}
