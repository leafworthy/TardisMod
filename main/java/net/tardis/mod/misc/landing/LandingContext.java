package net.tardis.mod.misc.landing;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;

public abstract class LandingContext {

    boolean canLandInWater = false;
    boolean canLandInAir = false;

    public LandingContext(boolean landInWater, boolean landInAir){
        this.canLandInWater = landInWater;
        this.canLandInAir = landInAir;
    }

    public LandingContext(){
        this(false, false);
    }

    public boolean isBlockEmpty(Level level, BlockPos pos){
        BlockState state = level.getBlockState(pos);
        return state.isAir();
    }

    public boolean canLandonTopOf(Level level, BlockPos pos){
        //Land in water if it can
        BlockState state = level.getBlockState(pos);
        if(state.getBlock().getFluidState(state) == Fluids.WATER.defaultFluidState() && this.canLandInWater)
            return true;
        if(state.isAir())
            return this.canLandInAir;
        return state.isCollisionShapeFullBlock(level, pos);
    }

    public abstract boolean canLandAt(Level level, BlockPos pos);
}
