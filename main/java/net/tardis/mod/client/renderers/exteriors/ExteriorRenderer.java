package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;
import net.minecraft.client.model.Model;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.ExteriorTile;
import net.tardis.mod.client.models.exteriors.IExteriorModel;
import net.tardis.mod.client.models.exteriors.SteamExteriorModel;
import net.tardis.mod.helpers.Helper;

public class ExteriorRenderer<T extends ExteriorTile, M extends Model & IExteriorModel<T>> implements BlockEntityRenderer<T> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/exteriors/steam.png");
    public final M model;

    public ExteriorRenderer(BlockEntityRendererProvider.Context context){
        this.model = (M)new SteamExteriorModel(context.bakeLayer(SteamExteriorModel.LAYER_LOCATION));
    }

    @Override
    public void render(T pBlockEntity, float pPartialTick, PoseStack pose, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {
        pose.translate(0.5, 1.5, 0.5);
        pose.mulPose(Vector3f.ZP.rotationDegrees(180));
        this.model.animateSolid(pBlockEntity, pPartialTick);
        this.model.animateDoor(pBlockEntity);
        this.model.renderToBuffer(pose, pBufferSource.getBuffer(this.model.renderType(TEXTURE)), pPackedLight, pPackedOverlay, 1.0F, 1.0F, 1.0F, 1.0F);
    }

    @Override
    public int getViewDistance() {
        return 512;
    }
}
