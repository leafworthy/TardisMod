package net.tardis.mod.block.monitor;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.BaseMonitorTile;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Properties;
import java.util.function.Supplier;

public class BaseMonitorBlock<T extends BaseMonitorTile> extends BaseEntityBlock {

    public final Supplier<BlockEntityType<T>> type;

    public BaseMonitorBlock(Properties pProperties, Supplier<BlockEntityType<T>> type) {
        super(pProperties);
        Objects.requireNonNull(type);
        this.type = type;
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) {
        return this.type.get().create(pPos, pState);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return pLevel.isClientSide ? new MonitorTicker() : null;
    }

    public class MonitorTicker<E extends BlockEntity> implements BlockEntityTicker<E>{

        @Override
        public void tick(Level pLevel, BlockPos pPos, BlockState pState, E pBlockEntity) {
            ((BaseMonitorTile)pBlockEntity).clientTick();
        }
    }

}
