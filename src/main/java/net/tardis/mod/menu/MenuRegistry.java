package net.tardis.mod.menu;

import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraftforge.network.IContainerFactory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;

public class MenuRegistry {

    public static final DeferredRegister<MenuType<?>> MENUS = DeferredRegister.create(ForgeRegistries.MENU_TYPES, Tardis.MODID);

    public static final RegistryObject<MenuType<DraftingTableMenu>> DRAFTING_TABLE = MENUS.register("drafting_table", () -> create(DraftingTableMenu::new));


    public static <T extends AbstractContainerMenu> MenuType<T> create(IContainerFactory<T> factory){
        return new MenuType<T>(factory);
    }

}
