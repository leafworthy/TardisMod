package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.cap.Capabilities;
import org.jetbrains.annotations.Nullable;

public class InteriorDoorBlock extends BaseEntityBlock {


    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    public InteriorDoorBlock() {
        super(Properties.of(Material.WOOD)
                .strength(3.0F, 4.0F)
                .sound(SoundType.WOOD)
                .noOcclusion());
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return TileRegistry.INTERIOR_DOOR.get().create(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level p_153212_, BlockState p_153213_, BlockEntityType<T> type) {
        return createTickerHelper(type, TileRegistry.INTERIOR_DOOR.get(), InteriorDoorTile::tick);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return super.getStateForPlacement(context)
                .setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(FACING);
    }

    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pIsMoving) {
        if(pLevel.getBlockEntity(pPos) instanceof InteriorDoorTile door){
            pLevel.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.getInteriorManager().removeDoor(door.getDoorID());
            });
        }
        super.onRemove(pState, pLevel, pPos, pNewState, pIsMoving);
    }


}
