package net.tardis.mod.misc;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;

import java.util.Random;

public class SpaceTimeCoord implements INBTSerializable<CompoundTag> {

    public static final SpaceTimeCoord ZERO = new SpaceTimeCoord(Level.OVERWORLD, BlockPos.ZERO);

    private BlockPos pos;
    private ResourceKey<Level> dim;
    private Direction direction;

    public SpaceTimeCoord(ResourceKey<Level> level, BlockPos pos, Direction direction){
        this.dim = level;
        this.pos = pos.immutable();
        this.direction = direction;
    }
    
    public SpaceTimeCoord(ResourceKey<Level> level, BlockPos pos){
        this(level, pos, Direction.NORTH);
    }
    private SpaceTimeCoord(){

    }

    public SpaceTimeCoord(Level level, BlockPos pos){
        this(level.dimension(), pos, Direction.NORTH);
    }

    public BlockPos getPos(){
        return this.pos;
    }

    public ResourceKey<Level> getLevel(){
        return this.dim;
    }
    
    public Direction getDirection() {
        return this.direction;
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putString("level", this.dim.location().toString());
        tag.putLong("pos", this.pos.asLong());
        tag.putInt("direction", this.direction.ordinal());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
    	this.dim = ResourceKey.create(Registry.DIMENSION_REGISTRY, new ResourceLocation(nbt.getString("level")));
    	this.pos = BlockPos.of(nbt.getLong("pos"));
    	this.direction = Direction.values()[nbt.getInt("direction")];
    }

    public void encode(FriendlyByteBuf buf){
        buf.writeResourceKey(this.dim);
        buf.writeBlockPos(this.pos);

        boolean hasDir = this.direction != null;
        if(hasDir)
            buf.writeEnum(this.direction);
    }

    public static SpaceTimeCoord decode(FriendlyByteBuf buf){
        ResourceKey dim = buf.readResourceKey(Registry.DIMENSION_REGISTRY);
        BlockPos pos = buf.readBlockPos();
        if(buf.readBoolean()){
            return new SpaceTimeCoord(dim, pos, buf.readEnum(Direction.class));
        }
        return new SpaceTimeCoord(dim, pos);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof SpaceTimeCoord))
            return false;
        SpaceTimeCoord other = (SpaceTimeCoord) obj;
        if (pos.equals(other.pos) && this.dim.location().equals(other.dim.location()) && this.direction.equals(other.direction))
            return true;
        return false;
    }

    public static SpaceTimeCoord of(CompoundTag tag){
        SpaceTimeCoord coord = new SpaceTimeCoord();
        coord.deserializeNBT(tag);
        return coord;
    }

    public SpaceTimeCoord randomize(RandomSource rand, int range) {
        BlockPos pos = this.getPos().offset(-range + rand.nextInt(range * 2), -range + rand.nextInt(range * 2), -range + rand.nextInt(range * 2));
        return new SpaceTimeCoord(this.getLevel(), pos);
    }

    @Override
    public String toString() {
        return this.pos.toString() + " " + this.dim.toString();
    }
}
