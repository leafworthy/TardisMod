package net.tardis.mod.exterior;

import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.enums.DoorState;

import java.util.function.BiFunction;
public class ExteriorType {

    private final BiFunction<ExteriorType, ITardisLevel, Exterior> factory;
    private final DoorState[] validDoorStates;

    public ExteriorType(BiFunction<ExteriorType, ITardisLevel, Exterior> factory, DoorState[] doorStates){
        this.factory = factory;
        this.validDoorStates = doorStates;
    }

    public ExteriorType(BiFunction<ExteriorType, ITardisLevel, Exterior> factory){
        this(factory, DoorState.values());
    }

    public Exterior create(ITardisLevel level){
        return this.factory.apply(this, level);
    }

    public DoorState[] getDoorStates(){
        return this.validDoorStates;
    }

}
