package net.tardis.mod.control.datas;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;

public class ControlDataBlockPos extends ControlData<BlockPos>{

    public ControlDataBlockPos(ControlType type, ITardisLevel tardis) {
        super(type, tardis, BlockPos.ZERO);
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeBlockPos(this.get());
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.set(buf.readBlockPos());
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putLong("value", this.get().asLong());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.set(BlockPos.of(tag.getLong("value")));
    }
}
