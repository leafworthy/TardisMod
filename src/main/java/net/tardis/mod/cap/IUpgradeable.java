package net.tardis.mod.cap;

import net.tardis.mod.upgrade.Upgrade;
import net.tardis.mod.upgrade.UpgradeType;

import java.util.List;

public interface IUpgradeable<T> {

    List<Upgrade<T>> getUpgrades();
    Upgrade getUpgrade(UpgradeType type);
}
