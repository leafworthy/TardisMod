package net.tardis.mod.menu;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.blockentities.crafting.DraftingTableTile;
import net.tardis.mod.helpers.GuiHelper;

public class DraftingTableMenu extends AbstractContainerMenu {

    public DraftingTableTile table;

    public DraftingTableMenu(int id, Inventory inventory, DraftingTableTile table){
        super(MenuRegistry.DRAFTING_TABLE.get(), id);
        this.table = table;
        this.setupSlots(inventory);

    }

    public DraftingTableMenu(int id, Inventory inv, FriendlyByteBuf buf){
        this(id, inv, getTile(inv, buf.readBlockPos()));
    }

    public static DraftingTableTile getTile(Inventory inv, BlockPos pos){
        if(inv.player.level.getBlockEntity(pos) instanceof DraftingTableTile tile){
            return tile;
        }
        return null;
    }

    public void setupSlots(Inventory inv){

        final int startX = 30;
        final int startY = 17;

        for(int i = 0; i < 9; ++i){
            this.addSlot(new SlotItemHandler(this.table.inventory, i,
                    startX + (i % 3) * 18,
                    startY + (i / 3) * 18));
        }

        this.addSlot(new SlotItemHandler(this.table.inventory, 9, startX + 94, startY + 18));

        for(Slot s : GuiHelper.getPlayerSlots(inv, 8, 84)){
            this.addSlot(s);
        }
    }

    @Override
    public ItemStack quickMoveStack(Player pPlayer, int pIndex) {
        return ItemStack.EMPTY;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
