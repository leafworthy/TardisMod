package net.tardis.mod.dimension;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.dimension.LevelStem;
import net.tardis.mod.helpers.Helper;

public class DimensionTypes {

   public static ResourceKey<DimensionType> TARDIS_TYPE;
   //public static final Holder<DimensionType> TARDIS_TYPE_HOLDER = BuiltinRegistries.DIMENSION_TYPE.getOrCreateHolderOrThrow(TARDIS_TYPE);


    public static void register(){
        TARDIS_TYPE = ResourceKey.create(Registry.DIMENSION_TYPE_REGISTRY, Helper.createRL("tardis"));
    }
}
