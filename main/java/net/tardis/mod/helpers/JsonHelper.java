package net.tardis.mod.helpers;

import com.google.gson.JsonSyntaxException;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.ForgeRegistries;

public class JsonHelper {

    public static Item getItemFromString(String key){
        ResourceLocation loc = ResourceLocation.tryParse(key);
        Item item = ForgeRegistries.ITEMS.getValue(loc);
        if(item == null)
            throw new JsonSyntaxException("No item registered for %s!".formatted(key));
        return item;
    }

}
