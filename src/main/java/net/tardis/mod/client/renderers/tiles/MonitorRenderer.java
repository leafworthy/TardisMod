package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.network.chat.Component;
import net.tardis.mod.blockentities.BaseMonitorTile;
import net.tardis.mod.client.renderers.WorldText;
import net.tardis.mod.helpers.WorldHelper;

public class MonitorRenderer implements BlockEntityRenderer<BaseMonitorTile> {

    public final WorldText text;

    public MonitorRenderer(BlockEntityRendererProvider.Context context){
        this.text = new WorldText(1.0F, 1.0F);
    }

    @Override
    public void render(BaseMonitorTile tile, float pPartialTick, PoseStack pose, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {
        pose.pushPose();
        pose.translate(0.5, 0.5, 0.5);
        pose.mulPose(Vector3f.YP.rotationDegrees(WorldHelper.getHorizontalFacing(tile.getBlockState()).toYRot()));
        pose.mulPose(Vector3f.ZN.rotationDegrees(180));
        pose.translate(-0.5, 0, -0.5);
        this.text.renderText(tile.monitorText, pose, pBufferSource);
        /*
        final float scale = 0.01f;
        pose.scale(scale, scale, scale);
        for(int i = 0; i < tile.monitorText.size(); ++i){
            Minecraft.getInstance().font.drawInBatch(tile.monitorText.get(i), 0, (i * (Minecraft.getInstance().font.lineHeight + 2)), 0xFFFFFF, false, pose.last().pose(), pBufferSource, false, 0xFFFFF, LightTexture.FULL_BRIGHT);
        }
         */
        pose.popPose();
    }
}
