package net.tardis.mod.client.models;

import net.minecraft.client.animation.AnimationDefinition;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.level.block.entity.BlockEntity;

public interface IAnimatableTile<T extends BlockEntity> {

    void setupAnimations(T tile, float ageInTicks);

}
