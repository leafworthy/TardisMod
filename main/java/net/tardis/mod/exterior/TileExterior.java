package net.tardis.mod.exterior;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.BlockRegistry;
import net.tardis.mod.blockentities.ExteriorTile;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.TeleportEntry;
import net.tardis.mod.misc.enums.DoorState;

public class TileExterior extends Exterior{

    final BlockState exteriorState;
    final AABB size;

    public TileExterior(ExteriorType type, ITardisLevel level, BlockState state) {
        super(type, level);
        this.exteriorState = state;
        this.size = new AABB(0, 0, 0, 1, 2, 1);
    }

    @Override
    public void demat(Level level) {
        level.getChunkAt(tardis.getLocation().getPos());
        level.setBlock(tardis.getLocation().getPos(), Blocks.AIR.defaultBlockState(), 3);
    }

    @Override
    public void remat(Level level) {
        this.setPosition(level, tardis.getDestination().getPos());
    }

    @Override
    public void setPosition(Level level, BlockPos pos) {
        level.setBlock(pos, this.exteriorState, 3);
        level.setBlock(pos.above(), BlockRegistry.EXTERIOR_COLLISION.get().defaultBlockState(), 3);
        if(level.getBlockEntity(pos) instanceof ExteriorTile exterior){
            exterior.setInterior(this.tardis.getLevel().dimension(), this.type);
        }
    }

    @Override
    public void fly(Vec3 motion) {

    }

    @Override
    public void placeEntityAt(ServerLevel exteriorLevel, Entity e) {
        BlockPos pos = this.tardis.getLocation().getPos();
        if(exteriorLevel.getBlockEntity(pos) instanceof ExteriorTile exterior){
            Direction dir = WorldHelper.getHorizontalFacing(exterior.getBlockState());
            TeleportEntry.TELEPORTS.computeIfAbsent(e.getUUID(), id -> new TeleportEntry(e, exteriorLevel, WorldHelper.centerOfBlockPos(exterior.getBlockPos().relative(dir)), WorldHelper.getDegreeFromRotation(dir)));
            return;
        }
        e.setPos(WorldHelper.centerOfBlockPos(pos));
    }

    @Override
    public AABB getSize() {
        return this.size;
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {

    }
}
