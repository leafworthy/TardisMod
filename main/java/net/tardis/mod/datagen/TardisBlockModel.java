package net.tardis.mod.datagen;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.BlockModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.tardis.mod.Tardis;

public class TardisBlockModel extends BlockModelProvider {

    public TardisBlockModel(DataGenerator generator, ExistingFileHelper existingFileHelper) {
        super(generator, Tardis.MODID, existingFileHelper);
    }

    @Override
    protected void registerModels() {

    }
}
