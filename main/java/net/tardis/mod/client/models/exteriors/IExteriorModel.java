package net.tardis.mod.client.models.exteriors;

public interface IExteriorModel<T> {

    void animateDemat(T exterior, float age);
    void animateRemat(T exterior, float age);
    void animateSolid(T exterior, float age);

    void animateDoor(T tile);

}
