package net.tardis.mod.misc;

public enum ToolType {

    WELDING("welding"),
    HAMMER("hammer");

    final String name;

    ToolType(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public static ToolType getTypeFromName(String name){
        for(ToolType type : ToolType.values()){
            if(type.getName().equals(name))
                return type;
        }
        return null;
    }

}
