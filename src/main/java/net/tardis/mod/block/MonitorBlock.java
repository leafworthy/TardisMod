package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.BaseMonitorTile;
import net.tardis.mod.blockentities.TileRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class MonitorBlock<T extends BaseMonitorTile> extends BaseEntityBlock {

    public final Supplier<BlockEntityType<T>> type;

    public MonitorBlock(BlockBehaviour.Properties properties, Supplier<BlockEntityType<T>> type) {
        super(properties);
        this.type = type;
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) {
        return this.type.get().create(pPos, pState);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
       return pBlockEntityType == TileRegistry.STEAM_MONITOR.get() && pLevel.isClientSide ? (Level level, BlockPos pPos, BlockState state, T pBlockEntity) -> {
           ((BaseMonitorTile)pBlockEntity).clientTick();
       } : null;
    }
}
