package net.tardis.mod.creative_tabs;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;

public class Tabs {

    public static final CreativeModeTab MAIN = new CreativeModeTab(buildName("main")) {

        @Override
        public ItemStack makeIcon() {
            return new ItemStack(Items.ENDER_EYE);
        }
    };

    public static final CreativeModeTab ROUNDELS = new CreativeModeTab(buildName("roundel")) {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ForgeRegistries.ITEMS.getValue(Helper.createRL("decoration/roundels/terracotta/purple_terracotta_full")));
        }
    };

    public static String buildName(String name){
        return "creativetab." + Tardis.MODID + name;
    }

}
