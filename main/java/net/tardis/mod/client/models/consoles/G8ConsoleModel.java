package net.tardis.mod.client.models.consoles;// Made with Blockbench 4.3.1
// Exported for Minecraft version 1.17 - 1.18 with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.world.entity.Entity;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.animations.consoles.G8ConsoleAnimations;
import net.tardis.mod.client.models.IAnimatableTile;
import net.tardis.mod.control.ControlRegistry;
import net.tardis.mod.control.RandomizerControl;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.helpers.Helper;

public class G8ConsoleModel extends HierarchicalModel<Entity> implements IAnimatableTile<ConsoleTile> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("console/g8"), "main");
	private final ModelPart time_rotor;
	private final ModelPart stations;
	private final ModelPart glow_core;
	private final ModelPart bottom_plate;
	private final ModelPart side_s;
	private final ModelPart side_sw;
	private final ModelPart side_nw;
	private final ModelPart side_se;
	private final ModelPart side_ne;
	private final ModelPart side_n;

	private final ModelPart root;

	public G8ConsoleModel(ModelPart root) {
		this.time_rotor = root.getChild("time_rotor");
		this.stations = root.getChild("stations");
		this.glow_core = root.getChild("glow_core");
		this.bottom_plate = root.getChild("bottom_plate");
		this.side_s = root.getChild("side_s");
		this.side_sw = root.getChild("side_sw");
		this.side_nw = root.getChild("side_nw");
		this.side_se = root.getChild("side_se");
		this.side_ne = root.getChild("side_ne");
		this.side_n = root.getChild("side_n");
		this.root = root;
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition time_rotor = partdefinition.addOrReplaceChild("time_rotor", CubeListBuilder.create(), PartPose.offset(0.0F, 26.75F, 0.0F));

		PartDefinition rotor_glass = time_rotor.addOrReplaceChild("rotor_glass", CubeListBuilder.create().texOffs(28, 38).addBox(-2.9855F, -26.0F, -4.0901F, 6.0F, 7.0F, 1.0F, new CubeDeformation(-0.8F))
		.texOffs(27, 37).addBox(-3.9605F, -26.75F, -2.1651F, 6.0F, 2.0F, 9.0F, new CubeDeformation(-0.8F))
		.texOffs(27, 37).addBox(-2.5355F, -26.75F, -5.7651F, 6.0F, 2.0F, 9.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r1 = rotor_glass.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(28, 38).addBox(-2.925F, -26.0F, -4.125F, 6.0F, 7.0F, 1.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.0605F, 0.0F, 0.0349F, 0.0F, 1.0472F, 0.0F));

		PartDefinition cube_r2 = rotor_glass.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(28, 38).addBox(-2.925F, -26.0F, -4.125F, 6.0F, 7.0F, 1.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.0605F, 0.0F, 0.0349F, 0.0F, 2.0944F, 0.0F));

		PartDefinition cube_r3 = rotor_glass.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(28, 38).addBox(-2.925F, -26.0F, -4.125F, 6.0F, 7.0F, 1.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.0605F, 0.0F, 0.0349F, 0.0F, 3.1416F, 0.0F));

		PartDefinition cube_r4 = rotor_glass.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(28, 38).addBox(-2.925F, -26.0F, -4.125F, 6.0F, 7.0F, 1.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.0605F, 0.0F, 0.0349F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r5 = rotor_glass.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(28, 38).addBox(-2.925F, -26.0F, -4.125F, 6.0F, 7.0F, 1.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.0605F, 0.0F, 0.0349F, 0.0F, -1.0472F, 0.0F));

		PartDefinition glow_spinner_rings = time_rotor.addOrReplaceChild("glow_spinner_rings", CubeListBuilder.create(), PartPose.offset(-0.05F, 11.5F, -0.075F));

		PartDefinition spinner_1 = glow_spinner_rings.addOrReplaceChild("spinner_1", CubeListBuilder.create(), PartPose.offset(0.0F, -32.175F, 0.0F));

		PartDefinition panel_0 = spinner_1.addOrReplaceChild("panel_0", CubeListBuilder.create().texOffs(94, 102).addBox(-0.975F, -1.9989F, 0.9387F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 0.775F, 0.8F, -0.0175F, 0.0F, 0.0F));

		PartDefinition panel_1 = spinner_1.addOrReplaceChild("panel_1", CubeListBuilder.create().texOffs(94, 102).addBox(-1.7969F, -1.9901F, 0.4301F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 0.775F, 0.8F, -0.0175F, -0.7854F, 0.0F));

		PartDefinition panel_2 = spinner_1.addOrReplaceChild("panel_2", CubeListBuilder.create().texOffs(94, 102).addBox(-2.7378F, -1.9939F, 0.6515F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 0.775F, 0.8F, -0.0175F, -1.5708F, 0.0F));

		PartDefinition panel_3 = spinner_1.addOrReplaceChild("panel_3", CubeListBuilder.create().texOffs(94, 102).addBox(-3.2465F, -2.0083F, 1.4733F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 0.775F, 0.8F, -0.0175F, -2.3562F, 0.0F));

		PartDefinition panel_4 = spinner_1.addOrReplaceChild("panel_4", CubeListBuilder.create().texOffs(94, 102).addBox(-3.025F, -2.0247F, 2.414F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 0.775F, 0.8F, -0.0175F, -3.1416F, 0.0F));

		PartDefinition panel_5 = spinner_1.addOrReplaceChild("panel_5", CubeListBuilder.create().texOffs(94, 102).addBox(-2.2031F, -2.0336F, 2.9226F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 0.775F, 0.8F, -0.0175F, -3.927F, 0.0F));

		PartDefinition panel_6 = spinner_1.addOrReplaceChild("panel_6", CubeListBuilder.create().texOffs(94, 102).addBox(-1.2622F, -2.0297F, 2.7012F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 0.775F, 0.8F, -0.0175F, -4.7124F, 0.0F));

		PartDefinition panel_7 = spinner_1.addOrReplaceChild("panel_7", CubeListBuilder.create().texOffs(94, 102).addBox(-0.7535F, -2.0154F, 1.8794F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 0.775F, 0.8F, -0.0175F, -5.4978F, 0.0F));

		PartDefinition spinner_2 = glow_spinner_rings.addOrReplaceChild("spinner_2", CubeListBuilder.create(), PartPose.offset(0.0F, -31.775F, 0.0F));

		PartDefinition panel_8 = spinner_2.addOrReplaceChild("panel_8", CubeListBuilder.create().texOffs(94, 102).addBox(-0.975F, -1.9989F, 0.9387F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 1.15F, 0.8F, -0.0175F, 0.0F, 0.0F));

		PartDefinition panel_9 = spinner_2.addOrReplaceChild("panel_9", CubeListBuilder.create().texOffs(94, 102).addBox(-1.7969F, -1.9901F, 0.4301F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 1.15F, 0.8F, -0.0175F, -0.7854F, 0.0F));

		PartDefinition panel_10 = spinner_2.addOrReplaceChild("panel_10", CubeListBuilder.create().texOffs(94, 102).addBox(-2.7378F, -1.9939F, 0.6515F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 1.15F, 0.8F, -0.0175F, -1.5708F, 0.0F));

		PartDefinition panel_11 = spinner_2.addOrReplaceChild("panel_11", CubeListBuilder.create().texOffs(94, 102).addBox(-3.2465F, -2.0083F, 1.4733F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 1.15F, 0.8F, -0.0175F, -2.3562F, 0.0F));

		PartDefinition panel_12 = spinner_2.addOrReplaceChild("panel_12", CubeListBuilder.create().texOffs(94, 102).addBox(-3.025F, -2.0247F, 2.414F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 1.15F, 0.8F, -0.0175F, -3.1416F, 0.0F));

		PartDefinition panel_13 = spinner_2.addOrReplaceChild("panel_13", CubeListBuilder.create().texOffs(94, 102).addBox(-2.2031F, -2.0336F, 2.9226F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 1.15F, 0.8F, -0.0175F, -3.927F, 0.0F));

		PartDefinition panel_14 = spinner_2.addOrReplaceChild("panel_14", CubeListBuilder.create().texOffs(94, 102).addBox(-1.2622F, -2.0297F, 2.7012F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 1.15F, 0.8F, -0.0175F, -4.7124F, 0.0F));

		PartDefinition panel_15 = spinner_2.addOrReplaceChild("panel_15", CubeListBuilder.create().texOffs(94, 102).addBox(-0.7535F, -2.0154F, 1.8794F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.975F, 1.15F, 0.8F, -0.0175F, -5.4978F, 0.0F));

		PartDefinition rotor_core = time_rotor.addOrReplaceChild("rotor_core", CubeListBuilder.create(), PartPose.offset(0.0F, -18.75F, 0.0F));

		PartDefinition posts = rotor_core.addOrReplaceChild("posts", CubeListBuilder.create(), PartPose.offset(0.0F, 20.0F, 0.0F));

		PartDefinition cube_r6 = posts.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(106, 35).addBox(0.5F, 14.25F, 0.5F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(116, 1).addBox(-1.25F, 14.25F, 0.5F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(105, 32).addBox(-1.25F, 14.25F, -1.25F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(116, 1).addBox(0.5F, 14.25F, -1.25F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1F, 0.0F, -0.15F, 0.0F, 0.0F, -3.1416F));

		PartDefinition rotor_lid = time_rotor.addOrReplaceChild("rotor_lid", CubeListBuilder.create(), PartPose.offset(-0.0605F, 0.0F, 0.0349F));

		PartDefinition shape = rotor_lid.addOrReplaceChild("shape", CubeListBuilder.create(), PartPose.offset(0.0605F, -25.0F, -0.0349F));

		PartDefinition panel_0_1 = shape.addOrReplaceChild("panel_0_1", CubeListBuilder.create().texOffs(25, 29).addBox(-3.1692F, -1.9983F, 2.9023F, 6.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0175F, 0.0F, 0.0F));

		PartDefinition panel_1_1 = shape.addOrReplaceChild("panel_1_1", CubeListBuilder.create().texOffs(25, 29).addBox(-3.0F, -2.0F, 3.0F, 6.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0175F, -1.0472F, 0.0F));

		PartDefinition panel_2_1 = shape.addOrReplaceChild("panel_2_1", CubeListBuilder.create().texOffs(25, 29).addBox(-2.8308F, -1.9983F, 2.9023F, 6.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0175F, -2.0944F, 0.0F));

		PartDefinition panel_3_1 = shape.addOrReplaceChild("panel_3_1", CubeListBuilder.create().texOffs(25, 29).addBox(-2.8308F, -1.9949F, 2.707F, 6.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0175F, -3.1416F, 0.0F));

		PartDefinition panel_4_1 = shape.addOrReplaceChild("panel_4_1", CubeListBuilder.create().texOffs(25, 29).addBox(-3.0F, -1.9932F, 2.6093F, 6.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0175F, -4.1888F, 0.0F));

		PartDefinition panel_5_1 = shape.addOrReplaceChild("panel_5_1", CubeListBuilder.create().texOffs(25, 29).addBox(-3.1692F, -1.9949F, 2.707F, 6.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0175F, -5.236F, 0.0F));

		PartDefinition stations = partdefinition.addOrReplaceChild("stations", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition station_1 = stations.addOrReplaceChild("station_1", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition edge_1 = station_1.addOrReplaceChild("edge_1", CubeListBuilder.create().texOffs(74, 28).addBox(-9.0F, -14.9857F, -16.052F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(55, 9).addBox(-8.95F, -14.5F, -15.5F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(74, 28).addBox(-8.95F, -14.0F, -15.775F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(78, 6).addBox(-3.0F, -18.25F, -5.75F, 6.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r7 = edge_1.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(80, 9).addBox(-2.575F, 2.075F, -0.225F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -20.425F, -3.35F, -0.5236F, 0.0F, 0.0F));

		PartDefinition cooling_blades_1 = edge_1.addOrReplaceChild("cooling_blades_1", CubeListBuilder.create().texOffs(96, 1).addBox(-3.25F, -15.7F, -5.5F, 6.0F, 15.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(68, 6).addBox(-4.75F, -1.525F, -8.5F, 9.0F, 2.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(64, 7).addBox(-5.75F, -1.775F, -10.5F, 12.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition cube_r8 = cooling_blades_1.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(106, 12).addBox(-3.25F, -2.5F, -0.5F, 7.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(104, 12).addBox(-3.75F, 0.0F, -0.5F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.25F, -2.7F, -6.0F, -0.5236F, 0.0F, 0.0F));

		PartDefinition plane_1 = station_1.addOrReplaceChild("plane_1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -13.0F, -15.0F, 0.3054F, 0.0F, 0.0F));

		PartDefinition station_top = plane_1.addOrReplaceChild("station_top", CubeListBuilder.create().texOffs(1, 19).addBox(-8.525F, -20.45F, -6.4F, 17.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-7.55F, -20.45F, -3.9F, 15.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.75F, -20.45F, -2.4F, 14.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.45F, -20.45F, -1.9F, 13.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.05F, -20.45F, -0.4F, 12.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-5.525F, -20.45F, 0.1F, 11.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.8F, -20.45F, 1.6F, 10.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.5F, -20.45F, 2.1F, 9.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-3.9F, -20.45F, 3.6F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition underbox = plane_1.addOrReplaceChild("underbox", CubeListBuilder.create().texOffs(54, 15).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(60, 2).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition rib_1 = station_1.addOrReplaceChild("rib_1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_1 = rib_1.addOrReplaceChild("rib_tilt_1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r9 = rib_tilt_1.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(19, 2).addBox(0.75F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.9439F, -8.4298F, -7.2814F, 0.9926F, 0.1836F, -0.1186F));

		PartDefinition cube_r10 = rib_tilt_1.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(19, 2).addBox(-0.5F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.2765F, -8.5752F, -7.5096F, 0.9926F, -0.1836F, 0.1186F));

		PartDefinition cube_r11 = rib_tilt_1.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(35, 16).addBox(-0.35F, -1.65F, -3.825F, 1.0F, 1.0F, 8.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.15F, -8.7818F, -7.2291F, 1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r12 = rib_tilt_1.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(47, 19).addBox(-0.375F, -4.25F, 2.0F, 1.0F, 4.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 17).addBox(-0.375F, -4.5F, 0.5F, 1.0F, 6.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 16).addBox(-0.375F, -5.5F, -1.0F, 1.0F, 7.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 14).addBox(-0.375F, -5.3F, -2.5F, 1.0F, 9.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.125F, -15.2642F, -5.3189F, 0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r13 = rib_tilt_1.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(86, 44).addBox(0.0F, -0.75F, -0.7F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(90, 43).addBox(-0.5F, -1.25F, 0.0F, 2.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.5F, -20.3167F, -10.1795F, -0.4712F, 0.0F, 0.0F));

		PartDefinition cube_r14 = rib_tilt_1.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(85, 40).addBox(-0.5F, -1.25F, -2.125F, 1.0F, 3.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -20.2832F, -9.1645F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r15 = rib_tilt_1.addOrReplaceChild("cube_r15", CubeListBuilder.create().texOffs(74, 31).addBox(-0.1976F, -0.843F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, -0.0552F, -0.4331F));

		PartDefinition cube_r16 = rib_tilt_1.addOrReplaceChild("cube_r16", CubeListBuilder.create().texOffs(68, 29).addBox(-1.3024F, -1.343F, -2.0068F, 2.0F, 3.0F, 15.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, 0.0552F, 0.4331F));

		PartDefinition cube_r17 = rib_tilt_1.addOrReplaceChild("cube_r17", CubeListBuilder.create().texOffs(73, 31).addBox(-0.5F, -1.0209F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1309F, 0.0F, 0.0F));

		PartDefinition cube_r18 = rib_tilt_1.addOrReplaceChild("cube_r18", CubeListBuilder.create().texOffs(2, 20).addBox(-0.5F, 2.75F, -1.5F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.7088F, 7.0375F, -0.9861F, 0.0F, 0.0F));

		PartDefinition base_fin_1 = station_1.addOrReplaceChild("base_fin_1", CubeListBuilder.create().texOffs(35, 15).addBox(-1.0F, -2.425F, -12.375F, 2.0F, 3.0F, 7.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition station_2 = stations.addOrReplaceChild("station_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition edge_2 = station_2.addOrReplaceChild("edge_2", CubeListBuilder.create().texOffs(74, 28).addBox(-9.0F, -14.9857F, -16.052F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(55, 9).addBox(-8.95F, -14.5F, -15.5F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(74, 28).addBox(-8.95F, -14.0F, -15.775F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(78, 6).addBox(-3.0F, -18.25F, -5.75F, 6.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r19 = edge_2.addOrReplaceChild("cube_r19", CubeListBuilder.create().texOffs(80, 9).addBox(-2.575F, 2.075F, -0.225F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -20.425F, -3.35F, -0.5236F, 0.0F, 0.0F));

		PartDefinition cooling_blades_2 = edge_2.addOrReplaceChild("cooling_blades_2", CubeListBuilder.create().texOffs(96, 1).addBox(-3.25F, -15.7F, -5.5F, 6.0F, 15.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(68, 6).addBox(-4.75F, -1.525F, -8.5F, 9.0F, 2.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(64, 7).addBox(-5.75F, -1.775F, -10.5F, 12.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition cube_r20 = cooling_blades_2.addOrReplaceChild("cube_r20", CubeListBuilder.create().texOffs(106, 12).addBox(-3.25F, -2.5F, -0.5F, 7.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(104, 12).addBox(-3.75F, 0.0F, -0.5F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.25F, -2.7F, -6.0F, -0.5236F, 0.0F, 0.0F));

		PartDefinition plane_2 = station_2.addOrReplaceChild("plane_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -13.0F, -15.0F, 0.3054F, 0.0F, 0.0F));

		PartDefinition station_top2 = plane_2.addOrReplaceChild("station_top2", CubeListBuilder.create().texOffs(1, 19).addBox(-8.525F, -20.45F, -6.4F, 17.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-7.55F, -20.45F, -3.9F, 15.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.75F, -20.45F, -2.4F, 14.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.45F, -20.45F, -1.9F, 13.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.05F, -20.45F, -0.4F, 12.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-5.525F, -20.45F, 0.1F, 11.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.8F, -20.45F, 1.6F, 10.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.5F, -20.45F, 2.1F, 9.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-3.9F, -20.45F, 3.6F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition underbox2 = plane_2.addOrReplaceChild("underbox2", CubeListBuilder.create().texOffs(54, 15).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(60, 2).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition rib_2 = station_2.addOrReplaceChild("rib_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_2 = rib_2.addOrReplaceChild("rib_tilt_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r21 = rib_tilt_2.addOrReplaceChild("cube_r21", CubeListBuilder.create().texOffs(19, 2).addBox(0.75F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.9439F, -8.4298F, -7.2814F, 0.9926F, 0.1836F, -0.1186F));

		PartDefinition cube_r22 = rib_tilt_2.addOrReplaceChild("cube_r22", CubeListBuilder.create().texOffs(19, 2).addBox(-0.5F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.2765F, -8.5752F, -7.5096F, 0.9926F, -0.1836F, 0.1186F));

		PartDefinition cube_r23 = rib_tilt_2.addOrReplaceChild("cube_r23", CubeListBuilder.create().texOffs(35, 16).addBox(-0.35F, -1.65F, -3.825F, 1.0F, 1.0F, 8.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.15F, -8.7818F, -7.2291F, 1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r24 = rib_tilt_2.addOrReplaceChild("cube_r24", CubeListBuilder.create().texOffs(47, 19).addBox(-0.375F, -4.25F, 2.0F, 1.0F, 4.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 17).addBox(-0.375F, -4.5F, 0.5F, 1.0F, 6.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 16).addBox(-0.375F, -5.5F, -1.0F, 1.0F, 7.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 14).addBox(-0.375F, -5.3F, -2.5F, 1.0F, 9.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.125F, -15.2642F, -5.3189F, 0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r25 = rib_tilt_2.addOrReplaceChild("cube_r25", CubeListBuilder.create().texOffs(86, 44).addBox(0.0F, -0.75F, -0.7F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(90, 43).addBox(-0.5F, -1.25F, 0.0F, 2.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.5F, -20.3167F, -10.1795F, -0.4712F, 0.0F, 0.0F));

		PartDefinition cube_r26 = rib_tilt_2.addOrReplaceChild("cube_r26", CubeListBuilder.create().texOffs(85, 40).addBox(-0.5F, -1.25F, -2.125F, 1.0F, 3.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -20.2832F, -9.1645F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r27 = rib_tilt_2.addOrReplaceChild("cube_r27", CubeListBuilder.create().texOffs(74, 31).addBox(-0.1976F, -0.843F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, -0.0552F, -0.4331F));

		PartDefinition cube_r28 = rib_tilt_2.addOrReplaceChild("cube_r28", CubeListBuilder.create().texOffs(68, 29).addBox(-1.3024F, -1.343F, -2.0068F, 2.0F, 3.0F, 15.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, 0.0552F, 0.4331F));

		PartDefinition cube_r29 = rib_tilt_2.addOrReplaceChild("cube_r29", CubeListBuilder.create().texOffs(73, 31).addBox(-0.5F, -1.0209F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1309F, 0.0F, 0.0F));

		PartDefinition cube_r30 = rib_tilt_2.addOrReplaceChild("cube_r30", CubeListBuilder.create().texOffs(2, 20).addBox(-0.5F, 2.75F, -1.5F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.7088F, 7.0375F, -0.9861F, 0.0F, 0.0F));

		PartDefinition base_fin_2 = station_2.addOrReplaceChild("base_fin_2", CubeListBuilder.create().texOffs(35, 15).addBox(-1.0F, -2.425F, -12.375F, 2.0F, 3.0F, 7.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition station_3 = stations.addOrReplaceChild("station_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition edge_3 = station_3.addOrReplaceChild("edge_3", CubeListBuilder.create().texOffs(74, 28).addBox(-9.0F, -14.9857F, -16.052F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(55, 9).addBox(-8.95F, -14.5F, -15.5F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(74, 28).addBox(-8.95F, -14.0F, -15.775F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(78, 6).addBox(-3.0F, -18.25F, -5.75F, 6.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r31 = edge_3.addOrReplaceChild("cube_r31", CubeListBuilder.create().texOffs(80, 9).addBox(-2.575F, 2.075F, -0.225F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -20.425F, -3.35F, -0.5236F, 0.0F, 0.0F));

		PartDefinition cooling_blades_3 = edge_3.addOrReplaceChild("cooling_blades_3", CubeListBuilder.create().texOffs(96, 1).addBox(-3.25F, -15.7F, -5.5F, 6.0F, 15.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(68, 6).addBox(-4.75F, -1.525F, -8.5F, 9.0F, 2.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(64, 7).addBox(-5.75F, -1.775F, -10.5F, 12.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition cube_r32 = cooling_blades_3.addOrReplaceChild("cube_r32", CubeListBuilder.create().texOffs(106, 12).addBox(-3.25F, -2.5F, -0.5F, 7.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(104, 12).addBox(-3.75F, 0.0F, -0.5F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.25F, -2.7F, -6.0F, -0.5236F, 0.0F, 0.0F));

		PartDefinition plane_3 = station_3.addOrReplaceChild("plane_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -13.0F, -15.0F, 0.3054F, 0.0F, 0.0F));

		PartDefinition station_top3 = plane_3.addOrReplaceChild("station_top3", CubeListBuilder.create().texOffs(1, 19).addBox(-8.525F, -20.45F, -6.4F, 17.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-7.55F, -20.45F, -3.9F, 15.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.75F, -20.45F, -2.4F, 14.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.45F, -20.45F, -1.9F, 13.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.05F, -20.45F, -0.4F, 12.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-5.525F, -20.45F, 0.1F, 11.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.8F, -20.45F, 1.6F, 10.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.5F, -20.45F, 2.1F, 9.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-3.9F, -20.45F, 3.6F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition underbox3 = plane_3.addOrReplaceChild("underbox3", CubeListBuilder.create().texOffs(54, 15).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(60, 2).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition rib_3 = station_3.addOrReplaceChild("rib_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_3 = rib_3.addOrReplaceChild("rib_tilt_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r33 = rib_tilt_3.addOrReplaceChild("cube_r33", CubeListBuilder.create().texOffs(19, 2).addBox(0.75F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.9439F, -8.4298F, -7.2814F, 0.9926F, 0.1836F, -0.1186F));

		PartDefinition cube_r34 = rib_tilt_3.addOrReplaceChild("cube_r34", CubeListBuilder.create().texOffs(19, 2).addBox(-0.5F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.2765F, -8.5752F, -7.5096F, 0.9926F, -0.1836F, 0.1186F));

		PartDefinition cube_r35 = rib_tilt_3.addOrReplaceChild("cube_r35", CubeListBuilder.create().texOffs(35, 16).addBox(-0.35F, -1.65F, -3.825F, 1.0F, 1.0F, 8.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.15F, -8.7818F, -7.2291F, 1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r36 = rib_tilt_3.addOrReplaceChild("cube_r36", CubeListBuilder.create().texOffs(47, 19).addBox(-0.375F, -4.25F, 2.0F, 1.0F, 4.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 17).addBox(-0.375F, -4.5F, 0.5F, 1.0F, 6.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 16).addBox(-0.375F, -5.5F, -1.0F, 1.0F, 7.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 14).addBox(-0.375F, -5.3F, -2.5F, 1.0F, 9.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.125F, -15.2642F, -5.3189F, 0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r37 = rib_tilt_3.addOrReplaceChild("cube_r37", CubeListBuilder.create().texOffs(86, 44).addBox(0.0F, -0.75F, -0.7F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(90, 43).addBox(-0.5F, -1.25F, 0.0F, 2.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.5F, -20.3167F, -10.1795F, -0.4712F, 0.0F, 0.0F));

		PartDefinition cube_r38 = rib_tilt_3.addOrReplaceChild("cube_r38", CubeListBuilder.create().texOffs(85, 40).addBox(-0.5F, -1.25F, -2.125F, 1.0F, 3.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -20.2832F, -9.1645F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r39 = rib_tilt_3.addOrReplaceChild("cube_r39", CubeListBuilder.create().texOffs(74, 31).addBox(-0.1976F, -0.843F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, -0.0552F, -0.4331F));

		PartDefinition cube_r40 = rib_tilt_3.addOrReplaceChild("cube_r40", CubeListBuilder.create().texOffs(68, 29).addBox(-1.3024F, -1.343F, -2.0068F, 2.0F, 3.0F, 15.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, 0.0552F, 0.4331F));

		PartDefinition cube_r41 = rib_tilt_3.addOrReplaceChild("cube_r41", CubeListBuilder.create().texOffs(73, 31).addBox(-0.5F, -1.0209F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1309F, 0.0F, 0.0F));

		PartDefinition cube_r42 = rib_tilt_3.addOrReplaceChild("cube_r42", CubeListBuilder.create().texOffs(2, 20).addBox(-0.5F, 2.75F, -1.5F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.7088F, 7.0375F, -0.9861F, 0.0F, 0.0F));

		PartDefinition base_fin_3 = station_3.addOrReplaceChild("base_fin_3", CubeListBuilder.create().texOffs(35, 15).addBox(-1.0F, -2.425F, -12.375F, 2.0F, 3.0F, 7.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition station_4 = stations.addOrReplaceChild("station_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition edge_4 = station_4.addOrReplaceChild("edge_4", CubeListBuilder.create().texOffs(74, 28).addBox(-9.0F, -14.9857F, -16.052F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(55, 9).addBox(-8.95F, -14.5F, -15.5F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(74, 28).addBox(-8.95F, -14.0F, -15.775F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(78, 6).addBox(-3.0F, -18.25F, -5.75F, 6.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r43 = edge_4.addOrReplaceChild("cube_r43", CubeListBuilder.create().texOffs(80, 9).addBox(-2.575F, 2.075F, -0.225F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -20.425F, -3.35F, -0.5236F, 0.0F, 0.0F));

		PartDefinition cooling_blades_4 = edge_4.addOrReplaceChild("cooling_blades_4", CubeListBuilder.create().texOffs(96, 1).addBox(-3.25F, -15.7F, -5.5F, 6.0F, 15.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(68, 6).addBox(-4.75F, -1.525F, -8.5F, 9.0F, 2.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(64, 7).addBox(-5.75F, -1.775F, -10.5F, 12.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition cube_r44 = cooling_blades_4.addOrReplaceChild("cube_r44", CubeListBuilder.create().texOffs(106, 12).addBox(-3.25F, -2.5F, -0.5F, 7.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(104, 12).addBox(-3.75F, 0.0F, -0.5F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.25F, -2.7F, -6.0F, -0.5236F, 0.0F, 0.0F));

		PartDefinition plane_4 = station_4.addOrReplaceChild("plane_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -13.0F, -15.0F, 0.3054F, 0.0F, 0.0F));

		PartDefinition station_top4 = plane_4.addOrReplaceChild("station_top4", CubeListBuilder.create().texOffs(1, 19).addBox(-8.525F, -20.45F, -6.4F, 17.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-7.55F, -20.45F, -3.9F, 15.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.75F, -20.45F, -2.4F, 14.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.45F, -20.45F, -1.9F, 13.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.05F, -20.45F, -0.4F, 12.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-5.525F, -20.45F, 0.1F, 11.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.8F, -20.45F, 1.6F, 10.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.5F, -20.45F, 2.1F, 9.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-3.9F, -20.45F, 3.6F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition underbox4 = plane_4.addOrReplaceChild("underbox4", CubeListBuilder.create().texOffs(54, 15).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(60, 2).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition rib_4 = station_4.addOrReplaceChild("rib_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_4 = rib_4.addOrReplaceChild("rib_tilt_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r45 = rib_tilt_4.addOrReplaceChild("cube_r45", CubeListBuilder.create().texOffs(19, 2).addBox(0.75F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.9439F, -8.4298F, -7.2814F, 0.9926F, 0.1836F, -0.1186F));

		PartDefinition cube_r46 = rib_tilt_4.addOrReplaceChild("cube_r46", CubeListBuilder.create().texOffs(19, 2).addBox(-0.5F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.2765F, -8.5752F, -7.5096F, 0.9926F, -0.1836F, 0.1186F));

		PartDefinition cube_r47 = rib_tilt_4.addOrReplaceChild("cube_r47", CubeListBuilder.create().texOffs(35, 16).addBox(-0.35F, -1.65F, -3.825F, 1.0F, 1.0F, 8.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.15F, -8.7818F, -7.2291F, 1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r48 = rib_tilt_4.addOrReplaceChild("cube_r48", CubeListBuilder.create().texOffs(47, 19).addBox(-0.375F, -4.25F, 2.0F, 1.0F, 4.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 17).addBox(-0.375F, -4.5F, 0.5F, 1.0F, 6.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 16).addBox(-0.375F, -5.5F, -1.0F, 1.0F, 7.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 14).addBox(-0.375F, -5.3F, -2.5F, 1.0F, 9.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.125F, -15.2642F, -5.3189F, 0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r49 = rib_tilt_4.addOrReplaceChild("cube_r49", CubeListBuilder.create().texOffs(86, 44).addBox(0.0F, -0.75F, -0.7F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(90, 43).addBox(-0.5F, -1.25F, 0.0F, 2.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.5F, -20.3167F, -10.1795F, -0.4712F, 0.0F, 0.0F));

		PartDefinition cube_r50 = rib_tilt_4.addOrReplaceChild("cube_r50", CubeListBuilder.create().texOffs(85, 40).addBox(-0.5F, -1.25F, -2.125F, 1.0F, 3.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -20.2832F, -9.1645F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r51 = rib_tilt_4.addOrReplaceChild("cube_r51", CubeListBuilder.create().texOffs(74, 31).addBox(-0.1976F, -0.843F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, -0.0552F, -0.4331F));

		PartDefinition cube_r52 = rib_tilt_4.addOrReplaceChild("cube_r52", CubeListBuilder.create().texOffs(68, 29).addBox(-1.3024F, -1.343F, -2.0068F, 2.0F, 3.0F, 15.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, 0.0552F, 0.4331F));

		PartDefinition cube_r53 = rib_tilt_4.addOrReplaceChild("cube_r53", CubeListBuilder.create().texOffs(73, 31).addBox(-0.5F, -1.0209F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1309F, 0.0F, 0.0F));

		PartDefinition cube_r54 = rib_tilt_4.addOrReplaceChild("cube_r54", CubeListBuilder.create().texOffs(2, 20).addBox(-0.5F, 2.75F, -1.5F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.7088F, 7.0375F, -0.9861F, 0.0F, 0.0F));

		PartDefinition base_fin_4 = station_4.addOrReplaceChild("base_fin_4", CubeListBuilder.create().texOffs(35, 15).addBox(-1.0F, -2.425F, -12.375F, 2.0F, 3.0F, 7.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition station_5 = stations.addOrReplaceChild("station_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition edge_5 = station_5.addOrReplaceChild("edge_5", CubeListBuilder.create().texOffs(74, 28).addBox(-9.0F, -14.9857F, -16.052F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(55, 9).addBox(-8.95F, -14.5F, -15.5F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(74, 28).addBox(-8.95F, -14.0F, -15.775F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(78, 6).addBox(-3.0F, -18.25F, -5.75F, 6.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r55 = edge_5.addOrReplaceChild("cube_r55", CubeListBuilder.create().texOffs(80, 9).addBox(-2.575F, 2.075F, -0.225F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -20.425F, -3.35F, -0.5236F, 0.0F, 0.0F));

		PartDefinition cooling_blades_5 = edge_5.addOrReplaceChild("cooling_blades_5", CubeListBuilder.create().texOffs(96, 1).addBox(-3.25F, -15.7F, -5.5F, 6.0F, 15.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(68, 6).addBox(-4.75F, -1.525F, -8.5F, 9.0F, 2.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(64, 7).addBox(-5.75F, -1.775F, -10.5F, 12.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition cube_r56 = cooling_blades_5.addOrReplaceChild("cube_r56", CubeListBuilder.create().texOffs(106, 12).addBox(-3.25F, -2.5F, -0.5F, 7.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(104, 12).addBox(-3.75F, 0.0F, -0.5F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.25F, -2.7F, -6.0F, -0.5236F, 0.0F, 0.0F));

		PartDefinition plane_5 = station_5.addOrReplaceChild("plane_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -13.0F, -15.0F, 0.3054F, 0.0F, 0.0F));

		PartDefinition station_top5 = plane_5.addOrReplaceChild("station_top5", CubeListBuilder.create().texOffs(1, 19).addBox(-8.525F, -20.45F, -6.4F, 17.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-7.55F, -20.45F, -3.9F, 15.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.75F, -20.45F, -2.4F, 14.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.45F, -20.45F, -1.9F, 13.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.05F, -20.45F, -0.4F, 12.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-5.525F, -20.45F, 0.1F, 11.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.8F, -20.45F, 1.6F, 10.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.5F, -20.45F, 2.1F, 9.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-3.9F, -20.45F, 3.6F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition underbox5 = plane_5.addOrReplaceChild("underbox5", CubeListBuilder.create().texOffs(54, 15).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(60, 2).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition rib_5 = station_5.addOrReplaceChild("rib_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_5 = rib_5.addOrReplaceChild("rib_tilt_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r57 = rib_tilt_5.addOrReplaceChild("cube_r57", CubeListBuilder.create().texOffs(19, 2).addBox(0.75F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.9439F, -8.4298F, -7.2814F, 0.9926F, 0.1836F, -0.1186F));

		PartDefinition cube_r58 = rib_tilt_5.addOrReplaceChild("cube_r58", CubeListBuilder.create().texOffs(19, 2).addBox(-0.5F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.2765F, -8.5752F, -7.5096F, 0.9926F, -0.1836F, 0.1186F));

		PartDefinition cube_r59 = rib_tilt_5.addOrReplaceChild("cube_r59", CubeListBuilder.create().texOffs(35, 16).addBox(-0.35F, -1.65F, -3.825F, 1.0F, 1.0F, 8.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.15F, -8.7818F, -7.2291F, 1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r60 = rib_tilt_5.addOrReplaceChild("cube_r60", CubeListBuilder.create().texOffs(47, 19).addBox(-0.375F, -4.25F, 2.0F, 1.0F, 4.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 17).addBox(-0.375F, -4.5F, 0.5F, 1.0F, 6.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 16).addBox(-0.375F, -5.5F, -1.0F, 1.0F, 7.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 14).addBox(-0.375F, -5.3F, -2.5F, 1.0F, 9.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.125F, -15.2642F, -5.3189F, 0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r61 = rib_tilt_5.addOrReplaceChild("cube_r61", CubeListBuilder.create().texOffs(86, 44).addBox(0.0F, -0.75F, -0.7F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(90, 43).addBox(-0.5F, -1.25F, 0.0F, 2.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.5F, -20.3167F, -10.1795F, -0.4712F, 0.0F, 0.0F));

		PartDefinition cube_r62 = rib_tilt_5.addOrReplaceChild("cube_r62", CubeListBuilder.create().texOffs(85, 40).addBox(-0.5F, -1.25F, -2.125F, 1.0F, 3.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -20.2832F, -9.1645F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r63 = rib_tilt_5.addOrReplaceChild("cube_r63", CubeListBuilder.create().texOffs(74, 31).addBox(-0.1976F, -0.843F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, -0.0552F, -0.4331F));

		PartDefinition cube_r64 = rib_tilt_5.addOrReplaceChild("cube_r64", CubeListBuilder.create().texOffs(68, 29).addBox(-1.3024F, -1.343F, -2.0068F, 2.0F, 3.0F, 15.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, 0.0552F, 0.4331F));

		PartDefinition cube_r65 = rib_tilt_5.addOrReplaceChild("cube_r65", CubeListBuilder.create().texOffs(73, 31).addBox(-0.5F, -1.0209F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1309F, 0.0F, 0.0F));

		PartDefinition cube_r66 = rib_tilt_5.addOrReplaceChild("cube_r66", CubeListBuilder.create().texOffs(2, 20).addBox(-0.5F, 2.75F, -1.5F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.7088F, 7.0375F, -0.9861F, 0.0F, 0.0F));

		PartDefinition base_fin_5 = station_5.addOrReplaceChild("base_fin_5", CubeListBuilder.create().texOffs(35, 15).addBox(-1.0F, -2.425F, -12.375F, 2.0F, 3.0F, 7.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition station_6 = stations.addOrReplaceChild("station_6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition edge_6 = station_6.addOrReplaceChild("edge_6", CubeListBuilder.create().texOffs(74, 28).addBox(-9.0F, -14.9857F, -16.052F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(55, 9).addBox(-8.95F, -14.5F, -15.5F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(74, 28).addBox(-8.95F, -14.0F, -15.775F, 18.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(78, 6).addBox(-3.0F, -18.25F, -5.75F, 6.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r67 = edge_6.addOrReplaceChild("cube_r67", CubeListBuilder.create().texOffs(80, 9).addBox(-2.575F, 2.075F, -0.225F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -20.425F, -3.35F, -0.5236F, 0.0F, 0.0F));

		PartDefinition cooling_blades_6 = edge_6.addOrReplaceChild("cooling_blades_6", CubeListBuilder.create().texOffs(96, 1).addBox(-3.25F, -15.7F, -5.5F, 6.0F, 15.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(68, 6).addBox(-4.75F, -1.525F, -8.5F, 9.0F, 2.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(64, 7).addBox(-5.75F, -1.775F, -10.5F, 12.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition cube_r68 = cooling_blades_6.addOrReplaceChild("cube_r68", CubeListBuilder.create().texOffs(106, 12).addBox(-3.25F, -2.5F, -0.5F, 7.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(104, 12).addBox(-3.75F, 0.0F, -0.5F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.25F, -2.7F, -6.0F, -0.5236F, 0.0F, 0.0F));

		PartDefinition plane_6 = station_6.addOrReplaceChild("plane_6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -13.0F, -15.0F, 0.3054F, 0.0F, 0.0F));

		PartDefinition station_top6 = plane_6.addOrReplaceChild("station_top6", CubeListBuilder.create().texOffs(1, 19).addBox(-8.525F, -20.45F, -6.4F, 17.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-7.55F, -20.45F, -3.9F, 15.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.75F, -20.45F, -2.4F, 14.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.45F, -20.45F, -1.9F, 13.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-6.05F, -20.45F, -0.4F, 12.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-5.525F, -20.45F, 0.1F, 11.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.8F, -20.45F, 1.6F, 10.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-4.5F, -20.45F, 2.1F, 9.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(1, 19).addBox(-3.9F, -20.45F, 3.6F, 8.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition underbox6 = plane_6.addOrReplaceChild("underbox6", CubeListBuilder.create().texOffs(54, 15).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(60, 2).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 18.7583F, 6.4904F));

		PartDefinition rib_6 = station_6.addOrReplaceChild("rib_6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_6 = rib_6.addOrReplaceChild("rib_tilt_6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r69 = rib_tilt_6.addOrReplaceChild("cube_r69", CubeListBuilder.create().texOffs(19, 2).addBox(0.75F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.9439F, -8.4298F, -7.2814F, 0.9926F, 0.1836F, -0.1186F));

		PartDefinition cube_r70 = rib_tilt_6.addOrReplaceChild("cube_r70", CubeListBuilder.create().texOffs(19, 2).addBox(-0.5F, -1.5F, -3.5F, 1.0F, 7.0F, 16.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.2765F, -8.5752F, -7.5096F, 0.9926F, -0.1836F, 0.1186F));

		PartDefinition cube_r71 = rib_tilt_6.addOrReplaceChild("cube_r71", CubeListBuilder.create().texOffs(35, 16).addBox(-0.35F, -1.65F, -3.825F, 1.0F, 1.0F, 8.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.15F, -8.7818F, -7.2291F, 1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r72 = rib_tilt_6.addOrReplaceChild("cube_r72", CubeListBuilder.create().texOffs(47, 19).addBox(-0.375F, -4.25F, 2.0F, 1.0F, 4.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 17).addBox(-0.375F, -4.5F, 0.5F, 1.0F, 6.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 16).addBox(-0.375F, -5.5F, -1.0F, 1.0F, 7.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(47, 14).addBox(-0.375F, -5.3F, -2.5F, 1.0F, 9.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.125F, -15.2642F, -5.3189F, 0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r73 = rib_tilt_6.addOrReplaceChild("cube_r73", CubeListBuilder.create().texOffs(86, 44).addBox(0.0F, -0.75F, -0.7F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(90, 43).addBox(-0.5F, -1.25F, 0.0F, 2.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.5F, -20.3167F, -10.1795F, -0.4712F, 0.0F, 0.0F));

		PartDefinition cube_r74 = rib_tilt_6.addOrReplaceChild("cube_r74", CubeListBuilder.create().texOffs(85, 40).addBox(-0.5F, -1.25F, -2.125F, 1.0F, 3.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -20.2832F, -9.1645F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r75 = rib_tilt_6.addOrReplaceChild("cube_r75", CubeListBuilder.create().texOffs(74, 31).addBox(-0.1976F, -0.843F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, -0.0552F, -0.4331F));

		PartDefinition cube_r76 = rib_tilt_6.addOrReplaceChild("cube_r76", CubeListBuilder.create().texOffs(68, 29).addBox(-1.3024F, -1.343F, -2.0068F, 2.0F, 3.0F, 15.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1188F, 0.0552F, 0.4331F));

		PartDefinition cube_r77 = rib_tilt_6.addOrReplaceChild("cube_r77", CubeListBuilder.create().texOffs(73, 31).addBox(-0.5F, -1.0209F, -1.5068F, 1.0F, 2.0F, 14.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.9291F, -8.3932F, -0.1309F, 0.0F, 0.0F));

		PartDefinition cube_r78 = rib_tilt_6.addOrReplaceChild("cube_r78", CubeListBuilder.create().texOffs(2, 20).addBox(-0.5F, 2.75F, -1.5F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, -19.7088F, 7.0375F, -0.9861F, 0.0F, 0.0F));

		PartDefinition base_fin_6 = station_6.addOrReplaceChild("base_fin_6", CubeListBuilder.create().texOffs(35, 15).addBox(-1.0F, -2.425F, -12.375F, 2.0F, 3.0F, 7.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition glow_core = partdefinition.addOrReplaceChild("glow_core", CubeListBuilder.create().texOffs(2, 37).addBox(-3.0F, -16.0F, -6.0F, 6.0F, 4.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 23.75F, 0.0F));

		PartDefinition cube_r79 = glow_core.addOrReplaceChild("cube_r79", CubeListBuilder.create().texOffs(2, 37).addBox(-3.0F, -16.0F, -6.0F, 6.0F, 4.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition cube_r80 = glow_core.addOrReplaceChild("cube_r80", CubeListBuilder.create().texOffs(2, 37).addBox(-3.0F, -16.0F, -6.0F, 6.0F, 4.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition bottom_plate = partdefinition.addOrReplaceChild("bottom_plate", CubeListBuilder.create().texOffs(24, 29).addBox(-5.0F, -0.9F, -2.0F, 10.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(32, 30).addBox(-4.0F, -0.9F, -4.0F, 8.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(35, 29).addBox(-4.0F, -1.9F, -5.0F, 8.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(35, 30).addBox(-4.0F, -1.9F, 3.0F, 8.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(30, 30).addBox(-4.0F, -0.9F, 2.0F, 8.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition cube_r81 = bottom_plate.addOrReplaceChild("cube_r81", CubeListBuilder.create().texOffs(32, 28).addBox(-4.0F, -1.9F, -5.0F, 8.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r82 = bottom_plate.addOrReplaceChild("cube_r82", CubeListBuilder.create().texOffs(33, 29).addBox(-4.0F, -1.9F, -5.0F, 8.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r83 = bottom_plate.addOrReplaceChild("cube_r83", CubeListBuilder.create().texOffs(30, 30).addBox(-4.0F, -1.9F, -5.0F, 8.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition cube_r84 = bottom_plate.addOrReplaceChild("cube_r84", CubeListBuilder.create().texOffs(33, 29).addBox(-4.0F, -1.9F, -5.0F, 8.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition side_s = partdefinition.addOrReplaceChild("side_s", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 9.25F, 14.25F, -2.7925F, 0.0F, -3.1416F));

		PartDefinition dummy_bitz2 = side_s.addOrReplaceChild("dummy_bitz2", CubeListBuilder.create(), PartPose.offset(0.0F, 20.256F, 3.2127F));

		PartDefinition trim = dummy_bitz2.addOrReplaceChild("trim", CubeListBuilder.create(), PartPose.offset(12.8462F, -16.525F, -6.5897F));

		PartDefinition cube_r85 = trim.addOrReplaceChild("cube_r85", CubeListBuilder.create().texOffs(68, 8).addBox(-18.375F, -4.7424F, 6.5998F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r86 = trim.addOrReplaceChild("cube_r86", CubeListBuilder.create().texOffs(68, 8).addBox(-18.375F, -4.7424F, 6.5998F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 1.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition keyboard = dummy_bitz2.addOrReplaceChild("keyboard", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r87 = keyboard.addOrReplaceChild("cube_r87", CubeListBuilder.create().texOffs(44, 28).addBox(-16.375F, -4.8F, 4.2F, 7.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(12.8462F, -16.525F, -7.3397F, -0.0873F, 0.0F, 0.0F));

		PartDefinition glow_light8 = dummy_bitz2.addOrReplaceChild("glow_light8", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r88 = glow_light8.addOrReplaceChild("cube_r88", CubeListBuilder.create().texOffs(100, 20).addBox(-16.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-18.875F, -4.8F, 6.2F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-19.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-19.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(16.8462F, -16.8F, -9.0897F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r89 = glow_light8.addOrReplaceChild("cube_r89", CubeListBuilder.create().texOffs(100, 20).addBox(-16.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-16.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-17.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-17.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-18.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-18.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-19.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-19.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-20.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(18, 108).addBox(-20.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(17.3462F, -16.775F, -8.5897F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r90 = glow_light8.addOrReplaceChild("cube_r90", CubeListBuilder.create().texOffs(100, 20).addBox(-16.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-16.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-17.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-17.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-18.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-18.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-19.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-19.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-20.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 113).addBox(-20.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(17.3462F, -16.775F, -8.0897F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r91 = glow_light8.addOrReplaceChild("cube_r91", CubeListBuilder.create().texOffs(98, 21).addBox(-16.375F, -4.7424F, 6.5998F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(98, 21).addBox(-17.475F, -4.7424F, 6.5998F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(98, 21).addBox(-18.675F, -4.7424F, 6.5998F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(98, 21).addBox(-19.875F, -4.7424F, 6.5998F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(98, 21).addBox(-21.125F, -4.7424F, 6.5998F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(98, 21).addBox(-22.375F, -4.7424F, 6.5998F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(98, 21).addBox(-23.625F, -4.7424F, 6.5998F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(19.0462F, -16.375F, -6.1397F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r92 = glow_light8.addOrReplaceChild("cube_r92", CubeListBuilder.create().texOffs(100, 20).addBox(-16.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-16.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-17.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-17.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-18.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-18.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-19.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-19.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 20).addBox(-20.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(63, 121).addBox(-20.875F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(17.3462F, -16.775F, -7.5897F, -0.0873F, 0.0F, 0.0F));

		PartDefinition fast_return = side_s.addOrReplaceChild("fast_return", CubeListBuilder.create(), PartPose.offset(0.0F, 20.256F, 3.2127F));

		PartDefinition return_slider_x = fast_return.addOrReplaceChild("return_slider_x", CubeListBuilder.create(), PartPose.offset(3.8712F, -19.6849F, -2.2545F));

		PartDefinition cube_r93 = return_slider_x.addOrReplaceChild("cube_r93", CubeListBuilder.create().texOffs(47, 121).addBox(-0.5F, -1.5F, -1.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition return_plate = fast_return.addOrReplaceChild("return_plate", CubeListBuilder.create(), PartPose.offset(5.4212F, -19.9702F, -1.4571F));

		PartDefinition cube_r94 = return_plate.addOrReplaceChild("cube_r94", CubeListBuilder.create().texOffs(56, 5).addBox(-1.5F, -1.0F, -2.0F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r95 = return_plate.addOrReplaceChild("cube_r95", CubeListBuilder.create().texOffs(56, 5).addBox(-2.2F, -0.8083F, -3.1916F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.7F, 0.025F, 2.15F, -0.1071F, -0.6169F, 0.0621F));

		PartDefinition cube_r96 = return_plate.addOrReplaceChild("cube_r96", CubeListBuilder.create().texOffs(45, 29).addBox(-2.175F, -0.8061F, -3.2165F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.525F, -0.25F, 1.475F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r97 = return_plate.addOrReplaceChild("cube_r97", CubeListBuilder.create().texOffs(33, 15).addBox(-1.175F, -0.8061F, -4.2165F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-1.375F, -0.075F, 4.2F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r98 = return_plate.addOrReplaceChild("cube_r98", CubeListBuilder.create().texOffs(33, 15).addBox(-1.175F, -0.8061F, -4.2165F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(33, 15).addBox(-1.675F, -0.8061F, -4.2165F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.875F, -0.075F, 3.7F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r99 = return_plate.addOrReplaceChild("cube_r99", CubeListBuilder.create().texOffs(33, 15).addBox(-1.175F, -0.8061F, -4.2165F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(33, 15).addBox(-1.675F, -0.8061F, -4.2165F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(33, 15).addBox(-2.175F, -0.8061F, -4.2165F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.375F, -0.075F, 3.175F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r100 = return_plate.addOrReplaceChild("cube_r100", CubeListBuilder.create().texOffs(56, 5).addBox(-2.175F, -0.8061F, -4.2165F, 3.0F, 2.0F, 5.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.725F, 0.0F, 2.225F, -0.0873F, 0.0F, 0.0F));

		PartDefinition telepathics = side_s.addOrReplaceChild("telepathics", CubeListBuilder.create(), PartPose.offset(0.0F, 20.256F, 3.4627F));

		PartDefinition cube_r101 = telepathics.addOrReplaceChild("cube_r101", CubeListBuilder.create().texOffs(45, 29).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(45, 29).addBox(-5.35F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(1.8962F, -19.5979F, 3.4453F, -0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r102 = telepathics.addOrReplaceChild("cube_r102", CubeListBuilder.create().texOffs(44, 28).addBox(-11.675F, -4.8F, 7.95F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(44, 28).addBox(-15.075F, -4.8F, 7.95F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(12.2712F, -16.1052F, -4.5603F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r103 = telepathics.addOrReplaceChild("cube_r103", CubeListBuilder.create().texOffs(44, 28).addBox(-12.675F, -4.8F, 7.95F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(10.5712F, -16.0666F, -4.4663F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r104 = telepathics.addOrReplaceChild("cube_r104", CubeListBuilder.create().texOffs(44, 28).addBox(-14.675F, -4.8F, 7.95F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(44, 28).addBox(-16.775F, -4.8F, 7.95F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(13.1962F, -16.1363F, -5.2633F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r105 = telepathics.addOrReplaceChild("cube_r105", CubeListBuilder.create().texOffs(37, 30).addBox(-14.675F, -4.8F, 7.95F, 8.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(10.6462F, -16.475F, -6.8397F, -0.0873F, 0.0F, 0.0F));

		PartDefinition glow_lamps = telepathics.addOrReplaceChild("glow_lamps", CubeListBuilder.create(), PartPose.offset(-2.2538F, -19.7624F, 3.3069F));

		PartDefinition cube_r106 = glow_lamps.addOrReplaceChild("cube_r106", CubeListBuilder.create().texOffs(85, 120).addBox(0.0F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(1.175F, 0.3626F, 0.4197F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r107 = glow_lamps.addOrReplaceChild("cube_r107", CubeListBuilder.create().texOffs(15, 46).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.95F))
		.texOffs(15, 46).addBox(-5.25F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(4.15F, 0.0F, 0.0F, -0.9599F, 0.0F, 0.0F));

		PartDefinition eye = telepathics.addOrReplaceChild("eye", CubeListBuilder.create(), PartPose.offset(-0.5788F, -19.5498F, 4.2517F));

		PartDefinition cube_r108 = eye.addOrReplaceChild("cube_r108", CubeListBuilder.create().texOffs(2, 28).addBox(-1.0F, -1.0F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r109 = eye.addOrReplaceChild("cube_r109", CubeListBuilder.create().texOffs(2, 28).addBox(0.0F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-1.4F, 0.05F, -0.25F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r110 = eye.addOrReplaceChild("cube_r110", CubeListBuilder.create().texOffs(2, 28).addBox(0.0F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(2, 28).addBox(1.05F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-1.025F, -0.0372F, -1.8712F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r111 = eye.addOrReplaceChild("cube_r111", CubeListBuilder.create().texOffs(2, 28).addBox(0.0F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.4F, -0.0372F, -0.6712F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r112 = eye.addOrReplaceChild("cube_r112", CubeListBuilder.create().texOffs(2, 28).addBox(0.0F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.4F, -0.0023F, -0.2727F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r113 = eye.addOrReplaceChild("cube_r113", CubeListBuilder.create().texOffs(2, 28).addBox(0.0F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-1.4F, 0.0151F, -0.6485F, -0.0873F, 0.0F, 0.0F));

		PartDefinition comms = side_s.addOrReplaceChild("comms", CubeListBuilder.create(), PartPose.offset(-0.75F, 20.456F, 2.9627F));

		PartDefinition cube_r114 = comms.addOrReplaceChild("cube_r114", CubeListBuilder.create().texOffs(1, 29).addBox(-15.375F, -4.8F, 6.2F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(10.6962F, -16.925F, -8.7897F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r115 = comms.addOrReplaceChild("cube_r115", CubeListBuilder.create().texOffs(82, 8).addBox(-15.375F, -4.8F, 6.2F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(10.6962F, -17.025F, -8.0397F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r116 = comms.addOrReplaceChild("cube_r116", CubeListBuilder.create().texOffs(82, 8).addBox(-15.375F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(82, 8).addBox(-13.975F, -4.8F, 6.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(82, 8).addBox(-15.175F, -4.8F, 6.2F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(10.4962F, -17.025F, -7.6397F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r117 = comms.addOrReplaceChild("cube_r117", CubeListBuilder.create().texOffs(82, 8).addBox(-15.375F, -4.8F, 6.2F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(10.6962F, -17.025F, -7.2397F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r118 = comms.addOrReplaceChild("cube_r118", CubeListBuilder.create().texOffs(44, 28).addBox(-16.375F, -4.8F, 4.2F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(11.6962F, -16.7599F, -7.4882F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r119 = comms.addOrReplaceChild("cube_r119", CubeListBuilder.create().texOffs(44, 28).addBox(-16.375F, -4.8F, 4.2F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(11.6962F, -16.5158F, -4.6989F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r120 = comms.addOrReplaceChild("cube_r120", CubeListBuilder.create().texOffs(44, 28).addBox(-16.375F, -4.8F, 4.2F, 4.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(11.1962F, -16.725F, -7.0897F, -0.0873F, 0.0F, 0.0F));

		PartDefinition side_sw = partdefinition.addOrReplaceChild("side_sw", CubeListBuilder.create(), PartPose.offsetAndRotation(12.0F, 9.5F, 7.0F, -2.7925F, -1.0472F, 3.1416F));

		PartDefinition randomizer = side_sw.addOrReplaceChild("randomizer", CubeListBuilder.create(), PartPose.offset(-0.0622F, 20.196F, 3.5631F));

		PartDefinition bone = randomizer.addOrReplaceChild("bone", CubeListBuilder.create(), PartPose.offset(0.2712F, -19.813F, -0.2195F));

		PartDefinition cube_r121 = bone.addOrReplaceChild("cube_r121", CubeListBuilder.create().texOffs(32, 28).addBox(-1.5F, -1.0294F, -2.2494F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-0.2121F, -0.1567F, -0.5163F, 0.0393F, -2.0944F, 0.0F));

		PartDefinition cube_r122 = bone.addOrReplaceChild("cube_r122", CubeListBuilder.create().texOffs(32, 28).addBox(-1.5F, -1.0294F, -2.2494F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-0.2121F, -0.1567F, -0.4794F, 0.0393F, -1.0472F, 0.0F));

		PartDefinition cube_r123 = bone.addOrReplaceChild("cube_r123", CubeListBuilder.create().texOffs(32, 28).addBox(-1.5F, -1.0294F, -2.2494F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-0.1483F, -0.1567F, -0.5163F, 0.0393F, 2.0944F, 0.0F));

		PartDefinition cube_r124 = bone.addOrReplaceChild("cube_r124", CubeListBuilder.create().texOffs(32, 28).addBox(-1.5F, -1.0294F, -2.2494F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-0.1483F, -0.1567F, -0.4794F, 0.0393F, 1.0472F, 0.0F));

		PartDefinition cube_r125 = bone.addOrReplaceChild("cube_r125", CubeListBuilder.create().texOffs(32, 28).addBox(-1.0F, -1.5F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-0.1802F, -0.0616F, -2.2303F, -2.1075F, 0.0F, 0.0F));

		PartDefinition cube_r126 = bone.addOrReplaceChild("cube_r126", CubeListBuilder.create().texOffs(32, 28).addBox(-1.5F, -1.0294F, -2.2494F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-0.1802F, -0.1567F, -0.461F, 0.0393F, 0.0F, 0.0F));

		PartDefinition cube_r127 = bone.addOrReplaceChild("cube_r127", CubeListBuilder.create().texOffs(32, 28).addBox(-1.5F, -1.0294F, -2.2494F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-0.1802F, -0.0781F, 1.0046F, 0.0393F, 0.0F, 0.0F));

		PartDefinition globe_lift_spin = randomizer.addOrReplaceChild("globe_lift_spin", CubeListBuilder.create(), PartPose.offset(0.0428F, -19.7604F, -0.6273F));

		PartDefinition cube_r128 = globe_lift_spin.addOrReplaceChild("cube_r128", CubeListBuilder.create().texOffs(15, 112).addBox(-1.95F, -2.2F, -2.075F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(12, 107).addBox(-1.95F, -2.2F, 0.175F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(12, 107).addBox(-2.55F, -2.2F, -1.45F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(12, 107).addBox(-0.3F, -2.15F, -1.45F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.3332F, 0.4486F, 0.3161F, 0.1221F, -0.784F, -0.074F));

		PartDefinition cube_r129 = globe_lift_spin.addOrReplaceChild("cube_r129", CubeListBuilder.create().texOffs(12, 108).addBox(-2.375F, -2.5F, -2.075F, 4.0F, 4.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.2283F, 0.3446F, 0.3424F, 0.1221F, -0.784F, -0.074F));

		PartDefinition dimention = side_sw.addOrReplaceChild("dimention", CubeListBuilder.create(), PartPose.offset(-0.0622F, 19.596F, 2.8131F));

		PartDefinition dim_readout_rotate_x = dimention.addOrReplaceChild("dim_readout_rotate_x", CubeListBuilder.create(), PartPose.offsetAndRotation(0.1462F, -19.5564F, 4.7496F, -0.1745F, 0.0F, 0.0F));

		PartDefinition cube_r130 = dim_readout_rotate_x.addOrReplaceChild("cube_r130", CubeListBuilder.create().texOffs(93, 20).addBox(-3.5F, -1.0F, -1.5F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dim_text = dim_readout_rotate_x.addOrReplaceChild("dim_text", CubeListBuilder.create(), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition dimframe = dimention.addOrReplaceChild("dimframe", CubeListBuilder.create(), PartPose.offsetAndRotation(0.1462F, -19.3764F, 4.5901F, 0.48F, 0.0F, 0.0F));

		PartDefinition cube_r131 = dimframe.addOrReplaceChild("cube_r131", CubeListBuilder.create().texOffs(36, 29).addBox(1.55F, -1.075F, -1.975F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.35F, -0.0923F, 0.3979F, 0.1702F, 0.0F, 0.0F));

		PartDefinition cube_r132 = dimframe.addOrReplaceChild("cube_r132", CubeListBuilder.create().texOffs(36, 29).addBox(-1.0F, -1.075F, -1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(2.9F, -0.0891F, 0.323F, 0.1702F, 0.0F, 0.0F));

		PartDefinition cube_r133 = dimframe.addOrReplaceChild("cube_r133", CubeListBuilder.create().texOffs(36, 29).addBox(-4.25F, -1.0F, -1.175F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.35F, -0.0596F, -0.3513F, 0.0436F, 0.0F, 0.0F));

		PartDefinition cube_r134 = dimframe.addOrReplaceChild("cube_r134", CubeListBuilder.create().texOffs(36, 29).addBox(-4.25F, -1.0F, -0.675F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 29).addBox(-3.85F, -1.15F, 0.5F, 7.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 29).addBox(-3.85F, -0.8F, -0.925F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 29).addBox(-3.85F, -1.0F, -1.325F, 7.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.35F, -0.0553F, -0.4512F, 0.0436F, 0.0F, 0.0F));

		PartDefinition dummy_bits2 = side_sw.addOrReplaceChild("dummy_bits2", CubeListBuilder.create(), PartPose.offset(5.609F, 0.1989F, 0.9373F));

		PartDefinition dummy_lights = dummy_bits2.addOrReplaceChild("dummy_lights", CubeListBuilder.create(), PartPose.offset(-5.6712F, 19.6471F, 2.1257F));

		PartDefinition cube_r135 = dummy_lights.addOrReplaceChild("cube_r135", CubeListBuilder.create().texOffs(31, 29).addBox(0.5F, -1.0F, -1.0F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-5.6346F, -19.9158F, -1.8029F, -0.1873F, -1.0836F, 0.1659F));

		PartDefinition cube_r136 = dummy_lights.addOrReplaceChild("cube_r136", CubeListBuilder.create().texOffs(37, 29).addBox(-0.5F, -0.925F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-3.9018F, -19.9974F, -1.8752F, -0.1427F, 0.9114F, -0.1131F));

		PartDefinition cube_r137 = dummy_lights.addOrReplaceChild("cube_r137", CubeListBuilder.create().texOffs(37, 29).addBox(-2.5F, -0.925F, -1.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-4.0235F, -19.9836F, -1.7172F, -0.1427F, 0.9114F, -0.1131F));

		PartDefinition cube_r138 = dummy_lights.addOrReplaceChild("cube_r138", CubeListBuilder.create().texOffs(29, 28).addBox(-6.5F, -1.0F, -1.0F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.0288F, -20.0161F, -2.4493F, -0.0873F, 0.0F, 0.0F));

		PartDefinition glow_lamps3 = dummy_lights.addOrReplaceChild("glow_lamps3", CubeListBuilder.create(), PartPose.offset(-2.8297F, -19.6125F, -0.1314F));

		PartDefinition cube_r139 = glow_lamps3.addOrReplaceChild("cube_r139", CubeListBuilder.create().texOffs(14, 46).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.1221F, -0.784F, -0.074F));

		PartDefinition cube_r140 = glow_lamps3.addOrReplaceChild("cube_r140", CubeListBuilder.create().texOffs(14, 46).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(5.825F, 0.0F, -0.025F, 0.1221F, -0.784F, -0.074F));

		PartDefinition cube_r141 = glow_lamps3.addOrReplaceChild("cube_r141", CubeListBuilder.create().texOffs(14, 46).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.4F, 0.1F, 1.65F, 0.1221F, -0.784F, -0.074F));

		PartDefinition cube_r142 = glow_lamps3.addOrReplaceChild("cube_r142", CubeListBuilder.create().texOffs(14, 46).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(5.45F, 0.1F, 1.65F, 0.1221F, -0.784F, -0.074F));

		PartDefinition cube_r143 = glow_lamps3.addOrReplaceChild("cube_r143", CubeListBuilder.create().texOffs(14, 46).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(4.175F, 0.2F, 2.775F, 0.1221F, -0.784F, -0.074F));

		PartDefinition cube_r144 = glow_lamps3.addOrReplaceChild("cube_r144", CubeListBuilder.create().texOffs(14, 46).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(1.675F, 0.2F, 2.775F, 0.1221F, -0.784F, -0.074F));

		PartDefinition dummy_knobs = dummy_bits2.addOrReplaceChild("dummy_knobs", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition dummyknob_rotate_y6 = dummy_knobs.addOrReplaceChild("dummyknob_rotate_y6", CubeListBuilder.create(), PartPose.offsetAndRotation(-11.25F, 0.0F, 0.0F, 0.0862F, 0.8038F, 0.0371F));

		PartDefinition cube_r145 = dummyknob_rotate_y6.addOrReplaceChild("cube_r145", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.75F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.98F))
		.texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, -0.7854F, 0.0F));

		PartDefinition cube_r146 = dummyknob_rotate_y6.addOrReplaceChild("cube_r146", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.525F, -0.575F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0352F, -0.0044F, -0.0352F, -0.0873F, 0.0F, 0.0F));

		PartDefinition dummyknob_rotate_y5 = dummy_knobs.addOrReplaceChild("dummyknob_rotate_y5", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.25F, 0.0F, 0.0F, 0.0862F, 0.8038F, 0.0371F));

		PartDefinition cube_r147 = dummyknob_rotate_y5.addOrReplaceChild("cube_r147", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.75F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.98F))
		.texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, -0.7854F, 0.0F));

		PartDefinition cube_r148 = dummyknob_rotate_y5.addOrReplaceChild("cube_r148", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.525F, -0.575F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0352F, -0.0044F, -0.0352F, -0.0873F, 0.0F, 0.0F));

		PartDefinition dummyknob_rotate_y4 = dummy_knobs.addOrReplaceChild("dummyknob_rotate_y4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0317F, -0.3477F, 0.0928F));

		PartDefinition cube_r149 = dummyknob_rotate_y4.addOrReplaceChild("cube_r149", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.75F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.98F))
		.texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, -0.7854F, 0.0F));

		PartDefinition cube_r150 = dummyknob_rotate_y4.addOrReplaceChild("cube_r150", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.525F, -0.575F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0352F, -0.0044F, -0.0352F, -0.0873F, 0.0F, 0.0F));

		PartDefinition side_nw = partdefinition.addOrReplaceChild("side_nw", CubeListBuilder.create(), PartPose.offsetAndRotation(12.5F, 9.0F, -7.25F, 0.3491F, -1.0472F, 0.0F));

		PartDefinition throttle = side_nw.addOrReplaceChild("throttle", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.7082F, -1.4428F, 2.5388F, 0.0F, 0.0F, 0.0F));

		PartDefinition throttle_holder = throttle.addOrReplaceChild("throttle_holder", CubeListBuilder.create(), PartPose.offset(0.625F, 2.8099F, 1.05F));

		PartDefinition cube_r151 = throttle_holder.addOrReplaceChild("cube_r151", CubeListBuilder.create().texOffs(81, 4).addBox(-0.625F, -1.7666F, -2.7027F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(81, 4).addBox(-1.375F, -1.7666F, -2.7027F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.1091F, 0.0F, 0.0F));

		PartDefinition cube_r152 = throttle_holder.addOrReplaceChild("cube_r152", CubeListBuilder.create().texOffs(81, 4).addBox(-0.625F, -1.7666F, -0.2973F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(81, 4).addBox(-1.375F, -1.7666F, -0.2973F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.3272F, 0.0F, 0.0F));

		PartDefinition cube_r153 = throttle_holder.addOrReplaceChild("cube_r153", CubeListBuilder.create().texOffs(81, 4).addBox(-0.625F, -1.6349F, -1.5F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(81, 4).addBox(-1.375F, -1.6349F, -1.5F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.1091F, 0.0F, 0.0F));

		PartDefinition cube_r154 = throttle_holder.addOrReplaceChild("cube_r154", CubeListBuilder.create().texOffs(44, 28).addBox(-0.375F, 0.4F, -1.45F, 2.0F, 2.0F, 5.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.625F, -1.8599F, -1.05F, -0.0873F, 0.0F, 0.0F));

		PartDefinition throttleknob_rotate_x = throttle.addOrReplaceChild("throttleknob_rotate_x", CubeListBuilder.create(), PartPose.offsetAndRotation(0.6F, 7.4291F, 1.1341F, 0.3054F, 0.0F, 0.0F));

		PartDefinition cube_r155 = throttleknob_rotate_x.addOrReplaceChild("cube_r155", CubeListBuilder.create().texOffs(47, 29).addBox(-0.925F, -0.85F, 0.25F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(-0.6F, -7.0791F, -1.1341F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r156 = throttleknob_rotate_x.addOrReplaceChild("cube_r156", CubeListBuilder.create().texOffs(44, 17).addBox(-0.375F, -0.6F, 0.25F, 2.0F, 6.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.6F, -6.9791F, -1.1341F, -0.0873F, 0.0F, 0.0F));

		PartDefinition handbreak = side_nw.addOrReplaceChild("handbreak", CubeListBuilder.create(), PartPose.offset(0.1957F, 20.1822F, 2.5286F));

		PartDefinition hb_plate = handbreak.addOrReplaceChild("hb_plate", CubeListBuilder.create(), PartPose.offset(11.1962F, -16.725F, -6.3397F));

		PartDefinition cube_r157 = hb_plate.addOrReplaceChild("cube_r157", CubeListBuilder.create().texOffs(44, 28).addBox(-8.675F, -4.225F, 3.7F, 3.0F, 2.0F, 5.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition brake_light_off = hb_plate.addOrReplaceChild("brake_light_off", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r158 = brake_light_off.addOrReplaceChild("cube_r158", CubeListBuilder.create().texOffs(2, 123).addBox(-8.4F, -4.45F, 3.95F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition brake_light_on = hb_plate.addOrReplaceChild("brake_light_on", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r159 = brake_light_on.addOrReplaceChild("cube_r159", CubeListBuilder.create().texOffs(47, 121).addBox(-7.9F, -4.45F, 3.95F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition hb_switch_rotate_y = handbreak.addOrReplaceChild("hb_switch_rotate_y", CubeListBuilder.create(), PartPose.offsetAndRotation(4.0462F, -19.4369F, 0.4902F, -0.0742F, 0.0F, 0.0F));

		PartDefinition cube_r160 = hb_switch_rotate_y.addOrReplaceChild("cube_r160", CubeListBuilder.create().texOffs(5, 28).addBox(-1.0F, -1.0F, -1.5F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.8552F, 0.0F, 0.0F));

		PartDefinition cube_r161 = hb_switch_rotate_y.addOrReplaceChild("cube_r161", CubeListBuilder.create().texOffs(7, 28).addBox(-7.9F, -4.45F, 3.95F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(6.9F, 2.9619F, -5.7299F, -0.0873F, 0.0F, 0.0F));

		PartDefinition door = side_nw.addOrReplaceChild("door", CubeListBuilder.create(), PartPose.offset(-1.1543F, 20.4322F, 1.7786F));

		PartDefinition door_marker = door.addOrReplaceChild("door_marker", CubeListBuilder.create(), PartPose.offset(-4.2855F, -19.3413F, 1.1809F));

		PartDefinition cube_r162 = door_marker.addOrReplaceChild("cube_r162", CubeListBuilder.create().texOffs(44, 28).addBox(-1.0543F, -1.802F, -0.1515F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(1.025F, 0.0F, -1.125F, -0.4363F, 0.7418F, 0.0F));

		PartDefinition cube_r163 = door_marker.addOrReplaceChild("cube_r163", CubeListBuilder.create().texOffs(44, 28).addBox(-1.0F, -1.175F, -1.5F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.48F, -0.7854F, 0.0F));

		PartDefinition doorknob_rotate_y = door.addOrReplaceChild("doorknob_rotate_y", CubeListBuilder.create(), PartPose.offsetAndRotation(-3.2288F, -19.4721F, 0.1243F, 0.0F, 0.1309F, 0.0F));

		PartDefinition cube_r164 = doorknob_rotate_y.addOrReplaceChild("cube_r164", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.75F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.98F))
		.texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, -0.7854F, 0.0F));

		PartDefinition cube_r165 = doorknob_rotate_y.addOrReplaceChild("cube_r165", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.525F, -0.575F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0352F, -0.0044F, -0.0352F, -0.0873F, 0.0F, 0.0F));

		PartDefinition dummy_bitz3 = side_nw.addOrReplaceChild("dummy_bitz3", CubeListBuilder.create(), PartPose.offset(-0.4043F, 20.4322F, 3.0286F));

		PartDefinition glow_lamps2 = dummy_bitz3.addOrReplaceChild("glow_lamps2", CubeListBuilder.create(), PartPose.offset(2.7079F, -19.2913F, 5.0048F));

		PartDefinition cube_r166 = glow_lamps2.addOrReplaceChild("cube_r166", CubeListBuilder.create().texOffs(16, 45).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(-0.5F, 0.0F, 0.0F, -0.061F, -0.7887F, -0.0064F));

		PartDefinition cube_r167 = glow_lamps2.addOrReplaceChild("cube_r167", CubeListBuilder.create().texOffs(16, 45).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(-4.15F, 0.0F, 0.0F, -0.061F, -0.7887F, -0.0064F));

		PartDefinition lamp5 = dummy_bitz3.addOrReplaceChild("lamp5", CubeListBuilder.create(), PartPose.offsetAndRotation(2.2004F, -19.3207F, 5.0112F, -0.0654F, 0.002F, 0.0305F));

		PartDefinition cube_r168 = lamp5.addOrReplaceChild("cube_r168", CubeListBuilder.create().texOffs(97, 10).addBox(-2.0F, -1.0F, -2.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.0175F, 0.0044F, 0.7186F, -0.061F, -0.7887F, -0.0064F));

		PartDefinition cube_r169 = lamp5.addOrReplaceChild("cube_r169", CubeListBuilder.create().texOffs(81, 6).addBox(-2.0F, -1.0F, -2.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.0175F, -0.2456F, 0.7186F, -0.061F, -0.7887F, -0.0064F));

		PartDefinition lamp6 = dummy_bitz3.addOrReplaceChild("lamp6", CubeListBuilder.create(), PartPose.offsetAndRotation(-1.4496F, -19.3207F, 5.0112F, -0.0654F, 0.002F, 0.0305F));

		PartDefinition cube_r170 = lamp6.addOrReplaceChild("cube_r170", CubeListBuilder.create().texOffs(97, 10).addBox(-2.0F, -1.0F, -2.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.0175F, 0.0044F, 0.7186F, -0.061F, -0.7887F, -0.0064F));

		PartDefinition cube_r171 = lamp6.addOrReplaceChild("cube_r171", CubeListBuilder.create().texOffs(81, 6).addBox(-2.0F, -1.0F, -2.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.0175F, -0.2456F, 0.7186F, -0.061F, -0.7887F, -0.0064F));

		PartDefinition button_cluster = dummy_bitz3.addOrReplaceChild("button_cluster", CubeListBuilder.create(), PartPose.offset(2.2113F, -19.0161F, 0.6778F));

		PartDefinition cube_r172 = button_cluster.addOrReplaceChild("cube_r172", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.45F, -2.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r173 = button_cluster.addOrReplaceChild("cube_r173", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.45F, -2.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r174 = button_cluster.addOrReplaceChild("cube_r174", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.4F, -2.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r175 = button_cluster.addOrReplaceChild("cube_r175", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.4F, -2.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r176 = button_cluster.addOrReplaceChild("cube_r176", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.35F, -1.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r177 = button_cluster.addOrReplaceChild("cube_r177", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.35F, -1.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r178 = button_cluster.addOrReplaceChild("cube_r178", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.3F, -1.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r179 = button_cluster.addOrReplaceChild("cube_r179", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.3F, -1.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r180 = button_cluster.addOrReplaceChild("cube_r180", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.25F, -0.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r181 = button_cluster.addOrReplaceChild("cube_r181", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.25F, -0.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r182 = button_cluster.addOrReplaceChild("cube_r182", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.25F, -0.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r183 = button_cluster.addOrReplaceChild("cube_r183", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.25F, -0.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r184 = button_cluster.addOrReplaceChild("cube_r184", CubeListBuilder.create().texOffs(61, 1).addBox(-3.925F, -1.5F, 0.15F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(61, 1).addBox(-4.125F, -1.5F, 3.25F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.8253F, 0.045F, 0.4974F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r185 = button_cluster.addOrReplaceChild("cube_r185", CubeListBuilder.create().texOffs(61, 1).addBox(-3.625F, -1.5F, -2.75F, 2.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(59, 15).addBox(-2.5F, -1.5F, -1.5F, 5.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.025F, 0.0F, 0.0F, -1.57F, -1.471F, 1.5639F));

		PartDefinition button_cluster2 = dummy_bitz3.addOrReplaceChild("button_cluster2", CubeListBuilder.create(), PartPose.offset(-1.5387F, -19.0161F, 0.6778F));

		PartDefinition cube_r186 = button_cluster2.addOrReplaceChild("cube_r186", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.45F, -2.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r187 = button_cluster2.addOrReplaceChild("cube_r187", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.45F, -2.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r188 = button_cluster2.addOrReplaceChild("cube_r188", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.4F, -2.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r189 = button_cluster2.addOrReplaceChild("cube_r189", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.4F, -2.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r190 = button_cluster2.addOrReplaceChild("cube_r190", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.35F, -1.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r191 = button_cluster2.addOrReplaceChild("cube_r191", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.35F, -1.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r192 = button_cluster2.addOrReplaceChild("cube_r192", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.3F, -1.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r193 = button_cluster2.addOrReplaceChild("cube_r193", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.3F, -1.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r194 = button_cluster2.addOrReplaceChild("cube_r194", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.25F, -0.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r195 = button_cluster2.addOrReplaceChild("cube_r195", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.25F, -0.7F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r196 = button_cluster2.addOrReplaceChild("cube_r196", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.725F, -1.25F, -0.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r197 = button_cluster2.addOrReplaceChild("cube_r197", CubeListBuilder.create().texOffs(99, 21).addBox(0.5F, -0.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.225F, -1.25F, -0.2F, -1.57F, -1.471F, 1.5639F));

		PartDefinition cube_r198 = button_cluster2.addOrReplaceChild("cube_r198", CubeListBuilder.create().texOffs(59, 15).addBox(-2.5F, -1.5F, -1.5F, 5.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.025F, 0.0F, 0.0F, -1.57F, -1.471F, 1.5639F));

		PartDefinition chunk3 = dummy_bitz3.addOrReplaceChild("chunk3", CubeListBuilder.create().texOffs(44, 28).addBox(-3.175F, -4.325F, -0.25F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.99F))
		.texOffs(44, 28).addBox(-1.675F, -3.2F, -0.6F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.99F))
		.texOffs(44, 28).addBox(-4.675F, -3.2F, -0.6F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.99F)), PartPose.offsetAndRotation(2.075F, -19.5826F, 1.4533F, -1.2654F, 0.0F, 0.0F));

		PartDefinition glow_light7 = chunk3.addOrReplaceChild("glow_light7", CubeListBuilder.create().texOffs(99, 20).addBox(-2.1F, -2.35F, -0.475F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(97, 21).addBox(-3.6F, -1.225F, -0.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(98, 21).addBox(-0.6F, -1.225F, -0.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(-0.575F, -1.475F, 0.225F));

		PartDefinition dummyknob_rotate_y2 = dummy_bitz3.addOrReplaceChild("dummyknob_rotate_y2", CubeListBuilder.create(), PartPose.offsetAndRotation(-3.2288F, -19.2221F, 1.6243F, 0.0F, 1.2654F, 0.0F));

		PartDefinition cube_r199 = dummyknob_rotate_y2.addOrReplaceChild("cube_r199", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.75F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.98F))
		.texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, -0.7854F, 0.0F));

		PartDefinition cube_r200 = dummyknob_rotate_y2.addOrReplaceChild("cube_r200", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.525F, -0.575F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0352F, -0.0044F, -0.0352F, -0.0873F, 0.0F, 0.0F));

		PartDefinition dummyknob_rotate_y3 = dummy_bitz3.addOrReplaceChild("dummyknob_rotate_y3", CubeListBuilder.create(), PartPose.offsetAndRotation(3.5212F, -19.2221F, 2.8743F, 0.0F, -0.3491F, 0.0F));

		PartDefinition cube_r201 = dummyknob_rotate_y3.addOrReplaceChild("cube_r201", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.75F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.98F))
		.texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, -0.7854F, 0.0F));

		PartDefinition cube_r202 = dummyknob_rotate_y3.addOrReplaceChild("cube_r202", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.525F, -0.575F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0352F, -0.0044F, -0.0352F, -0.0873F, 0.0F, 0.0F));

		PartDefinition side_se = partdefinition.addOrReplaceChild("side_se", CubeListBuilder.create(), PartPose.offsetAndRotation(-12.25F, 9.25F, 7.25F, -2.7925F, 1.0472F, 3.1416F));

		PartDefinition dummy_bitz = side_se.addOrReplaceChild("dummy_bitz", CubeListBuilder.create(), PartPose.offset(0.4998F, 0.4039F, 2.6119F));

		PartDefinition frills = dummy_bitz.addOrReplaceChild("frills", CubeListBuilder.create(), PartPose.offset(12.0F, 3.4923F, -3.0131F));

		PartDefinition cube_r203 = frills.addOrReplaceChild("cube_r203", CubeListBuilder.create().texOffs(64, 15).addBox(-15.375F, -4.725F, 2.2F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(2.5F, -0.125F, -1.75F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r204 = frills.addOrReplaceChild("cube_r204", CubeListBuilder.create().texOffs(79, 8).addBox(-15.375F, -4.725F, 2.2F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(2.3F, 0.053F, 1.3553F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r205 = frills.addOrReplaceChild("cube_r205", CubeListBuilder.create().texOffs(83, 7).addBox(-15.375F, -4.725F, 1.2F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(5.3F, 0.1576F, 2.5508F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r206 = frills.addOrReplaceChild("cube_r206", CubeListBuilder.create().texOffs(83, 7).addBox(-15.375F, -4.725F, 1.2F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(2.3F, 0.1576F, 2.5508F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r207 = frills.addOrReplaceChild("cube_r207", CubeListBuilder.create().texOffs(43, 30).addBox(-15.375F, -4.725F, 1.2F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(43, 30).addBox(-17.0F, -4.725F, 1.2F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(4.125F, 0.525F, 2.5F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r208 = frills.addOrReplaceChild("cube_r208", CubeListBuilder.create().texOffs(64, 15).addBox(-14.375F, -4.725F, 1.2F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(2.1F, -0.075F, -0.5F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r209 = frills.addOrReplaceChild("cube_r209", CubeListBuilder.create().texOffs(78, 7).addBox(-15.375F, -4.725F, 2.2F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(2.3F, 0.175F, 2.75F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r210 = frills.addOrReplaceChild("cube_r210", CubeListBuilder.create().texOffs(64, 15).addBox(-15.375F, -4.725F, 2.2F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(2.5F, 0.0F, -0.25F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r211 = frills.addOrReplaceChild("cube_r211", CubeListBuilder.create().texOffs(76, 3).addBox(-14.375F, -4.725F, 0.2F, 2.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(76, 3).addBox(-16.125F, -4.725F, 0.2F, 2.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition spinner = frills.addOrReplaceChild("spinner", CubeListBuilder.create(), PartPose.offset(-11.625F, -3.1994F, 5.7382F));

		PartDefinition cube_r212 = spinner.addOrReplaceChild("cube_r212", CubeListBuilder.create().texOffs(100, 21).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.48F, 0.0F, 0.0F));

		PartDefinition cube_r213 = spinner.addOrReplaceChild("cube_r213", CubeListBuilder.create().texOffs(100, 21).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, -0.0372F, -0.3397F, 0.2618F, 0.0F, 0.0F));

		PartDefinition spinner2 = frills.addOrReplaceChild("spinner2", CubeListBuilder.create(), PartPose.offset(-10.925F, -3.1994F, 5.7382F));

		PartDefinition cube_r214 = spinner2.addOrReplaceChild("cube_r214", CubeListBuilder.create().texOffs(100, 21).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.48F, 0.0F, 0.0F));

		PartDefinition cube_r215 = spinner2.addOrReplaceChild("cube_r215", CubeListBuilder.create().texOffs(100, 21).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, -0.0372F, -0.3397F, 0.2618F, 0.0F, 0.0F));

		PartDefinition spinner3 = frills.addOrReplaceChild("spinner3", CubeListBuilder.create(), PartPose.offset(-10.225F, -3.1994F, 5.7382F));

		PartDefinition cube_r216 = spinner3.addOrReplaceChild("cube_r216", CubeListBuilder.create().texOffs(100, 21).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.48F, 0.0F, 0.0F));

		PartDefinition cube_r217 = spinner3.addOrReplaceChild("cube_r217", CubeListBuilder.create().texOffs(100, 21).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, -0.0372F, -0.3397F, 0.2618F, 0.0F, 0.0F));

		PartDefinition spinner4 = frills.addOrReplaceChild("spinner4", CubeListBuilder.create(), PartPose.offset(-9.525F, -3.1994F, 5.7382F));

		PartDefinition cube_r218 = spinner4.addOrReplaceChild("cube_r218", CubeListBuilder.create().texOffs(100, 21).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.48F, 0.0F, 0.0F));

		PartDefinition cube_r219 = spinner4.addOrReplaceChild("cube_r219", CubeListBuilder.create().texOffs(100, 21).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, -0.0372F, -0.3397F, 0.2618F, 0.0F, 0.0F));

		PartDefinition grill = dummy_bitz.addOrReplaceChild("grill", CubeListBuilder.create(), PartPose.offset(12.5F, 3.5423F, -3.0131F));

		PartDefinition cube_r220 = grill.addOrReplaceChild("cube_r220", CubeListBuilder.create().texOffs(45, 27).addBox(-13.375F, -4.725F, 0.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(46, 29).addBox(-13.625F, -4.725F, 0.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(46, 29).addBox(-13.875F, -4.725F, 0.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(46, 29).addBox(-14.125F, -4.725F, 0.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(46, 29).addBox(-14.375F, -4.725F, 0.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -0.25F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r221 = grill.addOrReplaceChild("cube_r221", CubeListBuilder.create().texOffs(64, 15).addBox(-14.375F, -4.725F, 0.2F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition grill2 = dummy_bitz.addOrReplaceChild("grill2", CubeListBuilder.create(), PartPose.offset(15.7F, 3.5173F, -3.0131F));

		PartDefinition cube_r222 = grill2.addOrReplaceChild("cube_r222", CubeListBuilder.create().texOffs(45, 27).addBox(-13.375F, -4.725F, 0.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(46, 29).addBox(-13.625F, -4.725F, 0.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(46, 29).addBox(-13.875F, -4.725F, 0.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(46, 29).addBox(-14.125F, -4.725F, 0.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(46, 29).addBox(-14.375F, -4.725F, 0.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -0.25F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r223 = grill2.addOrReplaceChild("cube_r223", CubeListBuilder.create().texOffs(64, 15).addBox(-14.375F, -4.725F, 0.2F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition glow_lamp = dummy_bitz.addOrReplaceChild("glow_lamp", CubeListBuilder.create(), PartPose.offset(14.1F, 3.4173F, -3.5131F));

		PartDefinition cube_r224 = glow_lamp.addOrReplaceChild("cube_r224", CubeListBuilder.create().texOffs(16, 46).addBox(-14.375F, -4.725F, 2.2F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.5F, 0.15F, -0.525F, -0.0873F, 0.0F, 0.0F));

		PartDefinition lamp = dummy_bitz.addOrReplaceChild("lamp", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.2F, 0.218F, 3.0383F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r225 = lamp.addOrReplaceChild("cube_r225", CubeListBuilder.create().texOffs(50, 30).addBox(-14.375F, -3.725F, 5.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(13.375F, 2.1743F, -6.3014F, -0.0873F, 0.0F, 0.0F));

		PartDefinition glow_light9 = lamp.addOrReplaceChild("glow_light9", CubeListBuilder.create(), PartPose.offset(13.375F, 2.1743F, -6.5264F));

		PartDefinition cube_r226 = glow_light9.addOrReplaceChild("cube_r226", CubeListBuilder.create().texOffs(99, 21).addBox(-14.375F, -3.725F, 5.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition lamp2 = dummy_bitz.addOrReplaceChild("lamp2", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.2F, 0.043F, 1.6633F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r227 = lamp2.addOrReplaceChild("cube_r227", CubeListBuilder.create().texOffs(50, 30).addBox(-14.375F, -3.725F, 5.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(13.375F, 2.1743F, -6.3014F, -0.0873F, 0.0F, 0.0F));

		PartDefinition glow_light10 = lamp2.addOrReplaceChild("glow_light10", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r228 = glow_light10.addOrReplaceChild("cube_r228", CubeListBuilder.create().texOffs(96, 21).addBox(-14.375F, -3.725F, 5.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(13.375F, 2.1743F, -6.5264F, -0.0873F, 0.0F, 0.0F));

		PartDefinition lamp3 = dummy_bitz.addOrReplaceChild("lamp3", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.2F, -0.157F, 0.2133F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r229 = lamp3.addOrReplaceChild("cube_r229", CubeListBuilder.create().texOffs(50, 30).addBox(-14.375F, -3.725F, 5.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(13.375F, 2.1743F, -6.3014F, -0.0873F, 0.0F, 0.0F));

		PartDefinition glow_light11 = lamp3.addOrReplaceChild("glow_light11", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r230 = glow_light11.addOrReplaceChild("cube_r230", CubeListBuilder.create().texOffs(96, 21).addBox(-14.375F, -3.725F, 5.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(13.375F, 2.1743F, -6.5264F, -0.0873F, 0.0F, 0.0F));

		PartDefinition lamp4 = dummy_bitz.addOrReplaceChild("lamp4", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.2F, -0.157F, -1.1117F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r231 = lamp4.addOrReplaceChild("cube_r231", CubeListBuilder.create().texOffs(50, 30).addBox(-14.375F, -3.725F, 5.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(13.375F, 2.1743F, -6.3014F, -0.0873F, 0.0F, 0.0F));

		PartDefinition glow_light12 = lamp4.addOrReplaceChild("glow_light12", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r232 = glow_light12.addOrReplaceChild("cube_r232", CubeListBuilder.create().texOffs(96, 21).addBox(-14.375F, -3.725F, 5.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(13.375F, 2.1743F, -6.5264F, -0.0873F, 0.0F, 0.0F));

		PartDefinition sonic = side_se.addOrReplaceChild("sonic", CubeListBuilder.create(), PartPose.offset(0.1537F, 20.2467F, 8.1994F));

		PartDefinition cube_r233 = sonic.addOrReplaceChild("cube_r233", CubeListBuilder.create().texOffs(81, 7).addBox(-12.55F, -4.9F, 4.375F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(81, 7).addBox(-12.55F, -4.9F, 6.125F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(81, 7).addBox(-12.925F, -4.925F, 4.7F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(37, 30).addBox(-13.975F, -4.375F, 5.7F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(32, 30).addBox(-15.975F, -4.375F, 5.2F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(39, 29).addBox(-9.975F, -4.375F, 3.625F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(81, 7).addBox(-11.175F, -4.925F, 4.7F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(11.0962F, -16.6005F, -7.1006F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r234 = sonic.addOrReplaceChild("cube_r234", CubeListBuilder.create().texOffs(41, 28).addBox(-14.225F, -4.375F, 7.45F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(11.1462F, -16.6135F, -7.2501F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r235 = sonic.addOrReplaceChild("cube_r235", CubeListBuilder.create().texOffs(41, 28).addBox(-14.225F, -4.375F, 7.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(11.1462F, -16.6092F, -7.2003F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r236 = sonic.addOrReplaceChild("cube_r236", CubeListBuilder.create().texOffs(41, 28).addBox(-14.225F, -4.375F, 6.95F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(11.1462F, -16.6048F, -7.1504F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r237 = sonic.addOrReplaceChild("cube_r237", CubeListBuilder.create().texOffs(41, 28).addBox(-14.225F, -4.375F, 5.7F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(11.1462F, -16.6005F, -7.1006F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r238 = sonic.addOrReplaceChild("cube_r238", CubeListBuilder.create().texOffs(43, 29).addBox(-11.175F, -4.375F, 5.7F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(11.0962F, -16.607F, -7.1754F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r239 = sonic.addOrReplaceChild("cube_r239", CubeListBuilder.create().texOffs(44, 28).addBox(-12.675F, -4.8F, 4.7F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(11.1962F, -16.725F, -7.0897F, -0.0873F, 0.0F, 0.0F));

		PartDefinition facing = side_se.addOrReplaceChild("facing", CubeListBuilder.create(), PartPose.offset(-1.1463F, 20.2467F, 2.4494F));

		PartDefinition cube_r240 = facing.addOrReplaceChild("cube_r240", CubeListBuilder.create().texOffs(46, 121).addBox(-1.5F, -1.0F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-3.0468F, -19.945F, -0.2836F, -0.0524F, -0.7854F, 0.0F));

		PartDefinition cube_r241 = facing.addOrReplaceChild("cube_r241", CubeListBuilder.create().texOffs(81, 7).addBox(-1.5F, -1.0F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-3.0468F, -20.045F, -0.2836F, -0.0524F, 0.0F, 0.0F));

		PartDefinition facing_knob_rotate_y = facing.addOrReplaceChild("facing_knob_rotate_y", CubeListBuilder.create(), PartPose.offset(-3.0359F, -20.4447F, -0.2761F));

		PartDefinition cube_r242 = facing_knob_rotate_y.addOrReplaceChild("cube_r242", CubeListBuilder.create().texOffs(46, 29).addBox(-1.25F, -1.0F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0112F, 0.0625F, 0.1181F, 0.0F, 0.7854F, 0.0F));

		PartDefinition cube_r243 = facing_knob_rotate_y.addOrReplaceChild("cube_r243", CubeListBuilder.create().texOffs(70, 17).addBox(-1.0F, -1.375F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(69, 16).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.0F, 0.1875F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition landing = side_se.addOrReplaceChild("landing", CubeListBuilder.create(), PartPose.offset(5.0063F, 0.5004F, 2.182F));

		PartDefinition cube_r244 = landing.addOrReplaceChild("cube_r244", CubeListBuilder.create().texOffs(50, 122).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(-0.0039F, 0.0256F, -0.7116F, -0.0924F, -0.7833F, 0.0653F));

		PartDefinition cube_r245 = landing.addOrReplaceChild("cube_r245", CubeListBuilder.create().texOffs(47, 122).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(-0.0039F, 0.1756F, 0.6884F, -0.0924F, -0.7833F, 0.0653F));

		PartDefinition cube_r246 = landing.addOrReplaceChild("cube_r246", CubeListBuilder.create().texOffs(53, 29).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.65F)), PartPose.offsetAndRotation(0.0149F, -0.2234F, 0.0011F, -0.1288F, -0.7854F, 0.0948F));

		PartDefinition cube_r247 = landing.addOrReplaceChild("cube_r247", CubeListBuilder.create().texOffs(77, 5).addBox(-1.4788F, -0.992F, -2.8312F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.9526F, -0.1287F, 0.9424F, -0.1288F, -0.7854F, 0.0948F));

		PartDefinition landing_switch_rotate_x = landing.addOrReplaceChild("landing_switch_rotate_x", CubeListBuilder.create().texOffs(8, 29).addBox(-1.0F, -1.05F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(8, 29).addBox(-1.0F, -1.325F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(46, 120).addBox(-1.0F, -1.525F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(8, 29).addBox(-1.0F, -1.725F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0149F, -0.5234F, 0.0011F, 0.4363F, 0.0F, 0.0F));

		PartDefinition side_ne = partdefinition.addOrReplaceChild("side_ne", CubeListBuilder.create(), PartPose.offsetAndRotation(-12.25F, 9.5F, -7.25F, 0.3491F, 1.0472F, 0.0F));

		PartDefinition x = side_ne.addOrReplaceChild("x", CubeListBuilder.create(), PartPose.offset(-2.4037F, 20.2419F, 2.5928F));

		PartDefinition cube_r248 = x.addOrReplaceChild("cube_r248", CubeListBuilder.create().texOffs(43, 29).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0212F, -19.3081F, 5.3562F, -1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r249 = x.addOrReplaceChild("cube_r249", CubeListBuilder.create().texOffs(80, 6).addBox(-12.425F, -4.8F, 8.45F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(10.9462F, -16.725F, -6.3397F, -0.0873F, 0.0F, 0.0F));

		PartDefinition x_light = x.addOrReplaceChild("x_light", CubeListBuilder.create(), PartPose.offset(10.9462F, -16.95F, -6.3397F));

		PartDefinition cube_r250 = x_light.addOrReplaceChild("cube_r250", CubeListBuilder.create().texOffs(40, 120).addBox(-12.425F, -4.8F, 8.425F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.98F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r251 = x_light.addOrReplaceChild("cube_r251", CubeListBuilder.create().texOffs(14, 46).addBox(-1.475F, -1.5F, -1.6F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-10.925F, -2.5081F, 11.5959F, -1.0036F, 0.0F, 0.0F));

		PartDefinition y = side_ne.addOrReplaceChild("y", CubeListBuilder.create(), PartPose.offset(-0.1537F, 20.2419F, 3.3428F));

		PartDefinition cube_r252 = y.addOrReplaceChild("cube_r252", CubeListBuilder.create().texOffs(43, 29).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0212F, -19.3081F, 5.3562F, -1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r253 = y.addOrReplaceChild("cube_r253", CubeListBuilder.create().texOffs(80, 6).addBox(-12.425F, -4.8F, 8.45F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(10.9462F, -16.725F, -6.3397F, -0.0873F, 0.0F, 0.0F));

		PartDefinition y_light = y.addOrReplaceChild("y_light", CubeListBuilder.create(), PartPose.offset(10.9462F, -16.95F, -6.3397F));

		PartDefinition cube_r254 = y_light.addOrReplaceChild("cube_r254", CubeListBuilder.create().texOffs(2, 112).addBox(-12.425F, -4.8F, 8.425F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.98F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r255 = y_light.addOrReplaceChild("cube_r255", CubeListBuilder.create().texOffs(14, 46).addBox(-1.475F, -1.5F, -1.6F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-10.925F, -2.5081F, 11.5959F, -1.0036F, 0.0F, 0.0F));

		PartDefinition z = side_ne.addOrReplaceChild("z", CubeListBuilder.create(), PartPose.offset(2.0963F, 20.2419F, 2.5928F));

		PartDefinition cube_r256 = z.addOrReplaceChild("cube_r256", CubeListBuilder.create().texOffs(43, 29).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0212F, -19.3081F, 5.3562F, -1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r257 = z.addOrReplaceChild("cube_r257", CubeListBuilder.create().texOffs(80, 6).addBox(-12.425F, -4.8F, 8.45F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(10.9462F, -16.725F, -6.3397F, -0.0873F, 0.0F, 0.0F));

		PartDefinition z_light = z.addOrReplaceChild("z_light", CubeListBuilder.create(), PartPose.offset(10.9462F, -16.95F, -6.3397F));

		PartDefinition cube_r258 = z_light.addOrReplaceChild("cube_r258", CubeListBuilder.create().texOffs(87, 117).addBox(-12.425F, -4.8F, 8.425F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.98F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r259 = z_light.addOrReplaceChild("cube_r259", CubeListBuilder.create().texOffs(14, 46).addBox(-1.475F, -1.5F, -1.6F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-10.925F, -2.5081F, 11.5959F, -1.0036F, 0.0F, 0.0F));

		PartDefinition inc = side_ne.addOrReplaceChild("inc", CubeListBuilder.create(), PartPose.offset(-0.1537F, 20.0419F, 3.3428F));

		PartDefinition inc_frame = inc.addOrReplaceChild("inc_frame", CubeListBuilder.create(), PartPose.offset(11.1962F, -16.55F, -7.5897F));

		PartDefinition cube_r260 = inc_frame.addOrReplaceChild("cube_r260", CubeListBuilder.create().texOffs(39, 28).addBox(-12.175F, -4.8F, 4.45F, 2.0F, 2.0F, 5.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r261 = inc_frame.addOrReplaceChild("cube_r261", CubeListBuilder.create().texOffs(78, 4).addBox(-12.175F, -4.8F, 4.45F, 2.0F, 2.0F, 5.0F, new CubeDeformation(-0.8F))
		.texOffs(78, 4).addBox(-12.975F, -4.8F, 4.45F, 2.0F, 2.0F, 5.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.4F, -0.175F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r262 = inc_frame.addOrReplaceChild("cube_r262", CubeListBuilder.create().texOffs(78, 4).addBox(-12.175F, -4.8F, 4.45F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(78, 4).addBox(-12.575F, -4.8F, 4.45F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.2F, 0.1213F, 3.3871F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r263 = inc_frame.addOrReplaceChild("cube_r263", CubeListBuilder.create().texOffs(78, 4).addBox(-12.175F, -4.8F, 4.45F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(78, 4).addBox(-12.575F, -4.8F, 4.45F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.2F, -0.2099F, -0.3985F, -0.0873F, 0.0F, 0.0F));

		PartDefinition inc_lever = inc.addOrReplaceChild("inc_lever", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0212F, -16.5374F, -0.3729F, 0.384F, 0.0F, 0.0F));

		PartDefinition cube_r264 = inc_lever.addOrReplaceChild("cube_r264", CubeListBuilder.create().texOffs(96, 20).addBox(-1.0F, 0.5F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0F, -6.25F, 0.275F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r265 = inc_lever.addOrReplaceChild("cube_r265", CubeListBuilder.create().texOffs(51, 28).addBox(-1.0F, -2.5F, -1.0F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, -3.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition dummy_bits = side_ne.addOrReplaceChild("dummy_bits", CubeListBuilder.create(), PartPose.offset(-0.1537F, 20.0419F, 3.3428F));

		PartDefinition chunk4 = dummy_bits.addOrReplaceChild("chunk4", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.99F)), PartPose.offsetAndRotation(-2.175F, -19.5826F, 0.9533F, -1.2654F, 0.0F, 0.0F));

		PartDefinition glow_light = chunk4.addOrReplaceChild("glow_light", CubeListBuilder.create().texOffs(98, 20).addBox(-0.425F, 0.4478F, -1.5253F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(-0.575F, -1.475F, 0.225F));

		PartDefinition chunk6 = dummy_bits.addOrReplaceChild("chunk6", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.99F)), PartPose.offsetAndRotation(2.325F, -19.5826F, 0.9533F, -1.2654F, 0.0F, 0.0F));

		PartDefinition glow_light3 = chunk6.addOrReplaceChild("glow_light3", CubeListBuilder.create().texOffs(98, 20).addBox(-0.425F, 0.4478F, -1.5253F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(-0.575F, -1.475F, 0.225F));

		PartDefinition chunk5 = dummy_bits.addOrReplaceChild("chunk5", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.99F)), PartPose.offsetAndRotation(-2.175F, -19.5826F, -0.6717F, -1.2654F, 0.0F, 0.0F));

		PartDefinition glow_light2 = chunk5.addOrReplaceChild("glow_light2", CubeListBuilder.create().texOffs(98, 20).addBox(-0.425F, 0.4478F, -1.5253F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(-0.575F, -1.475F, 0.225F));

		PartDefinition chunk7 = dummy_bits.addOrReplaceChild("chunk7", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.99F)), PartPose.offsetAndRotation(2.325F, -19.5826F, -0.6717F, -1.2654F, 0.0F, 0.0F));

		PartDefinition glow_light4 = chunk7.addOrReplaceChild("glow_light4", CubeListBuilder.create().texOffs(98, 20).addBox(-0.425F, 0.4478F, -1.5253F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(-0.575F, -1.475F, 0.225F));

		PartDefinition switch_cluster = dummy_bits.addOrReplaceChild("switch_cluster", CubeListBuilder.create(), PartPose.offsetAndRotation(4.8675F, -20.2696F, -1.0163F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r266 = switch_cluster.addOrReplaceChild("cube_r266", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.0F, -2.5F, 3.0F, 2.0F, 5.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.2713F, 0.5223F, -0.0159F, -0.5236F, 0.0F, 0.0F));

		PartDefinition switch1 = switch_cluster.addOrReplaceChild("switch", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.0F, -0.1563F, 0.0F));

		PartDefinition switch2 = switch_cluster.addOrReplaceChild("switch2", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(-0.5F, -0.1563F, 0.0F));

		PartDefinition switch4 = switch_cluster.addOrReplaceChild("switch4", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.0F, 0.1937F, 0.575F));

		PartDefinition switch3 = switch_cluster.addOrReplaceChild("switch3", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(-0.5F, 0.1937F, 0.575F));

		PartDefinition switch5 = switch_cluster.addOrReplaceChild("switch5", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(-0.5F, 0.5187F, 1.15F));

		PartDefinition switch6 = switch_cluster.addOrReplaceChild("switch6", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.0F, 0.5187F, 1.15F));

		PartDefinition switch7 = switch_cluster.addOrReplaceChild("switch7", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.0F, 0.7937F, 1.725F));

		PartDefinition switch8 = switch_cluster.addOrReplaceChild("switch8", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(-0.5F, 0.7937F, 1.725F));

		PartDefinition switch_cluster2 = dummy_bits.addOrReplaceChild("switch_cluster2", CubeListBuilder.create(), PartPose.offsetAndRotation(-4.1325F, -20.2696F, -1.0163F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r267 = switch_cluster2.addOrReplaceChild("cube_r267", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.0F, -2.5F, 3.0F, 2.0F, 5.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.2713F, 0.5223F, -0.0159F, -0.5236F, 0.0F, 0.0F));

		PartDefinition switch9 = switch_cluster2.addOrReplaceChild("switch9", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.0F, -0.1563F, 0.0F));

		PartDefinition switch10 = switch_cluster2.addOrReplaceChild("switch10", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(-0.5F, -0.1563F, 0.0F));

		PartDefinition switch11 = switch_cluster2.addOrReplaceChild("switch11", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.0F, 0.1937F, 0.575F));

		PartDefinition switch12 = switch_cluster2.addOrReplaceChild("switch12", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(-0.5F, 0.1937F, 0.575F));

		PartDefinition switch13 = switch_cluster2.addOrReplaceChild("switch13", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(-0.5F, 0.5187F, 1.15F));

		PartDefinition switch14 = switch_cluster2.addOrReplaceChild("switch14", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.0F, 0.5187F, 1.15F));

		PartDefinition switch15 = switch_cluster2.addOrReplaceChild("switch15", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.0F, 0.7937F, 1.725F));

		PartDefinition switch16 = switch_cluster2.addOrReplaceChild("switch16", CubeListBuilder.create().texOffs(9, 29).addBox(-1.0F, -1.6688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -0.9937F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(9, 29).addBox(-1.0F, -1.2687F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(9, 29).addBox(-1.0F, -1.4688F, -1.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(-0.5F, 0.7937F, 1.725F));

		PartDefinition side_n = partdefinition.addOrReplaceChild("side_n", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 9.5F, -14.25F, 0.3491F, 0.0F, 0.0F));

		PartDefinition monitor_box = side_n.addOrReplaceChild("monitor_box", CubeListBuilder.create().texOffs(42, 29).addBox(-2.5F, -2.9875F, -1.0625F, 5.0F, 4.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(39, 29).addBox(-3.5F, -0.0125F, -1.9375F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 1.2887F, 6.4186F, -1.3526F, 0.0F, 0.0F));

		PartDefinition glow_monitor_screen = monitor_box.addOrReplaceChild("glow_monitor_screen", CubeListBuilder.create().texOffs(5, 117).addBox(-2.9F, -2.5F, -1.6F, 6.0F, 5.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offset(-0.05F, -1.0875F, -0.4375F));

		PartDefinition monitor_text = glow_monitor_screen.addOrReplaceChild("monitor_text", CubeListBuilder.create(), PartPose.offset(0.075F, 0.05F, -0.725F));

		PartDefinition refueler = side_n.addOrReplaceChild("refueler", CubeListBuilder.create(), PartPose.offset(0.0F, 20.0512F, 2.3561F));

		PartDefinition refuel_light = refueler.addOrReplaceChild("refuel_light", CubeListBuilder.create(), PartPose.offset(0.0212F, -20.1984F, -0.4831F));

		PartDefinition cube_r268 = refuel_light.addOrReplaceChild("cube_r268", CubeListBuilder.create().texOffs(46, 121).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, -0.75F, -0.1231F, -0.7816F, 0.0869F));

		PartDefinition needle = refueler.addOrReplaceChild("needle", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0212F, -20.2734F, -0.4581F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r269 = needle.addOrReplaceChild("cube_r269", CubeListBuilder.create().texOffs(43, 119).addBox(-1.0286F, -1.0458F, -1.0559F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.92F)), PartPose.offsetAndRotation(0.0214F, 0.0308F, 0.0022F, -0.1309F, -0.7854F, 0.0F));

		PartDefinition refuel_base = refueler.addOrReplaceChild("refuel_base", CubeListBuilder.create(), PartPose.offset(0.0212F, -20.1984F, -0.3331F));

		PartDefinition cube_r270 = refuel_base.addOrReplaceChild("cube_r270", CubeListBuilder.create().texOffs(44, 28).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, -0.15F, -0.1231F, -0.7816F, 0.0869F));

		PartDefinition cube_r271 = refuel_base.addOrReplaceChild("cube_r271", CubeListBuilder.create().texOffs(44, 28).addBox(-2.0F, -1.0F, -2.0F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.075F, -0.075F, -0.1231F, -0.7816F, 0.0869F));

		PartDefinition cube_r272 = refuel_base.addOrReplaceChild("cube_r272", CubeListBuilder.create().texOffs(44, 28).addBox(-2.0F, -1.0F, -2.0F, 4.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F))
		.texOffs(44, 28).addBox(-0.6F, -1.0F, -2.875F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(44, 28).addBox(-2.85F, -1.0F, 0.8F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(44, 28).addBox(0.8F, -1.0F, -2.875F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.25F, 0.0F, -0.1231F, -0.7816F, 0.0869F));

		PartDefinition cube_r273 = refuel_base.addOrReplaceChild("cube_r273", CubeListBuilder.create().texOffs(44, 28).addBox(-2.85F, -1.0F, -0.625F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.0177F, 0.2515F, 0.0176F, -0.1231F, -0.7816F, 0.0869F));

		PartDefinition glow_light_dial = refuel_base.addOrReplaceChild("glow_light_dial", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r274 = glow_light_dial.addOrReplaceChild("cube_r274", CubeListBuilder.create().texOffs(1, 27).addBox(-2.025F, -1.0F, -2.0F, 4.0F, 2.0F, 4.0F, new CubeDeformation(-0.99F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.1231F, -0.7816F, 0.0869F));

		PartDefinition stablizers = side_n.addOrReplaceChild("stablizers", CubeListBuilder.create(), PartPose.offset(-5.2288F, 0.1028F, 1.523F));

		PartDefinition cube_r275 = stablizers.addOrReplaceChild("cube_r275", CubeListBuilder.create().texOffs(42, 28).addBox(-0.25F, -1.0F, -1.6F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(42, 28).addBox(-0.725F, -1.0F, -1.6F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition glow_stablizers_slide_z = stablizers.addOrReplaceChild("glow_stablizers_slide_z", CubeListBuilder.create(), PartPose.offset(-0.1F, 0.95F, -0.775F));

		PartDefinition cube_r276 = glow_stablizers_slide_z.addOrReplaceChild("cube_r276", CubeListBuilder.create().texOffs(91, 123).addBox(-2.0F, -2.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(91, 123).addBox(-1.6F, -2.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(91, 123).addBox(-1.2F, -2.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(1.2F, 0.0F, 0.0F, -0.0873F, 0.0F, 0.0F));

		PartDefinition dummybuttons = side_n.addOrReplaceChild("dummybuttons", CubeListBuilder.create(), PartPose.offset(5.075F, 0.211F, 2.2969F));

		PartDefinition chunk1 = dummybuttons.addOrReplaceChild("chunk1", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.99F)), PartPose.offsetAndRotation(-7.25F, 0.2576F, 2.0125F, -1.2654F, 0.0F, 0.0F));

		PartDefinition glow_light5 = chunk1.addOrReplaceChild("glow_light5", CubeListBuilder.create().texOffs(98, 20).addBox(-0.425F, 0.4478F, -1.5253F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(-0.575F, -1.475F, 0.225F));

		PartDefinition chunk2 = dummybuttons.addOrReplaceChild("chunk2", CubeListBuilder.create().texOffs(44, 28).addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.99F)), PartPose.offsetAndRotation(-3.0F, 0.2576F, 2.0125F, -1.2654F, 0.0F, 0.0F));

		PartDefinition cube_r277 = chunk2.addOrReplaceChild("cube_r277", CubeListBuilder.create().texOffs(26, 28).addBox(-1.0F, -1.5F, -1.25F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(1.5962F, 0.3432F, -0.0241F, -0.3491F, 0.0F, 0.0F));

		PartDefinition cube_r278 = chunk2.addOrReplaceChild("cube_r278", CubeListBuilder.create().texOffs(26, 28).addBox(-5.5F, -1.0F, -1.0F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-3.1038F, -0.2121F, -0.0881F, -0.3491F, 0.0F, 0.0F));

		PartDefinition glow_light6 = chunk2.addOrReplaceChild("glow_light6", CubeListBuilder.create().texOffs(99, 21).addBox(-0.425F, 0.4478F, -1.5253F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(-0.575F, -1.475F, 0.225F));

		PartDefinition keypad1 = dummybuttons.addOrReplaceChild("keypad1", CubeListBuilder.create().texOffs(50, 119).addBox(-0.5F, -1.075F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(80, 3).addBox(-0.5F, -1.625F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(80, 3).addBox(0.0F, -1.625F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(80, 3).addBox(-1.0F, -1.625F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(80, 3).addBox(-0.5F, -2.225F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(80, 3).addBox(0.0F, -2.225F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(80, 3).addBox(-1.0F, -2.225F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(80, 3).addBox(-0.5F, -2.85F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(80, 3).addBox(0.0F, -2.85F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(80, 3).addBox(-1.0F, -2.85F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(44, 28).addBox(-1.5F, -3.375F, -1.45F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(-1.25F, 0.2576F, -0.7375F, -1.5708F, 0.0F, 0.0F));

		PartDefinition button_clusters = dummybuttons.addOrReplaceChild("button_clusters", CubeListBuilder.create(), PartPose.offset(-1.825F, -1.2174F, -0.5125F));

		PartDefinition glow_light13 = button_clusters.addOrReplaceChild("glow_light13", CubeListBuilder.create().texOffs(16, 48).addBox(-1.125F, 0.475F, 3.775F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(16, 48).addBox(-1.125F, 0.475F, 3.025F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(16, 48).addBox(-0.375F, 0.475F, 3.025F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(16, 48).addBox(-7.375F, 0.475F, 3.025F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(16, 48).addBox(-8.125F, 0.475F, 3.025F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(16, 48).addBox(-7.375F, 0.475F, 3.775F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnimations(ConsoleTile tile, float ageInTicks) {
		this.root.getAllParts().forEach(ModelPart::resetPose);
		this.animate(tile.rotorAnimationState, G8ConsoleAnimations.ROTOR_FLIGHT, ageInTicks);
		//Control Animation
		if(tile.getLevel() != null){
			tile.getLevel().getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
				this.animate(tardis.getControlDataOrCreate(ControlRegistry.RANDOMIZER.get()).getUseAnimationState(), G8ConsoleAnimations.RANDOMIZER_CONTROL, ageInTicks);
			});
		}
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		time_rotor.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		stations.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		glow_core.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		bottom_plate.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		side_s.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		side_sw.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		side_nw.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		side_se.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		side_ne.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		side_n.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public ModelPart root() {
		return this.root;
	}

	@Override
	public void setupAnim(Entity pEntity, float pLimbSwing, float pLimbSwingAmount, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch) {

	}
}