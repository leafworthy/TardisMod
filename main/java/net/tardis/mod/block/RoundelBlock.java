package net.tardis.mod.block;

import net.minecraft.world.level.block.Block;
import net.tardis.mod.Tardis;

public class RoundelBlock extends Block {

    public static final String TRANSLATION_KEY = "block." + Tardis.MODID + ".roundel";

    public RoundelBlock(Properties pProperties) {
        super(pProperties);
    }

    @Override
    public String getDescriptionId() {
        return TRANSLATION_KEY;
    }
}
