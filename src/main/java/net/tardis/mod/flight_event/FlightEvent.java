package net.tardis.mod.flight_event;

import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;

public abstract class FlightEvent {

    public final ITardisLevel tardis;
    private boolean isComplete = false;

    public FlightEvent(ITardisLevel level){
        this.tardis = level;
    }

    /**
     *
     * @param control - the control that was hit
     * @return true if this event overrides the default control action
     */
    public abstract boolean onControlUse(ControlData<?> control);

    public abstract void onStart();

    public void complete(){
        this.isComplete = true;
    }

    public boolean isComplete(){
        return this.isComplete;
    }

}
