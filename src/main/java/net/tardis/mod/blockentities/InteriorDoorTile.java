package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.misc.DoorHandler;
import net.tardis.mod.misc.IDoor;
import net.tardis.mod.misc.InteriorDoorData;

import java.util.UUID;

public class InteriorDoorTile extends BlockEntity implements IDoor {

    private UUID doorID;
    private DoorHandler doorHandler = new DoorHandler(true);

    public InteriorDoorTile(BlockPos pWorldPosition, BlockState pBlockState) {
        super(TileRegistry.INTERIOR_DOOR.get(), pWorldPosition, pBlockState);
    }


    public static void tick(Level level, BlockPos pos, BlockState state, InteriorDoorTile door){
        if(!level.isClientSide){
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                ServerLevel exteriorLevel = level.getServer().getLevel(tardis.getLocation().getLevel());
                if(exteriorLevel != null){
                    for(Entity e : tardis.getLevel().getEntitiesOfClass(Entity.class, new AABB(pos).inflate(0.1))){
                        tardis.getExterior().placeEntityAt(exteriorLevel, e);
                    }
                }
            });
        }
    }

    public UUID getDoorID() {
        return this.doorID;
    }

    public void setDoorID(UUID id){
        this.doorID = id;
        this.setChanged();
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        if(tag.contains("door_id"))
            this.doorID = UUID.fromString(tag.getString("door_id"));
        this.doorHandler.deserializeNBT(tag);
    }

    @Override
    protected void saveAdditional(CompoundTag pTag) {
        super.saveAdditional(pTag);
        if(this.doorID != null)
            pTag.putString("door_id", this.doorID.toString());
        pTag.put("door", this.doorHandler.serializeNBT());
    }

    @Override
    public void onLoad() {
        if(this.level != null && !level.isClientSide){
            this.level.getCapability(Capabilities.TARDIS).ifPresent(cap -> {
                if(this.doorID == null)
                    this.setDoorID(UUID.randomUUID());
                cap.getInteriorManager().addInteriorDoor(this.doorID, new InteriorDoorData(this.getBlockPos()));
            });
        }
    }

    @Override
    public DoorHandler getDoorHandler() {
        return this.doorHandler;
    }
}
