package net.tardis.mod.upgrade;

import java.util.function.Function;

public class UpgradeType {

    private final Function<UpgradeType, Upgrade<?>> upgradeFactory;

    public UpgradeType(Function<UpgradeType, Upgrade<?>> upgradeFactory){
        this.upgradeFactory = upgradeFactory;
    }

    public Upgrade<?> create(){
        return this.upgradeFactory.apply(this);
    }
}
