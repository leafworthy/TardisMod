package net.tardis.mod.cap;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class Capabilities {

    public static final Capability<ITardisLevel> TARDIS = CapabilityManager.get(new CapabilityToken<ITardisLevel>() {});

    @SubscribeEvent
    public static void register(RegisterCapabilitiesEvent event){
        event.register(ITardisLevel.class);
    }

}
