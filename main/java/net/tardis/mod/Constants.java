package net.tardis.mod;

public class Constants {

    public static class Translation{

        public static String makeGuiTitleTranslation(String title){
            return "screen." + Tardis.MODID + ".title." + title;
        }

    }

}
