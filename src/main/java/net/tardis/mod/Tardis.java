package net.tardis.mod;

import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.tardis.mod.advancements.TardisAdvancementTriggers;
import net.tardis.mod.block.BlockRegistry;
import net.tardis.mod.block.Roundels;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.control.ControlRegistry;
import net.tardis.mod.dimension.DimensionTypes;
import net.tardis.mod.entity.EntityRegistry;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.flight_event.FlightEventRegistry;
import net.tardis.mod.item.ItemRegistry;
import net.tardis.mod.menu.MenuRegistry;
import net.tardis.mod.network.Network;
import net.tardis.mod.world.TardisARSPieceRegistry;
import net.tardis.mod.recipes.RecipeRegistry;
import net.tardis.mod.upgrade.UpgradeRegistry;
import net.tardis.mod.world.WorldRegistry;

@Mod(Tardis.MODID)
public class Tardis {

    public static final String MODID = "tardis";

    public Tardis(){
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        bus.addListener(this::commonSetup);

        ItemRegistry.ITEMS.register(bus);
        BlockRegistry.BLOCKS.register(bus);
        TileRegistry.TYPES.register(bus);
        ControlRegistry.TYPES.register(bus);
        EntityRegistry.TYPES.register(bus);
        ExteriorRegistry.EXTERIORS.register(bus);
        WorldRegistry.CHUNK_GENERATORS.register(bus);
        TardisARSPieceRegistry.registerARSPieces();
        UpgradeRegistry.UPGRADES.register(bus);
        RecipeRegistry.RECIPES.register(bus);
        MenuRegistry.MENUS.register(bus);
        Roundels.register();
        FlightEventRegistry.FLIGHT_EVENTS.register(bus);
    }

    @SubscribeEvent
    public void commonSetup(FMLCommonSetupEvent event){
        Network.registerPackets();
        DimensionTypes.register();
        TardisAdvancementTriggers.register();
    }

}
