package net.tardis.mod.misc;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.*;

public class InteriorManager implements INBTSerializable<CompoundTag>, ITickableObject{

    private final ITardisLevel tardis;

    private HashMap<UUID, InteriorDoorData> interiorDoorPositions = new HashMap<>();

    public InteriorManager(ITardisLevel level){
        this.tardis = level;
    }

    @Override
    public void tick() {

    }

    public InteriorDoorData getMainInteriorDoor(){

        for(InteriorDoorData data : this.interiorDoorPositions.values()){
            if(data != null && data.isValidDoor(this.tardis))
                return data;
        }
        System.out.println("No valid doors found! Checked " + this.interiorDoorPositions.size());
        return new InteriorDoorData(new BlockPos(0, 200, 0));
    }

    public void addInteriorDoor(UUID id, InteriorDoorData pos){
        this.interiorDoorPositions.put(id, pos);
    }

    public HashMap<UUID, InteriorDoorData> getDoors(){
        return this.interiorDoorPositions;
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();

        ListTag doorList = new ListTag();
        for(Map.Entry<UUID, InteriorDoorData> data : this.interiorDoorPositions.entrySet()){
            CompoundTag dataTag = data.getValue().serializeNBT();
            dataTag.putString("interior_data_id", data.getKey().toString());
            doorList.add(dataTag);
        }
        tag.put("doors", doorList);

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.interiorDoorPositions.clear();

        ListTag doorTag = tag.getList("doors", Tag.TAG_COMPOUND);
        for(Tag data : doorTag){
            CompoundTag compoundTag = (CompoundTag)data;
            this.interiorDoorPositions.put(UUID.fromString(compoundTag.getString("interior_data_id")), new InteriorDoorData(compoundTag));
        }
    }

    public void removeDoor(UUID id) {
        this.interiorDoorPositions.remove(id);
    }
}
