package net.tardis.mod.datagen;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGen {

    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    @SubscribeEvent
    public static void registerDataGen(GatherDataEvent event){
        event.getGenerator().addProvider(true, new TardisLangProvider(event.getGenerator()));
        event.getGenerator().addProvider(true, new TardisAdvancementProvider(event.getGenerator()));
        event.getGenerator().addProvider(true, new TardisAnimationProvider(event.getGenerator()));
        event.getGenerator().addProvider(true, new TardisBlockStateProvider(event.getGenerator(), event.getExistingFileHelper()));
        event.getGenerator().addProvider(true, new DraftingTableRecipeProvider(event.getGenerator()));
    }

}
