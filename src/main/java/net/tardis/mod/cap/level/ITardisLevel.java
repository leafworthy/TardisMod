package net.tardis.mod.cap.level;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.exterior.Exterior;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.flight_event.FlightEvent;
import net.tardis.mod.misc.enums.FlightState;
import net.tardis.mod.misc.InteriorManager;
import net.tardis.mod.misc.SpaceTimeCoord;
/** An attachment to Levels to handle most Tardis logic.*/
public interface ITardisLevel extends INBTSerializable<CompoundTag> {

    //Getters and setters

    Level getLevel();
    SpaceTimeCoord getLocation();
    SpaceTimeCoord getDestination();

    void setDestination(SpaceTimeCoord coord);
    void setDestination(ResourceKey<Level> level, BlockPos pos);
    void setLocation(SpaceTimeCoord coord);

    void setExterior(ExteriorType type);
    Exterior getExterior();

    int ticksInFlight();

    boolean isInFlight();
    boolean isInVortex();

    int getArtron();
    int getMaxArtron();

    FlightEvent getCurrentFlightEvent();

    //Functional

    void takeoff();
    boolean initLanding();
    void completeLanding();
    void flightTick();

    InteriorManager getInteriorManager();

    <T> ControlData<T> getControlDataOrCreate(ControlType type);

    void tick();

    /**
     * If you are not a packet, go away
     */
    void setFlightState(int flightTicks, int reachedTick, FlightState state);
}
