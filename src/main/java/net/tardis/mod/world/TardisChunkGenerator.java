package net.tardis.mod.world;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.resources.RegistryOps;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.WorldGenRegion;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.*;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeManager;
import net.minecraft.world.level.biome.FixedBiomeSource;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.*;
import net.minecraft.world.level.levelgen.blending.Blender;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;
import net.tardis.mod.JsonRegistries;
import net.tardis.mod.ars.ARSRoom;
import net.tardis.mod.block.BlockRegistry;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class TardisChunkGenerator extends ChunkGenerator {

    public static final Codec<TardisChunkGenerator> CODEC = RecordCodecBuilder
            .create(instance -> commonCodec(instance)
                    .and(RegistryOps.retrieveRegistry(Registry.BIOME_REGISTRY).forGetter((thing) -> thing.biomeReg))
                    .and(RegistryOps.retrieveRegistry(JsonRegistries.ARS_ROOM_REGISTRY.get().getRegistryKey()).forGetter(thing -> thing.arsRoom))
            .apply(instance, instance.stable(TardisChunkGenerator::new)));

    public final Registry<Biome> biomeReg;
    public final Registry<StructureSet> setReg;
    public final Registry<ARSRoom> arsRoom;
    public final RandomSource random;

    // Some parameter values.
    public final int distanceBetweenRooms = 9;
    public final int arsChunkSize = TardisARSPiece.LOCKED_PIECE_CHUNK_SIZE;
    private final int chunkSize = 16;


    public TardisChunkGenerator(Registry<StructureSet> setReg, Registry<Biome> biomeReg, Registry<ARSRoom> arsRooms) {
        super(setReg, Optional.empty(), new FixedBiomeSource(biomeReg.getHolderOrThrow(WorldRegistry.TARDIS_BIOME)));
        this.biomeReg = biomeReg;
        this.setReg = setReg;
        this.arsRoom = arsRooms;
        this.random = new SingleThreadedRandomSource(0l);
    }

    @Override
    public void applyBiomeDecoration(WorldGenLevel pLevel, ChunkAccess pChunk, StructureManager pStructureManager) {

        if (pChunk.getPos().x == 0 && pChunk.getPos().z == 0) {
            placePieceInWorld(pLevel, getRandomConsoleRoomPiece().getResourceLocation(), pChunk);
        } else {
            // For each ARS chunk size interval.
            if(pChunk.getPos().x % arsChunkSize == 0 && pChunk.getPos().z % arsChunkSize == 0){

                // Determine if the piece is a room or a corridor
                ResourceLocation pieceToPlace = (isChunkAtRoomInterval(pChunk.getPos())) ? getRandomRoomPiece().get().getRoomLocation() : getRandomCorridorPiece().getResourceLocation();
                placePieceInWorld(pLevel, pieceToPlace, pChunk);
            }
        }
    }

    private void placePieceInWorld(WorldGenLevel level, ResourceLocation pieceToPlace, ChunkAccess pChunk) {
        // Place the desired piece.
        level.getLevel().getStructureManager().get(pieceToPlace).ifPresent(structure -> {
            BlockPos pos = pChunk.getPos().getBlockAt(0, 64,0).north(chunkSize).west(chunkSize); // Must be offset to utilize all 3x3 chunks.
            StructurePlaceSettings settings = new StructurePlaceSettings();
            structure.placeInWorld(level, pos, pos, settings, level.getRandom(), 1);
        });
    }

    /**
     * Determines if the chunk is a room chunk
     * @param pos the position of the chunk
     * @return is the chunk a room chunk
     */
    private boolean isChunkAtRoomInterval(ChunkPos pos) {
        return pos.x % distanceBetweenRooms == 0 && pos.z % distanceBetweenRooms == 0;
    }

    /**
     * Fetch a random corridor piece to populate a chunk
     * @return random corridor ARS piece from the registry.
     */
    private TardisARSPiece getRandomCorridorPiece() {

        //5% chance to be a console room
        if(this.random.nextDouble() < 0.05){
            return TardisARSPieceRegistry.CONSOLE_ROOMS.get(this.random.nextInt(TardisARSPieceRegistry.CONSOLE_ROOMS.size()));
        }

        return TardisARSPieceRegistry.CORRIDORS.get(this.random.nextInt(TardisARSPieceRegistry.CORRIDORS.size()));
    }

    /**
     * Fetch a random console room piece to populate a chunk
     * @return random console room ARS piece from the registry.
     */
    private TardisARSPiece getRandomConsoleRoomPiece() {
        return TardisARSPieceRegistry.CONSOLE_ROOMS.get(this.random.nextInt(TardisARSPieceRegistry.CONSOLE_ROOMS.size()));
    }

    /**
     * Fetch a random room piece to populate a chunk
     * @return random room ARS piece from the registry.
     */
    private Holder<ARSRoom> getRandomRoomPiece() {
        return this.arsRoom.getRandom(this.random).get();
    }


    @Override
    public void createStructures(RegistryAccess pRegistryAccess, RandomState pRandom, StructureManager pStructureManager, ChunkAccess pChunk, StructureTemplateManager pStructureTemplateManager, long pSeed) {
        //super.createStructures(pRegistryAccess, pRandom, pStructureManager, pChunk, pStructureTemplateManager, pSeed);

    }

    @Override
    public void createReferences(WorldGenLevel pLevel, StructureManager pStructureManager, ChunkAccess pChunk) {
        //super.createReferences(pLevel, pStructureManager, pChunk);
    }

    @Override
    public Codec<TardisChunkGenerator> codec() {
        return CODEC;
    }

    @Override
    public void applyCarvers(WorldGenRegion p_223043_, long p_223044_, RandomState p_223045_, BiomeManager p_223046_, StructureManager p_223047_, ChunkAccess p_223048_, GenerationStep.Carving p_223049_) {

    }

    @Override
    public void buildSurface(WorldGenRegion p_223050_, StructureManager p_223051_, RandomState p_223052_, ChunkAccess p_223053_) {

    }

    @Override
    public void spawnOriginalMobs(WorldGenRegion p_62167_) {}

    @Override
    public int getGenDepth() {
        return 384;
    }

    @Override
    public CompletableFuture<ChunkAccess> fillFromNoise(Executor executor, Blender p_223210_, RandomState p_223211_, StructureManager p_223212_, ChunkAccess access) {
        return CompletableFuture.completedFuture(access);
    }

    @Override
    public int getSeaLevel() {
        return -63;
    }

    @Override
    public int getMinY() {
        return 0;
    }

    @Override
    public int getBaseHeight(int p_223032_, int p_223033_, Heightmap.Types p_223034_, LevelHeightAccessor p_223035_, RandomState p_223036_) {
        return 0;
    }

    @Override
    public NoiseColumn getBaseColumn(int p_223028_, int p_223029_, LevelHeightAccessor level, RandomState p_223031_) {

        BlockState[] states = new BlockState[level.getHeight()];
        for(int i = 0; i < states.length; ++i){
            states[i] = Blocks.AIR.defaultBlockState();
        }

        //return new NoiseColumn(this.getMinY(), states);

        return new NoiseColumn(0, new BlockState[0]);
    }

    @Override
    public void addDebugScreenInfo(List<String> p_223175_, RandomState p_223176_, BlockPos p_223177_) {}
}
