package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.client.models.IAnimatableTile;

public class ConsoleRenderer<C extends ConsoleTile, T extends HierarchicalModel<?> & IAnimatableTile<C>> implements BlockEntityRenderer<C> {

    public ResourceLocation tex;
    private T model;

    public ConsoleRenderer(BlockEntityRendererProvider.Context context, T model, ResourceLocation tex){
        this.model = model;
        this.tex = tex;
    }

    @Override
    public void render(ConsoleTile consoleTile, float partialTicks, PoseStack poseStack, MultiBufferSource multiBufferSource, int packedLight, int packedOverlay) {
        poseStack.translate(0.5, 1.5, 0.5);
        poseStack.mulPose(Vector3f.XP.rotationDegrees(180));
        poseStack.mulPose(Vector3f.YP.rotationDegrees(180));
        model.setupAnimations((C)consoleTile, (int)consoleTile.getLevel().getGameTime() + partialTicks);
        this.model.renderToBuffer(poseStack, multiBufferSource.getBuffer(this.model.renderType(this.tex)), packedLight, packedOverlay, 1.0F, 1.0F, 1.0F, 1.0F);
    }
}
