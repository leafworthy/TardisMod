package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.renderer.blockentity.SignRenderer;
import net.tardis.mod.blockentities.BaseMonitorTile;

public class MonitorRenderer implements BlockEntityRenderer<BaseMonitorTile> {

    public MonitorRenderer(BlockEntityRendererProvider.Context context){}

    @Override
    public void render(BaseMonitorTile pBlockEntity, float pPartialTick, PoseStack pose, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {
        pose.pushPose();
        Minecraft.getInstance().font.drawInBatch("Montior Test", 0, 0, 0x0000, false, pose.last().pose(), pBufferSource, true, 0xFFFFFF, pPackedLight);
        pose.popPose();
    }
}
