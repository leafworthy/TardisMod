package net.tardis.mod.exterior;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.BlockRegistry;
import net.tardis.mod.helpers.Helper;

import java.util.function.Supplier;

public class ExteriorRegistry {

    public static final DeferredRegister<ExteriorType> EXTERIORS = DeferredRegister.create(Helper.createRL("exterior"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<ExteriorType>> REGISTRY = EXTERIORS.makeRegistry(RegistryBuilder::new);

    public static final RegistryObject<ExteriorType> STEAM = EXTERIORS.register("steam", () -> new ExteriorType((type, tardis) -> new TileExterior(type, tardis, BlockRegistry.STEAM_EXTERIOR.get().defaultBlockState())));


}
