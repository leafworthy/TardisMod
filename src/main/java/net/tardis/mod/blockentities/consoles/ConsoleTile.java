package net.tardis.mod.blockentities.consoles;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.entity.ControlEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class ConsoleTile extends BlockEntity {

    public final AnimationState rotorAnimationState = new AnimationState();
    private List<UUID> controlEntities = new ArrayList<>();

    public ConsoleTile(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
    }

    public void clientTick(){

        if(level != null){
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                //rotor animations
                if(tardis.isInVortex()){
                    this.rotorAnimationState.startIfStopped((int)level.getGameTime());
                }
                else this.rotorAnimationState.stop();
            });
        }
    }

    public void addControl(ControlEntity control){
        this.controlEntities.add(control.getUUID());
    }

    public void killAllControls(){
        if(!level.isClientSide){
            for(UUID id : this.controlEntities){
                Entity e = ((ServerLevel)level).getEntity(id);
                if(e != null)
                    e.kill();
            }
        }
        this.controlEntities.clear();
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, ConsoleTile pBlockEntity){
        pBlockEntity.clientTick();
    }

    @Override
    public void load(CompoundTag pTag) {
        super.load(pTag);

        if(pTag.contains("control_list")){
            this.controlEntities.clear();
            ListTag controls = pTag.getList("control_list", Tag.TAG_STRING);
            for(Tag tag : controls){
                UUID id = UUID.fromString(((StringTag)tag).getAsString());
                this.controlEntities.add(id);
            }
        }

    }

    @Override
    protected void saveAdditional(CompoundTag pTag) {
        super.saveAdditional(pTag);

        ListTag controlList = new ListTag();
        for(UUID id : this.controlEntities){
            controlList.add(StringTag.valueOf(id.toString()));
        }
        pTag.put("control_list", controlList);
    }
}
