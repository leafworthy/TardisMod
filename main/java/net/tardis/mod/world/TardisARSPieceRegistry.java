package net.tardis.mod.world;

import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.helpers.Helper;

import java.util.ArrayList;
import java.util.List;

public class TardisARSPieceRegistry {

    public static List<TardisARSPiece> CORRIDORS = new ArrayList<>();
    public static List<TardisARSPiece> CONSOLE_ROOMS = new ArrayList<>();
    public static List<TardisARSPiece> ROOMS = new ArrayList<>();

    public static void registerARSPieces() {
        registerCorridors();
        registerRooms();
        registerConsoleRooms();
    }

    // Room registry.
    private static void registerRooms() {
        // TODO: DATA DRIVEN
        registerRoomPiece(new TardisARSPiece(Helper.createRL("room_circular")));
        registerRoomPiece(new TardisARSPiece(Helper.createRL("room_ars_template")));
    }

    // Corridor registry.
    private static void registerCorridors() {
        // TODO: DATA DRIVEN
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r0")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r3")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r19")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r20")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r21")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r28")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r29")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r30")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r35")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r36")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r61")));
        registerCorridorPiece(new TardisARSPiece(createCorridorRL("gs_r72")));
    }

    private static void registerConsoleRooms() {
        // TODO: DATA DRIVEN
        //registerConsoleRoomPiece(new TardisARSPiece(createConsoleRoomRL("console_room_dev")));
        registerConsoleRoomPiece(new TardisARSPiece(createConsoleRoomRL("console_room_dev_m")));
    }

    /**
     * Register a new room to be featured in corridor generation.
     * @param arsPiece ARSPiece to be placed.
     */
    public static void registerRoomPiece(TardisARSPiece arsPiece) {
        ROOMS.add(arsPiece);
    }

    /**
     * Register a new corridor piece to be featured in corridor generation.
     * @param arsPiece ARSPiece to be placed.
     */
    public static void registerCorridorPiece(TardisARSPiece arsPiece) {
        CORRIDORS.add(arsPiece);
    }

    /**
     * Register a new console room piece to be featured in corridor generation.
     * @param arsPiece ARSPiece to be placed.
     */
    public static void registerConsoleRoomPiece(TardisARSPiece arsPiece) {
        CONSOLE_ROOMS.add(arsPiece);
    }


    private static ResourceLocation createCorridorRL(String id) {
        return Helper.createRL("corridor/" + id);
    }

    private static ResourceLocation createConsoleRoomRL(String id) {
        return Helper.createRL("console_room/" + id);
    }
}
