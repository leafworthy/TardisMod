package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.block.model.ItemTransforms;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.blockentities.crafting.DraftingTableTile;
import net.tardis.mod.helpers.WorldHelper;

public class DraftingTileRenderer implements BlockEntityRenderer<DraftingTableTile> {

    public DraftingTileRenderer(BlockEntityRendererProvider.Context context){

    }

    @Override
    public void render(DraftingTableTile tile, float pPartialTick, PoseStack pose, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {
        //If this has a result, show that
        pose.pushPose();
        pose.translate(0.5, 1, 0.5);
        pose.mulPose(Vector3f.YP.rotationDegrees(WorldHelper.getHorizontalFacing(tile.getBlockState()).toYRot()));
        pose.mulPose(Vector3f.XP.rotationDegrees(90));
        if(!tile.getItem(9).isEmpty()){
            Minecraft.getInstance().getItemRenderer().renderStatic(tile.getItem(9), ItemTransforms.TransformType.FIXED, LightTexture.FULL_SKY, pPackedOverlay, pose, pBufferSource, 0);
        }
        else{
            pose.scale(0.333F, 0.333F, 0.333F);
            for(int i = 0; i < 9; ++i){
                final float startX = -0.75F, startY = -0.75F;
                pose.pushPose();
                pose.translate(startX + (i / 3) * 0.75, startY + (i % 3) * 0.75, 0);

                ItemStack stack = tile.getItem(i);
                if(!stack.isEmpty()) {
                    Minecraft.getInstance().getItemRenderer().renderStatic(stack, ItemTransforms.TransformType.FIXED, LightTexture.FULL_SKY, pPackedOverlay, pose, pBufferSource, 0);
                }
                pose.popPose();
            }
        }
        pose.popPose();
    }
}
