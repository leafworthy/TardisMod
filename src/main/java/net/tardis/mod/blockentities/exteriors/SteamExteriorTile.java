package net.tardis.mod.blockentities.exteriors;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.ExteriorTile;
import net.tardis.mod.blockentities.TileRegistry;

public class SteamExteriorTile extends ExteriorTile {

    public SteamExteriorTile(BlockPos pos, BlockState state) {
        super(TileRegistry.STEAM_EXTERIOR.get(), pos, state);
    }
}
