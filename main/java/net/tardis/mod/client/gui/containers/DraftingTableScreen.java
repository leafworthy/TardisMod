package net.tardis.mod.client.gui.containers;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.tardis.mod.blockentities.crafting.DraftingTableTile;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.menu.DraftingTableMenu;

public class DraftingTableScreen extends AbstractContainerScreen<DraftingTableMenu> {

    public static final ResourceLocation TEX = Helper.createRL("textures/screens/containers/drafting_table.png");

    public DraftingTableScreen(DraftingTableMenu pMenu, Inventory pPlayerInventory, Component pTitle) {
        super(pMenu, pPlayerInventory, pTitle);
    }

    @Override
    protected void renderBg(PoseStack pPoseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(pPoseStack);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEX);
        blit(pPoseStack, this.width / 2 - 176 / 2, this.height / 2 - 166 / 2, 0, 0, 176, 166);
    }
}
