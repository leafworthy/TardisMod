package net.tardis.mod.world;

import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.saveddata.SavedData;
import net.tardis.mod.dimension.DimensionHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TardisWorldData extends SavedData {

    public final List<ResourceKey<Level>> tardisLevelKeys = new ArrayList<>();

    @Override
    public CompoundTag save(CompoundTag tag) {
        ListTag list = new ListTag();
        for(ResourceKey<Level> key : tardisLevelKeys){
            list.add(StringTag.valueOf(key.location().toString()));
        }
        tag.put("tardis_world_data", list);
        return tag;
    }

    public static TardisWorldData deserialize(CompoundTag tag){

        TardisWorldData data = new TardisWorldData();
        data.tardisLevelKeys.clear();

        ListTag list = tag.getList("tardis_world_data", Tag.TAG_STRING);

        for(Tag keyTag : list){
            data.tardisLevelKeys.add(ResourceKey.create(Registry.DIMENSION_REGISTRY, new ResourceLocation(((StringTag)keyTag).getAsString())));
        }

        return data;
    }

    public static void registerOldTARDISes(MinecraftServer server, List<ResourceKey<Level>> keyList){
        for(ResourceKey<Level> level : keyList){
            DimensionHelper.getOrCreateWorld(server, level, DimensionHelper::createTARDISStem);
        }
    }
}
