package net.tardis.mod.cap.level;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.tardis.mod.control.ControlRegistry;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.control.datas.ControlDataEnum;
import net.tardis.mod.exterior.Exterior;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.flight_event.FlightEvent;
import net.tardis.mod.flight_event.FlightEventRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.LandingHelper;
import net.tardis.mod.misc.enums.FlightState;
import net.tardis.mod.misc.InteriorManager;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.enums.LandingType;
import net.tardis.mod.misc.landing.LandingContext;
import net.tardis.mod.misc.landing.TardisLandingContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TardisFlightStateMessage;

import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;

public class TardisCap implements ITardisLevel{

    private final Level level;
    private SpaceTimeCoord location = SpaceTimeCoord.ZERO;
    private SpaceTimeCoord destination = SpaceTimeCoord.ZERO;
    private HashMap<ControlType, ControlData<?>> controlData = new HashMap<>();
    private Exterior exterior;

    private final InteriorManager interiorManager;

    private int ticksInFlight = 0;
    private FlightState flightState = FlightState.LANDED;
    private int reachedDestinationTick = 0;
    private int artron;
    private int maxArtron;
    private FlightEvent flightEvent;

    public TardisCap(Level level){
        this.level = level;

        this.interiorManager = new InteriorManager(this);
    }

    @Override
    public Level getLevel() {
        return this.level;
    }

    @Override
    public SpaceTimeCoord getLocation() {
        return this.location;
    }

    @Override
    public SpaceTimeCoord getDestination() {
        return this.destination;
    }

    @Override
    public void setDestination(SpaceTimeCoord coord) {
        //TODO: If this is set in fight, update time and shit
        this.destination = coord;
    }

    @Override
    public void setDestination(ResourceKey<Level> level, BlockPos pos) {
        this.setDestination(new SpaceTimeCoord(level, pos));
    }

    @Override
    public void setLocation(SpaceTimeCoord coord) {
        this.location = coord;
    }

    @Override
    public void setExterior(ExteriorType type) {
        this.exterior = type.create(this);
    }

    @Override
    public Exterior getExterior() {
        if(exterior == null)
            this.exterior = ExteriorRegistry.STEAM.get().create(this);
        return this.exterior;
    }

    @Override
    public int ticksInFlight() {
        return this.ticksInFlight;
    }

    @Override
    public boolean isInFlight() {
        return this.ticksInFlight > 0;
    }

    @Override
    public boolean isInVortex() {
        return this.isInFlight();
    }

    @Override
    public int getArtron() {
        return this.artron;
    }

    @Override
    public int getMaxArtron() {
        return this.maxArtron;
    }

    @Override
    public FlightEvent getCurrentFlightEvent() {
        return this.flightEvent;
    }

    public void startFlightEvent(){
        this.flightEvent = null; // TODO: Make this a random event
        this.flightEvent.onStart();
    }

    @Override
    public void takeoff() {
        this.setFlightState(0, this.calculateTimeToLand(), FlightState.FLYING);
        this.flightEvent = null;

        //TODO: Handle land logic

        if(!this.level.isClientSide){
            ServerLevel destination = this.level.getServer().getLevel(this.getLocation().getLevel());
            this.getExterior().demat(destination);
        }

    }

    @Override
    public boolean initLanding() {
        Optional<BlockPos> landPos = this.calculateLandingPosition();
        boolean isClient = this.level.isClientSide;
        if(landPos.isPresent() || isClient){
            if(!isClient){
                landPos.ifPresent(pos -> this.destination = new SpaceTimeCoord(this.destination.getLevel(), pos));
            }
            this.completeLanding();
            return true;
        }
        return false;
    }

    @Override
    public void completeLanding() {
        this.setFlightState(0, 0, FlightState.LANDED);

        if(!this.level.isClientSide){
            ServerLevel destination = this.level.getServer().getLevel(this.destination.getLevel());
            if(destination == null)
                destination = this.level.getServer().overworld();

            this.getExterior().remat(destination);
            this.setLocation(this.getDestination());
        }
        ((ControlData<Float>)this.getControlDataOrCreate(ControlRegistry.THROTTLE.get())).set(0.0F);
    }

    /** Empty if we can't land near here */
    public Optional<BlockPos> calculateLandingPosition(){
        if(this.level.isClientSide)
            return Optional.empty();

        ServerLevel level = this.level.getServer().getLevel(this.destination.getLevel());

        if(level == null)
            return Optional.empty();

        ControlDataEnum<LandingType> type = (ControlDataEnum<LandingType>) this.getControlDataOrCreate(ControlRegistry.LANDING_TYPE.get());

        BlockPos pos = LandingHelper.findValidLandPos(level, this.destination.getPos(), new TardisLandingContext(this.exterior, false, false), type.get());
        System.out.println(Thread.currentThread().getName() + ": " + pos);
        return pos.equals(BlockPos.ZERO) ? Optional.empty() : Optional.of(pos);

    }

    @Override
    public void flightTick() {
        ++this.ticksInFlight;

        //TODO: Make this only happen with stabilizers
        //Automatically land when destination is reached
        if(this.ticksInFlight > this.reachedDestinationTick || this.flightState != FlightState.FLYING)
            this.initLanding();

        if(this.getCurrentFlightEvent() != null && this.getCurrentFlightEvent().isComplete()){
            this.flightEvent = null;
        }

    }

    @Override
    public InteriorManager getInteriorManager() {
        return this.interiorManager;
    }

    @Override
    public ControlData<?> getControlDataOrCreate(ControlType type) {
        Objects.requireNonNull(type); //Throw an error if th
        if(!this.controlData.containsKey(type)){
            ControlData<?> data = type.createData(this);
            this.controlData.put(type, data);
            return data;
        }

        return this.controlData.get(type);
    }

    @Override
    public void tick() {
        if(this.flightState == FlightState.FLYING) {
            this.flightTick();
        }

    }

    @Override
    public void setFlightState(int flightTicks, int reachedTick, FlightState state) {
        this.flightState = state;
        this.ticksInFlight = flightTicks;
        this.reachedDestinationTick = reachedTick;
        if(!this.level.isClientSide()){
            Network.sendPacketToDimension(this.level.dimension(), new TardisFlightStateMessage(this.ticksInFlight, this.reachedDestinationTick, this.flightState));
        }

    }

    public int calculateTimeToLand(){
        double distance = this.location.getPos().distSqr(this.destination.getPos());

        return 500;
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.put("location", this.location.serializeNBT());
        tag.put("destination", this.destination.serializeNBT());
        tag.putInt("flight_ticks", this.ticksInFlight);
        tag.putInt("flight_state", this.flightState.ordinal());
        tag.putString("exterior", ExteriorRegistry.REGISTRY.get().getKey(this.getExterior().getType()).toString());
        tag.put("exterior_data", this.exterior.serializeNBT());
        tag.put("interior_manager", this.interiorManager.serializeNBT());
        //Control Saving
        ListTag controlTag = new ListTag();
        for(ControlData<?> data : this.controlData.values()){
            CompoundTag control = data.serializeNBT();
            if(control != null && !control.isEmpty()){
                control.putString("id", ControlRegistry.REGISTRY.get().getKey(data.getType()).toString());
                controlTag.add(control);
            }
        }
        tag.put("controls", controlTag);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        this.location = SpaceTimeCoord.of(nbt.getCompound("location"));
        this.destination = SpaceTimeCoord.of(nbt.getCompound("destination"));
        this.ticksInFlight = nbt.getInt("flight_ticks");
        this.flightState = FlightState.values()[nbt.getInt("flight_state")];
        this.setExterior(ExteriorRegistry.REGISTRY.get().getValue(new ResourceLocation(nbt.getString("exterior"))));
        this.exterior.deserializeNBT(nbt.getCompound("exterior_data"));
        this.interiorManager.deserializeNBT(nbt.getCompound("interior_manager"));

        //Control data
        ListTag controlTag = nbt.getList("controls", Tag.TAG_COMPOUND);
        for(Tag tag : controlTag){
            CompoundTag controlData = (CompoundTag) tag;
            ControlType type = ControlRegistry.REGISTRY.get().getValue(new ResourceLocation(controlData.getString("id")));
            if(type != null){
                this.getControlDataOrCreate(type).deserializeNBT(controlData);
            }

        }
    }
}
