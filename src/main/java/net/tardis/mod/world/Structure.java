package net.tardis.mod.world;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.block.entity.StructureBlockEntity;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.minecraftforge.registries.DeferredRegister;
import net.tardis.mod.helpers.Helper;

public class Structure {

    public static ResourceKey<StructureSet> TARDIS_STRUCTURES = ResourceKey.create(Registry.STRUCTURE_SET_REGISTRY, Helper.createRL("tardis"));

    public static void register(){
    }

}
