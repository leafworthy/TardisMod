package net.tardis.mod.helpers;

import net.minecraft.client.Minecraft;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.level.Level;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;

import java.util.Objects;

public class ClientHelper {

    public static AnimationState getControlState(ITardisLevel level, ControlType type){
        Objects.requireNonNull(type);
        return level.getControlDataOrCreate(type).getUseAnimationState();
    }

}
