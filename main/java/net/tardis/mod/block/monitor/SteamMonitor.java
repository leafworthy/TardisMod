package net.tardis.mod.block.monitor;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.tardis.mod.blockentities.BaseMonitorTile;
import net.tardis.mod.blockentities.TileRegistry;
import org.jetbrains.annotations.Nullable;

public class SteamMonitor extends BaseMonitorBlock {
    public SteamMonitor(Properties pProperties) {
        super(BlockBehaviour.Properties.of(Material.METAL)
                .sound(SoundType.GLASS), TileRegistry.MONITOR);
    }

}
