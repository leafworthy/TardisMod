package net.tardis.mod.control;

import java.util.function.BiFunction;
import java.util.function.Function;

import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;

public class ControlType {

    private final Function<ControlType, Control> factory;
    private final BiFunction<ControlType, ITardisLevel, ControlData<?>> dataFactory;

    public ControlType(Function<ControlType, Control> factory, BiFunction<ControlType, ITardisLevel, ControlData<?>> data){
        this.factory = factory;
        this.dataFactory = data;
    }

    public ControlData<?> createData(ITardisLevel level){
        return this.dataFactory.apply(this, level);
    }

    public Control create(){
        return this.factory.apply(this);
    }

}
