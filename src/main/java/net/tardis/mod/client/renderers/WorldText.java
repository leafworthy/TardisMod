package net.tardis.mod.client.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Matrix4f;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.network.chat.Component;

import java.util.List;


public class WorldText {

    public static final float BASE_SCALE = 0.01F;
    public int pixelPadding = 2;
    public int color = 0x000000;
    public boolean dropShadow = false;
    private Font font;
    public float width, height;

    public WorldText(float width, float height){
        this.width = width;
        this.height = height;
        this.font = Minecraft.getInstance().font;
    }

    public void renderText(List<Component> text, PoseStack pose, MultiBufferSource source, int packedLight){
        pose.pushPose();
        pose.scale(BASE_SCALE, BASE_SCALE, BASE_SCALE);
        for(int i = 0; i < text.size(); ++i){

            pose.pushPose();
            float lineWidth = font.width(text.get(i).getVisualOrderText()) * BASE_SCALE;

            pose.translate(0, i * (this.font.lineHeight + pixelPadding), 0);
            if(lineWidth > this.width){
                float diff = (this.width / lineWidth);
                pose.scale(diff, diff, diff);
            }

            this.font.drawInBatch(text.get(i), 0, 0, color, dropShadow, pose.last().pose(), source, true, 0x00000, packedLight);
            pose.popPose();
        }
        pose.popPose();
    }

    public void renderText(List<Component> text, PoseStack matrix, MultiBufferSource source){
        this.renderText(text, matrix, source, LightTexture.FULL_BRIGHT);
    }


    public WorldText withColor(int color){
        this.color = color;
        return this;
    }

    public WorldText withPixelPadding(int pixels){
        this.pixelPadding = pixels;
        return this;
    }

    public WorldText withDropShadow(boolean shadow){
        this.dropShadow = shadow;
        return this;
    }

    public enum ClipType{
        SCALE,
        CLIP;
    }
}
