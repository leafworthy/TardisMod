package net.tardis.mod.datagen;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.tardis.mod.Tardis;

public class TardisItemModelProvider extends ItemModelProvider{

    public TardisItemModelProvider(DataGenerator generator, ExistingFileHelper existingFileHelper) {
        super(generator, Tardis.MODID, existingFileHelper);
    }

    @Override
    protected void registerModels() {



    }
}
