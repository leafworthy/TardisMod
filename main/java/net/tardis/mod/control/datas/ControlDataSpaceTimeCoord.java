package net.tardis.mod.control.datas;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.misc.SpaceTimeCoord;

public class ControlDataSpaceTimeCoord extends ControlData<SpaceTimeCoord>{

    public ControlDataSpaceTimeCoord(ControlType type, ITardisLevel tardis) {
        super(type, tardis, SpaceTimeCoord.ZERO);
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        this.get().encode(buf);
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.set(SpaceTimeCoord.decode(buf));
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        this.set(SpaceTimeCoord.of(tag.getCompound("value")));
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        tag.put("value", this.get().serializeNBT());
    }
}
