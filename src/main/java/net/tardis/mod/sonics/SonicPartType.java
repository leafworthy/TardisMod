package net.tardis.mod.sonics;

public enum SonicPartType {
    EMMITER,
    SHAFT,
    HANDLE,
    POMMEL
}
