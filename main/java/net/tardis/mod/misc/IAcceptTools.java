package net.tardis.mod.misc;

public interface IAcceptTools {

    boolean doWork(ToolType type);

}
