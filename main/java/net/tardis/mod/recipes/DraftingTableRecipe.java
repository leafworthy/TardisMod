package net.tardis.mod.recipes;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.blockentities.crafting.DraftingTableTile;
import net.tardis.mod.misc.ToolType;

import java.util.*;

public class DraftingTableRecipe implements Recipe<DraftingTableTile> {


    private final ResourceLocation id;
    public final List<String> pattern = new ArrayList<>();
    public final HashMap<Character, Ingredient> key = new HashMap<>();
    public final EnumMap<ToolType, Integer> tools = new EnumMap<>(ToolType.class);
    private final ItemStack result;

    private int width;
    private int height;

    public DraftingTableRecipe(ResourceLocation id, List<String> pattern, HashMap<Character, Ingredient> keys, EnumMap<ToolType, Integer> tools, ItemStack result){
        this.id = id;
        this.pattern.addAll(pattern);
        this.key.putAll(keys);
        this.tools.putAll(tools);
        this.result = result;
        this.setSizeByPattern(pattern);
    }

    @Override
    public boolean matches(DraftingTableTile table, Level level) {

        Ingredient[] ingredients = fullList();
        for(int slot = 0; slot < 9; ++slot){
            if(!ingredients[slot].test(table.getItem(slot))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public ItemStack assemble(DraftingTableTile container) {
        return this.result;
    }

    @Override
    public boolean canCraftInDimensions(int pWidth, int pHeight) {
        return pWidth >= this.width && pHeight >= this.height;
    }

    @Override
    public ItemStack getResultItem() {
        return this.result.copy();
    }

    @Override
    public ResourceLocation getId() {
        return this.id;
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return RecipeRegistry.DRAFTING_TABLE_SERIALIZER.get();
    }

    @Override
    public RecipeType<?> getType() {
        return RecipeRegistry.DRAFTING_TABLE_TYPE;
    }

    public Ingredient[] fullList(){
        Ingredient[] ings = new Ingredient[9];
        int i = 0;
        for(String line : this.pattern){
            for(Ingredient ing : this.makePatternArray(line)){
                ings[i] = ing;
                ++i;
            }
        }
        return ings;
    }

    public boolean rowMatchesString(String row, ItemStack... stack){
        Ingredient[] ings = makePatternArray(row);
        if(stack.length != 3 || ings.length != 3){
            for(int i = 0; i < 3; ++i){
                if(!ings[i].test(stack[i])){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public Ingredient[] makePatternArray(String line){

        char[] chars = line.toCharArray();

        Ingredient[] ingredients = new Ingredient[3];
        for(int i = 0; i < 3; ++i){
            if(chars[i] == ' ')
                ingredients[i] = Ingredient.EMPTY;
            else {
                if(this.key.containsKey(chars[i]))
                    ingredients[i] = this.key.get(chars[i]);
                else return null; //If there is a key we don't recognise, complain lol
            }
        }
        return ingredients;
    }

    public void setSizeByPattern(List<String> pattern){
        this.height = pattern.size();

        int width = 0;
        for(String line : pattern){
            int lineWidth = line.length();
            if(width < lineWidth)
                width = lineWidth;
        }
        this.width = width;

    }

    public Set<ToolType> getToolRequirements() {
        return this.tools.keySet();
    }

    public int getToolRequirement(ToolType type){
        return this.tools.get(type);
    }
}
