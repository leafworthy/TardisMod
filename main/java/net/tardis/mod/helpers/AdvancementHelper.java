package net.tardis.mod.helpers;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.critereon.PlayerTrigger;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;

public class AdvancementHelper {

    /**
     * @param player The ServerPlayer to check details of.
     * @param resourceLocation The ID of the advancement to check for
     * @return If the player has already gained the advancement.
     */
    public static boolean alreadyAwardedTo(ServerPlayer player, ResourceLocation resourceLocation) {
        Advancement advancement = player.getServer().getAdvancements().getAdvancement(resourceLocation);
        if (advancement == null) {
            return false; // A false negative may occur here if the advancement doesn't exist.
        }
        return player.getAdvancements().getOrStartProgress(advancement).isDone();
    }

    /**
     * @param player The ServerPlayer to grant the advancement to.
     * @param trigger Criteria Trigger to trigger.
     * */
    public static void firePlayerTrigger(ServerPlayer player, PlayerTrigger trigger) {
        trigger.trigger(player); // This function may be subject to change, could be abstracted more.
    }

}
