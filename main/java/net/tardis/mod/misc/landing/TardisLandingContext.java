package net.tardis.mod.misc.landing;

import net.minecraft.core.BlockPos;
import net.minecraft.core.GlobalPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.tardis.mod.exterior.Exterior;

public class TardisLandingContext extends LandingContext{

    public final Exterior exterior;

    public TardisLandingContext(Exterior exterior, boolean water, boolean air){
        super(water, air);
        this.exterior = exterior;
    }

    @Override
    public boolean canLandAt(Level level, BlockPos pos) {
        AABB box = this.exterior.getSize().move(pos);
        for(int x = (int)Math.floor(box.minX); x < (int)Math.ceil(box.maxX); ++x){
            for(int y = (int)Math.floor(box.minY); y < (int)Math.ceil(box.maxY); ++y){
                for(int z = (int)Math.floor(box.minZ); z < (int)Math.ceil(box.maxZ); ++z){
                    BlockPos cPos = new BlockPos(x, y, z);
                    if(!this.isBlockEmpty(level, cPos))
                        return false; //Return false if there's something in the way of this exterior
                }
            }

        }
        return this.canLandonTopOf(level, pos.below());//true;
    }
}
