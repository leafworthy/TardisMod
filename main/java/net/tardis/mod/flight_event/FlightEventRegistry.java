package net.tardis.mod.flight_event;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.control.ControlRegistry;
import net.tardis.mod.helpers.Helper;

import java.util.function.Supplier;

public class FlightEventRegistry {

    public static final DeferredRegister<FlightEventType> FLIGHT_EVENTS = DeferredRegister.create(Helper.createRL("flight_event"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<FlightEventType>> REGISTRY = FLIGHT_EVENTS.makeRegistry(RegistryBuilder::new);

    public static final RegistryObject<FlightEventType> TIME_WINDS = FLIGHT_EVENTS.register("time_winds", () -> new FlightEventType(TimeWindsEvent::new, tardis -> (float)tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).get() < 1.0F));



}
