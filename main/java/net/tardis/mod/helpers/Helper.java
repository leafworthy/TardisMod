package net.tardis.mod.helpers;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.IForgeRegistry;
import net.tardis.mod.Tardis;

import java.util.Optional;

public class Helper {

    public static final ResourceLocation createRL(String name){
        return new ResourceLocation(Tardis.MODID, name);
    }

    public static Vec3 blockPosToVec3(BlockPos pos, boolean centered) {
        double offset = centered ? 0.5 : 0;
        return new Vec3(pos.getX() + offset, pos.getY() + offset, pos.getZ() + offset);
    }

    public static <T> Optional<T> readRegistryFromString(CompoundTag tag, IForgeRegistry<T> registry, String name) {
        if(tag.contains(name)){
            ResourceLocation rl = new ResourceLocation(tag.getString(name));
            return Optional.of(registry.getValue(rl));
        }
        return Optional.empty();
    }

    public static <V> void writeRegistryToNBT(CompoundTag tag, IForgeRegistry<V> registry, V value, String name){
        if(registry.containsValue(value)){
            ResourceLocation rl = registry.getKey(value);
            tag.putString(name, rl.toString());
        }
    }
}
