package net.tardis.mod.network;

import net.minecraft.client.Minecraft;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.misc.IDoor;
import net.tardis.mod.network.packets.*;

import java.util.Set;

public class ClientPacketHandler {

    public static void handleDimSyncPacket(SyncDimensionListMessage mes){

        if(Minecraft.getInstance().player == null || Minecraft.getInstance().player.connection.levels() == null)
            return;

        Set<ResourceKey<Level>> levels = Minecraft.getInstance().player.connection.levels();
        //If this player knows about this dimension
        if(levels.contains(mes.level)){
            //If remove
            if(!mes.add){
                levels.remove(mes.level);
            }
        }
        //If player does not know about this dim and we're trying to add it
        else if(mes.add){
            levels.add(mes.level);
        }
    }

    public static void handleControlDataPacket(ControlDataMessage mes) {
        if(Minecraft.getInstance().level != null){
            Minecraft.getInstance().level.getCapability(Capabilities.TARDIS).ifPresent(cap -> {
                mes.data.copyTo(cap.getControlDataOrCreate(mes.data.getType()));
            });
        }
    }

    public static void handleControlSizeMessage(ControlEntitySizeMessage mes) {
        Level level = Minecraft.getInstance().level;
        if(level != null){
            if(level.getEntity(mes.entityID) instanceof ControlEntity control){
                control.setSize(mes.size);
            }
        }
    }

    public static void handleTardisFlightState(TardisFlightStateMessage mes) {
        System.out.println("Got flight packet with %i, %i, %s".formatted(mes.flightTicks, mes.reachedTick, mes.state));
        Level level = Minecraft.getInstance().level;
        if(level != null){
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.setFlightState(mes.flightTicks, mes.reachedTick, mes.state);
            });
        }
    }

    public static void handleUpdateDoorState(UpdateDoorStateMessage mes) {
        Level level = Minecraft.getInstance().level;
        if(level != null){
            if(level.getBlockEntity(mes.pos) instanceof IDoor door){
                door.getDoorHandler().setDoorState(mes.validStates, mes.state);
                door.getDoorHandler().setLocked(mes.locked);
            }
        }
    }

    public static void handleTardisLoadMessage(TardisLoadMessage mes) {
        Level level = Minecraft.getInstance().level;

        if(level != null){
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.deserializeNBT(mes.tag);
            });
        }
    }
}
