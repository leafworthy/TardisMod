package net.tardis.mod.events;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.profiling.jfr.event.ChunkGenerationEvent;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.JigsawBlock;
import net.minecraft.world.level.block.entity.JigsawBlockEntity;
import net.minecraftforge.event.AddReloadListenerEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.PlayerWakeUpEvent;
import net.minecraftforge.event.level.ChunkEvent;
import net.minecraftforge.event.level.LevelEvent;
import net.minecraftforge.event.server.ServerStartedEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.advancements.TardisAdvancementTriggers;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.GenericProvider;
import net.tardis.mod.cap.level.TardisCap;
import net.tardis.mod.control.ControlPositionData;
import net.tardis.mod.control.ControlPositionDataReloadListener;
import net.tardis.mod.dimension.DimensionTypes;
import net.tardis.mod.helpers.AdvancementHelper;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.TeleportEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Mod.EventBusSubscriber(modid = Tardis.MODID)
public class CommonEvents {

    @SubscribeEvent
    public static void attachWorldCapabilies(AttachCapabilitiesEvent<Level> event){
        if(event.getObject().dimensionTypeId().location().equals(DimensionTypes.TARDIS_TYPE.location()))
            event.addCapability(Helper.createRL("tardis"), new GenericProvider(Capabilities.TARDIS, new TardisCap(event.getObject())));
    }

    @SubscribeEvent
    public static void onWorldTick(TickEvent.LevelTickEvent event){
        if(event.phase == TickEvent.Phase.END){
            event.level.getCapability(Capabilities.TARDIS).ifPresent(cap -> cap.tick());

            //Handle server world only stuffs
            if(event.level instanceof ServerLevel level){

                //Handle teleporting
                List<UUID> teleportsToRemove = new ArrayList<>();
                for(TeleportEntry entry: TeleportEntry.TELEPORTS.values()){
                    if(entry.shouldRun(level)){ //If this is the level the entity is leaving from
                        if(entry.tick(level))
                            teleportsToRemove.add(entry.entityID); //Schedule removal of all completed teleports
                    }
                }
                teleportsToRemove.stream().forEach(id -> TeleportEntry.TELEPORTS.remove(id));
            }

        }
    }

    /**
     * Used for granting the "Bad Dreams" advancement.
     * */
    @SubscribeEvent
    public static void onPlayerWakeUp(PlayerWakeUpEvent event) {

        // Todo: Actual prerequisites for this to occur.
        // Todo: Attach recipe to advancement.

        if (!event.getEntity().getLevel().isClientSide()) {
            ServerPlayer player = (ServerPlayer) event.getEntity();
            if (player.getSleepTimer() > 60) {
                AdvancementHelper.firePlayerTrigger((ServerPlayer) event.getEntity(), TardisAdvancementTriggers.SLEPT_IN_BED);
            }
        }
    }
    
    @SubscribeEvent
    public static void onServerResourceReload(AddReloadListenerEvent event){
        event.addListener(ControlPositionDataReloadListener.INSTANCE);
    }
    
//    @SubscribeEvent
//    public static void onServerLoad(ServerStartedEvent event){
//        ControlData.load(event.getServer().getResourceManager());
//    }

}
