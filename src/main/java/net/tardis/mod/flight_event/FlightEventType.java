package net.tardis.mod.flight_event;

import net.tardis.mod.cap.level.ITardisLevel;

import java.util.function.Function;
import java.util.function.Predicate;

public class FlightEventType {

    public final Function<ITardisLevel, FlightEvent> supplier;
    public final Predicate<ITardisLevel> applies;

    public FlightEventType(Function<ITardisLevel,FlightEvent> supplier, Predicate<ITardisLevel> applies){
        this.supplier = supplier;
        this.applies = applies;
    }

    public FlightEvent create(ITardisLevel level){
        return this.supplier.apply(level);
    }

    public boolean canApply(ITardisLevel level){
        return this.applies.test(level);
    }

}
