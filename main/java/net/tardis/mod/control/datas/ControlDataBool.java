package net.tardis.mod.control.datas;

import net.minecraft.nbt.ByteTag;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NumericTag;
import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;

public class ControlDataBool extends ControlData<Boolean>{


    public ControlDataBool(ControlType type, ITardisLevel tardis) {
        super(type, tardis, false);
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeBoolean(this.get());
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.set(buf.readBoolean());
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putBoolean("value", this.get());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.set(tag.getBoolean("value"));
    }
}
