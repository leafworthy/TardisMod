package net.tardis.mod.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.MenuScreens;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.block.BlockRenderDispatcher;
import net.minecraftforge.client.ForgeRenderTypes;
import net.minecraftforge.client.RenderTypeGroup;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.BlockRegistry;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.client.gui.containers.DraftingTableScreen;
import net.tardis.mod.client.models.consoles.G8ConsoleModel;
import net.tardis.mod.client.models.consoles.SteamConsoleModel;
import net.tardis.mod.client.models.exteriors.SteamExteriorModel;
import net.tardis.mod.client.models.exteriors.interior_door.SteamInteriorDoorModel;
import net.tardis.mod.client.renderers.RendererControl;
import net.tardis.mod.client.renderers.consoles.ConsoleRenderer;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.client.renderers.tiles.ChameleonInteriorDoorRenderer;
import net.tardis.mod.client.renderers.tiles.DraftingTileRenderer;
import net.tardis.mod.client.renderers.tiles.InteriorDoorRender;
import net.tardis.mod.client.renderers.tiles.MonitorRenderer;
import net.tardis.mod.entity.EntityRegistry;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.menu.MenuRegistry;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ClientRegistry {

    @SubscribeEvent
    public static void registerClient(FMLClientSetupEvent event){
        MenuScreens.register(MenuRegistry.DRAFTING_TABLE.get(), DraftingTableScreen::new);


        ChameleonInteriorDoorRenderer.registerChameleonDoor(ExteriorRegistry.STEAM.get(),
                new InteriorDoorRender<>(Helper.createRL("textures/tiles/interiordoor/steam.png"),
                        context -> new SteamInteriorDoorModel(context.bakeLayer(SteamInteriorDoorModel.LAYER_LOCATION))));


    }

    @SubscribeEvent
    public static void registerModel(EntityRenderersEvent.RegisterLayerDefinitions event){
        event.registerLayerDefinition(G8ConsoleModel.LAYER_LOCATION, G8ConsoleModel::createBodyLayer);
        event.registerLayerDefinition(SteamConsoleModel.LAYER_LOCATION, SteamConsoleModel::createBodyLayer);
        event.registerLayerDefinition(SteamExteriorModel.LAYER_LOCATION, SteamExteriorModel::createBodyLayer);
        event.registerLayerDefinition(SteamInteriorDoorModel.LAYER_LOCATION, SteamInteriorDoorModel::createBodyLayer);
    }

    @SubscribeEvent
    public static void registerRendereres(EntityRenderersEvent.RegisterRenderers event){
        event.registerEntityRenderer(EntityRegistry.CONTROL.get(), RendererControl::new);

        //Consoles
        event.registerBlockEntityRenderer(TileRegistry.G8_CONSOLE.get(), context -> new ConsoleRenderer(context, new G8ConsoleModel(context.bakeLayer(G8ConsoleModel.LAYER_LOCATION)), Helper.createRL("textures/tiles/consoles/g8.png")));
        event.registerBlockEntityRenderer(TileRegistry.STEAM_CONSOLE.get(), context -> new ConsoleRenderer(context, new SteamConsoleModel(context.bakeLayer(SteamConsoleModel.LAYER_LOCATION)), Helper.createRL("textures/tiles/consoles/steam.png")));

        //Exteriors
        event.registerBlockEntityRenderer(TileRegistry.STEAM_EXTERIOR.get(), ExteriorRenderer::new);

        //Tiles
        event.registerBlockEntityRenderer(TileRegistry.DRAFTING_TABLE.get(), DraftingTileRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.INTERIOR_DOOR.get(), ChameleonInteriorDoorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.MONITOR.get(), MonitorRenderer::new);


    }


}
