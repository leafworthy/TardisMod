package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

import java.util.Optional;

public class ExteriorAuxillaryBlock extends Block {

    public ExteriorAuxillaryBlock() {
        super(Properties.of(Material.AIR));
    }

    @Override
    public boolean hasDynamicShape() {
        return true;
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {

        BlockState other = pLevel.getBlockState(pPos.below());
        if(other.getBlock() instanceof ExteriorBlock ext){
            return ext.getShape(other, pLevel, pPos.below(), pContext).move(0, -1, 0);
        }

        return super.getShape(pState, pLevel, pPos, pContext);
    }

    public Optional<BlockState> getExteriorState(LevelAccessor level, BlockPos pos){
        BlockState state = level.getBlockState(pos.below());
        return state.getBlock() instanceof ExteriorBlock ? Optional.of(state) : Optional.empty();
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        Optional<BlockState> other = this.getExteriorState(pLevel, pPos);
        if(other.isPresent()){
            System.out.println(pHit.getBlockPos());
            return other.get().use(pLevel, pPlayer, pHand, new BlockHitResult(pHit.getLocation(), pHit.getDirection(), pPos.below(), pHit.isInside()));
        }

        return super.use(pState, pLevel, pPos, pPlayer, pHand, pHit);
    }
}
