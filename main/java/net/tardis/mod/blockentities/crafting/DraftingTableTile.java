package net.tardis.mod.blockentities.crafting;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.PacketListener;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.PacketUtils;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BaseContainerBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.ToolActions;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.IAcceptTools;
import net.tardis.mod.misc.ToolType;
import net.tardis.mod.recipes.DraftingTableRecipe;
import net.tardis.mod.recipes.RecipeRegistry;

import javax.annotation.Nullable;
import java.util.EnumMap;

public class DraftingTableTile extends BaseContainerBlockEntity implements IAcceptTools {

    public final ItemStackHandler inventory = new ItemStackHandler(10);
    public final EnumMap<ToolType, Integer> toolWork = new EnumMap<ToolType, Integer>(ToolType.class);
    private DraftingTableRecipe recipe;

    public DraftingTableTile(BlockPos p_155229_, BlockState p_155230_) {
        super(TileRegistry.DRAFTING_TABLE.get(), p_155229_, p_155230_);
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        this.inventory.deserializeNBT(tag.getCompound("inventory"));
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.put("inventory", this.inventory.serializeNBT());


    }

    @Override
    protected Component getDefaultName() {
        return null;
    }

    @Override
    protected AbstractContainerMenu createMenu(int pContainerId, Inventory pInventory) {
        return null;
    }

    public void findRecipe(){
        for(DraftingTableRecipe rec : level.getRecipeManager().getAllRecipesFor(RecipeRegistry.DRAFTING_TABLE_TYPE)){
            if(rec.matches(this, this.level)){
                this.recipe = rec;
                return;
            }
        }
    }

    public boolean checkToolWork(){
        if(this.isRecipeValid()){
            for(ToolType type : this.recipe.getToolRequirements()){
                int time = this.toolWork.getOrDefault(type, 0);
                if(time < this.recipe.getToolRequirement(type)){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public void finishCrafting(){
        this.toolWork.clear();
        for(int slot = 0; slot < this.inventory.getSlots(); ++slot){
            this.inventory.getStackInSlot(slot).shrink(1);
        }
        this.setItem(9, this.recipe.getResultItem());
    }

    public boolean isRecipeValid(){
        if(this.recipe == null)
            return false;
        return this.recipe.matches(this, this.level);
    }

    @Override
    public int getContainerSize() {
        return this.inventory.getSlots();
    }

    @Override
    public boolean isEmpty() {
        for(int i = 0; i < this.inventory.getSlots(); ++i){
            if(!this.inventory.getStackInSlot(i).isEmpty())
                return false;
        }
        return true;
    }

    @Override
    public ItemStack getItem(int pSlot) {
        return this.inventory.getStackInSlot(pSlot);
    }

    @Override
    public ItemStack removeItem(int pSlot, int pAmount) {
        this.setChanged();
        return this.inventory.getStackInSlot(pSlot).split(pAmount);
    }

    @Override
    public ItemStack removeItemNoUpdate(int pSlot) {
        ItemStack stack = this.inventory.getStackInSlot(pSlot);
        this.inventory.setStackInSlot(pSlot, ItemStack.EMPTY);
        this.setChanged();
        return stack;
    }

    @Override
    public void setItem(int pSlot, ItemStack pStack) {
        this.inventory.setStackInSlot(pSlot, pStack);
        this.setChanged();
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }

    @Nullable
    public DraftingTableRecipe getRecipe(){
        if(this.isRecipeValid())
            return this.recipe;
        this.findRecipe();
        return this.recipe;

    }

    @Override
    public void clearContent() {
        for(int i = 0; i < this.inventory.getSlots(); ++i){
            this.inventory.setStackInSlot(i, ItemStack.EMPTY);
        }
        this.setChanged();
    }

    @Override
    public boolean doWork(ToolType type) {
        if(!this.isRecipeValid())
            this.findRecipe();

        if(this.isRecipeValid()){
            int time = this.toolWork.getOrDefault(type, 0);
            time += 1;
            this.toolWork.put(type, time);
            if(this.checkToolWork())
                this.finishCrafting();
            else return true;
        }
        else this.toolWork.clear();
        return false;
    }

    @org.jetbrains.annotations.Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
       return null;// return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }

    @Override
    public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt) {
        this.handleUpdateTag(pkt.getTag());
    }

    @Override
    public void handleUpdateTag(CompoundTag tag) {
        this.deserializeNBT(tag);
    }
}
