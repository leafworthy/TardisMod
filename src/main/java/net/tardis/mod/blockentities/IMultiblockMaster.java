package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;

import java.util.List;

public interface IMultiblockMaster {

    void createStructure(Level level, BlockPos pos);
    void destroyStructure();

    <T extends BlockEntity & IMultiblock> List<T> getChildren();

}
