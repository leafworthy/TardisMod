package net.tardis.mod.client.models.exteriors;// Made with Blockbench 4.3.1
// Exported for Minecraft version 1.17 - 1.18 with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.Model;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.RenderType;
import net.tardis.mod.blockentities.exteriors.SteamExteriorTile;
import net.tardis.mod.helpers.Helper;

public class SteamExteriorModel extends Model implements IExteriorModel<SteamExteriorTile>{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exterior/steam"), "main");
	private final ModelPart glow;
	private final ModelPart door_rotate_y;
	private final ModelPart boti;
	private final ModelPart box;

	public SteamExteriorModel(ModelPart root) {
		super(tex -> RenderType.entityCutoutNoCull(tex));
		this.glow = root.getChild("glow");
		this.door_rotate_y = root.getChild("door_rotate_y");
		this.boti = root.getChild("boti");
		this.box = root.getChild("box");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition glow = partdefinition.addOrReplaceChild("glow", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition glow_toplamp_glass = glow.addOrReplaceChild("glow_toplamp_glass", CubeListBuilder.create().texOffs(103, 208).addBox(-1.0F, -60.0F, -1.5F, 2.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(32, 208).addBox(-1.0F, -60.0F, 1.5F, 2.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(157, 206).addBox(1.0F, -60.0F, -0.5F, 1.0F, 5.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 191).addBox(-2.0F, -60.0F, -0.5F, 1.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition glow_front_window = glow.addOrReplaceChild("glow_front_window", CubeListBuilder.create().texOffs(205, 132).addBox(1.5F, -44.0556F, -12.9667F, 3.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(205, 132).addBox(-4.5F, -44.0556F, -12.9667F, 3.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.9444F, 0.4167F));

		PartDefinition glow_roof_windows_front = glow.addOrReplaceChild("glow_roof_windows_front", CubeListBuilder.create().texOffs(24, 178).addBox(-4.65F, -3.0556F, -0.1667F, 9.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -47.9444F, -7.5833F));

		PartDefinition glow_side_windows = glow.addOrReplaceChild("glow_side_windows", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.5F, 0.0F, -1.0472F, 0.0F));

		PartDefinition glow_window_1 = glow_side_windows.addOrReplaceChild("glow_window_1", CubeListBuilder.create().texOffs(160, 15).addBox(-3.0F, -3.0F, -0.875F, 6.0F, 6.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(177, 65).addBox(-2.5F, -3.75F, -0.375F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(104, 176).addBox(-2.5F, 2.75F, -0.375F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(75, 198).addBox(2.75F, -2.5F, -0.375F, 1.0F, 5.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(198, 32).addBox(-3.75F, -2.5F, -0.375F, 1.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -40.0F, -13.875F));

		PartDefinition glow_window_2 = glow_side_windows.addOrReplaceChild("glow_window_2", CubeListBuilder.create().texOffs(160, 15).addBox(-2.575F, -3.0F, -14.5F, 6.0F, 6.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(177, 65).addBox(-2.075F, -3.75F, -14.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(104, 176).addBox(-2.075F, 2.75F, -14.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(75, 198).addBox(3.175F, -2.5F, -14.0F, 1.0F, 5.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(198, 32).addBox(-3.325F, -2.5F, -14.0F, 1.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -40.0F, -0.5F, 0.0F, -1.0472F, 0.0F));

		PartDefinition glow_window_3 = glow_side_windows.addOrReplaceChild("glow_window_3", CubeListBuilder.create().texOffs(160, 15).addBox(-2.575F, -3.0F, -14.5F, 6.0F, 6.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(177, 65).addBox(-2.075F, -3.75F, -14.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(104, 176).addBox(-2.075F, 2.75F, -14.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(75, 198).addBox(3.175F, -2.5F, -14.0F, 1.0F, 5.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(198, 32).addBox(-3.325F, -2.5F, -14.0F, 1.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.433F, -40.0F, -0.25F, 0.0F, -2.0944F, 0.0F));

		PartDefinition glow_window_4 = glow_side_windows.addOrReplaceChild("glow_window_4", CubeListBuilder.create().texOffs(160, 15).addBox(-2.575F, -3.0F, -14.5F, 6.0F, 6.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(177, 65).addBox(-2.075F, -3.75F, -14.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(104, 176).addBox(-2.075F, 2.75F, -14.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(75, 198).addBox(3.175F, -2.5F, -14.0F, 1.0F, 5.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(198, 32).addBox(-3.325F, -2.5F, -14.0F, 1.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.437F, -40.0F, 0.2431F, 0.0F, 3.1416F, 0.0F));

		PartDefinition glow_window_5 = glow_side_windows.addOrReplaceChild("glow_window_5", CubeListBuilder.create().texOffs(160, 15).addBox(-2.575F, -3.0F, -14.5F, 6.0F, 6.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(177, 65).addBox(-2.075F, -3.75F, -14.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(104, 176).addBox(-2.075F, 2.75F, -14.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(75, 198).addBox(3.175F, -2.5F, -14.0F, 1.0F, 5.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(198, 32).addBox(-3.325F, -2.5F, -14.0F, 1.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0855F, -40.0F, 0.502F, 0.0F, 2.0944F, 0.0F));

		PartDefinition glow_roof_windows_side_1 = glow_side_windows.addOrReplaceChild("glow_roof_windows_side_1", CubeListBuilder.create().texOffs(164, 176).addBox(-4.75F, -3.0556F, -0.1667F, 9.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -47.9444F, -8.0833F));

		PartDefinition glow_side_2 = glow.addOrReplaceChild("glow_side_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.5F, 0.0F, -2.0944F, 0.0F));

		PartDefinition glow_roof_windows_side_2 = glow_side_2.addOrReplaceChild("glow_roof_windows_side_2", CubeListBuilder.create().texOffs(176, 15).addBox(-4.75F, -3.0556F, -0.1667F, 9.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -47.9444F, -8.0833F));

		PartDefinition glow_side_3 = glow.addOrReplaceChild("glow_side_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.5F, 0.0F, 3.1416F, 0.0F));

		PartDefinition glow_roof_windows_side_3 = glow_side_3.addOrReplaceChild("glow_roof_windows_side_3", CubeListBuilder.create().texOffs(98, 173).addBox(-4.75F, -3.0556F, -0.1667F, 9.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -47.9444F, -8.0833F));

		PartDefinition glow_side_4 = glow.addOrReplaceChild("glow_side_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.5F, 0.0F, 2.0944F, 0.0F));

		PartDefinition glow_roof_windows_side_4 = glow_side_4.addOrReplaceChild("glow_roof_windows_side_4", CubeListBuilder.create().texOffs(172, 47).addBox(-4.75F, -3.0556F, -0.1667F, 9.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -47.9444F, -8.0833F));

		PartDefinition glow_side_5 = glow.addOrReplaceChild("glow_side_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.5F, 0.0F, 1.0472F, 0.0F));

		PartDefinition glow_roof_windows_side_5 = glow_side_5.addOrReplaceChild("glow_roof_windows_side_5", CubeListBuilder.create().texOffs(172, 29).addBox(-4.75F, -3.0556F, -0.1667F, 9.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -47.9444F, -8.0833F));

		PartDefinition door_rotate_y = partdefinition.addOrReplaceChild("door_rotate_y", CubeListBuilder.create().texOffs(102, 96).addBox(-14.0F, 3.5F, -1.0F, 14.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 9).addBox(-14.0F, -26.5F, -0.75F, 14.0F, 35.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(134, 6).addBox(-14.0F, -26.5F, -1.0F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(8, 95).addBox(-3.0F, -24.5F, -1.0F, 3.0F, 28.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 95).addBox(-14.0F, -24.5F, -1.0F, 3.0F, 28.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(196, 22).addBox(-10.5F, -24.0F, -1.0F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(193, 111).addBox(-10.5F, 1.0F, -1.0F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(127, 160).addBox(-10.5F, -23.0F, -1.0F, 1.0F, 24.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(123, 160).addBox(-4.5F, -23.0F, -1.0F, 1.0F, 24.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(180, 185).addBox(-9.0F, -22.5F, -1.0F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(164, 185).addBox(-9.0F, -3.5F, -1.0F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(88, 164).addBox(-9.0F, -18.0F, -1.0F, 4.0F, 14.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(7.0F, 12.5F, -11.0F));

		PartDefinition handle = door_rotate_y.addOrReplaceChild("handle", CubeListBuilder.create().texOffs(70, 103).addBox(-11.5F, -21.5F, -1.25F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 8.5F, 0.0F));

		PartDefinition lock = handle.addOrReplaceChild("lock", CubeListBuilder.create().texOffs(8, 170).addBox(-0.5F, -0.5F, -0.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-11.0F, -17.45F, -0.95F, 0.0873F, 0.0873F, -0.7854F));

		PartDefinition kickplate = door_rotate_y.addOrReplaceChild("kickplate", CubeListBuilder.create().texOffs(48, 85).addBox(-11.5F, -4.5F, -1.25F, 11.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(8, 168).addBox(-11.25F, -4.25F, -1.45F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(98, 166).addBox(-11.25F, -1.75F, -1.45F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(98, 164).addBox(-1.75F, -1.75F, -1.45F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(160, 15).addBox(-1.75F, -4.25F, -1.45F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 8.5F, 0.0F));

		PartDefinition hinge = door_rotate_y.addOrReplaceChild("hinge", CubeListBuilder.create().texOffs(126, 136).addBox(-0.5F, -13.5F, -0.5F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(44, 99).addBox(-0.5F, -2.0F, -0.5F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(26, 86).addBox(-0.5F, 11.0F, -0.5F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -11.0F, -0.05F, 0.0F, -0.6981F, 0.0F));

		PartDefinition boti = partdefinition.addOrReplaceChild("boti", CubeListBuilder.create().texOffs(0, 0).addBox(-7.5F, -49.0F, -10.75F, 15.0F, 48.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition box = partdefinition.addOrReplaceChild("box", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition Lamp = box.addOrReplaceChild("Lamp", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lamp_ribs_1 = Lamp.addOrReplaceChild("lamp_ribs_1", CubeListBuilder.create().texOffs(190, 9).addBox(-2.5F, -58.95F, -2.6F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(189, 158).addBox(-2.5F, -58.95F, 0.65F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(93, 176).addBox(0.0F, -59.0F, -2.0F, 3.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F))
		.texOffs(8, 175).addBox(-3.1F, -59.0F, -2.0F, 3.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, -0.75F, 0.0F));

		PartDefinition lamp_ribs_2 = Lamp.addOrReplaceChild("lamp_ribs_2", CubeListBuilder.create().texOffs(189, 84).addBox(-2.5F, -58.95F, -2.6F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(188, 98).addBox(-2.5F, -58.95F, 0.65F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(131, 172).addBox(0.0F, -59.0F, -2.0F, 3.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F))
		.texOffs(148, 171).addBox(-3.1F, -59.0F, -2.0F, 3.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 0.5F, 0.0F));

		PartDefinition lamp_ribs_3 = Lamp.addOrReplaceChild("lamp_ribs_3", CubeListBuilder.create().texOffs(188, 79).addBox(-2.5F, -58.95F, -2.6F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(187, 178).addBox(-2.5F, -58.95F, 0.65F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(169, 169).addBox(0.0F, -59.0F, -2.0F, 3.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F))
		.texOffs(8, 168).addBox(-3.1F, -59.0F, -2.0F, 3.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 1.75F, 0.0F));

		PartDefinition lamp_base_1 = Lamp.addOrReplaceChild("lamp_base_1", CubeListBuilder.create().texOffs(130, 206).addBox(-1.5F, -55.2F, -2.0F, 3.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lamp_base_1_r1 = lamp_base_1.addOrReplaceChild("lamp_base_1_r1", CubeListBuilder.create().texOffs(201, 203).addBox(-1.5F, -55.2F, -2.0F, 3.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 1.1962F, 0.0F, 3.1416F, 0.0F));

		PartDefinition lamp_base_1_r2 = lamp_base_1.addOrReplaceChild("lamp_base_1_r2", CubeListBuilder.create().texOffs(20, 206).addBox(-1.5F, -55.2F, -2.0F, 3.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5179F, 0.0F, 0.8971F, 0.0F, -2.0944F, 0.0F));

		PartDefinition lamp_base_1_r3 = lamp_base_1.addOrReplaceChild("lamp_base_1_r3", CubeListBuilder.create().texOffs(122, 206).addBox(-1.5F, -55.2F, -2.0F, 3.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5179F, 0.0F, 0.299F, 0.0F, -1.0472F, 0.0F));

		PartDefinition lamp_base_1_r4 = lamp_base_1.addOrReplaceChild("lamp_base_1_r4", CubeListBuilder.create().texOffs(114, 205).addBox(-1.5F, -55.2F, -2.0F, 3.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5179F, 0.0F, 0.8971F, 0.0F, 2.0944F, 0.0F));

		PartDefinition lamp_base_1_r5 = lamp_base_1.addOrReplaceChild("lamp_base_1_r5", CubeListBuilder.create().texOffs(68, 206).addBox(-1.5F, -55.2F, -2.0F, 3.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5179F, 0.0F, 0.299F, 0.0F, 1.0472F, 0.0F));

		PartDefinition lamp_base_2 = Lamp.addOrReplaceChild("lamp_base_2", CubeListBuilder.create().texOffs(100, 200).addBox(-2.6095F, -52.2F, -2.2745F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.25F)), PartPose.offset(1.125F, -1.0F, 0.075F));

		PartDefinition lamp_base_2_r1 = lamp_base_2.addOrReplaceChild("lamp_base_2_r1", CubeListBuilder.create().texOffs(171, 193).addBox(-1.5F, -52.2F, -2.0F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.25F)), PartPose.offsetAndRotation(-1.1095F, 0.0F, 1.2877F, 0.0F, 3.1416F, 0.0F));

		PartDefinition lamp_base_2_r2 = lamp_base_2.addOrReplaceChild("lamp_base_2_r2", CubeListBuilder.create().texOffs(181, 193).addBox(-1.5F, -52.2F, -2.0F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.25F)), PartPose.offsetAndRotation(-0.433F, 0.0F, 0.8971F, 0.0F, -2.0944F, 0.0F));

		PartDefinition lamp_base_2_r3 = lamp_base_2.addOrReplaceChild("lamp_base_2_r3", CubeListBuilder.create().texOffs(197, 105).addBox(-1.5F, -52.2F, -2.0F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.25F)), PartPose.offsetAndRotation(-0.433F, 0.0F, 0.116F, 0.0F, -1.0472F, 0.0F));

		PartDefinition lamp_base_2_r4 = lamp_base_2.addOrReplaceChild("lamp_base_2_r4", CubeListBuilder.create().texOffs(197, 117).addBox(-1.5F, -52.2F, -2.0F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.25F)), PartPose.offsetAndRotation(-1.7859F, 0.0F, 0.8971F, 0.0F, 2.0944F, 0.0F));

		PartDefinition lamp_base_2_r5 = lamp_base_2.addOrReplaceChild("lamp_base_2_r5", CubeListBuilder.create().texOffs(155, 199).addBox(-1.5F, -52.2F, -2.0F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.25F)), PartPose.offsetAndRotation(-1.7859F, 0.0F, 0.116F, 0.0F, 1.0472F, 0.0F));

		PartDefinition lamp_cap = Lamp.addOrReplaceChild("lamp_cap", CubeListBuilder.create().texOffs(79, 185).addBox(-1.5F, -60.5F, -1.75F, 3.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(159, 173).addBox(-1.5F, -60.5F, 0.75F, 3.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(147, 206).addBox(0.25F, -60.5F, -1.0F, 2.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(206, 111).addBox(-2.25F, -60.5F, -1.0F, 2.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(201, 128).addBox(-1.5F, -61.0F, -1.0F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(159, 17).addBox(-0.5F, -61.3F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition roof = box.addOrReplaceChild("roof", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition roof_spine_1 = roof.addOrReplaceChild("roof_spine_1", CubeListBuilder.create(), PartPose.offset(0.0F, -60.0F, 0.0F));

		PartDefinition roof_spine_1_r1 = roof_spine_1.addOrReplaceChild("roof_spine_1_r1", CubeListBuilder.create().texOffs(112, 146).addBox(0.5064F, -51.6981F, -1.0F, 10.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(188, 123).addBox(-5.4936F, -52.8481F, 0.0F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(160, 179).addBox(5.2564F, -50.4481F, -1.0F, 5.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 60.0F, 0.0F, 0.0F, 0.0F, 0.1484F));

		PartDefinition roof_spine_4 = roof.addOrReplaceChild("roof_spine_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -60.0F, 1.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition roof_spine_2_r1 = roof_spine_4.addOrReplaceChild("roof_spine_2_r1", CubeListBuilder.create().texOffs(137, 123).addBox(0.5064F, -51.6981F, -1.0F, 10.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(183, 155).addBox(-5.4936F, -52.8481F, 0.0F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(144, 178).addBox(5.0064F, -50.4481F, -1.0F, 5.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 60.0F, 0.0F, 0.0F, 0.0F, 0.1484F));

		PartDefinition roof_spine_5 = roof.addOrReplaceChild("roof_spine_5", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.225F, -60.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition roof_spine_3_r1 = roof_spine_5.addOrReplaceChild("roof_spine_3_r1", CubeListBuilder.create().texOffs(100, 134).addBox(0.5064F, -51.6981F, -1.0F, 10.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(182, 95).addBox(-5.4936F, -52.8481F, 0.0F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 60.0F, 0.0F, 0.0F, 0.0F, 0.1484F));

		PartDefinition roof_spine_3_r2 = roof_spine_5.addOrReplaceChild("roof_spine_3_r2", CubeListBuilder.create().texOffs(57, 178).addBox(5.0064F, -50.4481F, -1.0F, 5.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 60.1F, 0.0F, 0.0F, 0.0F, 0.1484F));

		PartDefinition roof_spine_6 = roof.addOrReplaceChild("roof_spine_6", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.875F, -60.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition roof_spine_4_r1 = roof_spine_6.addOrReplaceChild("roof_spine_4_r1", CubeListBuilder.create().texOffs(16, 104).addBox(-0.2436F, -51.6981F, -1.0F, 10.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(0, 152).addBox(-5.4936F, -52.8481F, 0.0F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(41, 178).addBox(4.2064F, -50.4481F, -1.0F, 5.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 60.0F, 0.0F, 0.0F, 0.0F, 0.1484F));

		PartDefinition roof_spine_2 = roof.addOrReplaceChild("roof_spine_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -60.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition roof_spine_2_r2 = roof_spine_2.addOrReplaceChild("roof_spine_2_r2", CubeListBuilder.create().texOffs(140, 9).addBox(0.5064F, -51.6981F, -1.0F, 10.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.6F, 60.0F, -0.275F, 0.0F, 0.0F, 0.1484F));

		PartDefinition roof_spine_2_r3 = roof_spine_2.addOrReplaceChild("roof_spine_2_r3", CubeListBuilder.create().texOffs(186, 24).addBox(-5.4936F, -52.8481F, 0.0F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, 60.0F, -0.25F, 0.0F, 0.0F, 0.1484F));

		PartDefinition roof_spine_2_r4 = roof_spine_2.addOrReplaceChild("roof_spine_2_r4", CubeListBuilder.create().texOffs(179, 134).addBox(5.2564F, -50.4481F, -1.0F, 5.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.275F, 60.0F, -0.225F, 0.0F, 0.0F, 0.1484F));

		PartDefinition roof_spine_3 = roof.addOrReplaceChild("roof_spine_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -60.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition roof_spine_3_r3 = roof_spine_3.addOrReplaceChild("roof_spine_3_r3", CubeListBuilder.create().texOffs(67, 139).addBox(0.5064F, -51.6981F, -1.0F, 10.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.6F, 60.0F, -0.775F, 0.0F, 0.0F, 0.1484F));

		PartDefinition roof_spine_3_r4 = roof_spine_3.addOrReplaceChild("roof_spine_3_r4", CubeListBuilder.create().texOffs(184, 6).addBox(-5.4936F, -52.8481F, 0.0F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, 60.0F, -0.75F, 0.0F, 0.0F, 0.1484F));

		PartDefinition roof_spine_3_r5 = roof_spine_3.addOrReplaceChild("roof_spine_3_r5", CubeListBuilder.create().texOffs(73, 179).addBox(5.2564F, -50.4481F, -1.0F, 5.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.275F, 60.025F, -0.775F, 0.0F, 0.0F, 0.1484F));

		PartDefinition gears = box.addOrReplaceChild("gears", CubeListBuilder.create(), PartPose.offset(0.0F, -12.0F, 0.25F));

		PartDefinition gear_1_rotate_x = gears.addOrReplaceChild("gear_1_rotate_x", CubeListBuilder.create().texOffs(43, 190).addBox(-1.0646F, -5.5F, -0.8792F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(147, 150).addBox(-1.3146F, -3.5F, -3.6292F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(131, 150).addBox(0.6854F, -3.5F, -3.6292F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0604F, 0.0F, 11.3792F, 3.1416F, 0.0F, 1.5708F));

		PartDefinition panelbox_r1 = gear_1_rotate_x.addOrReplaceChild("panelbox_r1", CubeListBuilder.create().texOffs(134, 191).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.9354F, 0.0F, 0.0208F, -1.5708F, 0.0F, 0.0F));

		PartDefinition panelbox_r2 = gear_1_rotate_x.addOrReplaceChild("panelbox_r2", CubeListBuilder.create().texOffs(205, 41).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.9396F, 0.0F, 0.0208F, -1.5708F, 0.0F, 0.0F));

		PartDefinition panelbox_r3 = gear_1_rotate_x.addOrReplaceChild("panelbox_r3", CubeListBuilder.create().texOffs(147, 150).addBox(-0.5F, -3.5F, -3.5F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(147, 150).addBox(-2.5F, -3.5F, -3.5F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.9354F, 0.0F, -0.1292F, -0.7854F, 0.0F, 0.0F));

		PartDefinition panelbox_r4 = gear_1_rotate_x.addOrReplaceChild("panelbox_r4", CubeListBuilder.create().texOffs(35, 188).addBox(-1.0F, -5.5F, -1.0F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0646F, 0.0F, 0.1208F, 0.7854F, 0.0F, 0.0F));

		PartDefinition panelbox_r5 = gear_1_rotate_x.addOrReplaceChild("panelbox_r5", CubeListBuilder.create().texOffs(82, 189).addBox(-1.0F, -5.5F, -1.0F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0646F, 0.0F, 0.1208F, -0.7854F, 0.0F, 0.0F));

		PartDefinition panelbox_r6 = gear_1_rotate_x.addOrReplaceChild("panelbox_r6", CubeListBuilder.create().texOffs(90, 189).addBox(-1.0F, -5.5F, -1.0F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0646F, 0.0F, 0.1208F, -1.5708F, 0.0F, 0.0F));

		PartDefinition gear_2_rotate_x = gears.addOrReplaceChild("gear_2_rotate_x", CubeListBuilder.create().texOffs(43, 190).addBox(-1.0646F, -5.5F, -0.8792F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(147, 150).addBox(-1.3146F, -3.5F, -3.6292F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(131, 150).addBox(0.6854F, -3.5F, -3.6292F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.8983F, 0.0F, -5.5873F, -1.0472F, 0.0F, 1.5708F));

		PartDefinition panelbox_r7 = gear_2_rotate_x.addOrReplaceChild("panelbox_r7", CubeListBuilder.create().texOffs(134, 191).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.9354F, 0.0F, 0.0208F, -1.5708F, 0.0F, 0.0F));

		PartDefinition panelbox_r8 = gear_2_rotate_x.addOrReplaceChild("panelbox_r8", CubeListBuilder.create().texOffs(205, 41).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.9396F, 0.0F, 0.0208F, -1.5708F, 0.0F, 0.0F));

		PartDefinition panelbox_r9 = gear_2_rotate_x.addOrReplaceChild("panelbox_r9", CubeListBuilder.create().texOffs(147, 150).addBox(-0.5F, -3.5F, -3.5F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(147, 150).addBox(-2.5F, -3.5F, -3.5F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.9354F, 0.0F, -0.1292F, -0.7854F, 0.0F, 0.0F));

		PartDefinition panelbox_r10 = gear_2_rotate_x.addOrReplaceChild("panelbox_r10", CubeListBuilder.create().texOffs(35, 188).addBox(-1.0F, -5.5F, -1.0F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0646F, 0.0F, 0.1208F, 0.7854F, 0.0F, 0.0F));

		PartDefinition panelbox_r11 = gear_2_rotate_x.addOrReplaceChild("panelbox_r11", CubeListBuilder.create().texOffs(82, 189).addBox(-1.0F, -5.5F, -1.0F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0646F, 0.0F, 0.1208F, -0.7854F, 0.0F, 0.0F));

		PartDefinition panelbox_r12 = gear_2_rotate_x.addOrReplaceChild("panelbox_r12", CubeListBuilder.create().texOffs(90, 189).addBox(-1.0F, -5.5F, -1.0F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0646F, 0.0F, 0.1208F, -1.5708F, 0.0F, 0.0F));

		PartDefinition gear_3_rotate_x = gears.addOrReplaceChild("gear_3_rotate_x", CubeListBuilder.create().texOffs(43, 190).addBox(-1.0646F, -5.5F, -0.8792F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(147, 150).addBox(-1.3146F, -3.5F, -3.6292F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(131, 150).addBox(0.6854F, -3.5F, -3.6292F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.7378F, 0.0F, -5.4169F, 1.0472F, 0.0F, 1.5708F));

		PartDefinition panelbox_r13 = gear_3_rotate_x.addOrReplaceChild("panelbox_r13", CubeListBuilder.create().texOffs(134, 191).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.9354F, 0.0F, 0.0208F, -1.5708F, 0.0F, 0.0F));

		PartDefinition panelbox_r14 = gear_3_rotate_x.addOrReplaceChild("panelbox_r14", CubeListBuilder.create().texOffs(205, 41).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.9396F, 0.0F, 0.0208F, -1.5708F, 0.0F, 0.0F));

		PartDefinition panelbox_r15 = gear_3_rotate_x.addOrReplaceChild("panelbox_r15", CubeListBuilder.create().texOffs(147, 150).addBox(-0.5F, -3.5F, -3.5F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(147, 150).addBox(-2.5F, -3.5F, -3.5F, 1.0F, 7.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.9354F, 0.0F, -0.1292F, -0.7854F, 0.0F, 0.0F));

		PartDefinition panelbox_r16 = gear_3_rotate_x.addOrReplaceChild("panelbox_r16", CubeListBuilder.create().texOffs(35, 188).addBox(-1.0F, -5.5F, -1.0F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0646F, 0.0F, 0.1208F, 0.7854F, 0.0F, 0.0F));

		PartDefinition panelbox_r17 = gear_3_rotate_x.addOrReplaceChild("panelbox_r17", CubeListBuilder.create().texOffs(82, 189).addBox(-1.0F, -5.5F, -1.0F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0646F, 0.0F, 0.1208F, -0.7854F, 0.0F, 0.0F));

		PartDefinition panelbox_r18 = gear_3_rotate_x.addOrReplaceChild("panelbox_r18", CubeListBuilder.create().texOffs(90, 189).addBox(-1.0F, -5.5F, -1.0F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0646F, 0.0F, 0.1208F, -1.5708F, 0.0F, 0.0F));

		PartDefinition gear_mount = box.addOrReplaceChild("gear_mount", CubeListBuilder.create(), PartPose.offsetAndRotation(-29.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition south_mount = gear_mount.addOrReplaceChild("south_mount", CubeListBuilder.create().texOffs(114, 191).addBox(1.65F, -19.0F, 15.0F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(114, 191).addBox(-3.35F, -19.0F, 15.0F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offset(-12.1774F, -12.0F, -5.1461F));

		PartDefinition nw_mount = gear_mount.addOrReplaceChild("nw_mount", CubeListBuilder.create().texOffs(114, 191).addBox(1.5F, -2.0F, 9.5F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(114, 191).addBox(-3.5F, -2.0F, 9.5F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-12.0274F, -29.0F, 0.3539F, 2.0944F, 0.0F, 0.0F));

		PartDefinition ne_mount = gear_mount.addOrReplaceChild("ne_mount", CubeListBuilder.create().texOffs(114, 191).addBox(1.5F, -2.0F, 9.5F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(114, 191).addBox(-3.5F, -2.0F, 9.5F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-12.0274F, -29.0F, 0.3539F, -2.0944F, 0.0F, 0.0F));

		PartDefinition panel_front = box.addOrReplaceChild("panel_front", CubeListBuilder.create().texOffs(134, 0).addBox(-7.0F, -2.0556F, -12.6667F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(22, 48).addBox(-7.0F, -45.0556F, -12.6667F, 1.0F, 43.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(18, 48).addBox(6.0F, -45.0556F, -12.6667F, 1.0F, 43.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(132, 102).addBox(-7.0F, -47.0556F, -12.6667F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(156, 152).addBox(-6.0F, -40.0556F, -12.6667F, 12.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(149, 63).addBox(-7.0F, -47.4306F, -13.1667F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(124, 48).addBox(-7.1F, -39.0556F, -13.0667F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.9444F, 0.4167F));

		PartDefinition floorboards = panel_front.addOrReplaceChild("floorboards", CubeListBuilder.create().texOffs(100, 7).addBox(-8.0F, -0.0556F, -14.6667F, 16.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(156, 150).addBox(-6.5F, -0.3056F, -11.6667F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(163, 125).addBox(-6.0F, -0.3056F, -10.6667F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(168, 61).addBox(-5.5F, -0.3056F, -9.6667F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(179, 140).addBox(-4.5F, -0.3056F, -8.6667F, 9.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(179, 191).addBox(-4.0F, -0.3056F, -7.6667F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(196, 16).addBox(-3.5F, -0.3056F, -6.6667F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(201, 98).addBox(-3.0F, -0.3056F, -5.6667F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(206, 91).addBox(-2.5F, -0.3056F, -4.6667F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(207, 182).addBox(-2.0F, -0.3056F, -3.6667F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(208, 119).addBox(-1.5F, -0.3056F, -2.6667F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(54, 178).addBox(-1.0F, -0.3056F, -1.6667F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(124, 86).addBox(-7.5F, -0.3056F, -13.6667F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(55, 149).addBox(-7.0F, -0.3056F, -12.6667F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition roof_front = panel_front.addOrReplaceChild("roof_front", CubeListBuilder.create(), PartPose.offset(0.0F, -47.0F, -12.0F));

		PartDefinition center_peak = roof_front.addOrReplaceChild("center_peak", CubeListBuilder.create().texOffs(171, 112).addBox(-4.925F, -3.9556F, -0.3667F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(56, 209).addBox(-3.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 209).addBox(-0.5F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(44, 209).addBox(2.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(191, 168).addBox(-4.0F, -3.9556F, 0.6333F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(195, 138).addBox(-3.5F, -3.9556F, 1.6333F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(201, 56).addBox(-3.0F, -3.9556F, 2.6333F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(206, 26).addBox(-2.5F, -3.9556F, 3.6333F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(207, 174).addBox(-2.0F, -3.9556F, 4.6333F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 4.0F, 0.1047F, 0.0F, 0.0F));

		PartDefinition wroughtiron_front = roof_front.addOrReplaceChild("wroughtiron_front", CubeListBuilder.create().texOffs(98, 64).addBox(-8.0F, -55.0F, -13.5F, 16.0F, 6.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 47.9444F, 11.5833F));

		PartDefinition outer_peak = roof_front.addOrReplaceChild("outer_peak", CubeListBuilder.create().texOffs(46, 90).addBox(-7.9F, -1.2556F, -2.6667F, 16.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(156, 57).addBox(-6.4F, -1.0056F, 0.0833F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(163, 9).addBox(-5.9F, -1.0056F, 1.0833F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(154, 167).addBox(-5.4F, -1.0056F, 2.0833F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(98, 171).addBox(-4.9F, -1.0056F, 3.0833F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(148, 143).addBox(-6.9F, -1.0056F, -0.9167F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0873F, 0.0F, 0.0F));

		PartDefinition panel_front_top = panel_front.addOrReplaceChild("panel_front_top", CubeListBuilder.create().texOffs(44, 133).addBox(-6.0F, -45.0556F, -12.4167F, 12.0F, 6.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(112, 199).addBox(1.0F, -44.5556F, -12.7667F, 4.0F, 4.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(199, 63).addBox(-5.0F, -44.5556F, -12.7667F, 4.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panel_side_1 = box.addOrReplaceChild("panel_side_1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -0.9444F, 0.4167F, 0.0F, -1.0472F, 0.0F));

		PartDefinition side_1 = panel_side_1.addOrReplaceChild("side_1", CubeListBuilder.create().texOffs(138, 148).addBox(-7.0F, -9.0F, -12.25F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(68, 47).addBox(-7.0F, -46.0F, -12.25F, 1.0F, 37.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(68, 9).addBox(6.0F, -46.0F, -12.25F, 1.0F, 37.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(132, 99).addBox(-7.0F, -48.0F, -12.25F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(103, 156).addBox(-6.0F, -34.0F, -12.25F, 12.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 148).addBox(-6.0F, -20.0F, -12.25F, 12.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(124, 84).addBox(-7.425F, -48.3F, -12.975F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(124, 39).addBox(-7.0F, -33.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(124, 36).addBox(-7.0F, -19.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(124, 33).addBox(-7.0F, -16.5F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(124, 30).addBox(-7.0F, -8.25F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(108, 123).addBox(-7.0F, -4.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(78, 123).addBox(-7.0F, -8.0F, -12.25F, 14.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(132, 96).addBox(-7.0F, -3.0F, -12.25F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.9444F, -0.4167F));

		PartDefinition top_panel_1 = panel_side_1.addOrReplaceChild("top_panel_1", CubeListBuilder.create().texOffs(72, 77).addBox(-6.0F, -45.0556F, -12.4167F, 12.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cage = top_panel_1.addOrReplaceChild("cage", CubeListBuilder.create().texOffs(202, 80).addBox(-2.25F, -44.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(185, 203).addBox(1.25F, -44.25F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(187, 39).addBox(-4.5F, -42.25F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(11, 187).addBox(-4.5F, -38.75F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(140, 203).addBox(1.25F, -37.75F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(81, 203).addBox(-2.25F, -37.75F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(186, 117).addBox(1.25F, -38.75F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(186, 105).addBox(1.25F, -42.25F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(186, 89).addBox(-1.5F, -42.25F, -14.9F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(185, 185).addBox(-1.5F, -38.75F, -14.9F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(10, 202).addBox(-2.25F, -41.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(201, 196).addBox(1.25F, -41.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(22, 133).addBox(-4.0F, -44.0F, -13.0F, 8.0F, 8.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.9444F, -0.4167F));

		PartDefinition middle_panel_1 = panel_side_1.addOrReplaceChild("middle_panel_1", CubeListBuilder.create().texOffs(98, 76).addBox(-6.0F, -30.0556F, -12.4167F, 12.0F, 11.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 164).addBox(-4.5F, -28.5556F, -12.9167F, 9.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 157).addBox(3.25F, -28.3056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(99, 156).addBox(-4.25F, -28.3056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(156, 49).addBox(-4.25F, -21.8056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(156, 31).addBox(3.25F, -21.8056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lower_panel_1 = panel_side_1.addOrReplaceChild("lower_panel_1", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panelbox = lower_panel_1.addOrReplaceChild("panelbox", CubeListBuilder.create().texOffs(108, 126).addBox(-6.0F, -14.5556F, -11.3167F, 12.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(167, 147).addBox(-5.55F, -14.5056F, -12.3167F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(167, 120).addBox(-5.55F, -8.7056F, -12.3167F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panelbox2 = lower_panel_1.addOrReplaceChild("panelbox2", CubeListBuilder.create().texOffs(28, 208).addBox(-6.55F, -14.8056F, -12.3167F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(4, 208).addBox(5.45F, -14.8056F, -12.3167F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards_1 = panel_side_1.addOrReplaceChild("floorboards_1", CubeListBuilder.create().texOffs(100, 0).addBox(-8.1F, -0.0556F, -14.6667F, 16.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(155, 85).addBox(-6.5F, -0.3056F, -11.6667F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(161, 110).addBox(-6.0F, -0.3056F, -10.6667F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(167, 118).addBox(-5.5F, -0.3056F, -9.6667F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(179, 63).addBox(-4.5F, -0.3056F, -8.6667F, 9.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(191, 166).addBox(-4.0F, -0.3056F, -7.6667F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(195, 75).addBox(-3.5F, -0.3056F, -6.6667F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(200, 178).addBox(-3.0F, -0.3056F, -5.6667F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(206, 24).addBox(-2.5F, -0.3056F, -4.6667F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(207, 172).addBox(-2.0F, -0.3056F, -3.6667F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(208, 107).addBox(-1.5F, -0.3056F, -2.6667F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(51, 156).addBox(-1.0F, -0.3056F, -1.6667F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(124, 57).addBox(-7.7F, -0.3056F, -13.6667F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(56, 147).addBox(-7.0F, -0.3056F, -12.6667F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition roof_side_1 = panel_side_1.addOrReplaceChild("roof_side_1", CubeListBuilder.create(), PartPose.offset(0.0F, -47.0F, -12.0F));

		PartDefinition center_peak_1 = roof_side_1.addOrReplaceChild("center_peak_1", CubeListBuilder.create().texOffs(62, 171).addBox(-4.9F, -3.9556F, -0.3667F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(208, 50).addBox(-1.5F, -4.5556F, 7.6333F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(38, 209).addBox(-3.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(209, 18).addBox(-0.5F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(14, 209).addBox(2.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(161, 191).addBox(-4.0F, -3.9556F, 0.6333F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(195, 54).addBox(-3.5F, -3.9556F, 1.6333F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(200, 176).addBox(-3.0F, -3.9556F, 2.6333F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(205, 144).addBox(-2.5F, -3.9556F, 3.6333F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(207, 60).addBox(-2.0F, -3.9556F, 4.6333F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 4.0F, 0.1047F, 0.0F, 0.0F));

		PartDefinition wroughtiron_front_1 = roof_side_1.addOrReplaceChild("wroughtiron_front_1", CubeListBuilder.create().texOffs(98, 70).addBox(-8.0F, -55.0F, -13.5F, 16.0F, 6.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 47.9444F, 11.5833F));

		PartDefinition outer_peak_1 = roof_side_1.addOrReplaceChild("outer_peak_1", CubeListBuilder.create().texOffs(71, 45).addBox(-8.0F, -1.2556F, -2.6667F, 16.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(154, 39).addBox(-6.4F, -1.0056F, 0.1833F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(161, 87).addBox(-5.9F, -1.0056F, 1.1583F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(64, 167).addBox(-5.4F, -1.0056F, 2.1583F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(40, 171).addBox(-4.9F, -1.0056F, 3.1583F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(135, 146).addBox(-6.9F, -1.0056F, -0.8167F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0873F, 0.0F, 0.0F));

		PartDefinition panel_side_2 = box.addOrReplaceChild("panel_side_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -0.9444F, 0.4167F, 0.0F, -2.0944F, 0.0F));

		PartDefinition side_2 = panel_side_2.addOrReplaceChild("side_2", CubeListBuilder.create().texOffs(85, 146).addBox(-7.0F, -9.0F, -12.25F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(64, 47).addBox(-7.0F, -46.0F, -12.25F, 1.0F, 37.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(64, 9).addBox(6.0F, -46.0F, -12.25F, 1.0F, 37.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(132, 3).addBox(-7.0F, -48.0F, -12.25F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(153, 137).addBox(-6.0F, -34.0F, -12.25F, 12.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 146).addBox(-6.0F, -20.0F, -12.25F, 12.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(56, 145).addBox(-7.0F, -48.325F, -12.75F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 122).addBox(-7.0F, -33.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(14, 122).addBox(-7.0F, -19.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(78, 120).addBox(-7.0F, -16.5F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(48, 119).addBox(-7.0F, -8.25F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(16, 119).addBox(-7.0F, -4.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(111, 118).addBox(-7.0F, -8.0F, -12.25F, 14.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(131, 109).addBox(-7.0F, -3.0F, -12.25F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.9444F, -0.4167F));

		PartDefinition top_panel_2 = panel_side_2.addOrReplaceChild("top_panel_2", CubeListBuilder.create().texOffs(72, 64).addBox(-6.0F, -45.0556F, -12.4167F, 12.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cage2 = top_panel_2.addOrReplaceChild("cage2", CubeListBuilder.create().texOffs(104, 201).addBox(-2.25F, -44.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(203, 69).addBox(1.25F, -44.25F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(169, 185).addBox(-4.5F, -42.25F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(153, 185).addBox(-4.5F, -38.75F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(58, 203).addBox(1.25F, -37.75F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(48, 203).addBox(-2.25F, -37.75F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(131, 185).addBox(1.25F, -38.75F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(115, 185).addBox(1.25F, -42.25F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(68, 185).addBox(-1.5F, -42.25F, -14.9F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(184, 172).addBox(-1.5F, -38.75F, -14.9F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(28, 201).addBox(-2.25F, -41.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(0, 201).addBox(1.25F, -41.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(0, 133).addBox(-4.0F, -44.0F, -13.0F, 8.0F, 8.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.9444F, -0.4167F));

		PartDefinition middle_panel_2 = panel_side_2.addOrReplaceChild("middle_panel_2", CubeListBuilder.create().texOffs(98, 48).addBox(-6.0F, -30.0556F, -12.4167F, 12.0F, 11.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(164, 0).addBox(-4.5F, -28.5556F, -12.9167F, 9.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(196, 183).addBox(-2.75F, -27.0556F, -13.6667F, 5.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(122, 191).addBox(-1.75F, -26.0556F, -14.6667F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(8, 188).addBox(-1.75F, -12.6556F, -13.1667F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(195, 170).addBox(-1.75F, -12.6556F, -18.1667F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(60, 151).addBox(-1.75F, -26.0556F, -18.0667F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 166).addBox(-1.25F, -24.5556F, -17.6667F, 2.0F, 14.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(76, 93).addBox(-1.25F, -25.5056F, -15.6667F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 184).addBox(-1.25F, -12.0056F, -15.6667F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(126, 185).addBox(-2.25F, -13.1056F, -12.1667F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 155).addBox(3.25F, -28.3056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(82, 153).addBox(-4.25F, -28.3056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(76, 153).addBox(-4.25F, -21.8056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(76, 151).addBox(3.25F, -21.8056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lower_panel_2 = panel_side_2.addOrReplaceChild("lower_panel_2", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panelbox3 = lower_panel_2.addOrReplaceChild("panelbox3", CubeListBuilder.create().texOffs(52, 125).addBox(-6.0F, -14.5556F, -11.3167F, 12.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(167, 45).addBox(-5.55F, -14.5056F, -12.3167F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(40, 167).addBox(-5.55F, -8.7056F, -12.3167F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panelbox4 = lower_panel_2.addOrReplaceChild("panelbox4", CubeListBuilder.create().texOffs(0, 208).addBox(-6.55F, -14.8056F, -12.3167F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(195, 207).addBox(5.5F, -14.8056F, -12.3167F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards_2 = panel_side_2.addOrReplaceChild("floorboards_2", CubeListBuilder.create().texOffs(99, 5).addBox(-8.1F, -0.0556F, -14.6667F, 16.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(153, 135).addBox(-6.5F, -0.3056F, -11.6667F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(161, 43).addBox(-6.0F, -0.3056F, -10.6667F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(154, 165).addBox(-5.5F, -0.3056F, -9.6667F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(178, 143).addBox(-4.5F, -0.3056F, -8.6667F, 9.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(191, 147).addBox(-4.0F, -0.3056F, -7.6667F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(195, 14).addBox(-3.5F, -0.3056F, -6.6667F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(199, 140).addBox(-3.0F, -0.3056F, -5.6667F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(205, 87).addBox(-2.5F, -0.3056F, -4.6667F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(207, 58).addBox(-2.0F, -0.3056F, -3.6667F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(112, 187).addBox(-1.5F, -0.3056F, -2.6667F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(154, 47).addBox(-1.0F, -0.3056F, -1.6667F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(124, 55).addBox(-7.7F, -0.3056F, -13.6667F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(119, 144).addBox(-7.0F, -0.3056F, -12.6667F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition roof_side_2 = panel_side_2.addOrReplaceChild("roof_side_2", CubeListBuilder.create(), PartPose.offset(0.0F, -47.0F, -12.0F));

		PartDefinition center_peak_2 = roof_side_2.addOrReplaceChild("center_peak_2", CubeListBuilder.create().texOffs(177, 77).addBox(-4.25F, -3.9556F, -0.3667F, 9.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(95, 185).addBox(-1.5F, -4.5556F, 7.6333F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(208, 79).addBox(-3.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(208, 46).addBox(-0.5F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(208, 36).addBox(2.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(191, 115).addBox(-4.0F, -3.9556F, 0.6333F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(195, 2).addBox(-3.5F, -3.9556F, 1.6333F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(197, 142).addBox(-3.0F, -3.9556F, 2.6333F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(204, 170).addBox(-2.5F, -3.9556F, 3.6333F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(206, 164).addBox(-2.0F, -3.9556F, 4.6333F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 4.0F, 0.1047F, 0.0F, 0.0F));

		PartDefinition wroughtiron_front_2 = roof_side_2.addOrReplaceChild("wroughtiron_front_2", CubeListBuilder.create().texOffs(98, 21).addBox(-8.0F, -55.0F, -13.5F, 16.0F, 6.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 47.9444F, 11.5833F));

		PartDefinition outer_peak_2 = roof_side_2.addOrReplaceChild("outer_peak_2", CubeListBuilder.create().texOffs(72, 61).addBox(-8.0F, -1.2556F, -2.6667F, 16.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(153, 133).addBox(-6.4F, -1.0056F, 0.0833F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(160, 123).addBox(-5.9F, -1.0056F, 1.0833F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(64, 165).addBox(-5.4F, -1.0056F, 2.0833F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(131, 170).addBox(-4.9F, -1.0056F, 3.0833F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(144, 93).addBox(-6.9F, -1.0056F, -0.9167F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0873F, 0.0F, 0.0F));

		PartDefinition panel_side_3 = box.addOrReplaceChild("panel_side_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -0.9444F, 0.4167F, 0.0F, 3.1416F, 0.0F));

		PartDefinition roof_side_3 = panel_side_3.addOrReplaceChild("roof_side_3", CubeListBuilder.create(), PartPose.offset(0.0F, -47.0F, -12.0F));

		PartDefinition center_peak_3 = roof_side_3.addOrReplaceChild("center_peak_3", CubeListBuilder.create().texOffs(152, 169).addBox(-5.0F, -3.9556F, -0.3667F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(183, 85).addBox(-1.5F, -4.5556F, 7.6333F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(207, 195).addBox(-3.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(207, 187).addBox(-0.5F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(54, 203).addBox(2.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(191, 18).addBox(-4.0F, -3.9556F, 0.6333F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(194, 126).addBox(-3.5F, -3.9556F, 1.6333F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(197, 77).addBox(-3.0F, -3.9556F, 2.6333F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(203, 180).addBox(-2.5F, -3.9556F, 3.6333F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(206, 93).addBox(-2.0F, -3.9556F, 4.6333F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 4.0F, 0.1047F, 0.0F, 0.0F));

		PartDefinition wroughtiron_front_3 = roof_side_3.addOrReplaceChild("wroughtiron_front_3", CubeListBuilder.create().texOffs(98, 15).addBox(-8.0F, -55.0F, -13.5F, 16.0F, 6.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 47.9444F, 11.5833F));

		PartDefinition outer_peak_3 = roof_side_3.addOrReplaceChild("outer_peak_3", CubeListBuilder.create().texOffs(72, 9).addBox(-8.0F, -1.2556F, -2.6667F, 16.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(150, 76).addBox(-6.4F, -1.0056F, 0.0833F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(44, 140).addBox(-5.9F, -1.0056F, 1.0833F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 110).addBox(-5.4F, -1.0056F, 2.0833F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(44, 142).addBox(-4.9F, -1.0056F, 3.0833F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(130, 74).addBox(-6.9F, -1.0056F, -0.9167F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0873F, 0.0F, 0.0F));

		PartDefinition side_3 = panel_side_3.addOrReplaceChild("side_3", CubeListBuilder.create().texOffs(144, 90).addBox(-7.0F, -9.0F, -12.25F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(60, 47).addBox(-7.0F, -46.0F, -12.25F, 1.0F, 37.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(60, 9).addBox(6.0F, -46.0F, -12.25F, 1.0F, 37.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(131, 106).addBox(-7.0F, -48.0F, -12.25F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(111, 152).addBox(-6.0F, -34.0F, -12.25F, 12.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 144).addBox(-6.0F, -20.0F, -12.25F, 12.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 144).addBox(-7.0F, -48.375F, -12.75F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(80, 116).addBox(-7.0F, -33.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(48, 116).addBox(-7.0F, -19.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(16, 116).addBox(-7.0F, -16.5F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(114, 93).addBox(-7.0F, -8.25F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(114, 90).addBox(-7.0F, -4.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(112, 113).addBox(-7.0F, -8.0F, -12.25F, 14.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(131, 42).addBox(-7.0F, -3.0F, -12.25F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.9444F, -0.4167F));

		PartDefinition top_panel_3 = panel_side_3.addOrReplaceChild("top_panel_3", CubeListBuilder.create().texOffs(72, 48).addBox(-6.0F, -45.0556F, -12.4167F, 12.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cage3 = top_panel_3.addOrReplaceChild("cage3", CubeListBuilder.create().texOffs(191, 200).addBox(-2.25F, -44.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(38, 203).addBox(1.25F, -44.25F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(142, 184).addBox(-4.5F, -42.25F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(57, 184).addBox(-4.5F, -38.75F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(203, 4).addBox(1.25F, -37.75F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(175, 202).addBox(-2.25F, -37.75F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(41, 184).addBox(1.25F, -38.75F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(184, 0).addBox(1.25F, -42.25F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(183, 127).addBox(-1.5F, -42.25F, -14.9F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(84, 183).addBox(-1.5F, -38.75F, -14.9F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(94, 200).addBox(-2.25F, -41.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(200, 91).addBox(1.25F, -41.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(131, 131).addBox(-4.0F, -44.0F, -13.0F, 8.0F, 8.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.9444F, -0.4167F));

		PartDefinition middle_panel_3 = panel_side_3.addOrReplaceChild("middle_panel_3", CubeListBuilder.create().texOffs(98, 31).addBox(-6.0F, -30.0556F, -12.4167F, 12.0F, 11.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(163, 156).addBox(-4.5F, -28.5556F, -12.9167F, 9.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(69, 151).addBox(3.25F, -28.3056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(53, 151).addBox(-4.25F, -28.3056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(90, 139).addBox(-4.25F, -21.8056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(97, 128).addBox(3.25F, -21.8056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lower_panel_3 = panel_side_3.addOrReplaceChild("lower_panel_3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panelbox5 = lower_panel_3.addOrReplaceChild("panelbox5", CubeListBuilder.create().texOffs(26, 125).addBox(-6.0F, -14.5556F, -11.3167F, 12.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(131, 166).addBox(-5.55F, -14.6056F, -12.3167F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(166, 116).addBox(-5.55F, -8.6056F, -12.3167F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panelbox6 = lower_panel_3.addOrReplaceChild("panelbox6", CubeListBuilder.create().texOffs(99, 207).addBox(-6.55F, -14.8056F, -12.3167F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(95, 207).addBox(5.45F, -14.8056F, -12.3167F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards_3 = panel_side_3.addOrReplaceChild("floorboards_3", CubeListBuilder.create().texOffs(99, 2).addBox(-8.1F, -0.0556F, -14.6667F, 16.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(152, 141).addBox(-6.5F, -0.3056F, -11.6667F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(160, 108).addBox(-6.0F, -0.3056F, -10.6667F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(40, 165).addBox(-5.5F, -0.3056F, -9.6667F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(176, 55).addBox(-4.5F, -0.3056F, -8.6667F, 9.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(191, 20).addBox(-4.0F, -0.3056F, -7.6667F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(195, 0).addBox(-3.5F, -0.3056F, -6.6667F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(197, 89).addBox(-3.0F, -0.3056F, -5.6667F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(204, 100).addBox(-2.5F, -0.3056F, -4.6667F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(206, 123).addBox(-2.0F, -0.3056F, -3.6667F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(183, 158).addBox(-1.5F, -0.3056F, -2.6667F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(154, 29).addBox(-1.0F, -0.3056F, -1.6667F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(124, 53).addBox(-7.7F, -0.3056F, -13.6667F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(119, 142).addBox(-7.0F, -0.3056F, -12.6667F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panel_side_4 = box.addOrReplaceChild("panel_side_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -0.9444F, 0.4167F, 0.0F, 2.0944F, 0.0F));

		PartDefinition roof_side_4 = panel_side_4.addOrReplaceChild("roof_side_4", CubeListBuilder.create(), PartPose.offset(0.0F, -47.0F, -12.0F));

		PartDefinition center_peak_4 = roof_side_4.addOrReplaceChild("center_peak_4", CubeListBuilder.create().texOffs(98, 169).addBox(-5.0F, -3.9556F, -0.3667F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(173, 179).addBox(-1.5F, -4.5556F, 7.6333F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(44, 203).addBox(-3.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(181, 202).addBox(-0.5F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(171, 202).addBox(2.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(189, 163).addBox(-4.0F, -3.9556F, 0.6333F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(193, 149).addBox(-3.5F, -3.9556F, 1.6333F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(159, 171).addBox(-3.0F, -3.9556F, 2.6333F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(99, 154).addBox(-2.5F, -3.9556F, 3.6333F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(64, 204).addBox(-2.0F, -3.9556F, 4.6333F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 4.0F, 0.1047F, 0.0F, 0.0F));

		PartDefinition wroughtiron_front_4 = roof_side_4.addOrReplaceChild("wroughtiron_front_4", CubeListBuilder.create().texOffs(44, 93).addBox(-8.0F, -55.0F, -13.5F, 16.0F, 6.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 47.9444F, 11.5833F));

		PartDefinition outer_peak_4 = roof_side_4.addOrReplaceChild("outer_peak_4", CubeListBuilder.create().texOffs(72, 28).addBox(-8.0F, -1.2556F, -2.6667F, 16.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(150, 82).addBox(-6.4F, -1.0556F, 0.0833F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(160, 41).addBox(-5.9F, -1.0556F, 1.0833F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(131, 164).addBox(-5.4F, -1.0556F, 2.0833F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(62, 169).addBox(-4.9F, -1.0556F, 3.0833F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(137, 45).addBox(-6.9F, -1.0556F, -0.9167F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0873F, 0.0F, 0.0F));

		PartDefinition side_4 = panel_side_4.addOrReplaceChild("side_4", CubeListBuilder.create().texOffs(142, 114).addBox(-7.0F, -9.0F, -12.25F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(56, 45).addBox(-7.0F, -46.0F, -12.25F, 1.0F, 37.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 45).addBox(6.0F, -46.0F, -12.25F, 1.0F, 37.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(130, 24).addBox(-7.0F, -48.0F, -12.25F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(152, 69).addBox(-6.0F, -34.0F, -12.25F, 12.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(141, 117).addBox(-6.0F, -20.0F, -12.25F, 12.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(141, 112).addBox(-7.0F, -48.45F, -12.75F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(80, 113).addBox(-7.0F, -33.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(48, 113).addBox(-7.0F, -19.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(16, 113).addBox(-7.0F, -16.5F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(100, 110).addBox(-7.0F, -8.25F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(16, 110).addBox(-7.0F, -4.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(70, 108).addBox(-7.0F, -8.0F, -12.25F, 14.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(130, 21).addBox(-7.0F, -3.0F, -12.25F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.9444F, -0.4167F));

		PartDefinition top_panel_4 = panel_side_4.addOrReplaceChild("top_panel_4", CubeListBuilder.create().texOffs(72, 31).addBox(-6.0F, -45.0556F, -12.4167F, 12.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cage4 = top_panel_4.addOrReplaceChild("cage4", CubeListBuilder.create().texOffs(200, 24).addBox(-2.25F, -44.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(165, 202).addBox(1.25F, -44.25F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(182, 149).addBox(-4.5F, -42.25F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(30, 182).addBox(-4.5F, -38.75F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(202, 153).addBox(1.25F, -37.75F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(202, 117).addBox(-2.25F, -37.75F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(0, 182).addBox(1.25F, -38.75F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(104, 181).addBox(1.25F, -42.25F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(19, 181).addBox(-1.5F, -42.25F, -14.9F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(180, 166).addBox(-1.5F, -38.75F, -14.9F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(159, 199).addBox(-2.25F, -41.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(149, 199).addBox(1.25F, -41.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(130, 63).addBox(-4.0F, -44.0F, -13.0F, 8.0F, 8.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.9444F, -0.4167F));

		PartDefinition middle_panel_4 = panel_side_4.addOrReplaceChild("middle_panel_4", CubeListBuilder.create().texOffs(76, 96).addBox(-6.0F, -30.0556F, -12.4167F, 12.0F, 11.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(162, 95).addBox(-4.5F, -28.5556F, -12.9167F, 9.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(44, 122).addBox(3.25F, -28.3056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 119).addBox(-4.25F, -28.3056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(78, 116).addBox(-4.25F, -21.8056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 116).addBox(3.25F, -21.8056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lower_panel_4 = panel_side_4.addOrReplaceChild("lower_panel_4", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panelbox7 = lower_panel_4.addOrReplaceChild("panelbox7", CubeListBuilder.create().texOffs(0, 125).addBox(-6.0F, -14.5556F, -11.3167F, 12.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(44, 99).addBox(-5.0F, -6.5556F, -16.3167F, 9.0F, 7.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(20, 160).addBox(-3.0F, -5.5556F, -16.9167F, 5.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(149, 150).addBox(-3.5F, -5.0556F, -16.7167F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(39, 104).addBox(1.4F, -5.0556F, -16.7167F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(70, 133).addBox(1.0F, -10.5556F, -15.3167F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(115, 181).addBox(1.0F, -12.5556F, -12.8167F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(115, 181).addBox(-4.0F, -12.5556F, -12.8167F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(14, 193).addBox(0.5F, -7.0556F, -15.8167F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(140, 150).addBox(0.5F, -13.0556F, -15.8167F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(52, 173).addBox(0.5F, -13.0556F, -11.8167F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(34, 173).addBox(-4.5F, -13.0556F, -11.8167F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(44, 150).addBox(-4.5F, -13.0556F, -15.8167F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(100, 128).addBox(-4.0F, -10.5556F, -15.3167F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(11, 182).addBox(-4.5F, -7.0556F, -15.8167F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(166, 59).addBox(-5.55F, -14.6056F, -12.3167F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(166, 27).addBox(-5.55F, -8.6056F, -12.3167F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panelbox8 = lower_panel_4.addOrReplaceChild("panelbox8", CubeListBuilder.create().texOffs(76, 207).addBox(-6.55F, -14.8056F, -12.3167F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(91, 206).addBox(5.45F, -14.8056F, -12.3167F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards_4 = panel_side_4.addOrReplaceChild("floorboards_4", CubeListBuilder.create().texOffs(98, 88).addBox(-8.1F, -0.0556F, -14.6667F, 16.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(150, 131).addBox(-6.5F, -0.3056F, -11.6667F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(160, 105).addBox(-6.0F, -0.3056F, -10.6667F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(164, 145).addBox(-5.5F, -0.3056F, -9.6667F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(176, 37).addBox(-4.5F, -0.3056F, -8.6667F, 9.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(190, 59).addBox(-4.0F, -0.3056F, -7.6667F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(193, 151).addBox(-3.5F, -0.3056F, -6.6667F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(95, 183).addBox(-3.0F, -0.3056F, -5.6667F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(86, 179).addBox(-2.5F, -0.3056F, -4.6667F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(134, 204).addBox(-2.0F, -0.3056F, -3.6667F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(180, 172).addBox(-1.5F, -0.3056F, -2.6667F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(136, 9).addBox(-1.0F, -0.3056F, -1.6667F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(124, 51).addBox(-7.7F, -0.3056F, -13.6667F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(138, 61).addBox(-7.0F, -0.3056F, -12.6667F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panel_side_5 = box.addOrReplaceChild("panel_side_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -0.9444F, 0.4167F, 0.0F, 1.0472F, 0.0F));

		PartDefinition roof_side_5 = panel_side_5.addOrReplaceChild("roof_side_5", CubeListBuilder.create(), PartPose.offset(0.0F, -47.0F, -12.0F));

		PartDefinition center_peak_5 = roof_side_5.addOrReplaceChild("center_peak_5", CubeListBuilder.create().texOffs(40, 169).addBox(-5.25F, -3.9556F, -0.3667F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(123, 134).addBox(-1.5F, -4.5556F, 7.6333F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(34, 201).addBox(-3.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(185, 105).addBox(-0.5F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(35, 152).addBox(2.0F, -3.0556F, -0.2667F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(184, 57).addBox(-4.0F, -3.9556F, 0.6333F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(192, 61).addBox(-3.5F, -3.9556F, 1.6333F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(28, 150).addBox(-3.0F, -3.9556F, 2.6333F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(48, 83).addBox(-2.5F, -3.9556F, 3.6333F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(182, 39).addBox(-2.0F, -3.9556F, 4.6333F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 4.0F, 0.1047F, 0.0F, 0.0F));

		PartDefinition wroughtiron_front_5 = roof_side_5.addOrReplaceChild("wroughtiron_front_5", CubeListBuilder.create().texOffs(82, 90).addBox(-8.0F, -55.0F, -13.5F, 16.0F, 6.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 47.9444F, 11.5833F));

		PartDefinition outer_peak_5 = roof_side_5.addOrReplaceChild("outer_peak_5", CubeListBuilder.create().texOffs(72, 12).addBox(-8.0F, -1.2556F, -2.6667F, 16.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(150, 78).addBox(-6.4F, -1.0056F, 0.0833F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(159, 73).addBox(-5.9F, -1.0056F, 1.0833F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(163, 127).addBox(-5.4F, -1.0056F, 2.0833F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(131, 168).addBox(-4.9F, -1.0056F, 3.0833F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(132, 88).addBox(-6.9F, -1.0056F, -0.9167F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0873F, 0.0F, 0.0F));

		PartDefinition side_5 = panel_side_5.addOrReplaceChild("side_5", CubeListBuilder.create().texOffs(136, 59).addBox(-7.0F, -9.0F, -12.25F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(48, 45).addBox(-7.0F, -46.0F, -12.25F, 1.0F, 37.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(26, 48).addBox(6.0F, -46.0F, -12.25F, 1.0F, 37.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(130, 18).addBox(-7.0F, -48.0F, -12.25F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(152, 65).addBox(-6.0F, -34.0F, -12.25F, 12.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(93, 140).addBox(-6.0F, -20.0F, -12.25F, 12.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(136, 27).addBox(-7.0F, -48.475F, -12.75F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(100, 107).addBox(-7.0F, -33.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(107, 45).addBox(-7.0F, -19.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(106, 60).addBox(-7.0F, -16.5F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(106, 27).addBox(-7.0F, -8.25F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(106, 10).addBox(-7.0F, -4.0F, -12.65F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(102, 102).addBox(-7.0F, -8.0F, -12.25F, 14.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(130, 15).addBox(-7.0F, -3.0F, -12.25F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.9444F, -0.4167F));

		PartDefinition top_panel_5 = panel_side_5.addOrReplaceChild("top_panel_5", CubeListBuilder.create().texOffs(72, 15).addBox(-6.0F, -45.0556F, -12.4167F, 12.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cage5 = top_panel_5.addOrReplaceChild("cage5", CubeListBuilder.create().texOffs(124, 199).addBox(-2.25F, -44.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(202, 105).addBox(1.25F, -44.25F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(180, 70).addBox(-4.5F, -42.25F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(180, 18).addBox(-4.5F, -38.75F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(202, 48).addBox(1.25F, -37.75F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(201, 189).addBox(-2.25F, -37.75F, -14.5F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(176, 179).addBox(1.25F, -38.75F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(131, 179).addBox(1.25F, -42.25F, -14.65F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(178, 160).addBox(-1.5F, -42.25F, -14.9F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(177, 99).addBox(-1.5F, -38.75F, -14.9F, 3.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(199, 41).addBox(-2.25F, -41.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(76, 151).addBox(1.25F, -41.25F, -14.5F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(78, 128).addBox(-4.0F, -44.0F, -13.0F, 8.0F, 8.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.9444F, -0.4167F));

		PartDefinition middle_panel_5 = panel_side_5.addOrReplaceChild("middle_panel_5", CubeListBuilder.create().texOffs(18, 92).addBox(-6.0F, -30.0556F, -12.4167F, 12.0F, 11.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(103, 160).addBox(-4.5F, -28.5556F, -12.9167F, 9.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(78, 113).addBox(3.25F, -28.3056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 113).addBox(-4.25F, -28.3056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(105, 45).addBox(-4.25F, -21.8056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(26, 90).addBox(3.25F, -21.8056F, -13.1667F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lower_panel_5 = panel_side_5.addOrReplaceChild("lower_panel_5", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panelbox9 = lower_panel_5.addOrReplaceChild("panelbox9", CubeListBuilder.create().texOffs(124, 76).addBox(-6.0F, -14.5556F, -11.3167F, 12.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(166, 13).addBox(-5.55F, -14.5056F, -12.3167F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(166, 11).addBox(-5.55F, -8.7056F, -12.3167F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition panelbox10 = lower_panel_5.addOrReplaceChild("panelbox10", CubeListBuilder.create().texOffs(119, 172).addBox(-6.55F, -14.8056F, -12.3167F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(84, 169).addBox(5.45F, -14.8056F, -12.3167F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards_5 = panel_side_5.addOrReplaceChild("floorboards_5", CubeListBuilder.create().texOffs(97, 43).addBox(-8.1F, -0.0556F, -14.6667F, 16.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(150, 80).addBox(-6.5F, -0.3056F, -11.6667F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(160, 25).addBox(-6.0F, -0.3056F, -10.6667F, 12.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(164, 129).addBox(-5.5F, -0.3056F, -9.6667F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(172, 114).addBox(-4.5F, -0.3056F, -8.6667F, 9.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(188, 145).addBox(-4.0F, -0.3056F, -7.6667F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(193, 103).addBox(-3.5F, -0.3056F, -6.6667F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(6, 166).addBox(-3.0F, -0.3056F, -5.6667F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(119, 140).addBox(-2.5F, -0.3056F, -4.6667F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(87, 202).addBox(-2.0F, -0.3056F, -3.6667F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(177, 166).addBox(-1.5F, -0.3056F, -2.6667F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(19, 133).addBox(-1.0F, -0.3056F, -1.6667F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(108, 13).addBox(-7.7F, -0.3056F, -13.6667F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(134, 129).addBox(-7.0F, -0.3056F, -12.6667F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition hexspine = box.addOrReplaceChild("hexspine", CubeListBuilder.create(), PartPose.offset(0.0F, -0.5F, 0.5F));

		PartDefinition crossspine = hexspine.addOrReplaceChild("crossspine", CubeListBuilder.create().texOffs(30, 6).addBox(-17.0F, -1.25F, -0.5F, 34.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(12, 48).addBox(-15.5F, -47.2F, -0.5F, 2.0F, 46.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(6, 48).addBox(13.5F, -47.2F, -0.5F, 2.0F, 46.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(179, 208).addBox(-16.25F, -54.5F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(175, 208).addBox(15.25F, -54.5F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition leftspine = hexspine.addOrReplaceChild("leftspine", CubeListBuilder.create().texOffs(30, 3).addBox(-17.0625F, 36.7031F, -0.5F, 34.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 48).addBox(13.4375F, -9.2469F, -0.5F, 2.0F, 46.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(42, 45).addBox(-15.5625F, -9.2469F, -0.5F, 2.0F, 46.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(171, 208).addBox(-16.3125F, -16.5469F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(167, 208).addBox(15.1875F, -16.5469F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0625F, -37.9531F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition upperrails = leftspine.addOrReplaceChild("upperrails", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition upperrail_1 = upperrails.addOrReplaceChild("upperrail_1", CubeListBuilder.create().texOffs(207, 191).addBox(-1.4478F, -3.8366F, -0.5F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(8, 209).addBox(1.0914F, -4.3883F, -1.05F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.5F, -13.0F, 0.0F, 0.0F, 0.0F, 0.0873F));

		PartDefinition upperrails2 = leftspine.addOrReplaceChild("upperrails2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition upperrail_2 = upperrails2.addOrReplaceChild("upperrail_2", CubeListBuilder.create().texOffs(6, 202).addBox(-1.1542F, -3.8385F, -0.5F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(209, 2).addBox(1.385F, -4.3902F, -1.075F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.5F, -13.0F, 0.0F, 0.0F, 0.0F, 0.0873F));

		PartDefinition upperrails3 = leftspine.addOrReplaceChild("upperrails3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition upperrail_3 = upperrails3.addOrReplaceChild("upperrail_3", CubeListBuilder.create().texOffs(198, 153).addBox(-1.4042F, -3.8385F, -0.5F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(207, 208).addBox(1.135F, -4.3902F, -0.95F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.5F, -13.0F, 0.0F, 0.0F, 0.0F, 0.0873F));

		PartDefinition upperrails4 = leftspine.addOrReplaceChild("upperrails4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition upperrail_4 = upperrails4.addOrReplaceChild("upperrail_4", CubeListBuilder.create().texOffs(198, 48).addBox(-1.4042F, -3.8385F, -0.5F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(208, 151).addBox(1.135F, -4.3902F, -1.05F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.5F, -13.0F, 0.0F, 0.0F, 0.0F, 0.0873F));

		PartDefinition upperrails5 = leftspine.addOrReplaceChild("upperrails5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition upperrail_5 = upperrails5.addOrReplaceChild("upperrail_5", CubeListBuilder.create().texOffs(191, 193).addBox(-1.4042F, -3.8385F, -0.5F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(208, 115).addBox(1.135F, -4.3902F, -0.975F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.5F, -13.0F, 0.0F, 0.0F, 0.0F, 0.0873F));

		PartDefinition upperrails6 = leftspine.addOrReplaceChild("upperrails6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition upperrail_6 = upperrails6.addOrReplaceChild("upperrail_6", CubeListBuilder.create().texOffs(161, 193).addBox(-1.4042F, -3.8385F, -0.5F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(208, 103).addBox(1.135F, -4.3902F, -1.05F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.5F, -13.0F, 0.0F, 0.0F, 0.0F, 0.0873F));

		PartDefinition rightspine = hexspine.addOrReplaceChild("rightspine", CubeListBuilder.create().texOffs(30, 0).addBox(-17.0F, -1.25F, -0.5F, 34.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(36, 45).addBox(-15.5F, -47.2F, -0.5F, 2.0F, 46.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 45).addBox(13.5F, -47.2F, -0.5F, 2.0F, 46.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(163, 208).addBox(-16.25F, -54.5F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(109, 208).addBox(15.25F, -54.5F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {

		glow.render(poseStack, vertexConsumer, LightTexture.FULL_BRIGHT, packedOverlay, red, green, blue, alpha);
		door_rotate_y.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		boti.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		box.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void animateDemat(SteamExteriorTile exterior, float age) {
		this.animateGears((int)exterior.getLevel().getGameTime(), age, -3.0F);
	}

	@Override
	public void animateRemat(SteamExteriorTile exterior, float age) {
		this.animateGears((int)exterior.getLevel().getGameTime(), age, 3.0F);
	}

	@Override
	public void animateSolid(SteamExteriorTile exterior, float age) {
		this.animateGears((int)exterior.getLevel().getGameTime(), age, 1.0F);
	}

	@Override
	public void animateDoor(SteamExteriorTile tile) {
		this.door_rotate_y.yRot = (float) Math.toRadians(tile.getDoorHandler().getDoorState().isOpen() ? 75 : 0);
	}

	public void animateGears(int time, float age, float speedMult){
		ModelPart gearPart = this.box.getChild("gears");
		float gearRot = (float) Math.toRadians(time * speedMult + age);

		gearPart.getChild("gear_1_rotate_x").xRot = gearRot;
		gearPart.getChild("gear_2_rotate_x").xRot = gearRot;
		gearPart.getChild("gear_3_rotate_x").xRot = gearRot;
	}
}