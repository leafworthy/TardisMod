package net.tardis.mod.advancements;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.advancements.critereon.PlayerTrigger;
import net.tardis.mod.helpers.Helper;

public class TardisAdvancementTriggers {

    public static PlayerTrigger SLEPT_IN_BED;

    public static void register() {
        SLEPT_IN_BED = CriteriaTriggers.register(new PlayerTrigger(Helper.createRL("slept_in_bed")));
    }


}
