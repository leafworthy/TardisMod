package net.tardis.mod.exterior;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.TypeHolder;
import net.tardis.mod.misc.enums.DoorState;

/** Raw template for ExteriorsTypes.*/
public abstract class Exterior extends TypeHolder<ExteriorType> implements INBTSerializable<CompoundTag> {

    public final ITardisLevel tardis;

    public Exterior(ExteriorType type, ITardisLevel level){
        super(type);
        this.tardis = level;
    }

    public abstract void demat(Level level);
    public abstract  void remat(Level level);
    public abstract void setPosition(Level level, BlockPos pos);
    public abstract void fly(Vec3 motion);
    public abstract void placeEntityAt(ServerLevel exteriorLevel, Entity e);
    public abstract AABB getSize();
}
