package net.tardis.mod.client.models.exteriors.interior_door;// Made with Blockbench 4.3.1
// Exported for Minecraft version 1.17 - 1.18 with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.Model;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.enums.DoorState;

public class SteamInteriorDoorModel extends Model implements IInteriorDoorModel{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("interior_door_steam"), "main");
	private final ModelPart glow;
	private final ModelPart door_rotate_y;
	private final ModelPart boti;
	private final ModelPart box;

	public SteamInteriorDoorModel(ModelPart root) {
		super(tex -> RenderType.entityCutout(tex));
		this.glow = root.getChild("glow");
		this.door_rotate_y = root.getChild("door_rotate_y");
		this.boti = root.getChild("boti");
		this.box = root.getChild("box");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition glow = partdefinition.addOrReplaceChild("glow", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition glow_front_window = glow.addOrReplaceChild("glow_front_window", CubeListBuilder.create().texOffs(205, 132).addBox(1.5F, -44.0556F, 4.2833F, 3.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(205, 132).addBox(-4.5F, -44.0556F, 4.2833F, 3.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.9444F, 0.4167F));

		PartDefinition door_rotate_y = partdefinition.addOrReplaceChild("door_rotate_y", CubeListBuilder.create().texOffs(30, 9).addBox(-14.0F, -26.5F, 11.05F, 14.0F, 35.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(180, 185).addBox(-9.0F, -22.5F, 11.0F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(7.0F, 12.5F, -5.75F));

		PartDefinition front_crarved = door_rotate_y.addOrReplaceChild("front_crarved", CubeListBuilder.create().texOffs(102, 96).addBox(-7.0F, -8.0F, 0.0F, 14.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 95).addBox(-7.0F, -36.0F, 0.0F, 3.0F, 28.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(193, 111).addBox(-3.5F, -10.5F, 0.0F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(8, 95).addBox(4.0F, -36.0F, 0.0F, 3.0F, 28.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(164, 185).addBox(-2.0F, -15.0F, 0.0F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(123, 160).addBox(2.5F, -34.5F, 0.0F, 1.0F, 24.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(88, 164).addBox(-2.0F, -29.5F, 0.0F, 4.0F, 14.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(180, 185).addBox(-2.0F, -34.0F, 0.0F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(196, 22).addBox(-3.5F, -35.5F, 0.0F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(134, 6).addBox(-7.0F, -38.0F, 0.0F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(127, 160).addBox(-3.5F, -34.5F, 0.0F, 1.0F, 24.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-7.0F, 11.5F, 11.0F));

		PartDefinition handle = door_rotate_y.addOrReplaceChild("handle", CubeListBuilder.create().texOffs(70, 103).addBox(-11.5F, -21.5F, 15.825F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 8.5F, -5.25F));

		PartDefinition lock = handle.addOrReplaceChild("lock", CubeListBuilder.create().texOffs(8, 170).addBox(-2.0034F, 0.9977F, 16.869F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-11.0F, -17.45F, -0.95F, 0.0873F, 0.0873F, -0.7854F));

		PartDefinition kickplate = door_rotate_y.addOrReplaceChild("kickplate", CubeListBuilder.create().texOffs(48, 85).addBox(-11.5F, -4.5F, 16.0F, 11.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(8, 168).addBox(-11.25F, -4.25F, 15.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(98, 166).addBox(-11.25F, -1.75F, 15.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(98, 164).addBox(-1.75F, -1.75F, 15.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(160, 15).addBox(-1.75F, -4.25F, 15.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 8.5F, -5.25F));

		PartDefinition kickplate2 = door_rotate_y.addOrReplaceChild("kickplate2", CubeListBuilder.create().texOffs(48, 85).addBox(-11.5F, -4.5F, 14.75F, 11.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(8, 168).addBox(-11.25F, -4.25F, 15.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(98, 166).addBox(-11.25F, -1.75F, 15.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(98, 164).addBox(-1.75F, -1.75F, 15.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(160, 15).addBox(-1.75F, -4.25F, 15.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 8.5F, -3.25F));

		PartDefinition boti = partdefinition.addOrReplaceChild("boti", CubeListBuilder.create().texOffs(0, 0).addBox(-7.5F, -49.0F, 6.5F, 15.0F, 48.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, -0.55F));

		PartDefinition box = partdefinition.addOrReplaceChild("box", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition panel_front = box.addOrReplaceChild("panel_front", CubeListBuilder.create().texOffs(134, 0).addBox(-7.0F, -2.0556F, 4.5833F, 14.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(22, 48).addBox(-7.0F, -45.0556F, 4.5833F, 1.0F, 43.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(18, 48).addBox(6.0F, -45.0556F, 4.5833F, 1.0F, 43.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(34, 70).addBox(-8.0F, -47.0556F, 3.8833F, 16.0F, 2.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(37, 71).addBox(-7.0F, -39.8556F, 4.9833F, 14.0F, 3.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(124, 48).addBox(-7.1F, -39.0556F, 4.1833F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.9444F, 0.4167F));

		PartDefinition floorboards = panel_front.addOrReplaceChild("floorboards", CubeListBuilder.create().texOffs(33, 72).addBox(-8.0F, -0.0556F, 3.5833F, 16.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition roof_front = panel_front.addOrReplaceChild("roof_front", CubeListBuilder.create(), PartPose.offset(0.0F, -47.0F, -12.0F));

		PartDefinition outer_peak = roof_front.addOrReplaceChild("outer_peak", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0873F, 0.0F, 0.0F));

		PartDefinition panel_front_top = panel_front.addOrReplaceChild("panel_front_top", CubeListBuilder.create().texOffs(44, 133).addBox(-6.0F, -45.0556F, 4.8333F, 12.0F, 6.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(112, 199).addBox(1.0F, -44.5556F, 4.4833F, 4.0F, 4.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(199, 63).addBox(-5.0F, -44.5556F, 4.4833F, 4.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition hexspine = box.addOrReplaceChild("hexspine", CubeListBuilder.create(), PartPose.offset(0.0F, -0.5F, 0.5F));

		PartDefinition leftspine = hexspine.addOrReplaceChild("leftspine", CubeListBuilder.create().texOffs(3, 49).addBox(-13.3177F, -23.0F, 1.9088F, 3.0F, 46.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(7.0312F, -22.5F, -6.8202F, 0.0F, 1.3963F, 0.0F));

		PartDefinition rightspine = hexspine.addOrReplaceChild("rightspine", CubeListBuilder.create().texOffs(6, 50).addBox(10.3177F, -23.0F, 1.7838F, 3.0F, 46.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-7.0F, -22.5F, -6.8744F, 0.0F, -1.3963F, 0.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		glow.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door_rotate_y.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		boti.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		box.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void animateDoor(DoorState state){
		this.door_rotate_y.yRot = (float)Math.toRadians(state.isOpen() ? 70 : 0);
	}
}