package net.tardis.mod.item.tools;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.UseAnim;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.tardis.mod.creative_tabs.Tabs;
import net.tardis.mod.item.tools.ITardisTool;
import net.tardis.mod.misc.IAcceptTools;
import net.tardis.mod.misc.ToolType;

public class WeldingTorchItem extends Item implements ITardisTool {

    public WeldingTorchItem() {
        super(new Properties().stacksTo(1).tab(Tabs.MAIN));
    }

    @Override
    public InteractionResult useOn(UseOnContext context) {
        if(context.getLevel().getBlockEntity(context.getClickedPos()) instanceof IAcceptTools tools){

            boolean work = tools.doWork(this.getType());
            if(work){
                this.spawnParticles(context.getLevel(), context.getPlayer(), context.getClickedPos(), context.getClickLocation());
                return InteractionResult.sidedSuccess(!context.getLevel().isClientSide);
            }
            return InteractionResult.PASS;
        }
        return super.useOn(context);
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity entity) {
        return true;
    }

    @Override
    public UseAnim getUseAnimation(ItemStack pStack) {
        return UseAnim.BOW;
    }

    @Override
    public int getUseDuration(ItemStack pStack) {
        return 72000;
    }

    @Override
    public ToolType getType() {
        return ToolType.WELDING;
    }

    @Override
    public void spawnParticles(Level level, LivingEntity user, BlockPos pos, Vec3 hit) {
        level.addParticle(ParticleTypes.FLAME, hit.x, hit.y, hit.z, 0, 0, 0);

    }

}
