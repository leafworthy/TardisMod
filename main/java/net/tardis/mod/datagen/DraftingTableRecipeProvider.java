package net.tardis.mod.datagen;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.item.ItemRegistry;
import net.tardis.mod.misc.ToolType;

import java.io.IOException;
import java.nio.file.Path;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class DraftingTableRecipeProvider implements DataProvider {

    private final DataGenerator generator;

    public DraftingTableRecipeProvider(DataGenerator generator){
        this.generator = generator;
    }

    @Override
    public void run(CachedOutput pOutput) throws IOException {
       this.saveRecipe(new Builder(Helper.createRL("drafting_table"))
                .addPattern(" d ", " a ")
                .mapItem('d', Items.DIAMOND).mapItem('a', Items.ANVIL)
                .addToolTime(ToolType.WELDING, 40)
                .addResult(new ItemStack(ItemRegistry.DRAFTING_TABLE.get())), pOutput);
    }

    @Override
    public String getName() {
        return "TARDIS Drafting Table Recipe Provider";
    }

    public void saveRecipe(Builder builder, CachedOutput cache) throws IOException{
        try {
            DataProvider.saveStable(cache, builder.toJson(), getPath(builder.path));
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public Path getPath(ResourceLocation loc){
        return this.generator.getOutputFolder().resolve("data/" + loc.getNamespace() + "/recipes/drafting_table/" + loc.getPath() + ".json");
    }

    class Builder{
        String[] pattern = new String[3];
        Map<Character, Ingredient> ingredients = new HashMap<>();
        EnumMap<ToolType, Integer> tool = new EnumMap<ToolType, Integer>(ToolType.class);
        ItemStack result = ItemStack.EMPTY;

        ResourceLocation path;

        public Builder(ResourceLocation loc){
            this.path = loc;
        }


        public Builder addPattern(String... pattern){
            for(int i = 0; i < 3; ++i){
                if(i < pattern.length){
                    this.pattern[i] = pattern[i];
                }
                else this.pattern[i] = "   ";
            }
            return this;
        }

        public Builder mapItem(char c, Item item){
            this.ingredients.put(c, Ingredient.of(item));
            return this;
        }

        public Builder mapItem(char c, TagKey<Item> item){
            this.ingredients.put(c, Ingredient.of(item));
            return this;
        }

        public Builder addToolTime(ToolType type, int time){
            tool.put(type, time);
            return this;
        }

        public Builder addResult(ItemStack stack){
            this.result = stack;
            return this;
        }

        public JsonObject toJson(){
            JsonObject root = new JsonObject();
            root.add("type", new JsonPrimitive("tardis:drafting_table"));

            JsonArray toolJson = new JsonArray();
            for(Map.Entry<ToolType, Integer> tools : this.tool.entrySet()){
                JsonObject data = new JsonObject();
                data.add("tool", new JsonPrimitive(tools.getKey().getName()));
                data.add("time", new JsonPrimitive(tools.getValue()));
                toolJson.add(data);
            }
            root.add("tools", toolJson);

            JsonArray patternJson = new JsonArray();
            for(String pattern : this.pattern){
                patternJson.add(pattern);
            }
            root.add("pattern", patternJson);

            JsonObject keyJson = new JsonObject();
            for(Map.Entry<Character, Ingredient> key : this.ingredients.entrySet()){
                keyJson.add(key.getKey() + "", key.getValue().toJson());
            }
            root.add("keys", keyJson);

            JsonObject resultJson = new JsonObject();
            resultJson.add("item", new JsonPrimitive(ForgeRegistries.ITEMS.getKey(this.result.getItem()).toString()));
            if(this.result.getCount() > 1)
                resultJson.add("count", new JsonPrimitive(this.result.getCount()));
            root.add("result", resultJson);

            return root;
        }

    }
}
