package net.tardis.mod.control;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;

public class IncrementControl extends Control{

    public static final int MAX_INC = 100000;

    public IncrementControl(ControlType type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        ControlData<Integer> data = level.getControlDataOrCreate(this.type);

        int newInc = data.get() * 10;
        if(newInc > MAX_INC)
            newInc = 1;
        data.set(newInc);

        return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
    }
}
