package net.tardis.mod.control;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;

public class RandomizerControl extends Control{

    public RandomizerControl(ControlType type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        if(!level.getLevel().isClientSide)
            level.setDestination(level.getDestination().randomize(level.getLevel().random, 45));
        return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
    }
}
