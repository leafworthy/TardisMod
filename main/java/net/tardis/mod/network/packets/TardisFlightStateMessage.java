package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.misc.enums.FlightState;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

//SHould be sent anytime a journey changes in any way. Taking off, altering course, landing, etc
public class TardisFlightStateMessage {

    public int flightTicks;
    public int reachedTick;
    public FlightState state;

    public TardisFlightStateMessage(int flightTicks, int reachedTick, FlightState state){
        this.flightTicks = flightTicks;
        this.reachedTick = reachedTick;
        this.state = state;
    }

    public static void encode(TardisFlightStateMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.flightTicks);
        buf.writeInt(mes.reachedTick);
        buf.writeEnum(mes.state);
    }

    public static TardisFlightStateMessage decode(FriendlyByteBuf buf){
        return new TardisFlightStateMessage(buf.readInt(), buf.readInt(), buf.readEnum(FlightState.class));
    }

    public static void handle(TardisFlightStateMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleTardisFlightState(mes);
        });
        context.get().setPacketHandled(true);
    }

}
