package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.blockentities.ExteriorTile;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class ExteriorBlock extends BaseEntityBlock {

    private final Supplier<BlockEntityType<? extends ExteriorTile>> exteriorType;

    private static final VoxelShape SHAPE = makeShape();

    public ExteriorBlock(Supplier<BlockEntityType<? extends ExteriorTile>> factory) {
        super(Properties.of(Material.WOOD).strength(9999, 9999).noOcclusion());
        this.exteriorType = factory;
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return this.exteriorType.get().create(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState pos, BlockEntityType<T> type) {
        return createTickerHelper(type, exteriorType.get(), ExteriorTile::tick);
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE;
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pLevel.getBlockEntity(pPos) instanceof ExteriorTile tile){
            if(!pLevel.isClientSide)
                tile.getDoorHandler().onInteract(pPlayer, pPlayer.getItemInHand(pHand), pHand);
            return InteractionResult.sidedSuccess(pLevel.isClientSide);
        }
        return super.use(pState, pLevel, pPos, pPlayer, pHand, pHit);
    }

    public static VoxelShape makeShape(){
        VoxelShape shape = Shapes.empty();
        shape = Shapes.join(shape, Shapes.box(1, 0, -0.125, 1.125, 2.5, 1.125), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(-0.125, 0, -0.125, 0, 2.5, 1.125), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0, 0, 1, 1, 2.5, 1.125), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0, 0, -0.125, 1, 0.125, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0, 2.375, -0.125, 1, 2.5, 1), BooleanOp.OR);

        return shape;
    }
}
