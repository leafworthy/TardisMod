package net.tardis.mod.misc;

public interface ITickableObject {
    void tick();
}
