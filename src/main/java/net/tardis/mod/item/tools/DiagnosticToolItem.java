package net.tardis.mod.item.tools;

import io.netty.util.NetUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.NetworkHooks;
import net.tardis.mod.client.gui.diagnostic_tool.DiagnosticToolBaseScreen;
import net.tardis.mod.creative_tabs.Tabs;
import org.apache.http.util.NetUtils;
import org.jetbrains.annotations.Nullable;

// File created by CommandrMoose / DeepFriedIbis (trying a new name)
public class DiagnosticToolItem extends Item {

    public DiagnosticToolItem() {
        super(new Properties().tab(Tabs.MAIN).stacksTo(1));
    }

    // TODO: attuning to a TARDIS.
    // TODO: build gui
    // TODO: data saving

    @Override
    public InteractionResultHolder<ItemStack> use(Level world, Player p, InteractionHand hand) {
        Minecraft.getInstance().setScreen(new DiagnosticToolBaseScreen());
        return InteractionResultHolder.success(p.getItemInHand(hand));
    }

}
