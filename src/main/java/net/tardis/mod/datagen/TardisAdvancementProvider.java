package net.tardis.mod.datagen;

import net.minecraft.advancements.Advancement;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.advancements.AdvancementProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.tardis.mod.helpers.Helper;

import java.util.function.Consumer;

public class TardisAdvancementProvider extends AdvancementProvider {

    public TardisAdvancementProvider(DataGenerator p_123966_) {
        super(p_123966_);
    }

    @Override
    protected void registerAdvancements(Consumer<Advancement> consumer, ExistingFileHelper fileHelper) {
        //consumer.accept(new Advancement(Helper.createRL("bad_dream"), null, null, null, null, null));
    }
}
