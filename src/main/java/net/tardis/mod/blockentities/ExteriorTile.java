package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.exterior.Exterior;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.DoorHandler;
import net.tardis.mod.misc.IDoor;
import net.tardis.mod.misc.InteriorDoorData;
import net.tardis.mod.misc.TeleportEntry;
import net.tardis.mod.misc.enums.DoorState;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateDoorStateMessage;

import java.util.List;

public class ExteriorTile extends BlockEntity implements IDoor {

    private ResourceKey<Level> interior;
    private DoorHandler doorHandler = new DoorHandler(true, DoorState.values());
    private ExteriorType exteriorType;

    public ExteriorTile(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);

        this.getDoorHandler().setPacketSender(() -> {
            Network.sendToTracking(this, new UpdateDoorStateMessage(this.getBlockPos(), this.doorHandler.getDoorState(), this.doorHandler.isLocked(), this.exteriorType.getDoorStates()));
        });

        this.getDoorHandler().linkedDoor = () -> {
            if(level != null && !level.isClientSide){
                ServerLevel interior = level.getServer().getLevel(this.interior);
                if(interior != null){
                    interior.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                        for(InteriorDoorData data : tardis.getInteriorManager().getDoors().values()){
                            data.setDoorData(tardis, this.getDoorHandler());
                        }
                    });
                }
            }
        };
    }

    public void setInterior(ResourceKey<Level> interior, ExteriorType type){
        this.interior = interior;
        this.exteriorType = type;
        this.setChanged();
    }

    public static void tick(Level level, BlockPos pos, BlockState state, ExteriorTile tile){
       if(!level.isClientSide()){

           if(tile.getDoorHandler().getDoorState().isOpen()){
               List<Entity> collidedEntities = level.getEntitiesOfClass(Entity.class, new AABB(pos));

               if(!collidedEntities.isEmpty()){
                   final ServerLevel interiorWorld = level.getServer().getLevel(tile.interior);
                   if(interiorWorld != null){
                       interiorWorld.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                           InteriorDoorData data = tardis.getInteriorManager().getMainInteriorDoor();
                           for(Entity e : collidedEntities){
                               if(e.isAlive())
                                   TeleportEntry.TELEPORTS.put(e.getUUID(), new TeleportEntry(e, interiorWorld, data.placeAtMe(e, tardis), data.getRotation(tardis)));
                           }
                       });
                   }
               }
           }
       }
    }

    public DoorHandler getDoorHandler(){
        return this.doorHandler;
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        if(tag.contains("interior"))
            this.interior = ResourceKey.create(Registry.DIMENSION_REGISTRY, new ResourceLocation(tag.getString("interior")));
        this.doorHandler.deserializeNBT(tag.getCompound("door"));
        Helper.readRegistryFromString(tag, ExteriorRegistry.REGISTRY.get(), "exterior").ifPresent(ext -> this.exteriorType = ext);
        this.doorHandler.setValidDoorStates(this.exteriorType.getDoorStates());
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        if(this.interior != null){
            tag.putString("interior", this.interior.location().toString());
        }
        tag.put("door", this.doorHandler.serializeNBT());
        Helper.writeRegistryToNBT(tag, ExteriorRegistry.REGISTRY.get(), this.exteriorType, "exterior");
    }
}
