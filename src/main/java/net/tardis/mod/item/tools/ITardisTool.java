package net.tardis.mod.item.tools;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.tardis.mod.misc.ToolType;

public interface ITardisTool {

    ToolType getType();

    void spawnParticles(Level level, LivingEntity user, BlockPos pos, Vec3 hit);
}
