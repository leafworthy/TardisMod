package net.tardis.mod.control;

import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;

public class ThrottleControl extends Control{

    public ThrottleControl(ControlType type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        if(hand == player.getUsedItemHand()){
            ControlData<Float> throttleData = level.getControlDataOrCreate(this.getType());
            float last = throttleData.get();
            float delta = player.isShiftKeyDown() ? -0.1F : 0.1F;
            throttleData.set(Mth.clamp(last + delta, 0.0F, 1.0F));
            checkAndTakeoff(level);
            return InteractionResult.sidedSuccess(player.level.isClientSide);
        }
        return InteractionResult.PASS;
    }

    public boolean checkAndTakeoff(ITardisLevel level){
        ControlData<Boolean> brake = level.getControlDataOrCreate(ControlRegistry.HANDBRAKE.get());
        ControlData<Float> throttle = level.getControlDataOrCreate(ControlRegistry.THROTTLE.get());

        if(level.isInVortex())
            return false;

        boolean takeoff = !brake.get() && throttle.get() > 0.0F;
        if(takeoff)
            level.takeoff();
        return takeoff;
    }
}
