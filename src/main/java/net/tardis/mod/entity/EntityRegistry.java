package net.tardis.mod.entity;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;

public class EntityRegistry {

    public static final DeferredRegister<EntityType<?>> TYPES = DeferredRegister.create(ForgeRegistries.ENTITY_TYPES, Tardis.MODID);

    public static final RegistryObject<EntityType<ControlEntity>> CONTROL = TYPES.register("control", () -> createStatic(ControlEntity::new, 0.0625F, 0.0625F));


    public static <T extends Entity> EntityType<T> createStatic(EntityType.EntityFactory<T> factory, float width, float height){
        return EntityType.Builder.of(factory, MobCategory.MISC)
                .sized(width, height)
                .setTrackingRange(128)
                .setUpdateInterval(20)
                .setShouldReceiveVelocityUpdates(false)
                .build(Tardis.MODID);
    }
}
