package net.tardis.mod.misc;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.WorldHelper;

import java.util.UUID;

public class InteriorDoorData implements INBTSerializable<CompoundTag> {

    private String name = "";
    private BlockPos doorPos;
    private UUID doorEntityID;
    private boolean shouldBeRemoved = false;

    public InteriorDoorData(BlockPos pos){
        this.doorPos = pos.immutable();
    }

    public InteriorDoorData(UUID entityID){
        this.doorEntityID = entityID;
    }

    public InteriorDoorData(CompoundTag tag){
        this.deserializeNBT(tag);
    }

    public boolean isValidDoor(ITardisLevel tardis){
        if(this.doorEntityID == null && this.doorPos == null) {
            this.shouldBeRemoved = true; //Mark should be removed
            return false;
        }
        //TODO: Logic to check actual entities
        return true;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    /**
     * If this returns true, the next time someone tries to get door data, this will be purged
     * @return
     */
    public boolean killed(){
        return this.shouldBeRemoved;
    }

    public float getRotation(ITardisLevel tardis){
        Direction dir = WorldHelper.getHorizontalFacing(tardis.getLevel().getBlockState(this.doorPos));
        return WorldHelper.getDegreeFromRotation(dir);
    }

    public Vec3 placeAtMe(Entity e, ITardisLevel tardis){
        if(this.doorPos != null){
            tardis.getLevel().getChunkAt(this.doorPos);
            if(tardis.getLevel().getBlockEntity(this.doorPos) instanceof InteriorDoorTile door){
                return WorldHelper.centerOfBlockPos(door.getBlockPos().relative(WorldHelper.getHorizontalFacing(door.getBlockState())));
            }
            //If not this is no longer a valid door
            else {
                this.doorPos = null;
            }
        }
        return e.position();
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putString("name", this.name);
        if(this.doorPos != null)
            tag.putLong("door_pos", this.doorPos.asLong());
        else if(this.doorEntityID != null){
            tag.putString("door_id", this.doorEntityID.toString());
        }
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.name = tag.getString("name");
        if(tag.contains("door_pos"))
            this.doorPos = BlockPos.of(tag.getLong("door_pos"));
        else if(tag.contains("door_id"))
            this.doorEntityID = UUID.fromString(tag.getString("door_id"));
    }

    public void setDoorData(ITardisLevel level, DoorHandler doorHandler) {
        if(this.isValidDoor(level)){
            //For Tiles
            if(this.doorPos != null){
                if(level.getLevel().getBlockEntity(this.doorPos) instanceof IDoor door){
                    door.getDoorHandler().update(doorHandler);
                }
            }

            //For Entities
        }
    }
}
