package net.tardis.mod.network.packets;

import net.minecraft.core.Registry;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.control.ControlRegistry;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public class ControlDataMessage {

    public ResourceKey<Level> dimension;
    public ControlData<?> data;

    public ControlDataMessage(ResourceKey<Level> dimension, ControlData<?> data){
        this.dimension = dimension;
        this.data = data;
    }

    public static void encode(ControlDataMessage mes, FriendlyByteBuf buf){
        buf.writeResourceKey(mes.dimension);

        buf.writeRegistryId(ControlRegistry.REGISTRY.get(), mes.data.getType());
        mes.data.encode(buf);

    }

    public static ControlDataMessage decode(FriendlyByteBuf buf){
        ResourceKey<Level> level = buf.readResourceKey(Registry.DIMENSION_REGISTRY);
        ControlType type = buf.readRegistryIdSafe(ControlType.class);

        ControlData<?> data = type.createData(null);
        data.decode(buf);
        return new ControlDataMessage(level, data);
    }

    public static void handle(ControlDataMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleControlDataPacket(mes);
        });
        context.get().setPacketHandled(true);
    }

}
