package net.tardis.mod.blockentities;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.BlockRegistry;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.blockentities.consoles.G8ConsoleTile;
import net.tardis.mod.blockentities.consoles.SteamConsoleTile;
import net.tardis.mod.blockentities.crafting.DraftingTableTile;
import net.tardis.mod.blockentities.exteriors.SteamExteriorTile;

public class TileRegistry {

    public static final DeferredRegister<BlockEntityType<?>> TYPES = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES, Tardis.MODID);

    public static final RegistryObject<BlockEntityType<InteriorDoorTile>> INTERIOR_DOOR = TYPES.register("interior_door", () -> create(InteriorDoorTile::new, BlockRegistry.INTERIOR_DOOR.get()));

    //Consoles
    public static final RegistryObject<BlockEntityType<ConsoleTile>> G8_CONSOLE = TYPES.register("g8_console", () -> create(G8ConsoleTile::new, BlockRegistry.G8_CONSOLE.get()));
    public static final RegistryObject<BlockEntityType<SteamConsoleTile>> STEAM_CONSOLE = TYPES.register("steam_console", () -> create(SteamConsoleTile::new, BlockRegistry.STEAM_CONSOLE.get()));

    //Exteriors
    public static final RegistryObject<BlockEntityType<ExteriorTile>> STEAM_EXTERIOR = TYPES.register("steam_exterior", () -> create(SteamExteriorTile::new, BlockRegistry.STEAM_EXTERIOR.get()));

    //Machines
    public static final RegistryObject<BlockEntityType<DraftingTableTile>> DRAFTING_TABLE = TYPES.register("drafting_table", () -> create(DraftingTableTile::new, BlockRegistry.DRAFTING_TABLE.get()));
    public static final RegistryObject<BlockEntityType<BaseMonitorTile>> STEAM_MONITOR = TYPES.register("monitor", () -> create(BaseMonitorTile::new, BlockRegistry.STEAM_MONITOR.get()));

    public static <T extends BlockEntity> BlockEntityType<T> create(BlockEntityType.BlockEntitySupplier<T> factory, Block... blocks){
        return BlockEntityType.Builder.of(factory, blocks).build(null);
    }

}
