package net.tardis.mod.block;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.dev.CorridorStructureSaverBlock;
import net.tardis.mod.blockentities.TileRegistry;

public class BlockRegistry {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, Tardis.MODID);

    public static final RegistryObject<CorridorStructureSaverBlock> CORRIDOR_STRUCTURE_SAVER = BLOCKS.register("corridor_structure_saver", CorridorStructureSaverBlock::new);

    public static final RegistryObject<ConsoleBlock> G8_CONSOLE = BLOCKS.register("g8_console", () -> new ConsoleBlock(TileRegistry.G8_CONSOLE::get));
    public static final RegistryObject<ConsoleBlock> STEAM_CONSOLE = BLOCKS.register("steam_console", () -> new ConsoleBlock(TileRegistry.STEAM_CONSOLE::get));

    public static final RegistryObject<InteriorDoorBlock> INTERIOR_DOOR = BLOCKS.register("interior_door", InteriorDoorBlock::new);
    public static final RegistryObject<ExteriorBlock> STEAM_EXTERIOR = BLOCKS.register("steam_exterior",  () -> new ExteriorBlock(TileRegistry.STEAM_EXTERIOR::get));
    public static final RegistryObject<BrokenExteriorBlock> BROKEN_EXTERIOR = BLOCKS.register("broken_exterior", BrokenExteriorBlock::new);
    public static final RegistryObject<DraftingTableBlock> DRAFTING_TABLE = BLOCKS.register("drafting_table", DraftingTableBlock::new);
    public static final RegistryObject<ExteriorAuxillaryBlock> EXTERIOR_COLLISION = BLOCKS.register("exterior_axillary", ExteriorAuxillaryBlock::new);
    public static final RegistryObject<MonitorBlock> MONITOR = BLOCKS.register("monitor", () -> new MonitorBlock(BlockBehaviour.Properties.of(Material.METAL)));
}
