package net.tardis.mod.client.models.exteriors.interior_door;

import net.tardis.mod.misc.enums.DoorState;

public interface IInteriorDoorModel {

    void animateDoor(DoorState state);

}
