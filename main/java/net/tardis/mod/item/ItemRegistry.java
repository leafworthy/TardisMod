package net.tardis.mod.item;

import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.BlockRegistry;
import net.tardis.mod.creative_tabs.Tabs;
import net.tardis.mod.item.tools.DiagnosticToolItem;
import net.tardis.mod.item.tools.WeldingTorchItem;

import java.util.function.Function;

public class ItemRegistry {

    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, Tardis.MODID);

    //Tools
    public static final RegistryObject<Item> DIAGNOSTIC_TOOL = ITEMS.register("tools/diagnostic_tool", DiagnosticToolItem::new);
    public static final RegistryObject<Item> WELDING_TORCH = ITEMS.register("tools/welding_torch", WeldingTorchItem::new);

    //BlockItems
    public static final RegistryObject<BlockItem> DRAFTING_TABLE = registerBlockItem(BlockRegistry.DRAFTING_TABLE);
    //public static final RegistryObject<>

    public static RegistryObject<BlockItem> registerBlockItem(RegistryObject<?extends Block> block, Function<Item.Properties, Item.Properties> prop){
        return ITEMS.register(block.getId().getPath(), () -> new BlockItem(block.get(), prop.apply(new Item.Properties().tab(Tabs.MAIN))));
    }

    public static RegistryObject<BlockItem> registerBlockItem(RegistryObject<? extends Block> block){
        return registerBlockItem(block, prop -> prop);
    }

}
