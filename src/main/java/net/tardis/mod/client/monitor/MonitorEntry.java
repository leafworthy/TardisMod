package net.tardis.mod.client.monitor;

import net.minecraft.network.chat.Component;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.ArrayList;
import java.util.List;

public abstract class MonitorEntry {

    public static final List<MonitorEntry> ENTRIES = new ArrayList<>();

    public abstract List<Component> getText(ITardisLevel tardis);
    public abstract boolean shouldDisplay(ITardisLevel tardis);

}
