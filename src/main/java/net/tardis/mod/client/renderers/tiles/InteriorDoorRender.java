package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.Model;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.models.exteriors.interior_door.IInteriorDoorModel;

import java.util.function.Function;

public class InteriorDoorRender<M extends Model> {

    public final ResourceLocation texture;
    public final Function<EntityModelSet, M> modelFactory;
    public M model;

    public InteriorDoorRender(ResourceLocation texture, Function<EntityModelSet, M> model){
        this.texture = texture;
        this.modelFactory = model;
    }

    public ResourceLocation getTexture(){
        return this.texture;
    }

    public M getModel(){
        return this.model;
    }

    public void bake(EntityModelSet set){
        this.model = this.modelFactory.apply(set);
    }

    public void render(ITardisLevel tardis, InteriorDoorTile tile, PoseStack pose, MultiBufferSource buffer, int packedLight, int packedOverlay){
        this.preRender(tardis, tile, pose);
        float[] colors = this.getColors();
        this.model.renderToBuffer(pose, buffer.getBuffer(this.model.renderType(this.texture)), packedLight, packedOverlay, colors[0], colors[1], colors[2], colors[3]);

    }

    /**
     * Translate, Edit, or animate the interior door here
     * @param tardis
     * @param pose
     */
    public void preRender(ITardisLevel tardis, InteriorDoorTile tile, PoseStack pose){
        if(this.model instanceof IInteriorDoorModel)
            ((IInteriorDoorModel)this.model).animateDoor(tile.getDoorHandler().getDoorState());
    }

    /**
     *
     * @return MUST return an array of AT LEAST 4
     * Format: Red, Green, Blue, Alpha
     */
    public float[] getColors(){
        return new float[]{1.0F, 1.0F, 1.0F, 1.0F};
    }

}
