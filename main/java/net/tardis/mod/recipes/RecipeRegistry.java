package net.tardis.mod.recipes;

import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.recipes.serializers.DraftingTableRecipeSerializer;

public class RecipeRegistry {

    public static final DeferredRegister<RecipeSerializer<?>> RECIPES = DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, Tardis.MODID);

    public static final RecipeType<DraftingTableRecipe> DRAFTING_TABLE_TYPE = RecipeType.simple(Helper.createRL("drafting_table"));
    public static final RegistryObject<RecipeSerializer<DraftingTableRecipe>> DRAFTING_TABLE_SERIALIZER = RECIPES.register("drafting_table", DraftingTableRecipeSerializer::new);



}
