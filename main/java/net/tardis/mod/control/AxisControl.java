package net.tardis.mod.control;

import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.SpaceTimeCoord;

public class AxisControl extends Control {

    public final Direction.Axis axis;

    public AxisControl(ControlType type, Direction.Axis axis) {
        super(type);
        this.axis = axis;
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {

        SpaceTimeCoord coord = level.getDestination();
        int size = 10;//(int)level.getControlDataOrCreate(ControlRegistry.INCREMENT.get()).get();

        level.setDestination(coord.getLevel(), coord.getPos().relative(this.axis, size * (player.isShiftKeyDown() ? -1 : 1)));
        System.out.println("New Destination is " + level.getDestination().toString());
        return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
    }
}
