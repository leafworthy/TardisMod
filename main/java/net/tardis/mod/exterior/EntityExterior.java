package net.tardis.mod.exterior;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.enums.DoorState;

import java.util.UUID;
import java.util.function.BiFunction;
import java.util.function.Function;

public class EntityExterior extends Exterior{

    private final Function<Level, Entity> factory;
    private UUID exteriorID;

    public EntityExterior(ExteriorType type, ITardisLevel level, Function<Level, Entity> factory) {
        super(type, level);
        this.factory = factory;
    }

    @Override
    public void demat(Level level) {
        if(!level.isClientSide()){
            Entity e = ((ServerLevel)level).getEntity(this.exteriorID);
            if(e != null)
                e.kill();
        }
    }

    @Override
    public void remat(Level level) {
        if(!level.isClientSide){
            this.setPosition(level, tardis.getDestination().getPos());
        }
    }

    @Override
    public void setPosition(Level level, BlockPos pos) {
        if(!level.isClientSide()){
            Entity e = this.factory.apply(level);
            e.setPos(Helper.blockPosToVec3(pos, true));
            this.exteriorID = e.getUUID();
            level.addFreshEntity(e);
        }
    }

    @Override
    public void fly(Vec3 motion) {

    }

    @Override
    public void placeEntityAt(ServerLevel exteriorLevel, Entity e) {
        Entity exteriorEntity = exteriorLevel.getEntity(this.exteriorID);
        if(exteriorEntity != null){
            e.setPos(exteriorEntity.position());
        }
        else e.setPos(WorldHelper.centerOfBlockPos(this.tardis.getDestination().getPos()));
    }

    @Override
    public AABB getSize() {
        return new AABB(0, 0, 0, 1, 2, 1);
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        if(this.exteriorID != null)
            tag.putString("exterior_uuid", this.exteriorID.toString());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        if(nbt.contains("exterior_uuid"))
            this.exteriorID = UUID.fromString(nbt.getString("exterior_uuid"));
    }
}
