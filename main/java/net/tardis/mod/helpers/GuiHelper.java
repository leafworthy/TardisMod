package net.tardis.mod.helpers;

import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.Slot;

import java.util.ArrayList;
import java.util.List;

public class GuiHelper {

    public static List<Slot> getPlayerSlots(Inventory inv, int startX, int startY) {
        List<Slot> slots = new ArrayList<>();
        //Add player inv
        for(int i = 0; i < inv.items.size() - 9; ++i){
            slots.add(new Slot(inv, i + 9,
                    startX + (i % 9) * 18,
                    startY + (i / 9) * 18));
        }
        //hotbar
        for(int i = 0; i < 9; ++i){
            slots.add(new Slot(inv, i, startX + (i * 18), startY + 58));
        }
        return slots;
    }
}
