package net.tardis.mod.client.gui.minigame;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;

public class ChameleonGame extends Screen {

    public static Component TITLE = Component.translatable(Constants.Translation.makeGuiTitleTranslation("chameleon"));

    protected ChameleonGame() {
        super(TITLE);
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
    }
}
