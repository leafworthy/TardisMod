package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.crafting.DraftingTableTile;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.item.tools.ITardisTool;
import net.tardis.mod.menu.DraftingTableMenu;
import net.tardis.mod.menu.MenuRegistry;
import net.tardis.mod.recipes.DraftingTableRecipe;
import net.tardis.mod.recipes.RecipeRegistry;
import org.jetbrains.annotations.Nullable;

public class DraftingTableBlock extends BaseEntityBlock {

    public DraftingTableBlock() {
        super(Properties.of(Material.METAL)
            .sound(SoundType.ANVIL));
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pHand != pPlayer.getUsedItemHand())
            return InteractionResult.PASS;

        if(pLevel.getBlockEntity(pPos) instanceof DraftingTableTile table) {

            if(pPlayer.getItemInHand(pHand).getItem() instanceof ITardisTool tool){
                return InteractionResult.PASS; // Give interaction to tool
            }

            if (!pLevel.isClientSide) {
                NetworkHooks.openScreen((ServerPlayer) pPlayer, new SimpleMenuProvider((id, inv, player) -> new DraftingTableMenu(id, inv, table), Component.empty()), buf -> {
                    buf.writeBlockPos(pPos);
                });

            }
        }

        return super.use(pState, pLevel, pPos, pPlayer, pHand, pHit);
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) {
        return TileRegistry.DRAFTING_TABLE.get().create(pPos, pState);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext)
                .setValue(BlockStateProperties.HORIZONTAL_FACING, pContext.getHorizontalDirection().getOpposite());
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(BlockStateProperties.HORIZONTAL_FACING);
    }
}
