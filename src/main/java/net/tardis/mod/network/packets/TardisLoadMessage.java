package net.tardis.mod.network.packets;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public class TardisLoadMessage {

    public final CompoundTag tag;

    public TardisLoadMessage(CompoundTag tag){
        this.tag = tag;
    }

    public static void encode(TardisLoadMessage mes, FriendlyByteBuf buf){
        buf.writeNbt(mes.tag);
    }

    public static TardisLoadMessage decode(FriendlyByteBuf buf){
        return new TardisLoadMessage(buf.readNbt());
    }

    public static void handle(TardisLoadMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleTardisLoadMessage(mes);
        });
        context.get().setPacketHandled(true);
    }

}
