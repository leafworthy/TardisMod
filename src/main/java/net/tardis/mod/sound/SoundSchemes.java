package net.tardis.mod.sound;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.Decoder;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.Registry;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;

public class SoundSchemes {

    public static final Codec<SoundSchemes> CODEC = RecordCodecBuilder.create(instance ->
            instance.group(

                    SoundEvent.CODEC.fieldOf("takeoff").forGetter(SoundSchemes::getTakeoff),
                    Codec.INT.fieldOf("takeoff_duration").forGetter(SoundSchemes::getTakeoffDuration),
                    SoundEvent.CODEC.fieldOf("fly").forGetter(SoundSchemes::getFLY),
                    Codec.INT.fieldOf("flyloop_duration").forGetter(SoundSchemes::getFlyloopDuration),
                    SoundEvent.CODEC.fieldOf("land").forGetter(SoundSchemes::getLAND),
                    Codec.INT.fieldOf("land_duration").forGetter(SoundSchemes::getLandDuration)
            ).apply(instance, SoundSchemes::new)
    );

    public static final Codec<Pair<SoundEvent, Integer>> SOUND_BLOCK = Codec.pair(
            SoundEvent.CODEC.fieldOf("sound").codec(),
            Codec.INT.fieldOf("time").codec()
    );

    private final SoundEvent takeoff;
    private final SoundEvent FLY;
    private final SoundEvent LAND;

    private final int takeoffDuration;
    private final int flyloopDuration;
    private final int landDuration;

    public SoundSchemes(SoundEvent takeoff, int takeoffDuration, SoundEvent flyloop, int flyloopDuration, SoundEvent land, int landDuration){
        this.takeoff = takeoff;
        this.FLY = flyloop;
        this.LAND = land;
        this.takeoffDuration = takeoffDuration;
        this.flyloopDuration = flyloopDuration;
        this.landDuration = landDuration;
    }

    public SoundEvent getTakeoff() {
        return takeoff;
    }

    public SoundEvent getFLY() {
        return FLY;
    }

    public SoundEvent getLAND() {
        return LAND;
    }

    public int getTakeoffDuration() {
        return takeoffDuration;
    }

    public int getFlyloopDuration() {
        return flyloopDuration;
    }

    public int getLandDuration() {
        return landDuration;
    }

}
