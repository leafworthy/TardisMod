package net.tardis.mod.datagen;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.minecraft.client.animation.AnimationChannel;
import net.minecraft.client.animation.AnimationDefinition;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;

import java.io.*;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Map;

public class TardisAnimationProvider implements DataProvider {

    private final DataGenerator generator;

    public TardisAnimationProvider(DataGenerator generator){
        this.generator = generator;
    }

    @Override
    public void run(CachedOutput pOutput) throws IOException {
        File file = new File("../animationInput");

        if(!file.exists()){
            file.mkdirs();
            return; // No files to process if we create the folder, so die
        }

        for(File f : file.listFiles()){
            try {

                //Create output folder
                File outputFolder = new File("../animationOutput");
                if(!outputFolder.exists()){
                    outputFolder.mkdirs();
                }
                File outputFile = outputFolder.toPath().resolve(f.getName().substring(0, f.getName().lastIndexOf('.')) + ".txt").toFile();
                if(!outputFile.exists()){
                    outputFile.createNewFile();
                }
                FileWriter writer = new FileWriter(outputFile);

                JsonObject root = JsonParser.parseReader(new FileReader(f)).getAsJsonObject();

                for(Map.Entry<String, JsonElement> entry : root.get("animations").getAsJsonObject().entrySet()){
                    if(entry.getValue().isJsonObject()){
                        JsonObject animObj = entry.getValue().getAsJsonObject();
                        //Transform Title
                        String name = entry.getKey()
                                .replace("animation.", "")
                                .replace('.', '_')
                                .toUpperCase(Locale.ROOT);
                        float animationLength = animObj.get("animation_length").getAsFloat();

                        String animationString = "public static final AnimationDefinition %s = AnimationDefinition.Builder.withLength(%fF)".formatted(name, animationLength);

                        if(animObj.has("loop") && animObj.get("loop").getAsBoolean()){
                            animationString += ".looping()";
                        }

                        for(Map.Entry<String, JsonElement> bones : animObj.get("bones").getAsJsonObject().entrySet()){
                            JsonObject bone = bones.getValue().getAsJsonObject();
                            if(bone.has("rotation")){
                                JsonObject rotations = bone.getAsJsonObject("rotation");
                                animationString += ".addAnimation(\"%s\", new AnimationChannel(AnimationChannel.Targets.ROTATION, ".formatted(bones.getKey());

                                //keyframes
                                int keyframeIndex = 0;
                                for(Map.Entry<String, JsonElement> keyframe : rotations.entrySet()){
                                    JsonArray rot = keyframe.getValue().isJsonArray() ? keyframe.getValue().getAsJsonArray() : keyframe.getValue().getAsJsonObject().getAsJsonArray("post");

                                    String interpString = "AnimationChannel.Interpolations.LINEAR";
                                    if(keyframe.getValue().isJsonObject() && keyframe.getValue().getAsJsonObject().has("lerp_mode") && keyframe.getValue().getAsJsonObject().get("lerp_mode").getAsString().equals("catmullrom")){
                                        interpString = "AnimationChannel.Interpolations.CATMULLROM";
                                    }

                                    animationString += "new Keyframe(%fF, KeyframeAnimations.degreeVec(%fF, %fF, %fF), %s)"
                                        .formatted(Float.parseFloat(keyframe.getKey()), rot.get(0).getAsFloat(), rot.get(1).getAsFloat(), rot.get(2).getAsFloat(), interpString);

                                    if(keyframeIndex < rotations.entrySet().size() - 1)
                                        animationString += ", ";

                                    ++keyframeIndex;
                                }
                                animationString += "))";

                            }
                            //Position Animations
                            if(bone.has("position")){
                                int keyframeIndex = 0;
                                JsonObject pos = bone.getAsJsonObject("position");
                                animationString += ".addAnimation(\"%s\", new AnimationChannel(AnimationChannel.Targets.POSITION, "
                                    .formatted(bones.getKey());
                                for(Map.Entry<String, JsonElement> posEle : pos.entrySet()){
                                    JsonArray posFile = posEle.getValue().isJsonArray() ? posEle.getValue().getAsJsonArray() : posEle.getValue().getAsJsonObject().getAsJsonArray("post");
                                    String interpString = "AnimationChannel.Interpolations.LINEAR";
                                    if(posEle.getValue().isJsonObject() && posEle.getValue().getAsJsonObject().has("lerp_mode") && posEle.getValue().getAsJsonObject().get("lerp_mode").getAsString().equals("catmullrom")){
                                        interpString = "AnimationChannel.Interpolations.CATMULLROM";
                                    }


                                    animationString += "new Keyframe(%fF, KeyframeAnimations.posVec(%fF, %fF, %fF), %s)"
                                        .formatted(Float.parseFloat(posEle.getKey()), posFile.get(0).getAsFloat(), posFile.get(1).getAsFloat(), posFile.get(2).getAsFloat(), interpString);

                                    if(keyframeIndex < pos.entrySet().size() - 1)
                                        animationString += ", ";

                                    ++keyframeIndex;
                                }
                                animationString += "))";
                            }
                        }
                        animationString += ".build();";
                        writer.append(animationString + "\n");
                        writer.flush();
                    }
                }

            }
            catch(IOException e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public String getName() {
        return "TARDIS Animation Anti-Provider";
    }
}
