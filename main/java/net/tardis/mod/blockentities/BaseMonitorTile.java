package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.monitor.MonitorEntry;

import java.util.ArrayList;
import java.util.List;

public class BaseMonitorTile extends BlockEntity {

    public List<Component> monitorText = new ArrayList<>();

    public BaseMonitorTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public BaseMonitorTile(BlockPos pos, BlockState state){
        this(TileRegistry.MONITOR.get(), pos, state);
    }

    public void clientTick(){

        if(this.level != null){
            this.level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                this.monitorText.clear();

                for(MonitorEntry entry : MonitorEntry.ENTRIES){
                    if(entry.shouldDisplay(tardis)){
                        for(Component comp : entry.getText(tardis)){
                            this.monitorText.add(comp);
                        }
                    }
                }

            });
        }

    }



}
