package net.tardis.mod.control;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;

public class StabilizerControl extends Control{

    public StabilizerControl(ControlType type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        return InteractionResult.PASS;
    }
}
