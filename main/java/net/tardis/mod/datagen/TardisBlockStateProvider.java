package net.tardis.mod.datagen;

import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.BlockRegistry;
import net.tardis.mod.helpers.Helper;

import java.time.chrono.HijrahEra;

public class TardisBlockStateProvider extends BlockStateProvider {

    public final ExistingFileHelper fileHelper;

    public TardisBlockStateProvider(DataGenerator gen, ExistingFileHelper exFileHelper) {
        super(gen, Tardis.MODID, exFileHelper);
        this.fileHelper = exFileHelper;
    }

    @Override
    protected void registerStatesAndModels() {
        this.horizontalBlock(BlockRegistry.DRAFTING_TABLE.get(), crateExistingFile(BlockRegistry.DRAFTING_TABLE));
    }

    public ResourceLocation createBlockRL(RegistryObject<? extends Block> block){
        ResourceLocation loc = block.getId();
        return new ResourceLocation(loc.getNamespace(), "block/" + loc.getPath());
    }

    public ModelFile crateExistingFile(RegistryObject<? extends Block> reg){
        return new ModelFile.ExistingModelFile(createBlockRL(reg), this.fileHelper);
    }
}
