package net.tardis.mod.client.models.consoles;// Made with Blockbench 4.3.1
// Exported for Minecraft version 1.17 - 1.18 with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.world.entity.Entity;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.animations.consoles.SteamConsoleAnimation;
import net.tardis.mod.client.models.IAnimatableTile;
import net.tardis.mod.control.ControlRegistry;
import net.tardis.mod.helpers.ClientHelper;
import net.tardis.mod.helpers.Helper;

public class SteamConsoleModel extends HierarchicalModel<Entity> implements IAnimatableTile<ConsoleTile> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("consoles/steam"), "main");
	private final ModelPart glow;
	private final ModelPart rotor;
	private final ModelPart controls;
	private final ModelPart structure;
	private final ModelPart side_bits;
	private final ModelPart bb_main;

	private ModelPart root;

	public SteamConsoleModel(ModelPart root) {
		this.glow = root.getChild("glow");
		this.rotor = root.getChild("rotor");
		this.controls = root.getChild("controls");
		this.structure = root.getChild("structure");
		this.side_bits = root.getChild("side_bits");
		this.bb_main = root.getChild("bb_main");

		this.root = root;
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition glow = partdefinition.addOrReplaceChild("glow", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 24.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition glow_meterglass_a1 = glow.addOrReplaceChild("glow_meterglass_a1", CubeListBuilder.create().texOffs(100, 91).addBox(-1.0F, -0.3F, -0.65F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(94, 113).addBox(0.0F, -0.41F, -0.65F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(121, 108).addBox(-1.0F, -0.43F, -0.65F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(19, 110).addBox(-0.5F, -0.41F, -0.15F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(19, 110).addBox(-0.5F, -0.41F, 0.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(104, 110).addBox(-1.0F, -0.425F, -0.65F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(17, 108).addBox(-0.8F, -0.44F, 0.575F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(17, 108).addBox(-1.2F, -0.44F, 0.575F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(0.0F, -14.5F, 6.5F, -0.3491F, 0.0F, 0.0F));

		PartDefinition glow_lamp_a1 = glow.addOrReplaceChild("glow_lamp_a1", CubeListBuilder.create().texOffs(67, 82).addBox(-0.5F, -1.25F, -0.125F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.75F, -14.7F, 9.5F, -0.3491F, 0.0F, 0.0F));

		PartDefinition glow_lamp_a2 = glow.addOrReplaceChild("glow_lamp_a2", CubeListBuilder.create().texOffs(67, 82).addBox(-0.75F, -1.25F, 0.1F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.75F, -14.75F, 9.25F, -0.3491F, 0.0F, 0.0F));

		PartDefinition glow_glass_b1 = glow.addOrReplaceChild("glow_glass_b1", CubeListBuilder.create().texOffs(67, 82).addBox(0.2333F, 0.65F, -0.1333F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(67, 82).addBox(0.2333F, -0.6F, -0.1333F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.2667F, -16.0F, 3.6333F, -0.0873F, -0.5236F, 0.1745F));

		PartDefinition glow_glass_b2 = glow.addOrReplaceChild("glow_glass_b2", CubeListBuilder.create().texOffs(67, 82).addBox(0.1333F, 0.75F, -0.1333F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(67, 82).addBox(-0.9917F, -1.375F, -1.1833F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(67, 82).addBox(-0.9417F, -1.8F, -1.1833F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 82).addBox(1.4083F, -1.8F, -1.1833F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 82).addBox(1.4083F, -1.375F, -1.1833F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(67, 82).addBox(0.1333F, -0.5F, -0.1333F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(5.5167F, -16.0F, 1.7333F, 0.1745F, -2.0944F, 0.0F));

		PartDefinition glow_lamp_c1 = glow.addOrReplaceChild("glow_lamp_c1", CubeListBuilder.create(), PartPose.offsetAndRotation(10.0F, -15.1F, -2.45F, 0.2618F, -1.0472F, 0.0F));

		PartDefinition glow_lamp_c2 = glow.addOrReplaceChild("glow_lamp_c2", CubeListBuilder.create().texOffs(67, 82).addBox(-0.85F, -1.725F, -0.85F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(67, 82).addBox(-0.85F, -2.15F, -0.85F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(7.25F, -15.1F, -7.25F, 0.2618F, -1.0472F, 0.0F));

		PartDefinition glow_lamp_c3 = glow.addOrReplaceChild("glow_lamp_c3", CubeListBuilder.create().texOffs(67, 82).addBox(4.375F, -1.725F, -0.85F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(67, 82).addBox(4.375F, -2.15F, -0.85F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(7.25F, -15.1F, -7.25F, 0.2618F, -1.0472F, 0.0F));

		PartDefinition glow_core = glow.addOrReplaceChild("glow_core", CubeListBuilder.create().texOffs(67, 82).addBox(-4.1F, -13.5F, -3.0F, 8.0F, 4.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 1.0F, 0.0F));

		PartDefinition glow_radio = glow.addOrReplaceChild("glow_radio", CubeListBuilder.create().texOffs(104, 92).addBox(-0.25F, -1.0F, 0.295F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(104, 92).addBox(-0.225F, -1.0F, -2.705F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(104, 92).addBox(-0.45F, -0.575F, -1.68F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(6.35F, -14.5F, -3.5F, -0.1047F, 0.5236F, -0.2094F));

		PartDefinition rotor = partdefinition.addOrReplaceChild("rotor", CubeListBuilder.create(), PartPose.offset(0.0F, 25.0F, 0.0F));

		PartDefinition core = rotor.addOrReplaceChild("core", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition underbit_1 = core.addOrReplaceChild("underbit_1", CubeListBuilder.create().texOffs(5, 23).addBox(-5.1F, -2.75F, -3.0F, 10.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition underbit_2 = core.addOrReplaceChild("underbit_2", CubeListBuilder.create().texOffs(5, 23).addBox(-5.1F, -2.5F, -3.0F, 10.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition underbit_3 = core.addOrReplaceChild("underbit_3", CubeListBuilder.create().texOffs(5, 23).addBox(-5.1F, -2.6F, -3.0F, 10.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition spinner_slide_y = rotor.addOrReplaceChild("spinner_slide_y", CubeListBuilder.create(), PartPose.offset(0.0F, -21.675F, 0.0F));

		PartDefinition hourflip_rotate_x = spinner_slide_y.addOrReplaceChild("hourflip_rotate_x", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0375F, -0.0625F));

		PartDefinition glow_glass = hourflip_rotate_x.addOrReplaceChild("glow_glass", CubeListBuilder.create().texOffs(74, 71).addBox(-1.5F, -29.75F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(73, 74).addBox(-1.5F, -28.25F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(73, 88).addBox(-1.5F, -24.5F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(75, 78).addBox(-1.0F, -26.75F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 87).addBox(-1.0F, -25.0F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(77, 83).addBox(-0.5F, -26.0F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(73, 90).addBox(-1.5F, -23.0F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 25.3125F, 0.0625F));

		PartDefinition center = hourflip_rotate_x.addOrReplaceChild("center", CubeListBuilder.create().texOffs(75, 20).addBox(-2.625F, -29.7F, -1.0F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 20).addBox(-2.625F, -25.8F, -1.0F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 20).addBox(-3.925F, -25.875F, -0.475F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 20).addBox(-2.1F, -25.85F, -0.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(75, 20).addBox(0.575F, -29.7F, -1.0F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 20).addBox(0.575F, -25.8F, -1.0F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 20).addBox(2.025F, -25.875F, -0.475F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 20).addBox(1.1F, -25.85F, -0.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(0.0F, 25.3125F, 0.0625F));

		PartDefinition top_ring = hourflip_rotate_x.addOrReplaceChild("top_ring", CubeListBuilder.create().texOffs(5, 40).addBox(-2.525F, -30.25F, -2.475F, 5.0F, 1.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 25.3125F, 0.0625F));

		PartDefinition top_ring_r1 = top_ring.addOrReplaceChild("top_ring_r1", CubeListBuilder.create().texOffs(75, 20).addBox(5.5F, -2.5F, -2.5F, 2.0F, 5.0F, 5.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.025F, -22.5F, 0.025F, 0.0F, 0.0F, -1.5708F));

		PartDefinition under_ring = hourflip_rotate_x.addOrReplaceChild("under_ring", CubeListBuilder.create().texOffs(5, 40).addBox(-2.525F, -21.25F, -2.475F, 5.0F, 1.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 25.3125F, 0.0625F));

		PartDefinition under_ring_r1 = under_ring.addOrReplaceChild("under_ring_r1", CubeListBuilder.create().texOffs(100, 10).addBox(-2.0F, -2.5F, -2.5F, 2.0F, 5.0F, 5.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.025F, -22.5F, 0.025F, 0.0F, 0.0F, -1.5708F));

		PartDefinition strut_1 = spinner_slide_y.addOrReplaceChild("strut_1", CubeListBuilder.create().texOffs(75, 10).addBox(-2.875F, -26.5F, -1.075F, 1.0F, 13.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 10).addBox(-2.875F, -26.5F, 0.5F, 1.0F, 13.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 10).addBox(-2.875F, -27.1F, 0.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 10).addBox(-2.875F, -27.1F, -0.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 10).addBox(-2.875F, -27.1F, -0.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 25.35F, -0.175F));

		PartDefinition strut_2 = spinner_slide_y.addOrReplaceChild("strut_2", CubeListBuilder.create().texOffs(75, 10).addBox(-2.875F, -26.5F, -1.1F, 1.0F, 13.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 10).addBox(-2.875F, -26.5F, 0.5F, 1.0F, 13.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 10).addBox(-2.875F, -27.1F, -0.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 10).addBox(-2.875F, -27.1F, -0.325F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 10).addBox(-2.875F, -27.1F, 0.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(4.7F, 25.35F, -0.175F));

		PartDefinition controls = partdefinition.addOrReplaceChild("controls", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 24.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition contolset_a = controls.addOrReplaceChild("contolset_a", CubeListBuilder.create(), PartPose.offsetAndRotation(12.5F, -12.5F, 3.5F, 0.0F, 0.0F, -2.7925F));

		PartDefinition door_switch = contolset_a.addOrReplaceChild("door_switch", CubeListBuilder.create().texOffs(100, 10).addBox(-2.3F, -0.5F, 0.3F, 3.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.525F, 0.0F, 0.0F, 0.0F, 0.5236F));

		PartDefinition door_switch_r1 = door_switch.addOrReplaceChild("door_switch_r1", CubeListBuilder.create().texOffs(100, 10).addBox(-1.1F, -0.9F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-0.05F, -0.9F, 1.3F, 0.0F, 0.0F, 0.2618F));

		PartDefinition door_crank_rotate_y = door_switch.addOrReplaceChild("door_crank_rotate_y", CubeListBuilder.create().texOffs(37, 106).addBox(-0.5F, -1.775F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-0.5F, -1.025F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-0.5F, -1.025F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-1.0F, -1.025F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(0.0F, -1.025F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(0.5F, -1.025F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-1.5F, -1.025F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-1.0F, -1.025F, 0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-1.0F, -1.025F, -1.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 110).addBox(0.5F, -0.525F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 110).addBox(0.5F, -0.025F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(-0.25F, 1.525F, 1.25F));

		PartDefinition sonic_port = contolset_a.addOrReplaceChild("sonic_port", CubeListBuilder.create().texOffs(100, 10).addBox(1.0F, -0.5F, -4.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(100, 10).addBox(1.0F, -0.5F, -3.4F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(20, 110).addBox(1.5F, -1.0F, -4.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(100, 10).addBox(2.225F, -0.65F, -3.95F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.15F))
		.texOffs(100, 10).addBox(0.75F, -0.65F, -3.95F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.15F))
		.texOffs(100, 10).addBox(1.5F, -0.55F, -4.95F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(1.5F, -0.55F, -2.95F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(-0.75F, 0.0F, 0.0F));

		PartDefinition refueler = contolset_a.addOrReplaceChild("refueler", CubeListBuilder.create().texOffs(17, 110).addBox(5.0F, -6.2333F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 4.0833F, -3.5F));

		PartDefinition refueler_needle_a1_rotate_y = refueler.addOrReplaceChild("refueler_needle_a1_rotate_y", CubeListBuilder.create().texOffs(17, 110).addBox(-0.3749F, -1.375F, -0.9794F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F))
		.texOffs(17, 110).addBox(-0.6749F, -1.375F, -0.9794F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F))
		.texOffs(17, 110).addBox(-0.9749F, -1.375F, -0.9794F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(5.975F, -3.8333F, -0.025F, 0.0F, 2.1031F, 0.0F));

		PartDefinition button_set_a1 = contolset_a.addOrReplaceChild("button_set_a1", CubeListBuilder.create().texOffs(17, 110).addBox(4.05F, -0.75F, -4.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 110).addBox(4.05F, -0.75F, -3.95F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 110).addBox(4.05F, -0.75F, -3.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition baseplate_a1 = contolset_a.addOrReplaceChild("baseplate_a1", CubeListBuilder.create().texOffs(100, 10).addBox(7.0F, -2.0F, -5.0F, 1.0F, 2.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(4.5F, -2.0F, -5.5F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(4.0F, -1.0F, -5.0F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition baseplate_a2 = contolset_a.addOrReplaceChild("baseplate_a2", CubeListBuilder.create().texOffs(100, 10).addBox(-3.75F, -1.0F, 1.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offset(6.0F, -0.5F, -3.5F));

		PartDefinition baseplate_a3 = contolset_a.addOrReplaceChild("baseplate_a3", CubeListBuilder.create().texOffs(100, 10).addBox(-3.75F, -1.0F, -3.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-6.0F, -0.25F, -3.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-6.6F, -0.25F, -7.525F, 1.0F, 1.0F, 5.0F, new CubeDeformation(-0.25F))
		.texOffs(20, 12).addBox(-6.6F, -0.8F, -3.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(6.0F, -0.5F, -3.5F));

		PartDefinition controlset_b = controls.addOrReplaceChild("controlset_b", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition throttle = controlset_b.addOrReplaceChild("throttle", CubeListBuilder.create().texOffs(100, 10).addBox(-2.5F, 2.75F, 2.5F, 4.0F, 1.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(17, 110).addBox(-2.525F, 1.925F, 2.525F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(123, 103).addBox(-2.15F, 1.85F, 3.05F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(98, 111).addBox(0.15F, 1.85F, 3.05F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(111, 104).addBox(-1.0F, 1.85F, 3.05F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(100, 10).addBox(-0.7F, 2.9F, 4.0F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(13.0F, -15.5F, 0.0F, 0.0F, 0.0F, 0.1745F));

		PartDefinition leaver_b1_rotate_z = throttle.addOrReplaceChild("leaver_b1_rotate_z", CubeListBuilder.create().texOffs(100, 10).addBox(-0.9761F, -1.0172F, -3.175F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 9).addBox(-0.7611F, -4.0361F, -2.3839F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(-0.7661F, -3.9873F, -0.2455F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.85F))
		.texOffs(100, 10).addBox(-0.7761F, -2.3172F, -1.275F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.78F))
		.texOffs(47, 25).addBox(-0.7761F, -4.0172F, -2.825F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(-1.0F, 2.6F, 4.825F, 0.0F, 0.0F, 1.0472F));

		PartDefinition leaver_b3_r1 = leaver_b1_rotate_z.addOrReplaceChild("leaver_b3_r1", CubeListBuilder.create().texOffs(100, 10).addBox(-1.4F, 0.0F, -2.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.81F)), PartPose.offsetAndRotation(0.6289F, -3.1514F, 0.8518F, 0.48F, 0.0F, 0.0F));

		PartDefinition leaver_b1_jig = leaver_b1_rotate_z.addOrReplaceChild("leaver_b1_jig", CubeListBuilder.create(), PartPose.offsetAndRotation(0.4239F, -2.2672F, -0.2F, -1.1345F, 0.0F, 0.0F));

		PartDefinition x = controlset_b.addOrReplaceChild("x", CubeListBuilder.create().texOffs(123, 103).addBox(-0.425F, -0.4F, 0.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 110).addBox(-0.85F, -0.4F, 0.3F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(7.75F, -14.5F, -1.25F, 0.0F, 0.0F, 0.7854F));

		PartDefinition x2 = controlset_b.addOrReplaceChild("x2", CubeListBuilder.create().texOffs(100, 104).addBox(-0.425F, -0.4F, 0.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 110).addBox(-0.85F, -0.4F, 0.3F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(7.75F, -14.5F, -2.4F, 0.0F, 0.0F, 0.7854F));

		PartDefinition x3 = controlset_b.addOrReplaceChild("x3", CubeListBuilder.create().texOffs(69, 108).addBox(-0.425F, -0.4F, 0.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 110).addBox(-0.85F, -0.4F, 0.3F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(7.75F, -14.5F, -0.15F, 0.0F, 0.0F, 0.7854F));

		PartDefinition cordselect_inc = controlset_b.addOrReplaceChild("cordselect_inc", CubeListBuilder.create().texOffs(17, 110).addBox(2.1F, 0.95F, -4.125F, 4.0F, 1.0F, 3.0F, new CubeDeformation(-1.1F))
		.texOffs(17, 110).addBox(2.1F, 0.95F, -4.125F, 2.0F, 1.0F, 3.0F, new CubeDeformation(-1.1F))
		.texOffs(17, 110).addBox(1.9F, 0.95F, -4.125F, 2.0F, 1.0F, 3.0F, new CubeDeformation(-1.1F))
		.texOffs(100, 10).addBox(1.7F, 0.95F, -4.125F, 2.0F, 1.0F, 3.0F, new CubeDeformation(-1.1F))
		.texOffs(100, 10).addBox(2.38F, 0.37F, -3.63F, 3.0F, 2.0F, 1.0F, new CubeDeformation(-0.38F))
		.texOffs(100, 10).addBox(2.38F, 0.37F, -2.63F, 3.0F, 2.0F, 1.0F, new CubeDeformation(-0.38F))
		.texOffs(100, 10).addBox(2.38F, 0.37F, -3.13F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.38F))
		.texOffs(100, 10).addBox(4.62F, 0.37F, -3.63F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.38F))
		.texOffs(100, 10).addBox(2.14F, 0.37F, -3.63F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.38F)), PartPose.offsetAndRotation(8.5F, -14.5F, -1.0F, 0.0F, 0.0F, 0.2618F));

		PartDefinition cord_slider_rotate_z = cordselect_inc.addOrReplaceChild("cord_slider_rotate_z", CubeListBuilder.create().texOffs(17, 110).addBox(-0.425F, -14.575F, -0.725F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-0.925F, -14.025F, -0.975F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-0.925F, -14.025F, -0.475F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(3.625F, 14.5F, -2.9F, 0.0F, 0.0F, 0.0873F));

		PartDefinition baseplate_b1 = controlset_b.addOrReplaceChild("baseplate_b1", CubeListBuilder.create().texOffs(100, 10).addBox(-1.5F, 0.55F, -1.875F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(100, 10).addBox(-1.5F, 0.55F, 1.875F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(100, 10).addBox(-1.5F, 0.45F, -1.5F, 2.0F, 1.0F, 4.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(8.5F, -14.5F, -0.5F, 0.0F, 0.0F, 0.2618F));

		PartDefinition baseplate_b2 = controlset_b.addOrReplaceChild("baseplate_b2", CubeListBuilder.create().texOffs(100, 10).addBox(-2.9F, 0.85F, -0.5F, 0.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-3.85F, 0.5F, -1.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(17, 110).addBox(-3.85F, -0.1F, -1.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(17, 110).addBox(-3.85F, -0.1F, 0.65F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(-3.85F, 0.5F, 0.65F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(8.5F, -14.5F, -0.5F, 0.0F, 0.0F, 0.2618F));

		PartDefinition baseplate_b3 = controlset_b.addOrReplaceChild("baseplate_b3", CubeListBuilder.create().texOffs(100, 10).addBox(2.0F, 0.5F, -0.75F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.1F))
		.texOffs(100, 10).addBox(1.3F, 0.5F, -0.75F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.1F))
		.texOffs(100, 10).addBox(0.85F, -0.6F, -1.85F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(0.85F, -0.6F, 1.35F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(0.45F, -0.6F, -1.85F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(0.45F, -0.6F, 1.35F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(1.1F, 0.4F, -1.65F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(100, 10).addBox(1.1F, 0.4F, 1.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(8.5F, -14.5F, -0.8F, 0.0F, 0.0F, 0.2618F));

		PartDefinition readout_b1 = baseplate_b3.addOrReplaceChild("readout_b1", CubeListBuilder.create().texOffs(17, 110).addBox(-0.6F, -0.2F, -1.25F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.1F))
		.texOffs(104, 89).addBox(-0.6F, -0.5F, -1.25F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(2.25F, 0.5F, 0.5F, 0.0F, 0.0F, 0.6109F));

		PartDefinition baseplate_b4 = controlset_b.addOrReplaceChild("baseplate_b4", CubeListBuilder.create().texOffs(100, 10).addBox(2.0F, 0.5F, -0.75F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.1F))
		.texOffs(100, 10).addBox(1.3F, 0.5F, -0.75F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.1F))
		.texOffs(100, 10).addBox(0.85F, -0.6F, -1.85F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(0.85F, -0.6F, 1.35F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(0.45F, -0.6F, -1.85F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(0.45F, -0.6F, 1.35F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(1.1F, 0.4F, -1.65F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(100, 10).addBox(1.1F, 0.4F, 1.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(10.6F, -13.775F, -0.8F, 0.0F, 0.0F, 0.2618F));

		PartDefinition readout_b2 = baseplate_b4.addOrReplaceChild("readout_b2", CubeListBuilder.create().texOffs(17, 110).addBox(-0.6F, -0.2F, -1.25F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.1F))
		.texOffs(104, 89).addBox(-0.6F, -0.5F, -1.25F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(2.25F, 0.5F, 0.5F, 0.0F, 0.0F, 0.6109F));

		PartDefinition controlset_c = controls.addOrReplaceChild("controlset_c", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition communications = controlset_c.addOrReplaceChild("communications", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition radio = communications.addOrReplaceChild("radio", CubeListBuilder.create(), PartPose.offsetAndRotation(6.25F, -16.0F, 0.0F, 0.0F, 0.0F, -0.2618F));

		PartDefinition radio_dial_rotate_y = radio.addOrReplaceChild("radio_dial_rotate_y", CubeListBuilder.create().texOffs(21, 110).addBox(-0.5F, -0.6875F, -0.4875F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(21, 110).addBox(-0.5F, -0.3125F, -0.5125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.1984F, 3.0289F, 0.0625F, 0.0F, 0.0F, 0.6109F));

		PartDefinition radio_dial_sub = radio_dial_rotate_y.addOrReplaceChild("radio_dial_sub", CubeListBuilder.create().texOffs(21, 110).addBox(-0.275F, 0.55F, -0.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.225F, -0.7625F, -0.0625F, 0.0F, -0.7854F, 0.0F));

		PartDefinition radio_needle_rotate_y = radio.addOrReplaceChild("radio_needle_rotate_y", CubeListBuilder.create().texOffs(95, 111).addBox(6.675F, -0.725F, -0.125F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(95, 111).addBox(6.675F, -0.925F, -0.125F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(95, 111).addBox(6.675F, -1.125F, -0.125F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-6.425F, 1.625F, 0.125F, 0.0F, 0.3054F, 0.0F));

		PartDefinition speakers = radio.addOrReplaceChild("speakers", CubeListBuilder.create().texOffs(17, 109).addBox(-1.6F, -0.75F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 109).addBox(-1.0F, -2.35F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(17, 109).addBox(-1.1F, -1.15F, 0.05F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(17, 109).addBox(-1.1F, -1.15F, -2.075F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(17, 109).addBox(0.05F, -2.0F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(17, 109).addBox(0.25F, -2.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition radio_body = radio.addOrReplaceChild("radio_body", CubeListBuilder.create().texOffs(35, 86).addBox(-1.75F, -0.75F, -2.0F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(35, 86).addBox(-2.45F, 0.35F, -3.075F, 4.0F, 4.0F, 6.0F, new CubeDeformation(-0.5F))
		.texOffs(35, 86).addBox(-1.775F, -1.75F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(35, 86).addBox(-1.8F, -2.6F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(35, 86).addBox(-1.925F, -2.875F, -0.5F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(15, 106).addBox(0.1F, 0.75F, -2.525F, 2.0F, 2.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition radio_dial_base = radio.addOrReplaceChild("radio_dial_base", CubeListBuilder.create().texOffs(100, 10).addBox(-2.25F, -0.25F, -1.7F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(3.25F, 3.75F, 0.75F, 0.0F, 0.0F, 0.6109F));

		PartDefinition stabilizer = controlset_c.addOrReplaceChild("stabilizer", CubeListBuilder.create().texOffs(100, 9).addBox(24.975F, -0.45F, -4.075F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.6F)), PartPose.offset(-12.5F, -12.625F, -2.125F));

		PartDefinition stabilizer_button_rotate_z = stabilizer.addOrReplaceChild("stabilizer_button_rotate_z", CubeListBuilder.create().texOffs(95, 111).addBox(25.25F, -0.5F, -1.475F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.25F, -0.125F, -2.125F));

		PartDefinition baseplate_c1 = controlset_c.addOrReplaceChild("baseplate_c1", CubeListBuilder.create().texOffs(100, 10).addBox(-1.25F, -0.875F, -0.875F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 110).addBox(-0.85F, -1.125F, -0.375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.8F))
		.texOffs(17, 110).addBox(-0.85F, -1.125F, 4.85F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(9.5F, -13.125F, -2.625F, 0.0F, 0.0F, 0.3491F));

		PartDefinition baseplate_c2 = controlset_c.addOrReplaceChild("baseplate_c2", CubeListBuilder.create().texOffs(100, 10).addBox(-1.25F, -0.875F, -1.175F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(9.5F, -13.125F, 2.875F, 0.0F, 0.0F, 0.3491F));

		PartDefinition dummy_c = controlset_c.addOrReplaceChild("dummy_c", CubeListBuilder.create(), PartPose.offset(12.5F, -13.225F, -0.625F));

		PartDefinition toggle_c5 = dummy_c.addOrReplaceChild("toggle_c5", CubeListBuilder.create().texOffs(100, 10).addBox(-0.75F, 0.125F, 1.875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(17, 110).addBox(-0.775F, -0.175F, 1.88F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.7F, 0.1F, 0.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition toggle_rotate_z_c1 = toggle_c5.addOrReplaceChild("toggle_rotate_z_c1", CubeListBuilder.create().texOffs(106, 93).addBox(-0.9883F, -1.4597F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.2883F, 0.6097F, 2.88F, 0.0F, 0.0F, -0.6109F));

		PartDefinition toggle_c2 = dummy_c.addOrReplaceChild("toggle_c2", CubeListBuilder.create().texOffs(100, 10).addBox(-0.75F, 0.125F, 1.875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(17, 110).addBox(-0.775F, -0.175F, 1.88F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.7F, 0.1F, 1.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition toggle_rotate_z_c2 = toggle_c2.addOrReplaceChild("toggle_rotate_z_c2", CubeListBuilder.create().texOffs(106, 93).addBox(-0.9883F, -1.4597F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.2883F, 0.6097F, 2.88F, 0.0F, 0.0F, -0.6109F));

		PartDefinition toggle_c3 = dummy_c.addOrReplaceChild("toggle_c3", CubeListBuilder.create().texOffs(100, 10).addBox(-0.75F, 0.125F, 1.875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(18, 110).addBox(-0.775F, -0.175F, 1.88F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.7F, 0.1F, 2.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition toggle_rotate_z_c3 = toggle_c3.addOrReplaceChild("toggle_rotate_z_c3", CubeListBuilder.create().texOffs(106, 93).addBox(-0.9883F, -1.4597F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.2883F, 0.6097F, 2.88F, 0.0F, 0.0F, -0.6109F));

		PartDefinition toggle_c6 = dummy_c.addOrReplaceChild("toggle_c6", CubeListBuilder.create().texOffs(100, 10).addBox(-0.75F, 0.125F, 1.875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(17, 110).addBox(-0.775F, -0.175F, 1.88F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.7F, 0.1F, 3.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition toggle_rotate_z_c5 = toggle_c6.addOrReplaceChild("toggle_rotate_z_c5", CubeListBuilder.create().texOffs(106, 93).addBox(-0.9883F, -1.4597F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.2883F, 0.6097F, 2.88F, 0.0F, 0.0F, -0.6109F));

		PartDefinition toggle_c4 = dummy_c.addOrReplaceChild("toggle_c4", CubeListBuilder.create().texOffs(100, 10).addBox(-0.75F, 0.125F, 1.875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.7F, 0.1F, 2.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition slider_c1 = dummy_c.addOrReplaceChild("slider_c1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.5F, 0.0F, 0.0F, 0.3491F));

		PartDefinition sliderknob_c1_rotate_z = slider_c1.addOrReplaceChild("sliderknob_c1_rotate_z", CubeListBuilder.create().texOffs(17, 110).addBox(-1.2131F, -8.4176F, -1.275F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(-1.1631F, -7.3676F, -1.275F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.525F, 7.7125F, -0.55F, 0.0F, 0.0F, 0.1745F));

		PartDefinition slider_track_c1 = slider_c1.addOrReplaceChild("slider_track_c1", CubeListBuilder.create().texOffs(100, 10).addBox(-2.175F, -0.3F, -2.025F, 4.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-2.175F, -0.3F, -1.625F, 4.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-0.175F, -0.3F, -1.825F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-2.175F, -0.3F, -1.825F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(17, 110).addBox(-1.875F, -0.075F, -1.825F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(19, 108).addBox(-0.475F, -0.075F, -1.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition slider_c2 = dummy_c.addOrReplaceChild("slider_c2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 1.5F, 0.0F, 0.0F, 0.3491F));

		PartDefinition sliderknob_c1_rotate_z2 = slider_c2.addOrReplaceChild("sliderknob_c1_rotate_z2", CubeListBuilder.create().texOffs(17, 110).addBox(-1.2131F, -8.4176F, -1.275F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(-1.1631F, -7.3676F, -1.275F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(-0.525F, 7.7125F, -0.55F));

		PartDefinition slider_track_c2 = slider_c2.addOrReplaceChild("slider_track_c2", CubeListBuilder.create().texOffs(100, 10).addBox(-2.175F, -0.3F, -2.025F, 4.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-2.175F, -0.3F, -1.625F, 4.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-0.175F, -0.3F, -1.825F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-2.175F, -0.3F, -1.825F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(17, 110).addBox(-1.875F, -0.075F, -1.825F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(19, 109).addBox(-0.475F, -0.075F, -1.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition slider_c3 = dummy_c.addOrReplaceChild("slider_c3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 2.5F, 0.0F, 0.0F, 0.3491F));

		PartDefinition sliderknob_c1_rotate_z3 = slider_c3.addOrReplaceChild("sliderknob_c1_rotate_z3", CubeListBuilder.create().texOffs(17, 110).addBox(-1.2131F, -8.4176F, -1.275F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 10).addBox(-1.1631F, -7.3676F, -1.275F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.525F, 7.7125F, -0.55F, 0.0F, 0.0F, 0.1745F));

		PartDefinition slider_track_c3 = slider_c3.addOrReplaceChild("slider_track_c3", CubeListBuilder.create().texOffs(100, 10).addBox(-2.175F, -0.3F, -2.025F, 4.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-2.175F, -0.3F, -1.625F, 4.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-0.175F, -0.3F, -1.825F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-2.175F, -0.3F, -1.825F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(17, 110).addBox(-1.875F, -0.075F, -1.825F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(18, 110).addBox(-0.475F, -0.075F, -1.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition controlset_d = controls.addOrReplaceChild("controlset_d", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition fast_return = controlset_d.addOrReplaceChild("fast_return", CubeListBuilder.create(), PartPose.offsetAndRotation(-9.5F, -13.25F, 0.0F, 0.0F, 0.0F, -0.2618F));

		PartDefinition fast_return_rotate_x = fast_return.addOrReplaceChild("fast_return_rotate_x", CubeListBuilder.create().texOffs(100, 10).addBox(-0.3412F, -0.8549F, -1.1096F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(-0.3412F, 0.3451F, -1.1096F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(21, 110).addBox(-0.2412F, 0.7951F, -1.1096F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.95F))
		.texOffs(21, 110).addBox(-0.3412F, 0.7951F, -1.1096F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.95F))
		.texOffs(21, 110).addBox(-0.4412F, 0.7951F, -1.1096F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(-3.075F, -2.5F, 3.075F, 0.8727F, 0.0F, 1.6581F));

		PartDefinition keys = fast_return.addOrReplaceChild("keys", CubeListBuilder.create().texOffs(18, 110).addBox(-3.3444F, -0.25F, -1.4606F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-3.3444F, -0.25F, -2.1106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-2.5944F, -0.25F, -2.3106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-2.5944F, -0.25F, -1.7106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-2.5944F, -0.25F, -0.5106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-2.5944F, -0.25F, -1.1106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-2.5944F, -0.25F, 1.1894F, 1.0F, 1.0F, 0.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-2.5944F, -0.25F, 0.0894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-2.5944F, -0.25F, 1.2894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-2.5944F, -0.25F, 2.4894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-2.5944F, -0.25F, 1.8894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-2.9444F, -0.25F, -3.1106F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.9444F, -0.25F, -2.5106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.9444F, -0.25F, -1.9106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.9444F, -0.25F, -0.7106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.9444F, -0.25F, -1.3106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.9444F, -0.25F, 0.9894F, 1.0F, 1.0F, 0.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.9444F, -0.25F, -0.1106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.9444F, -0.25F, 1.0894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.9444F, -0.25F, 2.2894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.9444F, -0.25F, 1.6894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.3444F, -0.25F, -2.9106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.3444F, -0.25F, -2.3106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.3444F, -0.25F, -1.7106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.3444F, -0.25F, -0.5106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.3444F, -0.25F, -1.1106F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.3444F, -0.25F, 1.1894F, 1.0F, 1.0F, 0.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.3444F, -0.25F, 0.0894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.3444F, -0.25F, 1.2894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.3444F, -0.25F, 2.4894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(18, 110).addBox(-1.3444F, -0.25F, 1.8894F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-4.5556F, -1.375F, -0.2894F, 0.0F, 0.0F, -0.1745F));

		PartDefinition type_body = fast_return.addOrReplaceChild("type_body", CubeListBuilder.create().texOffs(35, 80).addBox(-5.125F, -1.925F, -3.65F, 1.0F, 2.0F, 7.0F, new CubeDeformation(-0.25F))
		.texOffs(35, 80).addBox(-4.675F, -2.0F, 2.225F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(35, 80).addBox(-4.675F, -0.5F, 2.225F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(35, 80).addBox(-4.675F, -2.0F, -3.35F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(35, 80).addBox(-3.175F, -2.0F, -3.025F, 1.0F, 3.0F, 6.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 22).addBox(-4.675F, -1.5F, -3.025F, 2.0F, 3.0F, 6.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition type_body_r1 = type_body.addOrReplaceChild("type_body_r1", CubeListBuilder.create().texOffs(35, 80).addBox(-0.6F, -0.475F, -3.675F, 5.0F, 2.0F, 7.0F, new CubeDeformation(-0.25F))
		.texOffs(35, 80).addBox(-1.7F, -0.675F, -4.075F, 4.0F, 1.0F, 8.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-6.2522F, -0.4292F, 0.0F, 0.0F, 0.0F, -0.0873F));

		PartDefinition tumbler = fast_return.addOrReplaceChild("tumbler", CubeListBuilder.create().texOffs(100, 10).addBox(-1.2625F, -1.3625F, 2.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-1.9125F, -1.8625F, -3.05F, 1.0F, 1.0F, 6.0F, new CubeDeformation(-0.3F))
		.texOffs(104, 89).addBox(-1.9125F, -1.8625F, -1.8F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.2F))
		.texOffs(104, 89).addBox(-1.9125F, -1.8625F, -2.4F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(102, 90).addBox(-2.5625F, -1.7625F, -3.1F, 4.0F, 2.0F, 6.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-1.9875F, -1.7375F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition dimention_selector = controlset_d.addOrReplaceChild("dimention_selector", CubeListBuilder.create().texOffs(100, 10).addBox(2.25F, -0.2F, -3.4F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-9.0F, -13.5F, 0.0F, 0.0F, 0.0F, -0.3491F));

		PartDefinition tilt_d1 = dimention_selector.addOrReplaceChild("tilt_d1", CubeListBuilder.create().texOffs(15, 108).addBox(-1.625F, -0.2F, -1.875F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(105, 84).addBox(-1.675F, -0.5F, -1.875F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(3.625F, -1.0F, -0.025F, 0.0F, 0.0F, -0.6109F));

		PartDefinition dim_readout = dimention_selector.addOrReplaceChild("dim_readout", CubeListBuilder.create().texOffs(9, 92).addBox(-1.675F, -0.525F, -1.875F, 3.0F, 2.0F, 4.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(3.625F, -1.0F, -0.025F, 0.0F, 0.0F, -0.6109F));

		PartDefinition baseplate_d1 = controlset_d.addOrReplaceChild("baseplate_d1", CubeListBuilder.create().texOffs(100, 10).addBox(0.4F, -0.5F, 2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(0.15F, -0.5F, 1.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(0.15F, -0.5F, -2.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(1.15F, -0.5F, -2.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(0.15F, -0.5F, -2.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(0.4F, -0.5F, -3.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(75, 85).addBox(0.65F, -0.3F, -2.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-3.35F, -0.5F, 0.25F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-2.35F, -0.5F, -4.25F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-4.85F, -0.5F, -4.25F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-4.85F, -0.5F, -4.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-2.35F, -0.5F, -0.75F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-9.0F, -13.5F, 0.0F, 0.0F, 0.0F, -0.3491F));

		PartDefinition baseplate_d2 = controlset_d.addOrReplaceChild("baseplate_d2", CubeListBuilder.create(), PartPose.offsetAndRotation(-9.0F, -13.5F, 0.0F, 0.0F, 0.0F, -0.3491F));

		PartDefinition baseplate_d3 = controlset_d.addOrReplaceChild("baseplate_d3", CubeListBuilder.create().texOffs(100, 10).addBox(-6.35F, -0.9F, 4.0F, 3.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(33, 110).addBox(-6.3224F, -1.5648F, 4.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-9.0F, -13.1F, 0.0F, 0.0F, 0.0F, -0.2618F));

		PartDefinition baseplate_d4 = controlset_d.addOrReplaceChild("baseplate_d4", CubeListBuilder.create().texOffs(100, 10).addBox(-6.35F, -0.9F, 4.0F, 3.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(33, 110).addBox(-6.3224F, -1.5898F, 4.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-9.0F, -13.1F, -10.0F, 0.0F, 0.0F, -0.2618F));

		PartDefinition controlset_e = controls.addOrReplaceChild("controlset_e", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition telepathic_circuits = controlset_e.addOrReplaceChild("telepathic_circuits", CubeListBuilder.create(), PartPose.offsetAndRotation(-9.0F, -13.675F, 0.0F, 0.0F, 0.0F, -0.2618F));

		PartDefinition talking_board = telepathic_circuits.addOrReplaceChild("talking_board", CubeListBuilder.create().texOffs(5, 22).addBox(-4.0F, -0.2F, -3.5F, 5.0F, 1.0F, 7.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-0.85F, -0.55F, 2.1F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(0.75F, -0.525F, -0.55F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(0.35F, -0.55F, 0.1F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(0.35F, -0.55F, -0.1F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 10).addBox(0.35F, -0.55F, -0.3F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(17, 11).addBox(0.9F, -0.125F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(3.675F, -0.325F, -1.325F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.5F))
		.texOffs(100, 10).addBox(3.175F, -0.375F, -1.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(100, 10).addBox(3.175F, -0.375F, 0.375F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(100, 10).addBox(3.7F, 0.1F, -0.05F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(-1.25F, 0.0F, 0.0F));

		PartDefinition talkingboard_text = talking_board.addOrReplaceChild("talkingboard_text", CubeListBuilder.create().texOffs(100, 90).addBox(-9.15F, -13.9F, 2.45F, 1.0F, 0.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-9.15F, -13.9F, -3.4F, 1.0F, 0.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-13.025F, -13.9F, -3.4F, 1.0F, 0.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-13.025F, -13.9F, 2.45F, 1.0F, 0.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-11.4F, -13.95F, 1.7F, 1.0F, 0.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-11.4F, -13.95F, -2.9F, 1.0F, 0.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-9.35F, -13.95F, -1.6F, 1.0F, 0.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-12.05F, -13.95F, -2.125F, 1.0F, 0.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-12.8F, -13.95F, -2.125F, 1.0F, 0.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-10.35F, -13.95F, -2.075F, 1.0F, 0.0F, 4.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-10.7F, -13.95F, 2.325F, 1.0F, 0.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-10.7F, -13.95F, -3.3F, 1.0F, 0.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 90).addBox(-11.0F, -13.95F, -2.5F, 1.0F, 0.0F, 5.0F, new CubeDeformation(-0.25F)), PartPose.offset(9.0555F, 14.1512F, -0.0321F));

		PartDefinition talkingboard_corners = talking_board.addOrReplaceChild("talkingboard_corners", CubeListBuilder.create().texOffs(100, 10).addBox(-13.15F, -14.05F, 2.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-13.15F, -14.05F, -3.65F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-8.9F, -14.05F, -3.65F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-8.9F, -14.05F, 2.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(9.0F, 13.725F, 0.0F));

		PartDefinition scrying_glass_rotate_y = telepathic_circuits.addOrReplaceChild("scrying_glass_rotate_y", CubeListBuilder.create(), PartPose.offsetAndRotation(2.4456F, 16.417F, -0.0544F, 0.0F, -1.6144F, 0.0F));

		PartDefinition glow_frame = scrying_glass_rotate_y.addOrReplaceChild("glow_frame", CubeListBuilder.create(), PartPose.offset(0.029F, -2.017F, -0.2342F));

		PartDefinition scrying_glass_frame_e1 = glow_frame.addOrReplaceChild("scrying_glass_frame_e1", CubeListBuilder.create().texOffs(74, 83).addBox(-2.25F, -0.625F, -1.5F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(-0.9F, -14.975F, 6.6F));

		PartDefinition scrying_glass_frame_e3 = glow_frame.addOrReplaceChild("scrying_glass_frame_e3", CubeListBuilder.create().texOffs(74, 83).addBox(-3.4894F, -0.625F, -1.1137F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.1F, -14.975F, 6.35F, 0.0F, -1.0996F, 0.0F));

		PartDefinition scrying_glass_frame_e2 = glow_frame.addOrReplaceChild("scrying_glass_frame_e2", CubeListBuilder.create().texOffs(74, 83).addBox(-0.7737F, -0.625F, -1.6023F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-1.9F, -14.975F, 6.35F, 0.0F, 1.117F, 0.0F));

		PartDefinition post_e1 = scrying_glass_rotate_y.addOrReplaceChild("post_e1", CubeListBuilder.create().texOffs(100, 10).addBox(-1.3229F, -0.9F, -2.925F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(100, 10).addBox(-2.5729F, -0.9F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(100, 10).addBox(-0.0729F, -0.9F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offset(-0.8293F, -16.717F, 5.5408F));

		PartDefinition nixietube_e1 = controlset_e.addOrReplaceChild("nixietube_e1", CubeListBuilder.create().texOffs(17, 110).addBox(-15.275F, 2.525F, -0.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.8F))
		.texOffs(17, 110).addBox(-15.275F, 2.525F, 1.375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.8F))
		.texOffs(17, 110).addBox(-14.5F, 2.45F, 0.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.8F)), PartPose.offset(8.5F, -17.5F, -0.5F));

		PartDefinition nixietube_e2 = controlset_e.addOrReplaceChild("nixietube_e2", CubeListBuilder.create(), PartPose.offset(8.5F, -17.5F, -0.5F));

		PartDefinition nixietube_e3 = controlset_e.addOrReplaceChild("nixietube_e3", CubeListBuilder.create(), PartPose.offset(8.5F, -17.5F, -0.5F));

		PartDefinition baseplate_e2 = controlset_e.addOrReplaceChild("baseplate_e2", CubeListBuilder.create().texOffs(100, 10).addBox(-0.95F, 0.825F, 8.3F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 10).addBox(-0.95F, 0.825F, -0.95F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-13.0F, -13.5F, -4.75F, 0.0F, 0.0F, -0.3491F));

		PartDefinition switch_e1 = controlset_e.addOrReplaceChild("switch_e1", CubeListBuilder.create(), PartPose.offsetAndRotation(-8.0625F, -6.5F, -1.7125F, 0.0F, 3.1416F, -0.3491F));

		PartDefinition toggle_e1 = switch_e1.addOrReplaceChild("toggle_e1", CubeListBuilder.create().texOffs(17, 110).addBox(-1.575F, -0.65F, -1.195F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(105, 89).addBox(-1.525F, -0.875F, -1.195F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(105, 89).addBox(-1.525F, -1.075F, -1.195F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(17, 110).addBox(-1.575F, -0.65F, -0.445F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(105, 89).addBox(-1.525F, -0.875F, -0.445F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(105, 89).addBox(-1.525F, -1.075F, -0.445F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-2.3125F, -7.425F, -0.5375F, 0.0F, 0.0F, -0.6109F));

		PartDefinition toggle_e2 = controlset_e.addOrReplaceChild("toggle_e2", CubeListBuilder.create().texOffs(100, 10).addBox(-4.7375F, -7.6F, -3.0625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(100, 10).addBox(-4.7375F, -7.6F, -2.3125F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.1875F, -6.05F, -3.0375F, 0.0F, 3.1416F, -0.3491F));

		PartDefinition controlset_f = controls.addOrReplaceChild("controlset_f", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition baseplate_f1 = controlset_f.addOrReplaceChild("baseplate_f1", CubeListBuilder.create().texOffs(100, 5).addBox(-2.05F, -0.075F, -2.25F, 4.0F, 2.0F, 5.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-11.2F, -13.5F, 0.0F, -0.0087F, 0.0262F, -0.6109F));

		PartDefinition toggle_f2 = baseplate_f1.addOrReplaceChild("toggle_f2", CubeListBuilder.create().texOffs(100, 5).addBox(-0.2235F, -1.0692F, -0.4843F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.1239F, 0.5929F, 1.725F, 0.0F, 3.1416F, -0.3491F));

		PartDefinition toggle_f1 = toggle_f2.addOrReplaceChild("toggle_f1", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.084F, -0.428F, -0.0075F, 0.0F, 0.0F, -0.8727F));

		PartDefinition toggle_f1_r1 = toggle_f1.addOrReplaceChild("toggle_f1_r1", CubeListBuilder.create().texOffs(17, 110).addBox(0.024F, -1.3547F, -0.4843F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.0607F, 0.4997F, 0.0125F, 0.0F, 0.0F, 0.3491F));

		PartDefinition toggle_f4_r1 = toggle_f1.addOrReplaceChild("toggle_f4_r1", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -1.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.535F, -0.492F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f3_r1 = toggle_f1.addOrReplaceChild("toggle_f3_r1", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -0.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.3245F, -1.1596F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f2_r1 = toggle_f1.addOrReplaceChild("toggle_f2_r1", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, 0.0376F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.2643F, -1.3503F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f7 = baseplate_f1.addOrReplaceChild("toggle_f7", CubeListBuilder.create().texOffs(100, 5).addBox(-0.2235F, -1.0692F, -0.4843F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.2511F, 0.7429F, 1.725F, 0.0F, 3.1416F, -0.3491F));

		PartDefinition toggle_f8 = toggle_f7.addOrReplaceChild("toggle_f8", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.084F, -0.428F, -0.0075F, 0.0F, 0.0F, -0.8727F));

		PartDefinition toggle_f2_r2 = toggle_f8.addOrReplaceChild("toggle_f2_r2", CubeListBuilder.create().texOffs(17, 110).addBox(0.024F, -1.3547F, -0.4843F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.0607F, 0.4997F, 0.0125F, 0.0F, 0.0F, 0.3491F));

		PartDefinition toggle_f5_r1 = toggle_f8.addOrReplaceChild("toggle_f5_r1", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -1.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.535F, -0.492F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f4_r2 = toggle_f8.addOrReplaceChild("toggle_f4_r2", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -0.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.3245F, -1.1596F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f3_r2 = toggle_f8.addOrReplaceChild("toggle_f3_r2", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, 0.0376F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.2643F, -1.3503F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f9 = baseplate_f1.addOrReplaceChild("toggle_f9", CubeListBuilder.create().texOffs(100, 5).addBox(-0.2235F, -1.0692F, -0.4843F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.2511F, 0.7429F, 0.25F, 0.0F, 3.1416F, -0.3491F));

		PartDefinition toggle_f10 = toggle_f9.addOrReplaceChild("toggle_f10", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.084F, -0.428F, -0.0075F, 0.0F, 0.0F, -0.8727F));

		PartDefinition toggle_f3_r3 = toggle_f10.addOrReplaceChild("toggle_f3_r3", CubeListBuilder.create().texOffs(17, 110).addBox(0.024F, -1.3547F, -0.4843F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.0607F, 0.4997F, 0.0125F, 0.0F, 0.0F, 0.3491F));

		PartDefinition toggle_f6_r1 = toggle_f10.addOrReplaceChild("toggle_f6_r1", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -1.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.535F, -0.492F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f5_r2 = toggle_f10.addOrReplaceChild("toggle_f5_r2", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -0.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.3245F, -1.1596F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f4_r3 = toggle_f10.addOrReplaceChild("toggle_f4_r3", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, 0.0376F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.2643F, -1.3503F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f11 = baseplate_f1.addOrReplaceChild("toggle_f11", CubeListBuilder.create().texOffs(100, 5).addBox(-0.2235F, -1.0692F, -0.4843F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.2511F, 0.7429F, -1.175F, 0.0F, 3.1416F, -0.3491F));

		PartDefinition toggle_f12 = toggle_f11.addOrReplaceChild("toggle_f12", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.084F, -0.428F, -0.0075F, 0.0F, 0.0F, -0.8727F));

		PartDefinition toggle_f4_r4 = toggle_f12.addOrReplaceChild("toggle_f4_r4", CubeListBuilder.create().texOffs(17, 110).addBox(0.024F, -1.3547F, -0.4843F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.0607F, 0.4997F, 0.0125F, 0.0F, 0.0F, 0.3491F));

		PartDefinition toggle_f7_r1 = toggle_f12.addOrReplaceChild("toggle_f7_r1", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -1.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.535F, -0.492F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f6_r2 = toggle_f12.addOrReplaceChild("toggle_f6_r2", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -0.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.3245F, -1.1596F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f5_r3 = toggle_f12.addOrReplaceChild("toggle_f5_r3", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, 0.0376F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.2643F, -1.3503F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f3 = baseplate_f1.addOrReplaceChild("toggle_f3", CubeListBuilder.create().texOffs(100, 5).addBox(-0.2235F, -1.0692F, -0.4843F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.1239F, 0.5929F, 0.275F, 0.0F, 3.1416F, -0.3491F));

		PartDefinition toggle_f4 = toggle_f3.addOrReplaceChild("toggle_f4", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.084F, -0.428F, -0.0075F, 0.0F, 0.0F, -0.8727F));

		PartDefinition toggle_f2_r3 = toggle_f4.addOrReplaceChild("toggle_f2_r3", CubeListBuilder.create().texOffs(17, 110).addBox(0.024F, -1.3547F, -0.4843F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.0607F, 0.4997F, 0.0125F, 0.0F, 0.0F, 0.3491F));

		PartDefinition toggle_f5_r4 = toggle_f4.addOrReplaceChild("toggle_f5_r4", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -1.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.535F, -0.492F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f4_r5 = toggle_f4.addOrReplaceChild("toggle_f4_r5", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -0.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.3245F, -1.1596F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f3_r4 = toggle_f4.addOrReplaceChild("toggle_f3_r4", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, 0.0376F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.2643F, -1.3503F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f5 = baseplate_f1.addOrReplaceChild("toggle_f5", CubeListBuilder.create().texOffs(100, 5).addBox(-0.2235F, -1.0692F, -0.4843F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.1239F, 0.5929F, -1.2F, 0.0F, 3.1416F, -0.3491F));

		PartDefinition toggle_f6 = toggle_f5.addOrReplaceChild("toggle_f6", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.084F, -0.428F, -0.0075F, 0.0F, 0.0F, -0.8727F));

		PartDefinition toggle_f3_r5 = toggle_f6.addOrReplaceChild("toggle_f3_r5", CubeListBuilder.create().texOffs(17, 110).addBox(0.024F, -1.3547F, -0.4843F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.0607F, 0.4997F, 0.0125F, 0.0F, 0.0F, 0.3491F));

		PartDefinition toggle_f6_r3 = toggle_f6.addOrReplaceChild("toggle_f6_r3", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -1.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.535F, -0.492F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f5_r5 = toggle_f6.addOrReplaceChild("toggle_f5_r5", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, -0.4624F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.3245F, -1.1596F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition toggle_f4_r6 = toggle_f6.addOrReplaceChild("toggle_f4_r6", CubeListBuilder.create().texOffs(104, 85).addBox(-0.8184F, 0.0376F, -1.4843F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.2643F, -1.3503F, 0.4875F, 0.0F, 0.0F, -0.3054F));

		PartDefinition handbreak = controlset_f.addOrReplaceChild("handbreak", CubeListBuilder.create().texOffs(100, 5).addBox(-4.6F, -0.4F, -4.75F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -13.5F, 0.0F, 0.0F, 0.0F, -0.3491F));

		PartDefinition lever_f1_rotate_z = handbreak.addOrReplaceChild("lever_f1_rotate_z", CubeListBuilder.create().texOffs(100, 5).addBox(-1.025F, -1.0F, -0.85F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(100, 5).addBox(-1.025F, -1.0F, -2.125F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(100, 5).addBox(-1.075F, -0.5F, 0.65F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 5).addBox(-1.075F, -0.5F, -1.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 5).addBox(-1.425F, -0.5F, -0.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(100, 5).addBox(-1.425F, -0.5F, -1.4F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(100, 5).addBox(-1.425F, -0.5F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(100, 5).addBox(-1.425F, -0.5F, 0.4F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(100, 5).addBox(-2.7F, -0.5F, -0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(100, 5).addBox(-3.65F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(91, 56).addBox(-3.35F, -0.525F, -0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.15F)), PartPose.offsetAndRotation(-3.9F, -0.775F, -3.75F, 0.0F, 0.0F, 0.3491F));

		PartDefinition barrel = handbreak.addOrReplaceChild("barrel", CubeListBuilder.create().texOffs(35, 110).addBox(-1.15F, -0.55F, -0.75F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.08F)), PartPose.offset(-3.25F, -0.75F, -4.0F));

		PartDefinition randomize_cords = controlset_f.addOrReplaceChild("randomize_cords", CubeListBuilder.create().texOffs(100, 5).addBox(1.0F, -0.4F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-9.0F, -14.0F, 0.0F, 0.0F, 0.0349F, -0.0873F));

		PartDefinition globe_rotate_y = randomize_cords.addOrReplaceChild("globe_rotate_y", CubeListBuilder.create().texOffs(100, 4).addBox(-1.575F, -0.575F, -0.375F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(100, 5).addBox(-0.6F, -0.3F, -1.35F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(100, 4).addBox(-1.575F, 0.025F, -0.375F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(100, 5).addBox(-0.275F, 0.75F, -0.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 5).addBox(-0.225F, 0.15F, -0.725F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 5).addBox(-0.275F, 0.35F, -0.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 5).addBox(-1.35F, -0.6F, -0.3F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 5).addBox(-0.675F, 0.75F, -0.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 4).addBox(-1.575F, 0.65F, -1.6F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(100, 5).addBox(-1.25F, -0.525F, -1.6F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(100, 5).addBox(-1.75F, 0.8F, -1.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 4).addBox(-1.8F, 0.1F, -1.65F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 5).addBox(-1.775F, -0.275F, -0.9F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 4).addBox(-1.675F, -0.1F, -1.45F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(100, 5).addBox(-0.35F, 0.675F, -1.175F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(2.5F, -3.45F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition glow_globe = globe_rotate_y.addOrReplaceChild("glow_globe", CubeListBuilder.create().texOffs(76, 83).addBox(-7.7F, -19.175F, -0.05F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.15F)), PartPose.offset(6.725F, 19.25F, -0.925F));

		PartDefinition globe_mount = randomize_cords.addOrReplaceChild("globe_mount", CubeListBuilder.create().texOffs(17, 109).addBox(1.0F, -0.7F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.4F))
		.texOffs(100, 5).addBox(1.5F, -4.875F, -2.4F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 5).addBox(1.5F, -4.875F, -2.6F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 5).addBox(1.5F, -4.875F, -2.2F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 5).addBox(1.5F, -4.675F, -2.2F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 5).addBox(1.5F, -2.075F, -2.2F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 5).addBox(1.5F, -1.875F, -2.2F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 5).addBox(1.5F, -4.875F, -1.0F, 2.0F, 6.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(40, 87).addBox(1.0F, -1.25F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.7F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rotation_selector = controlset_f.addOrReplaceChild("rotation_selector", CubeListBuilder.create().texOffs(100, 4).addBox(-26.475F, -0.55F, 0.3F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offset(12.5F, -12.5F, 3.5F));

		PartDefinition rotation_crank_rotate_y = rotation_selector.addOrReplaceChild("rotation_crank_rotate_y", CubeListBuilder.create().texOffs(36, 107).addBox(-0.55F, -1.025F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 5).addBox(-0.55F, -0.775F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 5).addBox(-0.55F, -0.775F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 5).addBox(-1.05F, -0.775F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 5).addBox(-0.05F, -0.775F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 5).addBox(0.45F, -0.775F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 5).addBox(-1.55F, -0.775F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 5).addBox(-1.05F, -0.775F, 0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(100, 5).addBox(-1.05F, -0.775F, -1.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 110).addBox(0.45F, -1.275F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 110).addBox(0.45F, -1.775F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(-25.45F, -0.725F, 1.25F));

		PartDefinition structure = partdefinition.addOrReplaceChild("structure", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition side_1 = structure.addOrReplaceChild("side_1", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition glow_panel_e = side_1.addOrReplaceChild("glow_panel_e", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition glow_glass_e2 = glow_panel_e.addOrReplaceChild("glow_glass_e2", CubeListBuilder.create().texOffs(75, 4).addBox(-14.2F, -1.05F, -1.0F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.2F, -2.3F, -1.0F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition glow_glass_e3 = glow_panel_e.addOrReplaceChild("glow_glass_e3", CubeListBuilder.create().texOffs(75, 4).addBox(-13.6F, -1.05F, 0.4F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-13.6F, -2.3F, 0.4F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition glow_glass_e1 = glow_panel_e.addOrReplaceChild("glow_glass_e1", CubeListBuilder.create().texOffs(53, 111).addBox(-15.775F, -3.525F, 0.875F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(53, 111).addBox(-15.775F, -3.525F, -1.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(53, 111).addBox(-14.975F, -3.525F, -0.25F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition bone = glow_glass_e1.addOrReplaceChild("bone", CubeListBuilder.create(), PartPose.offset(-8.5F, 14.5F, 0.5F));

		PartDefinition glow_glass_e3_r1 = bone.addOrReplaceChild("glow_glass_e3_r1", CubeListBuilder.create().texOffs(53, 111).addBox(-1.0F, -1.5F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-6.225F, -16.025F, 1.375F, 0.0F, -0.7854F, 0.0F));

		PartDefinition glow_glass_e2_r1 = bone.addOrReplaceChild("glow_glass_e2_r1", CubeListBuilder.create().texOffs(53, 111).addBox(-1.0F, -1.5F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-5.425F, -16.025F, 0.275F, 0.0F, -0.7418F, 0.0F));

		PartDefinition glow_glass_e4_r1 = bone.addOrReplaceChild("glow_glass_e4_r1", CubeListBuilder.create().texOffs(53, 111).addBox(-1.0F, -1.5F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-6.225F, -16.025F, -0.8F, 0.0F, -0.7854F, 0.0F));

		PartDefinition redlamp_e3 = glow_panel_e.addOrReplaceChild("redlamp_e3", CubeListBuilder.create().texOffs(97, 113).addBox(-0.5F, 0.4F, -0.45F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-13.0F, -13.5F, -4.75F, 0.0F, 0.0F, -0.3491F));

		PartDefinition redlamp_e4 = glow_panel_e.addOrReplaceChild("redlamp_e4", CubeListBuilder.create().texOffs(97, 113).addBox(-0.5F, 0.4F, 8.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-13.0F, -13.5F, -4.75F, 0.0F, 0.0F, -0.3491F));

		PartDefinition skin = side_1.addOrReplaceChild("skin", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_skin = skin.addOrReplaceChild("floor_skin", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards = floor_skin.addOrReplaceChild("floorboards", CubeListBuilder.create().texOffs(5, 45).addBox(-12.5F, -0.1875F, -5.5F, 5.0F, 1.0F, 11.0F, new CubeDeformation(-0.02F))
		.texOffs(100, 10).addBox(-7.5F, -0.1875F, -4.5F, 3.0F, 1.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-6.3125F, 0.375F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 3.1416F, 0.0F, 0.0F));

		PartDefinition leg_skin = floor_skin.addOrReplaceChild("leg_skin", CubeListBuilder.create().texOffs(5, 45).addBox(0.0F, 0.25F, -3.0F, 3.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.75F, -0.75F, 0.0F, 0.0F, 0.0F, -1.2217F));

		PartDefinition knee_skin = leg_skin.addOrReplaceChild("knee_skin", CubeListBuilder.create().texOffs(100, 10).addBox(0.25F, -4.0F, -2.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(1.0F, -4.25F, -2.5F, 1.0F, 6.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-0.5F, 3.0F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -1.0F, 0.0F, 0.0F, 0.0F, 1.2217F));

		PartDefinition thigh_skin = knee_skin.addOrReplaceChild("thigh_skin", CubeListBuilder.create().texOffs(40, 80).addBox(0.0F, -3.0F, -3.0F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.25F, -3.5F, 0.0F, 0.0F, 0.0F, -0.6981F));

		PartDefinition belly_skin = thigh_skin.addOrReplaceChild("belly_skin", CubeListBuilder.create().texOffs(5, 45).addBox(0.45F, -9.225F, -8.15F, 1.0F, 3.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(75, 15).addBox(-0.375F, -4.5F, -3.9F, 2.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, -2.5F, 0.0F, 0.0F, 0.0F, -0.8727F));

		PartDefinition rimtop_skin = belly_skin.addOrReplaceChild("rimtop_skin", CubeListBuilder.create(), PartPose.offsetAndRotation(1.125F, -8.5F, 0.0F, 0.0F, 0.0F, 2.9671F));

		PartDefinition panel_skin = rimtop_skin.addOrReplaceChild("panel_skin", CubeListBuilder.create().texOffs(5, 45).addBox(-2.5625F, 0.25F, -7.3F, 1.0F, 3.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 3.25F, -5.2F, 1.0F, 3.0F, 10.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 6.25F, -3.725F, 1.0F, 3.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 9.25F, -3.425F, 1.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.625F, -1.0F, 0.25F, 0.0F, 0.0F, 3.0718F));

		PartDefinition skelly = side_1.addOrReplaceChild("skelly", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib = skelly.addOrReplaceChild("rib", CubeListBuilder.create().texOffs(75, 4).addBox(-0.25F, 0.0F, -0.425F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.2263F, -0.5692F, 0.125F, 17.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-17.0F, -12.25F, -0.5F, 0.0F, 0.0F, -0.2618F));

		PartDefinition rib_bolts_1 = rib.addOrReplaceChild("rib_bolts_1", CubeListBuilder.create(), PartPose.offset(17.0F, 12.25F, 0.5F));

		PartDefinition foot = skelly.addOrReplaceChild("foot", CubeListBuilder.create().texOffs(75, 4).addBox(-12.0F, -1.25F, -0.75F, 8.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.125F, -2.35F, -1.825F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.5F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, 1.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, -1.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-4.0F, -9.75F, 0.125F, 2.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.75F, -11.5F, -0.5F, 10.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-16.9915F, -12.1853F, -0.425F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.8253F, -20.5F, -0.9725F, 1.0F, 5.0F, 3.0F, new CubeDeformation(-0.3F))
		.texOffs(75, 4).addBox(-17.15F, -12.5F, 0.125F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 5).addBox(-2.1075F, -19.725F, -0.9575F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(30, 5).addBox(-2.1075F, -18.975F, -0.9575F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offset(-2.0F, 0.0F, -0.5F));

		PartDefinition footslope = foot.addOrReplaceChild("footslope", CubeListBuilder.create(), PartPose.offset(2.0F, -0.5F, 0.375F));

		PartDefinition foot_r1 = footslope.addOrReplaceChild("foot_r1", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -1.875F, -1.5F, 10.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.4961F, -0.6518F, 1.25F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r2 = footslope.addOrReplaceChild("foot_r2", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -2.025F, -1.85F, 5.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.875F, -0.875F, 0.125F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r3 = footslope.addOrReplaceChild("foot_r3", CubeListBuilder.create().texOffs(75, 4).addBox(-0.625F, -1.375F, -2.0F, 7.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(75, 4).addBox(-1.625F, -2.875F, -2.0F, 9.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.691F, -0.0451F, 0.275F, 0.0F, 0.0F, -0.2182F));

		PartDefinition leg = foot.addOrReplaceChild("leg", CubeListBuilder.create().texOffs(45, 75).addBox(-4.0F, -16.75F, 0.125F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.25F, -1.0F, 0.0F, 0.0F, 0.0F, 0.4363F));

		PartDefinition thigh = foot.addOrReplaceChild("thigh", CubeListBuilder.create().texOffs(40, 80).addBox(-2.0F, -5.775F, 0.12F, 2.0F, 7.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(40, 80).addBox(-1.5F, -5.775F, -0.38F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -8.0F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition front_rail = side_1.addOrReplaceChild("front_rail", CubeListBuilder.create().texOffs(75, 4).addBox(2.575F, -11.7125F, 2.0F, 1.0F, 1.0F, 9.0F, new CubeDeformation(0.1F))
		.texOffs(75, 4).addBox(2.575F, -11.7125F, 11.2F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.1F)), PartPose.offsetAndRotation(-22.0F, 0.0F, 0.5F, 0.0F, 0.5236F, 0.0F));

		PartDefinition front_railbolt = front_rail.addOrReplaceChild("front_railbolt", CubeListBuilder.create().texOffs(50, 10).addBox(-19.7625F, -11.7F, 13.575F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 10.975F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 8.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 5.375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 2.825F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 16.125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 18.525F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F)), PartPose.offset(22.0F, 0.0F, -0.5F));

		PartDefinition rail_topbevel = front_rail.addOrReplaceChild("rail_topbevel", CubeListBuilder.create().texOffs(75, 4).addBox(0.175F, 0.35F, -9.325F, 1.0F, 1.0F, 18.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -12.5F, 10.75F, 0.0F, 0.0F, 0.8727F));

		PartDefinition rail_underbevel = front_rail.addOrReplaceChild("rail_underbevel", CubeListBuilder.create().texOffs(75, 4).addBox(-0.5187F, -0.3875F, -8.55F, 1.0F, 1.0F, 17.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -10.5F, 10.75F, 0.0F, 0.0F, -0.9599F));

		PartDefinition rotor_gasket = side_1.addOrReplaceChild("rotor_gasket", CubeListBuilder.create().texOffs(75, 4).addBox(-4.25F, -18.0F, -1.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-3.9667F, -20.75F, -0.9996F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(30, 5).addBox(-4.2553F, -19.95F, -0.9912F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 4).addBox(-4.9803F, -19.9F, -0.9912F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(75, 4).addBox(-4.75F, -17.25F, -1.4F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_trim = side_1.addOrReplaceChild("floor_trim", CubeListBuilder.create().texOffs(75, 4).addBox(10.75F, -1.0F, -5.5F, 2.0F, 1.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -3.5F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 3.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -1.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 5.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-21.5F, -0.1875F, 11.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition side_2 = structure.addOrReplaceChild("side_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition skin2 = side_2.addOrReplaceChild("skin2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_skin2 = skin2.addOrReplaceChild("floor_skin2", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards2 = floor_skin2.addOrReplaceChild("floorboards2", CubeListBuilder.create().texOffs(5, 45).addBox(-12.5F, -0.1875F, -5.5F, 5.0F, 1.0F, 11.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-7.5F, -0.1875F, -4.5F, 3.0F, 1.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-6.3125F, 0.375F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 3.1416F, 0.0F, 0.0F));

		PartDefinition leg_skin2 = floor_skin2.addOrReplaceChild("leg_skin2", CubeListBuilder.create().texOffs(5, 45).addBox(0.0F, 0.25F, -3.0F, 3.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.75F, -0.75F, 0.0F, 0.0F, 0.0F, -1.2217F));

		PartDefinition knee_skin2 = leg_skin2.addOrReplaceChild("knee_skin2", CubeListBuilder.create().texOffs(100, 10).addBox(0.25F, -4.0F, -2.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(1.0F, -4.25F, -2.5F, 1.0F, 6.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-0.5F, 3.0F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -1.0F, 0.0F, 0.0F, 0.0F, 1.2217F));

		PartDefinition thigh_skin2 = knee_skin2.addOrReplaceChild("thigh_skin2", CubeListBuilder.create().texOffs(40, 80).addBox(0.0F, -3.0F, -3.0F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.25F, -3.5F, 0.0F, 0.0F, 0.0F, -0.6981F));

		PartDefinition belly_skin2 = thigh_skin2.addOrReplaceChild("belly_skin2", CubeListBuilder.create().texOffs(5, 45).addBox(0.2F, -8.925F, -7.65F, 1.0F, 2.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(75, 15).addBox(-0.375F, -4.5F, -3.9F, 2.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, -2.5F, 0.0F, 0.0F, 0.0F, -0.8727F));

		PartDefinition rimtop_skin2 = belly_skin2.addOrReplaceChild("rimtop_skin2", CubeListBuilder.create(), PartPose.offsetAndRotation(1.125F, -8.5F, 0.0F, 0.0F, 0.0F, 2.9671F));

		PartDefinition panel_skin2 = rimtop_skin2.addOrReplaceChild("panel_skin2", CubeListBuilder.create().texOffs(5, 45).addBox(-2.5625F, 1.25F, -6.75F, 1.0F, 3.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, -0.75F, 0.25F, 1.0F, 2.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, -0.75F, -7.75F, 1.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 4.25F, -4.55F, 1.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 7.25F, -3.125F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.625F, -1.0F, 0.25F, 0.0F, 0.0F, 3.0718F));

		PartDefinition skelly2 = side_2.addOrReplaceChild("skelly2", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib2 = skelly2.addOrReplaceChild("rib2", CubeListBuilder.create().texOffs(75, 4).addBox(-0.25F, 0.0F, -0.425F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.2263F, -0.5692F, 0.125F, 17.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-17.0F, -12.25F, -0.5F, 0.0F, 0.0F, -0.2618F));

		PartDefinition rib_bolts_2 = rib2.addOrReplaceChild("rib_bolts_2", CubeListBuilder.create(), PartPose.offset(17.0F, 12.25F, 0.5F));

		PartDefinition foot2 = skelly2.addOrReplaceChild("foot2", CubeListBuilder.create().texOffs(75, 4).addBox(-12.0F, -1.25F, -0.75F, 8.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.125F, -2.35F, -1.825F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.5F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, 1.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, -1.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-4.0F, -9.75F, 0.125F, 2.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.75F, -11.5F, -0.5F, 10.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-16.9915F, -12.1853F, -0.425F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.7781F, -20.5F, -0.9457F, 1.0F, 5.0F, 3.0F, new CubeDeformation(-0.3F))
		.texOffs(75, 4).addBox(-17.15F, -12.5F, 0.125F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 5).addBox(-2.0428F, -19.725F, -0.9304F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(30, 5).addBox(-2.0428F, -18.975F, -0.9304F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offset(-2.0F, 0.0F, -0.5F));

		PartDefinition footslope2 = foot2.addOrReplaceChild("footslope2", CubeListBuilder.create(), PartPose.offset(2.0F, -0.5F, 0.375F));

		PartDefinition foot_r4 = footslope2.addOrReplaceChild("foot_r4", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -1.875F, -1.5F, 10.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.4961F, -0.6518F, 1.25F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r5 = footslope2.addOrReplaceChild("foot_r5", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -2.025F, -1.85F, 5.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.875F, -0.875F, 0.125F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r6 = footslope2.addOrReplaceChild("foot_r6", CubeListBuilder.create().texOffs(75, 4).addBox(-0.625F, -1.375F, -2.0F, 7.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(75, 4).addBox(-1.625F, -2.875F, -2.0F, 9.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.691F, -0.0451F, 0.275F, 0.0F, 0.0F, -0.2182F));

		PartDefinition leg2 = foot2.addOrReplaceChild("leg2", CubeListBuilder.create().texOffs(45, 75).addBox(-4.0F, -16.75F, 0.125F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.25F, -1.0F, 0.0F, 0.0F, 0.0F, 0.4363F));

		PartDefinition thigh2 = foot2.addOrReplaceChild("thigh2", CubeListBuilder.create().texOffs(40, 80).addBox(-2.0F, -5.75F, 0.12F, 2.0F, 7.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(40, 80).addBox(-1.5F, -5.75F, -0.38F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -8.0F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition front_rail2 = side_2.addOrReplaceChild("front_rail2", CubeListBuilder.create().texOffs(75, 4).addBox(2.65F, -11.7125F, 2.05F, 1.0F, 1.0F, 9.0F, new CubeDeformation(0.1F))
		.texOffs(75, 4).addBox(2.65F, -11.7125F, 11.25F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.1F)), PartPose.offsetAndRotation(-22.0F, 0.0F, 0.5F, 0.0F, 0.5236F, 0.0F));

		PartDefinition front_railbolt2 = front_rail2.addOrReplaceChild("front_railbolt2", CubeListBuilder.create().texOffs(50, 10).addBox(-19.7625F, -11.7F, 10.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 8.125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 5.375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 2.875F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 13.325F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 15.825F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 18.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F)), PartPose.offset(22.0F, 0.0F, -0.5F));

		PartDefinition rail_topbevel2 = front_rail2.addOrReplaceChild("rail_topbevel2", CubeListBuilder.create().texOffs(75, 4).addBox(0.175F, 0.35F, -9.325F, 1.0F, 1.0F, 18.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -12.5F, 10.75F, 0.0F, 0.0F, 0.8727F));

		PartDefinition rail_underbevel2 = front_rail2.addOrReplaceChild("rail_underbevel2", CubeListBuilder.create().texOffs(75, 4).addBox(-0.5187F, -0.3875F, -8.45F, 1.0F, 1.0F, 17.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -10.5F, 10.75F, 0.0F, 0.0F, -0.9599F));

		PartDefinition rotor_gasket2 = side_2.addOrReplaceChild("rotor_gasket2", CubeListBuilder.create().texOffs(75, 4).addBox(-4.25F, -18.0F, -1.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-3.9392F, -20.75F, -0.9528F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(30, 5).addBox(-4.2128F, -19.95F, -0.9353F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 4).addBox(-4.9128F, -19.95F, -0.9353F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(75, 4).addBox(-4.75F, -17.25F, -1.4F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_trim2 = side_2.addOrReplaceChild("floor_trim2", CubeListBuilder.create().texOffs(75, 4).addBox(10.75F, -1.0F, -5.5F, 2.0F, 1.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -3.5F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 3.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -1.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 5.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-21.5F, -0.1875F, 11.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition side_3 = structure.addOrReplaceChild("side_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition skin3 = side_3.addOrReplaceChild("skin3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_skin3 = skin3.addOrReplaceChild("floor_skin3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards3 = floor_skin3.addOrReplaceChild("floorboards3", CubeListBuilder.create().texOffs(5, 45).addBox(-12.5F, -0.1875F, -5.5F, 5.0F, 1.0F, 11.0F, new CubeDeformation(-0.02F))
		.texOffs(100, 10).addBox(-7.5F, -0.1875F, -4.5F, 3.0F, 1.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-6.3125F, 0.375F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 3.1416F, 0.0F, 0.0F));

		PartDefinition leg_skin3 = floor_skin3.addOrReplaceChild("leg_skin3", CubeListBuilder.create().texOffs(5, 45).addBox(0.0F, 0.25F, -3.0F, 3.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.75F, -0.75F, 0.0F, 0.0F, 0.0F, -1.2217F));

		PartDefinition knee_skin3 = leg_skin3.addOrReplaceChild("knee_skin3", CubeListBuilder.create().texOffs(100, 10).addBox(0.25F, -4.0F, -2.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(1.0F, -4.25F, -2.5F, 1.0F, 6.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-0.5F, 3.0F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -1.0F, 0.0F, 0.0F, 0.0F, 1.2217F));

		PartDefinition thigh_skin3 = knee_skin3.addOrReplaceChild("thigh_skin3", CubeListBuilder.create().texOffs(40, 80).addBox(0.0F, -3.0F, -3.0F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.25F, -3.5F, 0.0F, 0.0F, 0.0F, -0.6981F));

		PartDefinition belly_skin3 = thigh_skin3.addOrReplaceChild("belly_skin3", CubeListBuilder.create().texOffs(5, 45).addBox(0.2256F, -8.9501F, -8.3F, 1.0F, 2.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(75, 15).addBox(-0.375F, -4.5F, -3.9F, 2.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, -2.5F, 0.0F, 0.0F, 0.0F, -0.8727F));

		PartDefinition rimtop_skin3 = belly_skin3.addOrReplaceChild("rimtop_skin3", CubeListBuilder.create(), PartPose.offsetAndRotation(1.125F, -8.5F, 0.0F, 0.0F, 0.0F, 2.9671F));

		PartDefinition panel_skin3 = rimtop_skin3.addOrReplaceChild("panel_skin3", CubeListBuilder.create().texOffs(5, 45).addBox(-2.5625F, 1.25F, -6.75F, 1.0F, 3.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, -0.75F, -7.35F, 1.0F, 2.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, -0.75F, -0.35F, 1.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 4.25F, -4.55F, 1.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 7.25F, -3.125F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.625F, -1.0F, 0.25F, 0.0F, 0.0F, 3.0718F));

		PartDefinition skelly3 = side_3.addOrReplaceChild("skelly3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib3 = skelly3.addOrReplaceChild("rib3", CubeListBuilder.create().texOffs(75, 4).addBox(-0.25F, 0.0F, -0.425F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.2263F, -0.5692F, 0.125F, 17.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-17.0F, -12.25F, -0.5F, 0.0F, 0.0F, -0.2618F));

		PartDefinition rib_bolts_3 = rib3.addOrReplaceChild("rib_bolts_3", CubeListBuilder.create(), PartPose.offset(17.0F, 12.25F, 0.5F));

		PartDefinition foot3 = skelly3.addOrReplaceChild("foot3", CubeListBuilder.create().texOffs(75, 4).addBox(-12.0F, -1.25F, -0.75F, 8.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.125F, -2.35F, -1.825F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.5F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, 1.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, -1.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-4.0F, -9.75F, 0.125F, 2.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.75F, -11.5F, -0.5F, 10.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-16.9915F, -12.1853F, -0.425F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.7313F, -20.5F, -0.9732F, 1.0F, 5.0F, 3.0F, new CubeDeformation(-0.3F))
		.texOffs(75, 4).addBox(-17.15F, -12.5F, 0.125F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 5).addBox(-1.987F, -19.725F, -0.9729F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(30, 5).addBox(-1.987F, -18.975F, -0.9729F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offset(-2.0F, 0.0F, -0.5F));

		PartDefinition footslope3 = foot3.addOrReplaceChild("footslope3", CubeListBuilder.create(), PartPose.offset(2.0F, -0.5F, 0.375F));

		PartDefinition foot_r7 = footslope3.addOrReplaceChild("foot_r7", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -1.875F, -1.5F, 10.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.4961F, -0.6518F, 1.25F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r8 = footslope3.addOrReplaceChild("foot_r8", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -2.025F, -1.85F, 5.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.875F, -0.875F, 0.125F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r9 = footslope3.addOrReplaceChild("foot_r9", CubeListBuilder.create().texOffs(75, 4).addBox(-0.625F, -1.375F, -2.0F, 7.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(75, 4).addBox(-1.625F, -2.875F, -2.0F, 9.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.691F, -0.0451F, 0.275F, 0.0F, 0.0F, -0.2182F));

		PartDefinition leg3 = foot3.addOrReplaceChild("leg3", CubeListBuilder.create().texOffs(45, 75).addBox(-4.0F, -16.75F, 0.125F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.25F, -1.0F, 0.0F, 0.0F, 0.0F, 0.4363F));

		PartDefinition thigh3 = foot3.addOrReplaceChild("thigh3", CubeListBuilder.create().texOffs(40, 80).addBox(-2.0F, -5.75F, 0.12F, 2.0F, 7.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(40, 80).addBox(-1.5F, -5.75F, -0.38F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -8.0F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition front_rail3 = side_3.addOrReplaceChild("front_rail3", CubeListBuilder.create().texOffs(75, 4).addBox(2.625F, -11.7125F, 2.05F, 1.0F, 1.0F, 9.0F, new CubeDeformation(0.1F))
		.texOffs(75, 4).addBox(2.625F, -11.7125F, 11.25F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.1F)), PartPose.offsetAndRotation(-22.0F, 0.0F, 0.5F, 0.0F, 0.5236F, 0.0F));

		PartDefinition front_railbolt3 = front_rail3.addOrReplaceChild("front_railbolt3", CubeListBuilder.create().texOffs(50, 10).addBox(-19.7625F, -11.7F, 10.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 7.975F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 5.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 3.175F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 13.225F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 15.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 18.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F)), PartPose.offset(22.0F, 0.0F, -0.5F));

		PartDefinition rail_topbevel3 = front_rail3.addOrReplaceChild("rail_topbevel3", CubeListBuilder.create().texOffs(75, 4).addBox(0.175F, 0.35F, -9.325F, 1.0F, 1.0F, 18.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -12.5F, 10.75F, 0.0F, 0.0F, 0.8727F));

		PartDefinition rail_underbevel3 = front_rail3.addOrReplaceChild("rail_underbevel3", CubeListBuilder.create().texOffs(75, 4).addBox(-0.5187F, -0.3875F, -8.55F, 1.0F, 1.0F, 17.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -10.5F, 10.75F, 0.0F, 0.0F, -0.9599F));

		PartDefinition rotor_gasket3 = side_3.addOrReplaceChild("rotor_gasket3", CubeListBuilder.create().texOffs(75, 4).addBox(-4.25F, -18.0F, -1.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-3.885F, -20.75F, -0.9532F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(30, 5).addBox(-4.1432F, -19.95F, -0.9441F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 4).addBox(-4.8932F, -19.95F, -0.9441F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(75, 4).addBox(-4.75F, -17.25F, -1.4F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_trim3 = side_3.addOrReplaceChild("floor_trim3", CubeListBuilder.create().texOffs(75, 4).addBox(10.75F, -1.0F, -5.5F, 2.0F, 1.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -3.5F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 3.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -1.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 5.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-21.5F, -0.1875F, 11.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition side_4 = structure.addOrReplaceChild("side_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition glow_panel_e4 = side_4.addOrReplaceChild("glow_panel_e4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition glow_glass_e10 = glow_panel_e4.addOrReplaceChild("glow_glass_e10", CubeListBuilder.create().texOffs(75, 4).addBox(-14.2F, -1.05F, -1.0F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.2F, -2.3F, -1.0F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition glow_glass_e11 = glow_panel_e4.addOrReplaceChild("glow_glass_e11", CubeListBuilder.create().texOffs(75, 4).addBox(-13.6F, -1.05F, 0.4F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-13.6F, -2.3F, 0.4F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition glow_glass_e12 = glow_panel_e4.addOrReplaceChild("glow_glass_e12", CubeListBuilder.create().texOffs(75, 4).addBox(-14.2F, -1.05F, 1.7F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.2F, -2.3F, 1.7F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition redlamp_e8 = glow_panel_e4.addOrReplaceChild("redlamp_e8", CubeListBuilder.create(), PartPose.offsetAndRotation(-13.0F, -13.5F, -4.75F, 0.0F, 0.0F, -0.3491F));

		PartDefinition redlamp_e9 = glow_panel_e4.addOrReplaceChild("redlamp_e9", CubeListBuilder.create(), PartPose.offsetAndRotation(-13.0F, -13.5F, -4.75F, 0.0F, 0.0F, -0.3491F));

		PartDefinition skin4 = side_4.addOrReplaceChild("skin4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_skin4 = skin4.addOrReplaceChild("floor_skin4", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards4 = floor_skin4.addOrReplaceChild("floorboards4", CubeListBuilder.create().texOffs(5, 45).addBox(-12.5F, -0.1875F, -5.5F, 5.0F, 1.0F, 11.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-7.5F, -0.1875F, -4.5F, 3.0F, 1.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-6.3125F, 0.375F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 3.1416F, 0.0F, 0.0F));

		PartDefinition leg_skin4 = floor_skin4.addOrReplaceChild("leg_skin4", CubeListBuilder.create().texOffs(5, 45).addBox(0.0F, 0.25F, -3.0F, 3.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.75F, -0.75F, 0.0F, 0.0F, 0.0F, -1.2217F));

		PartDefinition knee_skin4 = leg_skin4.addOrReplaceChild("knee_skin4", CubeListBuilder.create().texOffs(100, 10).addBox(0.25F, -4.0F, -2.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(1.0F, -4.25F, -2.5F, 1.0F, 6.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-0.5F, 3.0F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -1.0F, 0.0F, 0.0F, 0.0F, 1.2217F));

		PartDefinition thigh_skin4 = knee_skin4.addOrReplaceChild("thigh_skin4", CubeListBuilder.create().texOffs(40, 80).addBox(0.0F, -3.0F, -3.0F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.25F, -3.5F, 0.0F, 0.0F, 0.0F, -0.6981F));

		PartDefinition belly_skin4 = thigh_skin4.addOrReplaceChild("belly_skin4", CubeListBuilder.create().texOffs(5, 45).addBox(0.2256F, -9.525F, -7.65F, 1.0F, 2.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(75, 15).addBox(-0.375F, -4.5F, -3.9F, 2.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, -2.5F, 0.0F, 0.0F, 0.0F, -0.8727F));

		PartDefinition rimtop_skin4 = belly_skin4.addOrReplaceChild("rimtop_skin4", CubeListBuilder.create(), PartPose.offsetAndRotation(1.125F, -8.5F, 0.0F, 0.0F, 0.0F, 2.9671F));

		PartDefinition panel_skin4 = rimtop_skin4.addOrReplaceChild("panel_skin4", CubeListBuilder.create().texOffs(5, 45).addBox(-2.5625F, 1.25F, -6.95F, 1.0F, 3.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, -1.75F, -7.45F, 1.0F, 3.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, -1.75F, -0.45F, 1.0F, 3.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 4.25F, -4.55F, 1.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 7.25F, -3.125F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.625F, -1.0F, 0.25F, 0.0F, 0.0F, 3.0718F));

		PartDefinition skelly4 = side_4.addOrReplaceChild("skelly4", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib4 = skelly4.addOrReplaceChild("rib4", CubeListBuilder.create().texOffs(75, 4).addBox(-0.25F, 0.0F, -0.425F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.9763F, -0.5692F, 0.1F, 17.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-17.0F, -12.25F, -0.5F, 0.0F, 0.0F, -0.2618F));

		PartDefinition rib_bolts_4 = rib4.addOrReplaceChild("rib_bolts_4", CubeListBuilder.create(), PartPose.offset(17.0F, 12.25F, 0.5F));

		PartDefinition foot4 = skelly4.addOrReplaceChild("foot4", CubeListBuilder.create().texOffs(75, 4).addBox(-12.0F, -1.25F, -0.75F, 8.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.125F, -2.35F, -1.825F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.5F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, 1.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, -1.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-4.0F, -9.75F, 0.125F, 2.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.75F, -11.5F, -0.5F, 10.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-16.9915F, -12.1853F, -0.425F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.7317F, -20.5F, -1.0275F, 1.0F, 5.0F, 3.0F, new CubeDeformation(-0.3F))
		.texOffs(75, 4).addBox(-17.15F, -12.5F, 0.1F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 5).addBox(-1.9958F, -19.725F, -1.0425F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(30, 5).addBox(-1.9958F, -18.975F, -1.0425F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offset(-2.0F, 0.0F, -0.5F));

		PartDefinition footslope4 = foot4.addOrReplaceChild("footslope4", CubeListBuilder.create(), PartPose.offset(2.0F, -0.5F, 0.375F));

		PartDefinition foot_r10 = footslope4.addOrReplaceChild("foot_r10", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -1.875F, -1.5F, 10.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.4961F, -0.6518F, 1.25F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r11 = footslope4.addOrReplaceChild("foot_r11", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -2.025F, -1.85F, 5.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.875F, -0.875F, 0.125F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r12 = footslope4.addOrReplaceChild("foot_r12", CubeListBuilder.create().texOffs(75, 4).addBox(-0.625F, -1.375F, -2.0F, 7.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(75, 4).addBox(-1.625F, -2.875F, -2.0F, 9.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.691F, -0.0451F, 0.275F, 0.0F, 0.0F, -0.2182F));

		PartDefinition leg4 = foot4.addOrReplaceChild("leg4", CubeListBuilder.create().texOffs(45, 75).addBox(-4.0F, -16.75F, 0.1F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.25F, -1.0F, 0.0F, 0.0F, 0.0F, 0.4363F));

		PartDefinition thigh4 = foot4.addOrReplaceChild("thigh4", CubeListBuilder.create().texOffs(40, 80).addBox(-2.0F, -5.75F, 0.12F, 2.0F, 7.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(40, 80).addBox(-1.5F, -5.75F, -0.38F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -8.0F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition front_rail4 = side_4.addOrReplaceChild("front_rail4", CubeListBuilder.create().texOffs(75, 4).addBox(2.625F, -11.7125F, 2.075F, 1.0F, 1.0F, 9.0F, new CubeDeformation(0.1F))
		.texOffs(75, 4).addBox(2.625F, -11.7125F, 11.275F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.1F)), PartPose.offsetAndRotation(-22.0F, 0.0F, 0.5F, 0.0F, 0.5236F, 0.0F));

		PartDefinition front_railbolt4 = front_rail4.addOrReplaceChild("front_railbolt4", CubeListBuilder.create().texOffs(50, 10).addBox(-19.7625F, -11.7F, 10.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 8.025F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 5.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 3.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 13.225F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 15.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 18.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F)), PartPose.offset(22.0F, 0.0F, -0.5F));

		PartDefinition rail_topbevel4 = front_rail4.addOrReplaceChild("rail_topbevel4", CubeListBuilder.create().texOffs(75, 4).addBox(0.175F, 0.35F, -9.6F, 1.0F, 1.0F, 18.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -12.5F, 10.75F, 0.0F, 0.0F, 0.8727F));

		PartDefinition rail_underbevel4 = front_rail4.addOrReplaceChild("rail_underbevel4", CubeListBuilder.create().texOffs(75, 4).addBox(-0.5187F, -0.3875F, -8.55F, 1.0F, 1.0F, 17.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -10.5F, 10.75F, 0.0F, 0.0F, -0.9599F));

		PartDefinition rotor_gasket4 = side_4.addOrReplaceChild("rotor_gasket4", CubeListBuilder.create().texOffs(75, 4).addBox(-4.25F, -18.0F, -1.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-3.8582F, -20.75F, -1.0004F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(29, 5).addBox(-4.116F, -19.95F, -1.0088F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 4).addBox(-4.866F, -19.95F, -1.0088F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(75, 4).addBox(-4.75F, -17.25F, -1.4F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_trim4 = side_4.addOrReplaceChild("floor_trim4", CubeListBuilder.create().texOffs(75, 4).addBox(10.75F, -1.0F, -5.5F, 2.0F, 1.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -3.5F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 3.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -1.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 5.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-21.5F, -0.1875F, 11.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition side_5 = structure.addOrReplaceChild("side_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition glow_panel_e5 = side_5.addOrReplaceChild("glow_panel_e5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition glow_glass_e13 = glow_panel_e5.addOrReplaceChild("glow_glass_e13", CubeListBuilder.create().texOffs(75, 4).addBox(-14.2F, -1.05F, -1.0F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.2F, -2.3F, -1.0F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition glow_glass_e14 = glow_panel_e5.addOrReplaceChild("glow_glass_e14", CubeListBuilder.create().texOffs(75, 4).addBox(-13.6F, -1.05F, 0.4F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-13.6F, -2.3F, 0.4F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition glow_glass_e15 = glow_panel_e5.addOrReplaceChild("glow_glass_e15", CubeListBuilder.create().texOffs(75, 4).addBox(-14.2F, -1.05F, 1.7F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.2F, -2.3F, 1.7F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition redlamp_e10 = glow_panel_e5.addOrReplaceChild("redlamp_e10", CubeListBuilder.create(), PartPose.offsetAndRotation(-13.0F, -13.5F, -4.75F, 0.0F, 0.0F, -0.3491F));

		PartDefinition redlamp_e11 = glow_panel_e5.addOrReplaceChild("redlamp_e11", CubeListBuilder.create(), PartPose.offsetAndRotation(-13.0F, -13.5F, -4.75F, 0.0F, 0.0F, -0.3491F));

		PartDefinition skin5 = side_5.addOrReplaceChild("skin5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_skin5 = skin5.addOrReplaceChild("floor_skin5", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards5 = floor_skin5.addOrReplaceChild("floorboards5", CubeListBuilder.create().texOffs(5, 45).addBox(-12.5F, -0.1875F, -5.5F, 5.0F, 1.0F, 11.0F, new CubeDeformation(-0.02F))
		.texOffs(100, 10).addBox(-7.5F, -0.1875F, -4.5F, 3.0F, 1.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-6.3125F, 0.375F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 3.1416F, 0.0F, 0.0F));

		PartDefinition leg_skin5 = floor_skin5.addOrReplaceChild("leg_skin5", CubeListBuilder.create().texOffs(5, 45).addBox(0.0F, 0.25F, -3.0F, 3.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.75F, -0.75F, 0.0F, 0.0F, 0.0F, -1.2217F));

		PartDefinition knee_skin5 = leg_skin5.addOrReplaceChild("knee_skin5", CubeListBuilder.create().texOffs(100, 10).addBox(0.25F, -4.0F, -2.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(1.0F, -4.25F, -2.5F, 1.0F, 6.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-0.5F, 3.0F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -1.0F, 0.0F, 0.0F, 0.0F, 1.2217F));

		PartDefinition thigh_skin5 = knee_skin5.addOrReplaceChild("thigh_skin5", CubeListBuilder.create().texOffs(40, 80).addBox(0.0F, -3.0F, -3.0F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.25F, -3.5F, 0.0F, 0.0F, 0.0F, -0.6981F));

		PartDefinition belly_skin5 = thigh_skin5.addOrReplaceChild("belly_skin5", CubeListBuilder.create().texOffs(5, 45).addBox(0.45F, -9.225F, -7.65F, 1.0F, 5.0F, 15.0F, new CubeDeformation(0.0F))
		.texOffs(75, 15).addBox(-0.375F, -4.5F, -3.9F, 2.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, -2.5F, 0.0F, 0.0F, 0.0F, -0.8727F));

		PartDefinition rimtop_skin5 = belly_skin5.addOrReplaceChild("rimtop_skin5", CubeListBuilder.create(), PartPose.offsetAndRotation(1.125F, -8.5F, 0.0F, 0.0F, 0.0F, 2.9671F));

		PartDefinition panel_skin5 = rimtop_skin5.addOrReplaceChild("panel_skin5", CubeListBuilder.create().texOffs(5, 45).addBox(-2.5625F, 0.25F, -6.75F, 1.0F, 4.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 4.25F, -4.55F, 1.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 7.25F, -3.125F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.625F, -1.0F, 0.25F, 0.0F, 0.0F, 3.0718F));

		PartDefinition skelly5 = side_5.addOrReplaceChild("skelly5", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib5 = skelly5.addOrReplaceChild("rib5", CubeListBuilder.create().texOffs(75, 4).addBox(-0.25F, 0.0F, -0.425F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.7263F, -0.5692F, 0.125F, 17.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-17.0F, -12.25F, -0.5F, 0.0F, 0.0F, -0.2618F));

		PartDefinition rib_bolts_5 = rib5.addOrReplaceChild("rib_bolts_5", CubeListBuilder.create(), PartPose.offset(17.0F, 12.25F, 0.5F));

		PartDefinition foot5 = skelly5.addOrReplaceChild("foot5", CubeListBuilder.create().texOffs(75, 4).addBox(-12.0F, -1.25F, -0.75F, 8.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.125F, -2.35F, -1.825F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.5F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, 1.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, -1.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-4.0F, -9.75F, 0.125F, 2.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.75F, -11.5F, -0.5F, 10.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-16.9915F, -12.1853F, -0.425F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.7788F, -20.5F, -1.0543F, 1.0F, 5.0F, 3.0F, new CubeDeformation(-0.3F))
		.texOffs(75, 4).addBox(-17.15F, -12.5F, 0.125F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 5).addBox(-2.0605F, -19.725F, -1.0696F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(30, 5).addBox(-2.0605F, -18.975F, -1.0696F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offset(-2.0F, 0.0F, -0.5F));

		PartDefinition footslope5 = foot5.addOrReplaceChild("footslope5", CubeListBuilder.create(), PartPose.offset(2.0F, -0.5F, 0.375F));

		PartDefinition foot_r13 = footslope5.addOrReplaceChild("foot_r13", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -1.875F, -1.5F, 10.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.4961F, -0.6518F, 1.25F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r14 = footslope5.addOrReplaceChild("foot_r14", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -2.025F, -1.85F, 5.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.875F, -0.875F, 0.125F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r15 = footslope5.addOrReplaceChild("foot_r15", CubeListBuilder.create().texOffs(75, 4).addBox(-0.625F, -1.375F, -2.0F, 7.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(75, 4).addBox(-1.625F, -2.875F, -2.0F, 9.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.691F, -0.0451F, 0.275F, 0.0F, 0.0F, -0.2182F));

		PartDefinition leg5 = foot5.addOrReplaceChild("leg5", CubeListBuilder.create().texOffs(45, 75).addBox(-4.0F, -16.75F, 0.125F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.25F, -1.0F, 0.0F, 0.0F, 0.0F, 0.4363F));

		PartDefinition thigh5 = foot5.addOrReplaceChild("thigh5", CubeListBuilder.create().texOffs(40, 80).addBox(-2.0F, -5.75F, 0.12F, 2.0F, 7.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(40, 80).addBox(-1.5F, -5.75F, -0.38F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -8.0F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition front_rail5 = side_5.addOrReplaceChild("front_rail5", CubeListBuilder.create().texOffs(75, 4).addBox(2.625F, -11.7125F, 2.05F, 1.0F, 1.0F, 9.0F, new CubeDeformation(0.1F))
		.texOffs(75, 4).addBox(2.625F, -11.7125F, 11.25F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.1F)), PartPose.offsetAndRotation(-22.0F, 0.0F, 0.5F, 0.0F, 0.5236F, 0.0F));

		PartDefinition front_railbolt5 = front_rail5.addOrReplaceChild("front_railbolt5", CubeListBuilder.create().texOffs(50, 10).addBox(-19.7625F, -11.7F, 10.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 7.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 5.375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 2.975F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 13.225F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 15.825F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 18.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F)), PartPose.offset(22.0F, 0.0F, -0.5F));

		PartDefinition rail_topbevel5 = front_rail5.addOrReplaceChild("rail_topbevel5", CubeListBuilder.create().texOffs(75, 4).addBox(0.175F, 0.35F, -9.325F, 1.0F, 1.0F, 18.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -12.5F, 10.75F, 0.0F, 0.0F, 0.8727F));

		PartDefinition rail_underbevel5 = front_rail5.addOrReplaceChild("rail_underbevel5", CubeListBuilder.create().texOffs(75, 4).addBox(-0.5187F, -0.3875F, -8.55F, 1.0F, 1.0F, 17.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -10.5F, 10.75F, 0.0F, 0.0F, -0.9599F));

		PartDefinition rotor_gasket5 = side_5.addOrReplaceChild("rotor_gasket5", CubeListBuilder.create().texOffs(75, 4).addBox(-4.25F, -18.0F, -1.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-3.8856F, -20.75F, -1.0472F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(30, 5).addBox(-4.1585F, -19.95F, -1.0647F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 4).addBox(-4.8585F, -19.9F, -1.0647F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(75, 4).addBox(-4.75F, -17.25F, -1.4F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_trim5 = side_5.addOrReplaceChild("floor_trim5", CubeListBuilder.create().texOffs(75, 4).addBox(10.75F, -1.0F, -5.5F, 2.0F, 1.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -3.5F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 3.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -1.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 5.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-21.5F, -0.1875F, 11.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition side_6 = structure.addOrReplaceChild("side_6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition glow_panel_e6 = side_6.addOrReplaceChild("glow_panel_e6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition glow_glass_e16 = glow_panel_e6.addOrReplaceChild("glow_glass_e16", CubeListBuilder.create().texOffs(75, 4).addBox(-14.2F, -1.05F, -1.0F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.2F, -2.3F, -1.0F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition glow_glass_e17 = glow_panel_e6.addOrReplaceChild("glow_glass_e17", CubeListBuilder.create().texOffs(75, 4).addBox(-13.6F, -1.05F, 0.4F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-13.6F, -2.3F, 0.4F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition glow_glass_e18 = glow_panel_e6.addOrReplaceChild("glow_glass_e18", CubeListBuilder.create().texOffs(75, 4).addBox(-14.2F, -1.05F, 1.7F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.2F, -2.3F, 1.7F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -14.5F, -0.5F));

		PartDefinition redlamp_e12 = glow_panel_e6.addOrReplaceChild("redlamp_e12", CubeListBuilder.create(), PartPose.offsetAndRotation(-13.0F, -13.5F, -4.75F, 0.0F, 0.0F, -0.3491F));

		PartDefinition redlamp_e13 = glow_panel_e6.addOrReplaceChild("redlamp_e13", CubeListBuilder.create(), PartPose.offsetAndRotation(-13.0F, -13.5F, -4.75F, 0.0F, 0.0F, -0.3491F));

		PartDefinition skin6 = side_6.addOrReplaceChild("skin6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_skin6 = skin6.addOrReplaceChild("floor_skin6", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorboards6 = floor_skin6.addOrReplaceChild("floorboards6", CubeListBuilder.create().texOffs(5, 45).addBox(-12.5F, -0.1875F, -5.5F, 5.0F, 1.0F, 11.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-7.5F, -0.1875F, -4.5F, 3.0F, 1.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-6.3125F, 0.375F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 3.1416F, 0.0F, 0.0F));

		PartDefinition leg_skin6 = floor_skin6.addOrReplaceChild("leg_skin6", CubeListBuilder.create().texOffs(5, 45).addBox(0.0F, 0.25F, -3.0F, 3.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.75F, -0.75F, 0.0F, 0.0F, 0.0F, -1.2217F));

		PartDefinition knee_skin6 = leg_skin6.addOrReplaceChild("knee_skin6", CubeListBuilder.create().texOffs(100, 10).addBox(0.25F, -4.0F, -2.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(1.0F, -4.25F, -2.5F, 1.0F, 6.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(100, 10).addBox(-0.5F, 3.0F, -3.25F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -1.0F, 0.0F, 0.0F, 0.0F, 1.2217F));

		PartDefinition thigh_skin6 = knee_skin6.addOrReplaceChild("thigh_skin6", CubeListBuilder.create().texOffs(40, 80).addBox(0.0F, -3.0F, -3.0F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.25F, -3.5F, 0.0F, 0.0F, 0.0F, -0.6981F));

		PartDefinition belly_skin6 = thigh_skin6.addOrReplaceChild("belly_skin6", CubeListBuilder.create().texOffs(5, 45).addBox(0.45F, -9.225F, -7.65F, 1.0F, 5.0F, 15.0F, new CubeDeformation(0.0F))
		.texOffs(75, 15).addBox(-0.375F, -4.5F, -3.9F, 2.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, -2.5F, 0.0F, 0.0F, 0.0F, -0.8727F));

		PartDefinition rimtop_skin6 = belly_skin6.addOrReplaceChild("rimtop_skin6", CubeListBuilder.create(), PartPose.offsetAndRotation(1.125F, -8.5F, 0.0F, 0.0F, 0.0F, 2.9671F));

		PartDefinition panel_skin6 = rimtop_skin6.addOrReplaceChild("panel_skin6", CubeListBuilder.create().texOffs(5, 45).addBox(-2.5625F, 0.25F, -6.75F, 1.0F, 4.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 4.25F, -4.55F, 1.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(5, 45).addBox(-2.5625F, 7.25F, -3.125F, 1.0F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.625F, -1.0F, 0.25F, 0.0F, 0.0F, 3.0718F));

		PartDefinition skelly6 = side_6.addOrReplaceChild("skelly6", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib6 = skelly6.addOrReplaceChild("rib6", CubeListBuilder.create().texOffs(75, 4).addBox(-0.25F, 0.0F, -0.425F, 14.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.2263F, -0.5692F, 0.025F, 17.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-17.0F, -12.25F, -0.5F, 0.0F, 0.0F, -0.2618F));

		PartDefinition rib_bolts_6 = rib6.addOrReplaceChild("rib_bolts_6", CubeListBuilder.create(), PartPose.offset(17.0F, 12.25F, 0.5F));

		PartDefinition foot6 = skelly6.addOrReplaceChild("foot6", CubeListBuilder.create().texOffs(75, 4).addBox(-12.0F, -1.25F, -0.75F, 8.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.125F, -2.35F, -1.825F, 3.0F, 3.0F, 5.0F, new CubeDeformation(-0.5F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, 1.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-13.9125F, -1.5F, -1.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-4.0F, -9.75F, 0.125F, 2.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-14.75F, -11.5F, -0.5F, 10.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-16.9915F, -12.1853F, -0.425F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-1.8256F, -20.5F, -1.0268F, 1.0F, 5.0F, 3.0F, new CubeDeformation(-0.3F))
		.texOffs(75, 4).addBox(-17.15F, -12.5F, 0.025F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 5).addBox(-2.1164F, -19.725F, -1.0271F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(30, 5).addBox(-2.1164F, -18.975F, -1.0271F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offset(-2.0F, 0.0F, -0.5F));

		PartDefinition footslope6 = foot6.addOrReplaceChild("footslope6", CubeListBuilder.create(), PartPose.offset(2.0F, -0.5F, 0.375F));

		PartDefinition foot_r16 = footslope6.addOrReplaceChild("foot_r16", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -1.875F, -1.5F, 10.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.4961F, -0.6518F, 1.25F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r17 = footslope6.addOrReplaceChild("foot_r17", CubeListBuilder.create().texOffs(75, 4).addBox(-5.125F, -2.025F, -1.85F, 5.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.875F, -0.875F, 0.125F, 0.0F, 0.0F, -0.2182F));

		PartDefinition foot_r18 = footslope6.addOrReplaceChild("foot_r18", CubeListBuilder.create().texOffs(75, 4).addBox(-0.625F, -1.375F, -2.0F, 7.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(75, 4).addBox(-1.625F, -2.875F, -2.0F, 9.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-9.691F, -0.0451F, 0.275F, 0.0F, 0.0F, -0.2182F));

		PartDefinition leg6 = foot6.addOrReplaceChild("leg6", CubeListBuilder.create().texOffs(45, 75).addBox(-4.0F, -16.75F, 0.025F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.25F, -1.0F, 0.0F, 0.0F, 0.0F, 0.4363F));

		PartDefinition thigh6 = foot6.addOrReplaceChild("thigh6", CubeListBuilder.create().texOffs(40, 80).addBox(-2.0F, -5.75F, 0.12F, 2.0F, 7.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(40, 80).addBox(-1.5F, -5.75F, -0.38F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -8.0F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition front_rail6 = side_6.addOrReplaceChild("front_rail6", CubeListBuilder.create().texOffs(75, 4).addBox(2.625F, -11.7125F, 1.975F, 1.0F, 1.0F, 9.0F, new CubeDeformation(0.1F))
		.texOffs(75, 4).addBox(2.625F, -11.7125F, 11.175F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.1F)), PartPose.offsetAndRotation(-22.0F, 0.0F, 0.5F, 0.0F, 0.5236F, 0.0F));

		PartDefinition front_railbolt6 = front_rail6.addOrReplaceChild("front_railbolt6", CubeListBuilder.create().texOffs(50, 10).addBox(-19.7625F, -11.7F, 8.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 5.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 2.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 10.775F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 13.7F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 16.4F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F))
		.texOffs(50, 10).addBox(-19.7625F, -11.7F, 18.975F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.9F)), PartPose.offset(22.0F, 0.0F, -0.5F));

		PartDefinition rail_topbevel6 = front_rail6.addOrReplaceChild("rail_topbevel6", CubeListBuilder.create().texOffs(75, 4).addBox(0.175F, 0.35F, -9.325F, 1.0F, 1.0F, 18.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -12.5F, 10.75F, 0.0F, 0.0F, 0.8727F));

		PartDefinition rail_underbevel6 = front_rail6.addOrReplaceChild("rail_underbevel6", CubeListBuilder.create().texOffs(75, 4).addBox(-0.5187F, -0.3875F, -8.55F, 1.0F, 1.0F, 17.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -10.5F, 10.75F, 0.0F, 0.0F, -0.9599F));

		PartDefinition rotor_gasket6 = side_6.addOrReplaceChild("rotor_gasket6", CubeListBuilder.create().texOffs(75, 4).addBox(-4.25F, -18.0F, -1.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(-3.9399F, -20.75F, -1.0468F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(30, 5).addBox(-4.2281F, -19.95F, -1.0559F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(75, 4).addBox(-4.9281F, -19.95F, -1.0559F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(75, 4).addBox(-4.75F, -17.25F, -1.4F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition floor_trim6 = side_6.addOrReplaceChild("floor_trim6", CubeListBuilder.create().texOffs(75, 4).addBox(10.75F, -1.0F, -5.5F, 2.0F, 1.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -3.5F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 3.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, -1.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 4).addBox(11.0F, -0.3125F, 5.25F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-21.5F, -0.1875F, 11.0F, 0.0F, 0.5236F, 0.0F));

		PartDefinition side_bits = partdefinition.addOrReplaceChild("side_bits", CubeListBuilder.create(), PartPose.offset(0.0F, 25.375F, 0.0F));

		PartDefinition gear_a = side_bits.addOrReplaceChild("gear_a", CubeListBuilder.create().texOffs(75, 4).addBox(-4.2974F, 14.6F, 2.1061F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(17, 110).addBox(-3.2974F, 19.1F, -0.2439F, 2.0F, 2.0F, 6.0F, new CubeDeformation(-0.55F))
		.texOffs(3, 38).addBox(-3.8224F, 19.65F, -0.4439F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 38).addBox(-3.8224F, 18.55F, -0.4439F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-4.2974F, 20.6F, 2.1061F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-3.0F, -25.95F, -1.675F, 0.0F, 0.5236F, 0.0F));

		PartDefinition ga_rotate_z = gear_a.addOrReplaceChild("ga_rotate_z", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -0.9687F, -1.0031F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(19, 109).addBox(-1.0F, -0.4688F, -1.0031F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(40, 70).addBox(-1.5F, -0.9688F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F))
		.texOffs(50, 22).addBox(-1.0F, -2.2188F, -0.9781F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(40, 70).addBox(-1.5F, 0.0312F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F)), PartPose.offset(-3.2974F, 20.0687F, 3.0842F));

		PartDefinition foot_r19 = ga_rotate_z.addOrReplaceChild("foot_r19", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -0.7854F, 0.0F));

		PartDefinition foot_r20 = ga_rotate_z.addOrReplaceChild("foot_r20", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -1.5708F, 0.0F));

		PartDefinition foot_r21 = ga_rotate_z.addOrReplaceChild("foot_r21", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, 0.7854F, 0.0F));

		PartDefinition gear_b = side_bits.addOrReplaceChild("gear_b", CubeListBuilder.create().texOffs(75, 4).addBox(-7.2974F, -11.35F, 0.4311F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(17, 110).addBox(-6.2974F, -6.85F, -1.9189F, 2.0F, 2.0F, 6.0F, new CubeDeformation(-0.55F))
		.texOffs(3, 38).addBox(-6.8224F, -6.3F, -2.1189F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 38).addBox(-6.8224F, -7.4F, -2.1189F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-7.2974F, -5.35F, 0.4311F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(1.25F, 0.0F, 1.25F, 0.0F, -1.5708F, 0.0F));

		PartDefinition gb_rotate_y = gear_b.addOrReplaceChild("gb_rotate_y", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -0.9687F, -1.0031F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(19, 109).addBox(-1.0F, -0.4688F, -1.0031F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(40, 75).addBox(-1.5F, -0.9688F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F))
		.texOffs(51, 26).addBox(-1.0F, -0.3688F, -0.9781F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(51, 26).addBox(-1.0F, -1.5687F, -0.9781F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(40, 75).addBox(-1.5F, 0.0312F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F)), PartPose.offset(-6.2974F, -5.8813F, 1.4092F));

		PartDefinition foot_r22 = gb_rotate_y.addOrReplaceChild("foot_r22", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -0.7854F, 0.0F));

		PartDefinition foot_r23 = gb_rotate_y.addOrReplaceChild("foot_r23", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -1.5708F, 0.0F));

		PartDefinition foot_r24 = gb_rotate_y.addOrReplaceChild("foot_r24", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, 0.7854F, 0.0F));

		PartDefinition gear_c = side_bits.addOrReplaceChild("gear_c", CubeListBuilder.create().texOffs(75, 4).addBox(-7.2974F, -11.35F, 0.4311F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(17, 110).addBox(-6.2974F, -6.85F, -1.6689F, 2.0F, 2.0F, 6.0F, new CubeDeformation(-0.55F))
		.texOffs(3, 38).addBox(-6.8224F, -6.3F, -2.1189F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 38).addBox(-6.8224F, -7.4F, -2.1189F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-7.2974F, -5.35F, 0.4311F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-1.775F, 0.0F, 0.525F, 0.0F, 2.618F, 0.0F));

		PartDefinition gc_rotate_z = gear_c.addOrReplaceChild("gc_rotate_z", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -0.9687F, -1.0031F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(19, 109).addBox(-1.0F, -0.4688F, -1.0031F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(35, 70).addBox(-1.5F, -0.9688F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F))
		.texOffs(50, 22).addBox(-1.0F, -2.2188F, -0.9781F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(35, 70).addBox(-1.5F, 0.0312F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F)), PartPose.offset(-6.2974F, -5.8813F, 1.4092F));

		PartDefinition foot_r25 = gc_rotate_z.addOrReplaceChild("foot_r25", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -0.7854F, 0.0F));

		PartDefinition foot_r26 = gc_rotate_z.addOrReplaceChild("foot_r26", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -1.5708F, 0.0F));

		PartDefinition foot_r27 = gc_rotate_z.addOrReplaceChild("foot_r27", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, 0.7854F, 0.0F));

		PartDefinition gear_d = side_bits.addOrReplaceChild("gear_d", CubeListBuilder.create().texOffs(75, 4).addBox(-7.2974F, -10.35F, 0.4311F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(17, 110).addBox(-6.2974F, -6.85F, -1.6689F, 2.0F, 2.0F, 6.0F, new CubeDeformation(-0.55F))
		.texOffs(3, 38).addBox(-6.8224F, -6.3F, -2.1189F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 38).addBox(-6.8224F, -7.4F, -2.1189F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-7.2974F, -5.35F, 0.4311F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-0.375F, -1.0F, 1.625F, 0.0F, -2.618F, 0.0F));

		PartDefinition gd_rotate_z = gear_d.addOrReplaceChild("gd_rotate_z", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -0.9687F, -1.0031F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(19, 109).addBox(-1.0F, -0.4688F, -1.0031F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(35, 70).addBox(-1.5F, -0.9688F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F))
		.texOffs(50, 22).addBox(-1.0F, -2.2188F, -0.9781F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(35, 70).addBox(-1.5F, 0.0312F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F)), PartPose.offset(-6.2974F, -5.8813F, 1.4092F));

		PartDefinition foot_r28 = gd_rotate_z.addOrReplaceChild("foot_r28", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -0.7854F, 0.0F));

		PartDefinition foot_r29 = gd_rotate_z.addOrReplaceChild("foot_r29", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -1.5708F, 0.0F));

		PartDefinition foot_r30 = gd_rotate_z.addOrReplaceChild("foot_r30", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, 0.7854F, 0.0F));

		PartDefinition gear_e = side_bits.addOrReplaceChild("gear_e", CubeListBuilder.create().texOffs(75, 4).addBox(-7.2974F, -10.35F, 0.4311F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(17, 110).addBox(-6.2974F, -6.85F, -1.6689F, 2.0F, 2.0F, 6.0F, new CubeDeformation(-0.55F))
		.texOffs(3, 38).addBox(-6.8224F, -6.3F, -2.1189F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 38).addBox(-6.8224F, -7.4F, -2.1189F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-7.2974F, -5.35F, 0.4311F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-1.275F, -1.0F, -1.25F, 0.0F, 1.5708F, 0.0F));

		PartDefinition ge_rotate_z = gear_e.addOrReplaceChild("ge_rotate_z", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -0.9687F, -1.0031F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(19, 109).addBox(-1.0F, -0.4688F, -1.0031F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(35, 70).addBox(-1.5F, -0.9688F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F))
		.texOffs(50, 22).addBox(-1.0F, -2.2188F, -0.9781F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(35, 70).addBox(-1.5F, 0.0312F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F)), PartPose.offset(-6.2974F, -5.8813F, 1.4092F));

		PartDefinition foot_r31 = ge_rotate_z.addOrReplaceChild("foot_r31", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -0.7854F, 0.0F));

		PartDefinition foot_r32 = ge_rotate_z.addOrReplaceChild("foot_r32", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -1.5708F, 0.0F));

		PartDefinition foot_r33 = ge_rotate_z.addOrReplaceChild("foot_r33", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, 0.7854F, 0.0F));

		PartDefinition gear_f = side_bits.addOrReplaceChild("gear_f", CubeListBuilder.create().texOffs(75, 4).addBox(-7.2974F, -10.35F, 0.4311F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(17, 110).addBox(-6.2974F, -6.85F, -1.6689F, 2.0F, 2.0F, 6.0F, new CubeDeformation(-0.55F))
		.texOffs(3, 38).addBox(-6.8224F, -6.3F, -2.1189F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 38).addBox(-6.8224F, -7.4F, -2.1189F, 3.0F, 2.0F, 7.0F, new CubeDeformation(-0.9F))
		.texOffs(75, 4).addBox(-7.2974F, -5.35F, 0.4311F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(1.625F, -1.0F, -0.525F, 0.0F, -0.5236F, 0.0F));

		PartDefinition gf_rotate_z = gear_f.addOrReplaceChild("gf_rotate_z", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -0.9687F, -1.0031F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(19, 109).addBox(-1.0F, -0.4688F, -1.0031F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(35, 70).addBox(-1.5F, -0.9688F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F))
		.texOffs(50, 22).addBox(-1.0F, -2.2188F, -0.9781F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(35, 70).addBox(-1.5F, 0.0312F, -1.5031F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.4F)), PartPose.offset(-6.2974F, -5.8813F, 1.4092F));

		PartDefinition foot_r34 = gf_rotate_z.addOrReplaceChild("foot_r34", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -0.7854F, 0.0F));

		PartDefinition foot_r35 = gf_rotate_z.addOrReplaceChild("foot_r35", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, -1.5708F, 0.0F));

		PartDefinition foot_r36 = gf_rotate_z.addOrReplaceChild("foot_r36", CubeListBuilder.create().texOffs(45, 27).addBox(-2.5F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0313F, -0.0031F, 0.0F, 0.7854F, 0.0F));

		PartDefinition bb_main = partdefinition.addOrReplaceChild("bb_main", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		glow.render(poseStack, vertexConsumer, LightTexture.FULL_BRIGHT, packedOverlay, red, green, blue, alpha);
		rotor.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		controls.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		structure.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		side_bits.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		bb_main.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public ModelPart root() {
		return this.root;
	}

	@Override
	public void setupAnim(Entity pEntity, float pLimbSwing, float pLimbSwingAmount, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch) {}

	@Override
	public void setupAnimations(ConsoleTile tile, float ageInTicks) {
		this.root.getAllParts().forEach(ModelPart::resetPose);
		this.animate(tile.rotorAnimationState, SteamConsoleAnimation.MODEL_STEAMROTOR, ageInTicks);
		if(Minecraft.getInstance().level != null){
			Minecraft.getInstance().level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
				//Control Animations
				this.animate(ClientHelper.getControlState(tardis, ControlRegistry.TELEPATHICS.get()), SteamConsoleAnimation.TELEPATHICS, ageInTicks);
				this.animate(ClientHelper.getControlState(tardis, ControlRegistry.RANDOMIZER.get()), SteamConsoleAnimation.RANDOMIZER, ageInTicks);
				this.animate(ClientHelper.getControlState(tardis, ControlRegistry.FAST_RETURN.get()), SteamConsoleAnimation.FAST_RETURN, ageInTicks);
				this.animate(ClientHelper.getControlState(tardis, ControlRegistry.COMMUNICATOR.get()), SteamConsoleAnimation.RADIO, ageInTicks);

				SteamConsoleAnimation.animateThrottle(tardis, this);
				SteamConsoleAnimation.animateHandbrake(tardis, this);
			});
		}
	}
}