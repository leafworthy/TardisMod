package net.tardis.mod.ars;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;

import java.util.Optional;

public class ARSRoom {

    public static final Codec<ARSRoom> CODEC = RecordCodecBuilder.create(
            instance -> {
                return instance.group(
                        ResourceLocation.CODEC.fieldOf("roomLocation").forGetter(ARSRoom::getRoomLocation),
                        BlockPos.CODEC.fieldOf("offset").forGetter(ARSRoom::getOffset),
                        Codec.BOOL.fieldOf("canBeRemoved").forGetter(ARSRoom::canBeRemoved),
                        ResourceLocation.CODEC.optionalFieldOf("parent").forGetter(ARSRoom::getParent)
                ).apply(instance, ARSRoom::new);
            });

    private final ResourceLocation roomLocation;
    private final boolean canBeRemoved;
    private final BlockPos offset;
    private final Optional<ResourceLocation> parent;

    public ARSRoom(ResourceLocation roomLocation, BlockPos offset, boolean canBeRemoved, Optional<ResourceLocation> parent) {
        this.roomLocation = roomLocation;
        this.offset = offset;
        this.canBeRemoved = canBeRemoved;
        this.parent = parent;
    }

    public ARSRoom(ResourceLocation roomLocation, BlockPos offset){
        this(roomLocation, offset, true, Optional.empty());
    }

    public void spawnRoom(ServerLevel level, BlockPos pos, StructureTemplateManager manager, StructurePlaceSettings settings){
        manager.get(this.roomLocation).ifPresent(room -> {
            room.placeInWorld(level, pos, BlockPos.ZERO, settings, level.random, 2);
        });
    }

    public ResourceLocation getRoomLocation(){
        return this.roomLocation;
    }

    public boolean canBeRemoved(){
        return this.canBeRemoved;
    }

    public BlockPos getOffset(){
        return this.offset;
    }

    public Optional<ResourceLocation> getParent(){
        return this.parent;
    }

}
