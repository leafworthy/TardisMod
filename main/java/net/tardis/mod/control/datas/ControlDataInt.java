package net.tardis.mod.control.datas;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;

public class ControlDataInt extends ControlData<Integer>{

    public ControlDataInt(ControlType type, ITardisLevel tardis) {
        super(type, tardis, 0);
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeInt(this.get());
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.set(buf.readInt());
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        this.set(tag.getInt("value"));
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.set(tag.getInt("value"));
    }
}
