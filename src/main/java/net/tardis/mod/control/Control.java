package net.tardis.mod.control;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.TypeHolder;

/**
 * @author Lilith
 * The basic template of a Tardis Control
 * @implNote Extend this for custom controls
 *
 */
public abstract class Control extends TypeHolder<ControlType> {

    public Control(ControlType type){
        super(type);
    }

    public abstract InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level);


}
