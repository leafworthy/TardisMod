package net.tardis.mod.client.gui.diagnostic_tool;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.ChatFormatting;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;

public class DiagnosticToolBaseScreen extends Screen {

    public DiagnosticToolBaseScreen() {
        super(Component.translatable("gui.diagnostic_tool.title"));
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);

        drawCenteredString(matrixStack, font, "This item is still in development, I'm just here so people can start incorperating it into their systems :D", width/2, height/2, ChatFormatting.WHITE.getColor());
    }
}
