package net.tardis.mod.item.components;

import net.minecraft.world.item.Item;
import net.tardis.mod.creative_tabs.Tabs;

public class TardisComponentItem extends Item {

    public TardisComponentItem(Properties p_41383_) {
        super(p_41383_.tab(Tabs.MAIN).stacksTo(1));
    }
}
