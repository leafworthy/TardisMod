package net.tardis.mod.network;

import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.network.simple.SimpleChannel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.packets.*;

public class Network {

    public static final String NET_VERSION = "1.0";
    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(Helper.createRL("main"), () -> NET_VERSION, NET_VERSION::equals, NET_VERSION::equals);

    public static int ID = 0;

    public static void registerPackets(){
        INSTANCE.registerMessage(id(), SyncDimensionListMessage.class, SyncDimensionListMessage::encode, SyncDimensionListMessage::decode, SyncDimensionListMessage::handle);
        INSTANCE.registerMessage(id(), ControlDataMessage.class, ControlDataMessage::encode, ControlDataMessage::decode, ControlDataMessage::handle);
        INSTANCE.registerMessage(id(), ControlEntitySizeMessage.class, ControlEntitySizeMessage::encode, ControlEntitySizeMessage::decode, ControlEntitySizeMessage::handle);
        INSTANCE.registerMessage(id(), TardisFlightStateMessage.class, TardisFlightStateMessage::encode, TardisFlightStateMessage::decode, TardisFlightStateMessage::handle);
        INSTANCE.registerMessage(id(), UpdateDoorStateMessage.class, UpdateDoorStateMessage::encode, UpdateDoorStateMessage::decode, UpdateDoorStateMessage::handle);
    }

    public static void sendPacketToAll(Object message){
        Network.INSTANCE.send(PacketDistributor.ALL.noArg(), message);
    }

    public static void sendPacketToDimension(ResourceKey<Level> level, Object mes){
        INSTANCE.send(PacketDistributor.DIMENSION.with(() -> level), mes);
    }

    public static void sendToTracking(Entity e, Object mes) {
        INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> e), mes);
    }

    public static void sendToTracking(BlockEntity tile, Object mes){
        INSTANCE.send(PacketDistributor.TRACKING_CHUNK.with(() -> tile.getLevel().getChunkAt(tile.getBlockPos())), mes);
    }

    public static int id(){
        return ++ID;
    }


}
