package net.tardis.mod.misc;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.misc.enums.DoorState;

import java.util.function.Predicate;

public class DoorHandler implements INBTSerializable<CompoundTag> {

    public final boolean canLock;
    public DoorState[] validDoorStates = DoorState.values();
    public Predicate<ItemStack> isKey = stack -> false;
    public Runnable packetSender;
    public Runnable linkedDoor;

    public int doorIndex = 0;
    public boolean isLocked = true;

    public DoorHandler(boolean canLock, DoorState... doorStates){
        this.canLock = canLock;
        this.validDoorStates = doorStates;
    }

    public DoorHandler(Predicate<ItemStack> predicate, DoorState... states){
        this(true, states);
        this.setLockPredicate(predicate);
    }

    public boolean isKey(ItemStack stack){
        return this.isKey.test(stack);
    }

    public void setLockPredicate(Predicate<ItemStack> test){
        this.isKey = test;
    }

    public void setValidDoorStates(DoorState... states){
        this.validDoorStates = states;
    }

    public void setPacketSender(Runnable packetSender){
        this.packetSender = packetSender;
    }

    public DoorState getDoorState(){
        return this.validDoorStates == null || this.validDoorStates.length <= 0 ? DoorState.CLOSED : this.validDoorStates[this.doorIndex];
    }

    public boolean isLocked(){
        return this.canLock && this.isLocked;
    }

    public void setLocked(boolean locked){
        this.isLocked = locked;
    }

    public void updateLinkedDoor(){
        if(this.linkedDoor != null)
            this.linkedDoor.run();
    }

    /**
     * Packet use only. Does not trigger a sync or account for if the state is invalid
     * @param state
     * @return
     */
    public void setDoorState(DoorState[] validStates, DoorState state){
        this.validDoorStates = validStates;
        for(int i = 0; i < this.validDoorStates.length; ++i){
            if(this.validDoorStates[i] == state){
                this.doorIndex = i;
                return;
            }
        }
    }

    public void updateClients(){
        if(this.packetSender != null){
            this.packetSender.run();
        }
    }

    public DoorInteractResult onInteract(Player player, ItemStack stack, InteractionHand hand){

        System.out.println("Door handler called with state %s, locked: %s".formatted(this.getDoorState(), this.isLocked));
        //If this door has a lock, test the locked condition
        if(canLock){
            //If we're holding the key, cycle the lock
            if(this.isKey(stack)){
                this.isLocked = !this.isLocked;
                System.out.println("Cycled lock! " + this.isLocked);
                this.updateLinkedDoor();
                this.updateClients();
                return DoorInteractResult.LOCK_CYCLE;
            }
            //If we're not holding the key, this can lock, and is locked, fail
            if(this.isLocked)
                return DoorInteractResult.FAILED;
        }

        if(hand != InteractionHand.MAIN_HAND)
            return DoorInteractResult.FAILED;

        //cycle door index
        if(this.doorIndex + 1 >= this.validDoorStates.length){
            this.doorIndex = 0;
        }
        else ++this.doorIndex;
        System.out.println("Cycled door state! " + this.getDoorState());
        this.updateLinkedDoor();
        this.updateClients();
        return DoorInteractResult.DOOR_CYCLE;

    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("door_index", this.doorIndex);
        tag.putBoolean("locked", this.isLocked);

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.doorIndex = tag.getInt("door_index");
        this.isLocked = tag.getBoolean("locked");
    }

    public void update(DoorHandler other) {
        this.doorIndex = other.doorIndex;
        this.isLocked = other.isLocked;
        this.updateClients();
    }


    public enum DoorInteractResult{
        FAILED(false),
        DOOR_CYCLE(true),
        LOCK_CYCLE(true);

        public final boolean success;

        DoorInteractResult(boolean success){
            this.success = success;
        }

    }

}
