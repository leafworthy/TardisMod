package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.exterior.ExteriorType;

import java.util.HashMap;

public class ChameleonInteriorDoorRenderer implements BlockEntityRenderer<InteriorDoorTile> {

    //Register these in FMLClientSetup
    protected static final HashMap<ExteriorType, InteriorDoorRender> RENDERS = new HashMap<>();

    public ChameleonInteriorDoorRenderer(BlockEntityRendererProvider.Context set){

        //Bake all registered doors
        for(InteriorDoorRender<?> renderer : RENDERS.values()){
            renderer.bake(set.getModelSet());
        }
    }


    @Override
    public void render(InteriorDoorTile door, float pPartialTick, PoseStack pose, MultiBufferSource source, int pPackedLight, int pPackedOverlay) {
        pose.pushPose();
        pose.translate(0.5, 1.5, 0.5);
        pose.mulPose(Vector3f.ZP.rotationDegrees(180));
        if(door.getLevel() != null){
            door.getLevel().getCapability(Capabilities.TARDIS).ifPresent(cap -> {
                InteriorDoorRender<?> renderer = RENDERS.get(cap.getExterior().getType());
                if(renderer != null){
                    renderer.preRender(cap, door, pose);
                    renderer.render(cap, door, pose, source, pPackedLight, pPackedOverlay);
                }
            });
        }
        pose.popPose();
    }

    public static void registerChameleonDoor(ExteriorType type, InteriorDoorRender renderer){
        RENDERS.put(type, renderer);
    }

}
