package net.tardis.mod.flight_event;

import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlRegistry;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.control.datas.ControlDataFloat;

public class TimeWindsEvent extends FlightEvent{

    private float oldThrottleValue;

    public TimeWindsEvent(ITardisLevel level) {
        super(level);
    }

    @Override
    public boolean onControlUse(ControlData<?> control) {

        if(control.getType() == ControlRegistry.THROTTLE.get() && control instanceof ControlDataFloat f){

            if(this.oldThrottleValue > f.get()){
                this.complete(); //If the user has decreased throttle, do not kill them
            }
        }

        return false; //We're returning false because we want it to actually effect the throttle like normal too
    }

    @Override
    public void onStart() {
        this.oldThrottleValue = (float)this.tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).get();
    }
}
