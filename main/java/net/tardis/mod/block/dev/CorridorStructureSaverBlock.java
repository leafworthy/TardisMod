package net.tardis.mod.block.dev;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.tardis.mod.corridor.CorridorGenerator;

public class CorridorStructureSaverBlock extends Block {
    public CorridorStructureSaverBlock() {
        super(Properties.of(Material.METAL)
                .sound(SoundType.ANVIL));
    }

    @Override
    public InteractionResult use(BlockState p_60503_, Level level, BlockPos blockPos, Player player, InteractionHand p_60507_, BlockHitResult p_60508_) {
        if (!level.isClientSide()) {

            if (player.isCrouching()) {
                player.displayClientMessage(Component.translatable("Attempting to generate structure."), false);
                CorridorGenerator.generateFromPosition(level, blockPos, blockPos);
            } else {

                String name = "gs_r" + level.getRandom().nextInt(100);
                StructureTemplateManager manager = level.getServer().getStructureManager();
                StructureTemplate template = manager.getOrCreate(new ResourceLocation(name));
                template.fillFromWorld(level, blockPos.above(), new BlockPos(48, 48, 48), false, null);
                template.setAuthor("");
                manager.save(new ResourceLocation(name));

                player.displayClientMessage(Component.translatable("Generated structure at: " + name), false);
            }


        }

        return super.use(p_60503_, level, blockPos, player, p_60507_, p_60508_);
    }
}
