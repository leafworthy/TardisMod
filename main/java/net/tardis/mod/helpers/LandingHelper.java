package net.tardis.mod.helpers;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.tardis.mod.misc.enums.LandingType;
import net.tardis.mod.misc.landing.LandingContext;

public class LandingHelper {

    public static BlockPos findValidLandPos(Level level, BlockPos target, LandingContext context, LandingType type){
        if(context.canLandAt(level, target))
            return target; // Can land exactly where we want -- Perfect
        //If we can't get the exact Target, go hunting

        //Look up and down, depending on preference
        BlockPos result = type == LandingType.UP ? getHigherSpot(level, target, context) : getLowerSpot(level, target, context);
        //If this is not ZERO, then it found a place to land
        if(!BlockPos.ZERO.equals(result)){
            return result;
        }
        //If it did not, reverse the logic
        return type == LandingType.UP ? getLowerSpot(level, target, context) : getHigherSpot(level, target, context);

    }

    public static BlockPos getLowerSpot(Level level, BlockPos start, LandingContext context){
        for(int y = start.getY(); y > level.dimensionType().minY(); --y){
            BlockPos pos = new BlockPos(start.getX(), y, start.getZ());
            if(context.canLandAt(level, pos))
                return pos;
        }
        return BlockPos.ZERO; //Send this if no viable lower spot is found
    }

    public static BlockPos getHigherSpot(Level level, BlockPos start, LandingContext context){
        for(int y = start.getY(); y < level.dimensionType().height(); ++y){
            BlockPos pos = new BlockPos(start.getX(), y, start.getZ());
            if(context.canLandAt(level, pos))
                return pos;
        }
        return BlockPos.ZERO; //Send this if no viable higher spot is found
    }

}
