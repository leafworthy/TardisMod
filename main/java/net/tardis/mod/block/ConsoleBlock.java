package net.tardis.mod.block;

import java.util.List;
import java.util.function.Supplier;

import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.helpers.WorldHelper;
import org.jetbrains.annotations.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.control.ControlPositionData;
import net.tardis.mod.control.ControlPositionDataRegistry;
import net.tardis.mod.entity.ControlEntity;

public class ConsoleBlock extends BaseEntityBlock {

    public static final VoxelShape SHAPE = Shapes.create(0.25, 0, 0.25, 0.75, 0, 0.75);

    private final Supplier<BlockEntityType<? extends ConsoleTile>> consoleTileFactory;

    public ConsoleBlock(Supplier<BlockEntityType<? extends ConsoleTile>> consoleTileFactory) {
        super(Properties.of(Material.BARRIER)
                .strength(9999, 9999)
                .noOcclusion());
        this.consoleTileFactory = consoleTileFactory;

    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pPlayer.getUsedItemHand() != pHand){
            return InteractionResult.PASS;
        }
        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Override
    public void onPlace(BlockState state, Level level, BlockPos pos, BlockState newState, boolean p_60570_) {
        super.onPlace(state, level, pos, newState, p_60570_);

        if(!level.isClientSide()){
            if(level.getBlockEntity(pos) instanceof ConsoleTile console){
                this.clearPreexistingControls(console, level, pos);
                
                //Add new consoles
                List<ControlPositionData> positionData = this.getControlPositionData(console);
                if(positionData != null){
                    this.placeControls(positionData, console, level, pos);
                }
            }
        }
    }

    public List<ControlPositionData> getControlPositionData(ConsoleTile console){
        return ControlPositionDataRegistry.POSITION_DATAS.get(ForgeRegistries.BLOCK_ENTITY_TYPES.getKey(console.getType()));
    }

    public void clearPreexistingControls(@Nullable ConsoleTile console, Level level, BlockPos pos){
        //Remove left over control entities in case of fuckery
        for(ControlEntity control : level.getEntitiesOfClass(ControlEntity.class, new AABB(pos).inflate(2))){
            control.kill();
        }

        if(console != null)
            console.killAllControls();
    }

    public void placeControls(List<ControlPositionData> positionData, ConsoleTile console, Level level, BlockPos pos){
        for(ControlPositionData data : positionData){
            ControlEntity control = new ControlEntity(level, data, console);
            Vec3 offset = data.getOffset();
            control.setPos(WorldHelper.centerOfBlockPos(pos, false).add(offset.x, offset.y, offset.z));
            level.addFreshEntity(control);
            console.addControl(control);
        }
    }

    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pIsMoving) {
        if(!(pNewState.getBlock() instanceof ConsoleBlock) && !pLevel.isClientSide){
            //If the new block is not a console block, we're getting removed

            //If there is a pre-existing tile, tell it to kill it's controls
            if(pLevel.getBlockEntity(pPos) instanceof ConsoleTile console){
                console.killAllControls();
            }
        }
        super.onRemove(pState, pLevel, pPos, pNewState, pIsMoving);
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state){
        return this.consoleTileFactory.get().create(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> otherType) {
        return createTickerHelper(otherType, this.consoleTileFactory.get(), ConsoleTile::tick);
    }

    @Override
    public VoxelShape getInteractionShape(BlockState pState, BlockGetter pLevel, BlockPos pPos) {
        return SHAPE;
    }
}
