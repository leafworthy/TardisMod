package net.tardis.mod;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.NewRegistryEvent;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.ars.ARSRoom;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.sound.SoundSchemes;

import java.util.function.Supplier;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class JsonRegistries {

    public static Supplier<IForgeRegistry<SoundSchemes>> SOUND_SCHEMES_REGISTRY;
    public static Supplier<IForgeRegistry<ARSRoom>> ARS_ROOM_REGISTRY;

    @SubscribeEvent
    public static void register(NewRegistryEvent event){

        SOUND_SCHEMES_REGISTRY = event.create(new RegistryBuilder<SoundSchemes>()
                .dataPackRegistry(SoundSchemes.CODEC)
                .setName(Helper.createRL("sound_schemes")));

        ARS_ROOM_REGISTRY = event.create(new RegistryBuilder<ARSRoom>()
                .setName(Helper.createRL("ars/rooms"))
                .dataPackRegistry(ARSRoom.CODEC));
    }

}
