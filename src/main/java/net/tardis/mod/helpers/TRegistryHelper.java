package net.tardis.mod.helpers;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.control.ControlRegistry;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.ExteriorType;

public class TRegistryHelper {
    
    public static ResourceLocation getKey(Block obj) {
        return ForgeRegistries.BLOCKS.getKey(obj);
    }
    
    public static ResourceLocation getKey(Item obj) {
        return ForgeRegistries.ITEMS.getKey(obj);
    }
    
    public static ResourceLocation getKey(BlockEntityType<?> obj) {
        return ForgeRegistries.BLOCK_ENTITY_TYPES.getKey(obj);
    }
    
    public static ResourceLocation getKey(EntityType<?> obj) {
        return ForgeRegistries.ENTITY_TYPES.getKey(obj);
    }
    
    public static ResourceLocation getKey(SoundEvent obj) {
        return ForgeRegistries.SOUND_EVENTS.getKey(obj);
    }
    
    //Custom Registries
    public static ResourceLocation getKey(ControlType obj) {
        return ControlRegistry.REGISTRY.get().getKey(obj);
    }

    public static ResourceLocation getKey(ExteriorType obj) {
        return ExteriorRegistry.REGISTRY.get().getKey(obj);
    }
}
