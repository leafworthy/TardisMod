package net.tardis.mod.upgrade;

import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.misc.TypeHolder;

public abstract class Upgrade<T> extends TypeHolder<UpgradeType> implements INBTSerializable<CompoundTag> {

    private int health;

    public Upgrade(UpgradeType type) {
        super(type);
    }


    public void damage(int damage){
        this.health -= damage;
        if(this.health <= 0){
            this.health = 0;
            onBreak();
        }
    }

    public abstract void onBreak();
    public abstract void onTick();


    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("health", this.health);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.health = tag.getInt("health");
    }
}
