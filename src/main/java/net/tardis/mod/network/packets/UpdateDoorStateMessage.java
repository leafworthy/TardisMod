package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.misc.enums.DoorState;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public class UpdateDoorStateMessage {

    public final boolean locked;
    public final DoorState state;
    public final BlockPos pos;
    public final DoorState[] validStates;

    public UpdateDoorStateMessage(BlockPos pos, DoorState state, boolean locked, DoorState[] validStates){
        this.pos = pos;
        this.state = state;
        this.locked = locked;
        this.validStates = validStates;
    }

    public static void encode(UpdateDoorStateMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos);
        buf.writeEnum(mes.state);
        buf.writeBoolean(mes.locked);

        int size = mes.validStates.length;
        buf.writeInt(size);
        for(int i = 0; i < size; ++i){
            buf.writeEnum(mes.validStates[i]);
        }

    }

    public static UpdateDoorStateMessage decode(FriendlyByteBuf buf){

        BlockPos pos = buf.readBlockPos();
        DoorState state = buf.readEnum(DoorState.class);
        boolean locked = buf.readBoolean();

        int size = buf.readInt();
        DoorState[] valid = new DoorState[size];
        for(int i = 0; i < size; ++i){
            valid[i] = buf.readEnum(DoorState.class);
        }

        return new UpdateDoorStateMessage(pos, state, locked, valid);
    }

    public static void handle(UpdateDoorStateMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleUpdateDoorState(mes);
        });
        context.get().setPacketHandled(true);
    }

}
