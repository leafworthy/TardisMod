package net.tardis.mod.client.monitor;

import com.sun.jdi.request.MonitorWaitedRequest;
import net.minecraft.network.chat.Component;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.SpaceTimeCoord;

import java.util.List;

public class LocationalMonitorEntry extends MonitorEntry {

    public static final String LOCATION_KEY = "monitor." + Tardis.MODID + ".location";
    public static final String LOCATION_DIM_KEY = "monitor." + Tardis.MODID + ".location_dim";

    @Override
    public List<Component> getText(ITardisLevel tardis) {
        SpaceTimeCoord coord = tardis.getLocation();
        return List.of(Component.translatable(LOCATION_KEY, coord.getPos().getX(), coord.getPos().getY(), coord.getPos().getZ()),
                Component.translatable(LOCATION_DIM_KEY, coord.getLevel().location().getPath()),
                Component.literal("pneumonoultramicroscopicsilicovolcanoconiosis"));
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        return true;
    }
}
