package net.tardis.mod.control;

import net.minecraft.core.Direction;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.control.datas.*;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.enums.LandingType;

import java.util.function.Supplier;

public class ControlRegistry {

    public static final DeferredRegister<ControlType> TYPES = DeferredRegister.create(Helper.createRL("control"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<ControlType>> REGISTRY = TYPES.makeRegistry(RegistryBuilder::new);

    public static final RegistryObject<ControlType> THROTTLE = TYPES.register("throttle", () -> new ControlType(ThrottleControl::new, ControlDataFloat::new));
    public static final RegistryObject<ControlType> HANDBRAKE = TYPES.register("handbrake", () -> new ControlType(ControlHandbrake::new, ControlDataBool::new));
    public static final RegistryObject<ControlType> REFUELER = TYPES.register("refueler", () -> new ControlType(RefuelerControl::new, ControlDataBool::new));
    public static final RegistryObject<ControlType> STABILIZERS = TYPES.register("stabilizers", () -> new ControlType(StabilizerControl::new, ControlDataBool::new));
    public static final RegistryObject<ControlType> INCREMENT = TYPES.register("increment", () -> new ControlType(IncrementControl::new, ControlDataInt::new));
    public static final RegistryObject<ControlType> X = TYPES.register("x", () -> new ControlType(type -> new AxisControl(type, Direction.Axis.X), ControlDataNone::new));
    public static final RegistryObject<ControlType> Y = TYPES.register("y", () -> new ControlType(type -> new AxisControl(type, Direction.Axis.Y), ControlDataNone::new));
    public static final RegistryObject<ControlType> Z = TYPES.register("z", () -> new ControlType(type -> new AxisControl(type, Direction.Axis.Z), ControlDataNone::new));
    public static final RegistryObject<ControlType> LANDING_TYPE = TYPES.register("land_type", () -> new ControlType(LandControl::new, (type, tardis) -> new ControlDataEnum<>(type, tardis, i -> LandingType.values()[i], LandingType.DOWN)));
    public static final RegistryObject<ControlType> FACING = TYPES.register("facing", () -> new ControlType(FacingControl::new, (type, level) -> new ControlDataEnum<Direction>(type, level, index -> Direction.values()[index], Direction.NORTH)));
    public static final RegistryObject<ControlType> RANDOMIZER = TYPES.register("randomizer", () -> new ControlType(RandomizerControl::new, ControlDataNone::new));
    public static final RegistryObject<ControlType> DIMENSIONS = TYPES.register("dimension", () -> new ControlType(DimensionControl::new, ControlDataInt::new));
    public static final RegistryObject<ControlType> TELEPATHICS = TYPES.register("telepathics",  () -> new ControlType(TelepathicControl::new, ControlDataNone::new));
    public static final RegistryObject<ControlType> FAST_RETURN = TYPES.register("fast_return", () -> new ControlType(FastReturnControl::new, ControlDataSpaceTimeCoord::new));
    public static final RegistryObject<ControlType> COMMUNICATOR = TYPES.register("communicator", () -> new ControlType(CommsControl::new, ControlDataNone::new));




}
