package net.tardis.mod.misc.enums;

public enum FlightState {
    LANDED,
    CRASHED,
    FLYING
}
