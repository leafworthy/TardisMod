package net.tardis.mod.datagen;

import net.minecraft.data.DataGenerator;
import net.minecraft.network.chat.ComponentContents;
import net.minecraft.network.chat.contents.TranslatableContents;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.animal.Cat;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraftforge.common.data.LanguageProvider;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.BlockRegistry;
import net.tardis.mod.block.RoundelBlock;
import net.tardis.mod.client.gui.minigame.ChameleonGame;
import net.tardis.mod.client.monitor.LocationalMonitorEntry;
import net.tardis.mod.control.ControlRegistry;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.creative_tabs.Tabs;
import net.tardis.mod.item.ItemRegistry;

public class TardisLangProvider extends LanguageProvider {


    public TardisLangProvider(DataGenerator gen) {
        super(gen, Tardis.MODID, "en_us");
    }

    @Override
    protected void addTranslations() {
        this.add(Tabs.MAIN, "TARDIS Main Tab");
        this.add(Tabs.ROUNDELS, "TARDIS Mod Roundel Tab");

        //Items
        this.add(ItemRegistry.WELDING_TORCH.get(), "Welding Torch");
        this.add(ItemRegistry.DIAGNOSTIC_TOOL.get(), "Diagnostic Tool");

        //Blocks
        this.add(BlockRegistry.DRAFTING_TABLE.get(), "Drafting Table");
        this.add(BlockRegistry.BROKEN_EXTERIOR.get(), "Broken Exterior");
        this.add(BlockRegistry.G8_CONSOLE.get(), "G-8 Emergency Navigation Console");

        //Guis
        this.add(ChameleonGame.TITLE.getContents(), "Chameleon Circuit");

        this.add(RoundelBlock.TRANSLATION_KEY, "TARDIS Roundel");

        this.add(LocationalMonitorEntry.LOCATION_KEY, "Location: %s, %s, %s");
        this.add(LocationalMonitorEntry.LOCATION_DIM_KEY, "Dimension: %s");

        //this.printControlRegistry();

    }

    //Enabled to post the resource locations of all controls in a way that works for the control plugin
    public void printControlRegistry(){
        for(ResourceLocation loc : ControlRegistry.REGISTRY.get().getKeys()){
            System.out.println("\"%s\" : \"%s\"".formatted(loc, ""));
        }
    }

    public void add(CreativeModeTab tab, String name){
        this.add(tab.getDisplayName().getContents(), name);
    }

    public void add(ComponentContents contents, String name){
        if(contents instanceof TranslatableContents){
            add(((TranslatableContents)contents).getKey(), name);
        }
        else add(contents.toString(), name);
    }
}
