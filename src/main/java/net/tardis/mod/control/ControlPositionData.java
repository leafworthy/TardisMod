package net.tardis.mod.control;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.phys.Vec3;
/**
/**
 * @author Lilith
 * A data structure class to handle a Control's data such as position and size
 *
 */
public class ControlPositionData {
	
	private Vec3 offset;
	private Vec3 size;
	private ControlType type;
	
	public ControlPositionData(ControlType type){
	    this.type = type;
	}
	
	public Vec3 getOffset(){
	    return this.offset;
	}

	public Vec3 getSize(){
	    return this.size;
	}

    public ControlType getControlType(){
        return this.type;
    }
    
    public ControlPositionData setControlType(ResourceLocation key) {
        this.type = ControlRegistry.REGISTRY.get().getValue(key);
        return this;
    }

    public boolean deserialize(JsonElement element){
        if(element.isJsonObject()){
            JsonObject obj = element.getAsJsonObject();
            if(obj.has("offset") && obj.has("size")){
                JsonArray posData = obj.getAsJsonArray("offset");
                JsonArray sizeData = obj.getAsJsonArray("size");

                this.offset = new Vec3(posData.get(0).getAsDouble(), posData.get(1).getAsDouble(), posData.get(2).getAsDouble());
                this.size = new Vec3(sizeData.get(0).getAsDouble(), sizeData.get(1).getAsDouble(), sizeData.get(2).getAsDouble());
                return true;
            }
        }
        return false;
    }

}
