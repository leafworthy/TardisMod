package net.tardis.mod.control;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;

public class ControlHandbrake extends Control{

    public ControlHandbrake(ControlType type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        ControlData<Boolean> data = level.getControlDataOrCreate(this.type);
        data.set(!data.get());
        return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
    }
}
