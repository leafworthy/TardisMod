package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.TickTask;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.common.Tags;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.dimension.DimensionHelper;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.world.TardisARSPiece;
import net.tardis.mod.world.TardisARSPieceRegistry;

public class BrokenExteriorBlock extends Block {

    public BrokenExteriorBlock() {
        super(Properties.of(Material.METAL));
    }

    @Override
    public InteractionResult use(BlockState p_60503_, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult p_60508_) {
        if(hand != player.getUsedItemHand())
            return InteractionResult.PASS;

        if(!level.isClientSide){
            level.getServer().execute(() -> {
                ServerLevel tardisLevel = DimensionHelper.createTardis(level.getServer());
                tardisLevel.getCapability(Capabilities.TARDIS).ifPresent(cap -> {
                    SpaceTimeCoord coord = new SpaceTimeCoord(level.dimension(), pos);
                    cap.setDestination(coord);
                    cap.setLocation(coord);
                    cap.setExterior(ExteriorRegistry.STEAM.get());
                    cap.getExterior().setPosition(level, pos);

                    ResourceLocation rs = TardisARSPieceRegistry.CORRIDORS.get(0).getResourceLocation();
                    tardisLevel.getLevel().getStructureManager().get(rs).ifPresent(structure -> {
                        BlockPos newPos = new BlockPos(0,0,0).south(16).west(16); // Must be offset to utilize all 3x3 chunks.
                        StructurePlaceSettings settings = new StructurePlaceSettings().setRotation(Rotation.CLOCKWISE_90).setMirror(Mirror.FRONT_BACK);
                        structure.placeInWorld(tardisLevel, newPos, newPos, settings, tardisLevel.getRandom(), 1);
                    });
                    tardisLevel.getChunk(0, 0); //Make world gen generate test structure
                });

            });
        }
        return super.use(p_60503_, level, pos, player, hand, p_60508_);
    }
}
