package net.tardis.mod.recipes.serializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.tardis.mod.helpers.JsonHelper;
import net.tardis.mod.misc.ToolType;
import net.tardis.mod.recipes.DraftingTableRecipe;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.Charset;
import java.util.*;

public class DraftingTableRecipeSerializer implements RecipeSerializer<DraftingTableRecipe> {

    @Override
    public DraftingTableRecipe fromJson(ResourceLocation id, JsonObject json) {

        System.out.println("Reading drafting table recipe " + id);

        List<String> pattern = new ArrayList<>();
        JsonArray patternJson = json.get("pattern").getAsJsonArray();
        for(JsonElement ele : patternJson){
            pattern.add(ele.getAsString());
        }

        HashMap<Character, Ingredient> keys = new HashMap<>();
        JsonObject keysJson = json.get("keys").getAsJsonObject();
        for(Map.Entry<String, JsonElement> ele : keysJson.entrySet()){

            if(ele.getKey().length() > 1)
                throw new JsonSyntaxException("Error in recipe %s! Name of element in keys must be a character!".formatted(id));

            char c = ele.getKey().charAt(0);

            keys.put(c, Ingredient.fromJson(ele.getValue()));
        }

        EnumMap<ToolType, Integer> tools = new EnumMap<ToolType, Integer>(ToolType.class);
        JsonArray toolJson = json.getAsJsonArray("tools");
        for(JsonElement ele : toolJson){
            if(!ele.isJsonObject())
                throw new JsonSyntaxException("Error in recipe %s: Elements in tools must be json objects!".formatted(id));
            JsonObject data = ele.getAsJsonObject();
            ToolType type = ToolType.getTypeFromName(data.get("tool").getAsString());
            if(type != null){
                tools.put(type, data.get("time").getAsInt());
            }
            else throw new JsonSyntaxException("Error in Recipe %s! no tool type \"%s\"".formatted(id, data.get("tool").getAsString()));
        }

        JsonObject resultJson = json.get("result").getAsJsonObject();
        ItemStack result = new ItemStack(JsonHelper.getItemFromString(resultJson.get("item").getAsString()));

        return new DraftingTableRecipe(id, pattern, keys, tools, result);
    }

    @Override
    public @Nullable DraftingTableRecipe fromNetwork(ResourceLocation id, FriendlyByteBuf buf) {
        List<String> pattern = new ArrayList<>();
        int patternSize = buf.readInt();
        for(int i = 0; i < patternSize; ++i){
            int stringSize = buf.readInt();
            pattern.add(buf.readCharSequence(stringSize, Charset.defaultCharset()).toString());
        }

        HashMap<Character, Ingredient> keys = new HashMap<>();
        int keySize = buf.readInt();
        for(int i = 0; i < keySize; ++i){
            keys.put(buf.readChar(), Ingredient.fromNetwork(buf));
        }

        EnumMap<ToolType, Integer> tools = new EnumMap<ToolType, Integer>(ToolType.class);
        int toolSize = buf.readInt();
        for(int i = 0; i < toolSize; ++i){
            tools.put(buf.readEnum(ToolType.class), buf.readInt());
        }

        ItemStack result = buf.readItem();

        return new DraftingTableRecipe(id, pattern, keys, tools, result);
    }

    @Override
    public void toNetwork(FriendlyByteBuf buf, DraftingTableRecipe recipe) {
        buf.writeInt(recipe.pattern.size());
        for(String s : recipe.pattern){
            buf.writeInt(s.length());
            buf.writeCharSequence(s, Charset.defaultCharset());
        }

        buf.writeInt(recipe.key.size());
        for(Map.Entry<Character, Ingredient> ing : recipe.key.entrySet()){
            buf.writeChar(ing.getKey());
            ing.getValue().toNetwork(buf);
        }

        buf.writeInt(recipe.tools.size());
        for(Map.Entry<ToolType, Integer> entry : recipe.tools.entrySet()){
            buf.writeEnum(entry.getKey());
            buf.writeInt(entry.getValue());
        }

        buf.writeItem(recipe.getResultItem());
    }
}
