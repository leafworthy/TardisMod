package net.tardis.mod.events;

import net.minecraft.client.Minecraft;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;

@Mod.EventBusSubscriber(modid = Tardis.MODID, value = Dist.CLIENT)
public class ClientEvents {

    @SubscribeEvent
    public static void onWorldTickClient(TickEvent.ClientTickEvent event){
        //Needed to tick tardis caps on the client
        if(event.phase == TickEvent.Phase.END){
            Level level = Minecraft.getInstance().level;
            if(level != null && !Minecraft.getInstance().isPaused()){
                level.getCapability(Capabilities.TARDIS).ifPresent(ITardisLevel::tick);
            }
        }
    }


}
