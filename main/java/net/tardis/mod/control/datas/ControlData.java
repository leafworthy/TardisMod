package net.tardis.mod.control.datas;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ControlDataMessage;

/** Generic wrapper to apply data from a control to the Tardis Capability*/

public abstract class ControlData<T> implements INBTSerializable<CompoundTag> {

    public AnimationState usedState = new AnimationState();

    private final ControlType type;
    private final ITardisLevel tardis;
    private T value;

    public ControlData(ControlType type, ITardisLevel tardis, T defaultValue){
        this.type = type;
        this.tardis = tardis;
        this.value = defaultValue;
    }

    public ControlData(ControlType type, T defaultValue){
        this(type, null, defaultValue);
    }

    public ControlType getType(){
        return this.type;
    }

    public void set(T value){
        this.value = value;
        if(this.tardis != null && !this.tardis.getLevel().isClientSide()){
            ResourceKey<Level> levelKey = this.tardis.getLevel().dimension();
            Network.sendPacketToDimension(levelKey, new ControlDataMessage(levelKey, this));
        }
    }

    public T get(){
        return this.value;
    }

    public AnimationState getUseAnimationState(){
        return this.usedState;
    }

    public void playAnimation(int time){
        this.usedState.start(time);
    }

    public <T> void copyTo(ControlData<T> data){
        data.set((T)this.get());
    }

    public abstract void encode(FriendlyByteBuf buf);
    public abstract void decode(FriendlyByteBuf buf);

}
